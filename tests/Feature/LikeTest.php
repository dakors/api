<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Passport\Passport;

class LikeTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_guest_cannot_like_blog()
    {
        // If user is not logged in, will redirect to login.
        $this->postJson('api/blog/like')->assertStatus(401);
    }

    /** @test */
    public function a_guest_cannot_unlike_blog()
    {
        // If user is not logged in, will redirect to login.
        $this->postJson('api/blog/unlike')->assertStatus(401);
    }

    /** @test */
    public function a_guest_cannot_get_list_of_users_who_likes_a_blog()
    {
        // If user is not logged in, will redirect to login.
        $this->postJson('api/likes')->assertStatus(401);
    }

    /** @test */
    public function a_user_can_like_own_blog()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $blog = factory('App\Models\UserBlog')->create(
            [
                  'user_id' => $user->id,
              ]
        );
        $this->postJson('api/blog/like', ['blog_id' => $blog->blog_id])
              ->assertOk()
              ->assertJson(['message' => __('blog.blog_liked')]);

        $this->assertDatabaseHas(
            'blog_liked',
            [
                'blog_id' => $blog->blog_id,
                'user_id' => $user->id
            ]
        );
    }

    /** @test */
    public function a_user_can_like_other_users_blog()
    {
        $user = factory('App\User')->create();
        $otherUser = factory('App\User')->create();
        Passport::actingAs($user);
        $blog = factory('App\Models\UserBlog')->create(
            [
                  'user_id' => $otherUser->id,
              ]
        );
        $this->postJson('api/blog/like', ['blog_id' => $blog->blog_id])
              ->assertOk()
              ->assertJson(['message' => __('blog.blog_liked')]);

        $this->assertDatabaseHas(
            'blog_liked',
            [
                'blog_id' => $blog->blog_id,
                'user_id' => $user->id
            ]
        );
    }

    /** @test */
    public function a_user_cannot_like_blog_without_valid_inputs()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $blog = factory('App\Models\UserBlog')->create(
            [
                  'user_id' => $user->id,
              ]
        );
        $this->postJson('api/blog/like')
              ->assertJsonValidationErrors('blog_id')
              ->assertJsonStructure(['message']);

        $this->assertDatabaseMissing(
            'blog_liked',
            [
                'blog_id' => $blog->blog_id,
                'user_id' => $user->id
            ]
        );
    }

    /** @test */
    public function a_user_cannot_like_blog_already_liked()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        factory('App\Models\BlogLiked')->create(
            [
                'blog_id' => 1,
                'user_id' => $user->id
            ]
        );

        $this->postJson('api/blog/like', ['blog_id' => 1])
              ->assertStatus(500)
              ->assertJson(['message' => __('blog.blog_already_like')]);
    }

    /** @test */
    public function a_user_can_unlike_a_blog_he_liked()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $like = factory('App\Models\BlogLiked')->create(
            [
                'blog_id' => 1,
                'user_id' => $user->id
            ]
        );

        $this->postJson('api/blog/unlike', ['blog_id' => $like->blog_id])
              ->assertOk()
              ->assertJson(['message' => __('blog.blog_unliked')]);

        $this->assertDatabaseMissing(
            'blog_liked',
            [
                'blog_id' => $like->blog_id,
                'user_id' => $user->id
            ]
        );
    }

    /** @test */
    public function a_user_cannot_unlike_a_blog_without_valid_inputs()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $like = factory('App\Models\BlogLiked')->create(
            [
                'blog_id' => 1,
                'user_id' => $user->id
            ]
        );
        $this->postJson('api/blog/unlike')
              ->assertJsonValidationErrors('blog_id')
              ->assertJsonStructure(['message']);

        $this->assertDatabaseHas(
            'blog_liked',
            [
                'blog_id' => $like->blog_id,
                'user_id' => $user->id
            ]
        );
    }

    /** @test */
    public function a_user_cannot_unlike_a_blog_he_did_not_liked()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $like = factory('App\Models\BlogLiked')->create(
            [
                'blog_id' => 1,
                'user_id' => 559
            ]
        );

        // Case 1, user did not like blog
        $this->postJson('api/blog/unlike', ['blog_id' => $like->blog_id])
              ->assertNotFound()
              ->assertJson(['message' => __('blog.like_not_found')]);

        // Case 2, blog id not exist.
        $this->postJson('api/blog/unlike', ['blog_id' => 559])
              ->assertNotFound()
              ->assertJson(['message' => __('blog.like_not_found')]);

        $this->assertDatabaseHas(
            'blog_liked',
            [
                'blog_id' => $like->blog_id,
                'user_id' => 559
            ]
        );
    }

    /** @test */
    public function a_user_can_get_list_of_users_who_likes_a_blog()
    {
        $user = factory('App\User')->create();
        $user2 = factory('App\User')->create();
        $user3 = factory('App\User')->create();
        Passport::actingAs($user);
        $blog = factory('App\Models\UserBlog')->create(['user_id' => $user->id]);
        factory('App\Models\BlogLiked')->create(
            [
                'blog_id' => $blog->blog_id,
                'user_id' => $user->id
            ]
        );
        factory('App\Models\BlogLiked')->create(
            [
                'blog_id' => $blog->blog_id,
                'user_id' => $user2->id
            ]
        );
        factory('App\Models\BlogLiked')->create(
            [
                'blog_id' => $blog->blog_id,
                'user_id' => $user3->id
            ]
        );
        factory('App\Models\BlogLiked')->create(
            [
                'blog_id' => 559,
                'user_id' => $user3->id
            ]
        );
        $this->postJson('api/likes', ['blog_id' => $blog->blog_id])
              ->assertStatus(200)
              ->assertJsonCount(3, 'data')
              ->assertJsonStructure(
                  [
                       'data' =>
                       [
                           [
                               'user_id',
                               'nick_name',
                               'gender',
                               'age',
                               'location',
                               'photo',
                               'background',
                               'introduction',
                               'game_list',
                               'Fans',
                               'isFb',
                               'isGoogle',
                               'Issubscriber',
                               'IsFriend',
                               'IsFan',
                               'IsFollowing',
                               'IsRequest',
                               'game_name'
                           ]
                       ],
                       'meta',
                       'links'
                   ]
              );
    }

    /** @test */
    public function a_user_cannot_get_list_of_users_who_likes_a_blog_without_valid_blog_id()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $blog = factory('App\Models\UserBlog')->create(['user_id' => $user->id]);
        factory('App\Models\BlogLiked')->create(
            [
                'blog_id' => $blog->blog_id,
                'user_id' => $user->id
            ]
        );
        $this->postJson('api/likes')
              ->assertJsonValidationErrors('blog_id')
              ->assertJsonStructure(['message']);
    }

    /** @test */
    public function a_user_cannot_Get_list_of_users_who_likes_a_blog_not_found()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);

        factory('App\Models\BlogLiked')->create(
            [
                'blog_id' => 559,
                'user_id' => $user->id
            ]
        );
        $this->postJson('api/likes', ['blog_id' => 559])
              ->assertStatus(404)
              ->assertJson(['message' => __('blog.blog_not_found')]);
    }
}
