<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Passport\Passport;

class UserControllerTest extends TestCase
{
    use RefreshDatabase;

    public function setUpRecommendedUsersTest($size)
    {
        $users = factory('App\User', $size)->create()->each(function ($newUser) use (&$user) {
            $games = array("game1", "game2", "game3");
            factory('App\Models\UserGameList')->create(
                [
                    'user_id' => $newUser->id,
                    'game_name' => $games[0]
                ]
            );
            factory('App\Models\UserGameList')->create(
                [
                    'user_id' => $newUser->id,
                    'game_name' => $games[1]
                ]
            );
            factory('App\Models\UserGameList')->create(
                [
                    'user_id' => $newUser->id,
                    'game_name' => $games[2]
                ]
            );

            factory('App\Models\UserImages')->create(
                [
                    'user_id' => $newUser->id
                ]
            );
        });

        return $users;
    }

    /**
     * @test
     */
    public function a_guest_cannot_view_another_profile()
    {
        $this->postJson('/api/user')->assertStatus(401);
    }

    /**
     * @test
     */
    public function a_guest_cannot_update_profile()
    {
        $this->postJson('/api/user/update')->assertStatus(401);
    }

    /**
     * @test
     */
    public function a_guest_cannot_get_recommended_users()
    {
        $this->postJson('/api/user/recommended')->assertStatus(401);
    }

    /**
     * @test
     */
    public function a_guest_cannot_bind_account()
    {
        $this->postJson('/api/auth/user/bind')->assertStatus(401);
    }

    /**
     * @test
     */
    public function a_user_can_view_another_user_profile()
    {
        $this->withoutExceptionHandling();
        Passport::actingAs(factory('App\User')->create());
        $otherUser = factory('App\User')->create();
        factory('App\Models\UserImages')->create(
            [
                'user_id' => $otherUser->id
            ]
        );
        $this->postJson('/api/user', ['user_id' => $otherUser->id])
                ->assertStatus(200)
                ->assertJsonStructure(
                    [
                          'data' =>
                          [
                              [
                                  'user_id',
                                  'nick_name',
                                  'gender',
                                  'age',
                                  'location',
                                  'photo',
                                  'background',
                                  'introduction',
                                  'game_list',
                                  'Fans',
                                  'isFb',
                                  'isGoogle',
                                  'Issubscriber',
                                  'IsFriend',
                                  'IsFan',
                                  'IsFollowing',
                                  'IsRequest'
                              ]
                          ]
                      ]
                  );
    }

    /**
    * @test
    */
    public function a_user_cannot_view_another_user_profile_without_valid_inputs()
    {
        Passport::actingAs(factory('App\User')->create());
        $this->postJson('/api/user')
                ->assertJsonValidationErrors('user_id')
                ->assertJsonStructure(['message']);
    }

    /**
    * @test
    */
    public function a_user_cannot_view_user_who_is_not_found()
    {
        Passport::actingAs(factory('App\User')->create());
        $this->postJson('/api/user', ['user_id' => 559])
                ->assertNotFound()
                ->assertJson(['message' => __('User not found')]);
    }

    /**
    * @test
    */
    public function a_user_cannot_update_profile_without_valid_inputs()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $this->postJson('/api/user/update', [])
              ->assertJsonValidationErrors(['nick_name', 'gender', 'age', 'location', 'introduction'])
              ->assertJsonStructure(['message']);
    }

    /**
    * @test
    */
    public function a_user_can_update_username()
    {
        $this->withoutExceptionHandling();
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $this->postJson('/api/user/update', [
              'nick_name' => 'new nick name',
              'gender' => '0',
              'age' => 78,
              'location' => 11,
              'introduction' => 'this is updated introduction'])
              ->assertOk()
              ->assertJson(['message' => __('user.updated')]);

        $this->assertDatabaseHas(
            'users',
            [
                  'nick_name' => 'new nick name',
                  'gender' => '0',
                  'age' => 78,
                  'location' => 11,
                  'introduction' => 'this is updated introduction'
              ]
          );
    }

    /**
    * @test
    */
    public function a_user_cannot_update_empty_username()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $this->postJson('/api/user/update', [
              'nick_name' => '',
              'gender' => '0',
              'age' => 78,
              'location' => 11,
              'introduction' => 'this is updated introduction'])
              ->assertJsonValidationErrors(['nick_name'])
              ->assertJsonStructure(['message']);

        $this->assertDatabaseMissing(
            'users',
            [
                  'nick_name' => '',
                  'gender' => '0',
                  'age' => 78,
                  'location' => 11,
                  'introduction' => 'this is updated introduction'
              ]
          );
    }

    /**
    * @test
    */
    public function a_user_cannot_set_age_older_than_99()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $this->postJson('/api/user/update', [
              'nick_name' => 'new nick name',
              'gender' => 1,
              'age' => 100,
              'location' => 11,
              'introduction' => 'this is updated introduction'])
              ->assertJsonValidationErrors(['age'])
              ->assertJsonStructure(['message']);

        $this->assertDatabaseMissing(
            'users',
            [
                  'nick_name' => 'new nick name',
                  'gender' => 1,
                  'age' => 100,
                  'location' => 11,
                  'introduction' => 'this is updated introduction'
              ]
          );
    }

    /**
    * @test
    */
    public function a_user_cannot_set_age_younger_than_0()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $this->postJson('/api/user/update', [
              'nick_name' => 'new nick name',
              'gender' => 1,
              'age' => -1,
              'location' => 11,
              'introduction' => 'this is updated introduction'])
              ->assertJsonValidationErrors(['age'])
              ->assertJsonStructure(['message']);

        $this->assertDatabaseMissing(
            'users',
            [
                  'nick_name' => 'new nick name',
                  'gender' => 1,
                  'age' => -1,
                  'location' => 11,
                  'introduction' => 'this is updated introduction'
              ]
          );
    }

    /**
    * @test
    */
    public function a_user_can_set_age_between_0_and_99()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $this->postJson('/api/user/update', [
              'nick_name' => 'new nick name',
              'gender' => 1,
              'age' => 0,
              'location' => 11,
              'introduction' => 'this is updated introduction'])
              ->assertOk()
              ->assertJson(['message' => __('user.updated')]);

        $this->assertDatabaseHas(
            'users',
            [
                  'nick_name' => 'new nick name',
                  'gender' => 1,
                  'age' => 0,
                  'location' => 11,
                  'introduction' => 'this is updated introduction'
              ]
          );

        $this->postJson('/api/user/update', [
              'nick_name' => 'new nick name',
              'gender' => 1,
              'age' => 99,
              'location' => 11,
              'introduction' => 'this is updated introduction'])
              ->assertOk()
              ->assertJson(['message' => __('user.updated')]);

        $this->assertDatabaseHas(
            'users',
            [
                  'nick_name' => 'new nick name',
                  'gender' => 1,
                  'age' => 99,
                  'location' => 11,
                  'introduction' => 'this is updated introduction'
              ]
          );
    }

    /**
    * @test
    */
    public function a_user_can_set_his_gender_to_unspecified()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $this->postJson('/api/user/update', [
              'nick_name' => 'new nick name',
              'gender' => '0',
              'age' => 78,
              'location' => 11,
              'introduction' => 'this is updated introduction'])
              ->assertOk()
              ->assertJson(['message' => __('user.updated')]);

        $this->assertDatabaseHas(
            'users',
            [
                  'nick_name' => 'new nick name',
                  'gender' => '0',
                  'age' => 78,
                  'location' => 11,
                  'introduction' => 'this is updated introduction'
              ]
          );
    }

    /**
    * @test
    */
    public function a_user_can_set_his_gender_to_male()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $this->postJson('/api/user/update', [
              'nick_name' => 'new nick name',
              'gender' => '1',
              'age' => 78,
              'location' => 11,
              'introduction' => 'this is updated introduction'])
              ->assertOk()
              ->assertJson(['message' => __('user.updated')]);

        $this->assertDatabaseHas(
            'users',
            [
                  'nick_name' => 'new nick name',
                  'gender' => '1',
                  'age' => 78,
                  'location' => 11,
                  'introduction' => 'this is updated introduction'
              ]
          );
    }

    /**
    * @test
    */
    public function a_user_can_set_his_gender_to_female()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $this->postJson('/api/user/update', [
              'nick_name' => 'new nick name',
              'gender' => '2',
              'age' => 78,
              'location' => 11,
              'introduction' => 'this is updated introduction'])
              ->assertOk()
              ->assertJson(['message' => __('user.updated')]);

        $this->assertDatabaseHas(
            'users',
            [
                  'nick_name' => 'new nick name',
                  'gender' => '2',
                  'age' => 78,
                  'location' => 11,
                  'introduction' => 'this is updated introduction'
              ]
          );
    }

    /**
    * @test
    */
    public function a_user_cannot_update_invalid_gender()
    {
        // $this->withoutExceptionHandling();
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $this->postJson('/api/user/update', [
              'nick_name' => 'new nick name',
              'gender' => '3',
              'age' => 15,
              'location' => 11,
              'introduction' => 'this is updated introduction'])
              ->assertJsonValidationErrors(['gender'])
              ->assertJsonStructure(['message']);

        $this->assertDatabaseMissing(
            'users',
            [
                  'nick_name' => 'new nick name',
                  'gender' => '3',
                  'age' => 15,
                  'location' => 11,
                  'introduction' => 'this is updated introduction'
              ]
          );

        $this->postJson('/api/user/update', [
              'nick_name' => 'new nick name',
              'gender' => 'a',
              'age' => 15,
              'location' => 11,
              'introduction' => 'this is updated introduction'])
              ->assertJsonValidationErrors(['gender'])
              ->assertJsonStructure(['message']);

        $this->assertDatabaseMissing(
            'users',
            [
                  'nick_name' => 'new nick name',
                  'gender' => 'a',
                  'age' => 15,
                  'location' => 11,
                  'introduction' => 'this is updated introduction'
              ]
          );
    }

    /**
    * @test
    */
    public function a_user_can_update_valid_location()
    {
        $this->withoutExceptionHandling();
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $this->postJson('/api/user/update', [
              'nick_name' => 'new nick name',
              'gender' => '2',
              'age' => 78,
              'location' => 0,
              'introduction' => 'this is updated introduction'])
              ->assertOk()
              ->assertJson(['message' => __('user.updated')]);

        $this->assertDatabaseHas(
            'users',
            [
                  'nick_name' => 'new nick name',
                  'gender' => '2',
                  'age' => 78,
                  'location' => 0,
                  'introduction' => 'this is updated introduction'
              ]
          );

        $this->postJson('/api/user/update', [
              'nick_name' => 'new nick name',
              'gender' => '2',
              'age' => 78,
              'location' => 23,
              'introduction' => 'this is updated introduction'])
              ->assertOk()
              ->assertJson(['message' => __('user.updated')]);

        $this->assertDatabaseHas(
            'users',
            [
                  'nick_name' => 'new nick name',
                  'gender' => '2',
                  'age' => 78,
                  'location' => 23,
                  'introduction' => 'this is updated introduction'
              ]
          );
    }

    /**
    * @test
    */
    public function a_user_cannot_update_invalid_location()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $this->postJson('/api/user/update', [
              'nick_name' => 'new nick name',
              'gender' => 2,
              'age' => 15,
              'location' => 24,
              'introduction' => 'this is updated introduction'])
              ->assertJsonValidationErrors(['location'])
              ->assertJsonStructure(['message']);

        $this->assertDatabaseMissing(
            'users',
            [
                  'nick_name' => 'new nick name',
                  'gender' => 2,
                  'age' => 15,
                  'location' => 24,
                  'introduction' => 'this is updated introduction'
              ]
          );

        $this->postJson('/api/user/update', [
              'nick_name' => 'new nick name',
              'gender' => 2,
              'age' => 15,
              'location' => -1,
              'introduction' => 'this is updated introduction'])
              ->assertJsonValidationErrors(['location'])
              ->assertJsonStructure(['message']);

        $this->assertDatabaseMissing(
            'users',
            [
                  'nick_name' => 'new nick name',
                  'gender' => 2,
                  'age' => 15,
                  'location' => -1,
                  'introduction' => 'this is updated introduction'
              ]
          );
    }

    /**
    * @test
    */
    public function a_user_introduction_can_be_80_chars_and_below()
    {
        // Intro string of 80 characters.
        $intro = 'adipiscing diam doec adipiscing tristique risus nec feugiat o tincidunt praesent';
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $this->postJson('/api/user/update', [
              'nick_name' => 'new nick name',
              'gender' => 2,
              'age' => 15,
              'location' => 15,
              'introduction' => $intro])
              ->assertOk()
              ->assertJson(['message' => __('user.updated')]);

        $this->assertDatabaseHas(
            'users',
            [
                  'nick_name' => 'new nick name',
                  'gender' => 2,
                  'age' => 15,
                  'location' => 15,
                  'introduction' => $intro
              ]
          );
    }

    /**
    * @test
    */
    public function a_user_introduction_cannot_be_longer_than_80_chars()
    {
        // Intro string of 81 characters.
        $intro = 'adipiscing diam donec adipiscing tristique risus nec feugiat o tincidunt praesent';
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $this->postJson('/api/user/update', [
              'nick_name' => 'new nick name',
              'gender' => 2,
              'age' => 15,
              'location' => 15,
              'introduction' => $intro])
              ->assertJsonValidationErrors(['introduction'])
              ->assertJsonStructure(['message']);

        $this->assertDatabaseMissing(
            'users',
            [
                  'nick_name' => 'new nick name',
                  'gender' => 2,
                  'age' => 15,
                  'location' => 15,
                  'introduction' => $intro
              ]
          );
    }

    /**
    * @test
    */
    public function a_user_can_get_recommended_users()
    {
        $users = $this->setUpRecommendedUsersTest(16);
        for ($i = 1; $i < 6; $i++) {
            factory('App\Models\RecommendedUser')->create(['user_id' => $users[count($users) - $i]]);
        }

        Passport::actingAs($users[0]);
        $json = $this->postJson('/api/user/recommended')
              ->assertOk()
              ->assertJsonStructure(
                  [
                        'data' =>
                        [
                            [
                                'user_id',
                                'nick_name',
                                'gender',
                                'age',
                                'location',
                                'photo',
                                'background',
                                'introduction',
                                'game_list',
                                'Fans',
                                'isFb',
                                'isGoogle',
                                'Issubscriber',
                                'IsFriend',
                                'IsFan',
                                'IsFollowing',
                                'IsRequest',
                            ]
                        ]
                    ]
                )
              ->assertJsonMissing(['user_id' => $users[0]->id])
              ->assertJsonCount(15, 'data');
    }

    /**
    * @test
    */
    public function a_user_can_only_get_15_recommended_users()
    {
        $users = $this->setUpRecommendedUsersTest(100);
        for ($i = 1; $i < 10; $i++) {
            factory('App\Models\RecommendedUser')->create(['user_id' => $users[count($users) - $i]]);
        }

        Passport::actingAs($users[0]);
        $json = $this->postJson('/api/user/recommended')
              ->assertOk()
              ->assertJsonStructure(
                  [
                        'data' =>
                        [
                            [
                                'user_id',
                                'nick_name',
                                'gender',
                                'age',
                                'location',
                                'photo',
                                'background',
                                'introduction',
                                'game_list',
                                'Fans',
                                'isFb',
                                'isGoogle',
                                'Issubscriber',
                                'IsFriend',
                                'IsFan',
                                'IsFollowing',
                                'IsRequest',
                            ]
                        ]
                    ]
                )
              ->assertJsonMissing(['user_id' => $users[0]->id])
              ->assertJsonCount(15, 'data');
    }

    /**
    * @test
    */
    public function a_user_cannot_see_blocked_users_recommended_users()
    {
        $users = $this->setUpRecommendedUsersTest(16);
        Passport::actingAs($users[0]);
        for ($i = 1; $i < 6; $i++) {
            factory('App\Models\RecommendedUser')->create(['user_id' => $users[count($users) - $i]]);
        }
        // User 0 blocks user 10 and 15
        factory('App\Models\UserBlockList')->create(
            [
                'user_id' => $users[0],
                'block_id' => $users[10]
            ]
        );
        factory('App\Models\UserBlockList')->create(
            [
                'user_id' => $users[0],
                'block_id' => $users[15]
            ]
        );
        $json = $this->postJson('/api/user/recommended')
              ->assertOk()
              ->assertJsonStructure(
                  [
                        'data' =>
                        [
                            [
                                'user_id',
                                'nick_name',
                                'gender',
                                'age',
                                'location',
                                'photo',
                                'background',
                                'introduction',
                                'game_list',
                                'Fans',
                                'isFb',
                                'isGoogle',
                                'Issubscriber',
                                'IsFriend',
                                'IsFan',
                                'IsFollowing',
                                'IsRequest',
                            ]
                        ]
                    ]
                )
              ->assertJsonMissing(['user_id' => $users[10]->id])
              ->assertJsonMissing(['user_id' => $users[15]->id])
              ->assertJsonMissing(['user_id' => $users[0]->id])
              ->assertJsonCount(13, 'data');
    }

    /**
    * @test
    */
    public function a_user_can_get_empty_recommended_users()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        factory('App\Models\UserGameList')->create(
            [
                'user_id' => $user->id,
                'game_name' => 'game 1'
            ]
        );
        $json = $this->postJson('/api/user/recommended')
              ->assertOk()
              ->assertJson(
                  [
                      'data' =>[]
                  ]
              )
              ->assertJsonCount(0, 'data');
    }

    /**
    * @test
    */
    public function a_user_with_no_games_added_cannot_get_recommended_users()
    {
        $users = $this->setUpRecommendedUsersTest(16);
        Passport::actingAs(factory('App\User')->create());
        for ($i = 1; $i < 6; $i++) {
            factory('App\Models\RecommendedUser')->create(['user_id' => $users[count($users) - $i]]);
        }
        $json = $this->postJson('/api/user/recommended')
              ->assertStatus(500)
              ->assertJson(
                  [
                      'message' => __('user.recommended_error')
                  ]
              );
    }

    /**
    * @test
    */
    public function a_user_cannot_bind_without_any_inputs()
    {
        Passport::actingAs(factory('App\User')->create());
        $this->postJson('/api/auth/user/bind', [])
              ->assertJsonValidationErrors(['access_token', 'provider'])
              ->assertJsonStructure(['message']);
    }

    /**
    * @test
    */
    public function a_user_cannot_bind_with_invalid_provider()
    {
        Passport::actingAs(factory('App\User')->create());
        $params = ['access_token' => 'fakeaccesstoken', 'provider' => 'twitter'];
        $this->postJson('/api/auth/user/bind', $params)
              ->assertJsonValidationErrors(['provider'])
              ->assertJsonStructure(['message']);
    }

    /**
    * @test
    */
    public function a_user_cannot_bind_when_already_binded_to_FB()
    {
        Passport::actingAs(factory('App\User')->create(['fb_id' => '1123148979845313212468']));
        $params = ['access_token' => 'fakeaccesstokenstring', 'provider' => 'google'];
        $this->postJson('/api/auth/user/bind', $params)
              ->assertStatus(500)
              ->assertJson(['message' => __('user.social_auth_already_exist')]);
        $params = ['access_token' => 'fakeaccesstokenstring', 'provider' => 'facebook'];
        $this->postJson('/api/auth/user/bind', $params)
              ->assertStatus(500)
              ->assertJson(['message' => __('user.social_auth_already_exist')]);
    }

    /**
    * @test
    */
    public function a_user_cannot_bind_when_already_binded_to_Google()
    {
        Passport::actingAs(factory('App\User')->create(['google_id' => '1123148979845313212468']));
        $params = ['access_token' => 'fakeaccesstokenstring', 'provider' => 'google'];
        $this->postJson('/api/auth/user/bind', $params)
              ->assertStatus(500)
              ->assertJson(['message' => __('user.social_auth_already_exist')]);
        $params = ['access_token' => 'fakeaccesstokenstring', 'provider' => 'facebook'];
        $this->postJson('/api/auth/user/bind', $params)
              ->assertStatus(500)
              ->assertJson(['message' => __('user.social_auth_already_exist')]);
    }
}
