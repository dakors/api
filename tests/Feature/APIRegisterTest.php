<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class APIRegisterTest extends TestCase
{
    use RefreshDatabase;
    /** @test
    * This test fails because during issue token method, the client is null.
    * Possibly a limitation of TestCase.
    */
    public function a_guest_can_register_new_account()
    {
        factory('Laravel\Passport\Client')
          ->create(
                      [
                          'name' => 'Gamerbox Personal Access Client',
                          'personal_access_client' => 1,
                          'password_client' => 0
                      ]
                  );
        factory('Laravel\Passport\Client')
          ->create(
                      [
                          'name' => 'Gamerbox Password Grant Client',
                          'personal_access_client' => 0,
                          'password_client' => 1
                      ]
                  );
        $this->postJson('/api/auth/register',
                    ['name' => 'newtester2',
                     'email' => 'newtester2@gmail.com',
                     'password' => '123456',
                     'password_confirmation' => '123456'])
              ->assertOk()
             ->assertJsonStructure(
                  [
                      'token_type',
                      'expires_in',
                      'access_token',
                      'refresh_token'
                  ]
               );
    }

    /** @test */
    public function a_user_cannot_register_without_valid_inputs()
    {
        $this->postJson('/api/auth/register')
              ->assertJsonValidationErrors(['name', 'email', 'password'])
              ->assertJsonStructure(['message']);
    }

    /** @test */
    public function a_user_cannot_register_with_password_less_than_6_characters()
    {
        $this->postJson('/api/auth/register',
                    ['name' => 'test',
                     'email' => 'test@trendian.com',
                     'password' => '12345',
                     'password_confirmation' => '12345'])
              ->assertJsonValidationErrors('password')
              ->assertJsonMissingValidationErrors(['name', 'email'])
              ->assertJsonStructure(['message']);
    }

    /** @test */
    public function a_user_cannot_register_without_password_confirmation()
    {
        $this->postJson('/api/auth/register',
                    ['name' => 'test',
                     'email' => 'test@gmail.com',
                     'password' => '123456'])
              ->assertJsonValidationErrors('password')
              ->assertJsonMissingValidationErrors(['name', 'email'])
              ->assertJsonStructure(['message']);
    }

    /** @test */
    public function a_user_cannot_register_without_valid_email_format()
    {
        $this->postJson('/api/auth/register',
                    ['name' => 'test',
                     'email' => 'testattrendian.com',
                     'password' => '123456',
                     'password_confirmation' => '123456'])
              ->assertJsonValidationErrors('email')
              ->assertJsonMissingValidationErrors(['name', 'password'])
              ->assertJsonStructure(['message']);

        $this->json('POST', '/api/auth/register',
                    ['name' => 'test',
                     'email' => 'test@trendian',
                     'password' => '123456',
                     'password_confirmation' => '123456'])
              ->assertJsonValidationErrors('email')
              ->assertJsonMissingValidationErrors(['name', 'password'])
              ->assertJsonStructure(['message']);
    }

    /** @test */
    public function a_user_cannot_register_with_existing_email()
    {
        factory('App\User')->create(['email' => 'trendian@gmail.com']);
        $this->postJson('/api/auth/register',
                    ['name' => 'test',
                     'email' => 'trendian@gmail.com',
                     'password' => '123456',
                     'password_confirmation' => '123456'])
              ->assertJsonValidationErrors('email')
              ->assertJsonMissingValidationErrors(['name', 'password'])
              ->assertJsonStructure(['message']);
    }

    /** @test */
    public function a_user_cannot_register_with_empty_name()
    {
        $this->postJson('/api/auth/register',
                    ['name' => '',
                     'email' => 'test@trendian.com',
                     'password' => '123456',
                     'password_confirmation' => '123456'])
              ->assertJsonValidationErrors('name')
              ->assertJsonMissingValidationErrors(['password', 'email'])
              ->assertJsonStructure(['message']);
    }
}
