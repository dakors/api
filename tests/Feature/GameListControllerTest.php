<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Passport\Passport;

class GameListTest extends TestCase
{
    use RefreshDatabase;

    public function setUp()
    {
        Parent::setUp();
        factory('App\Models\GameNameList')->create(
            [
                'enUS' => 'World of Tanks',
                'zhTW' => '戰車世界',
                'acronyms' => 'WOT'
            ]
        );

        factory('App\Models\GameNameList')->create(
            [
                'enUS' => 'World of Warships',
                'zhTW' => '戰艦世界',
                'acronyms' => 'WOW'
            ]
        );

        factory('App\Models\GameNameList')->create(
            [
                'enUS' => 'World of warcraft',
                'zhTW' => '魔獸世界',
                'acronyms' => 'WOW'
            ]
        );

        factory('App\Models\GameNameList')->create(
            [
                'enUS' => 'League of legends',
                'zhTW' => '英雄聯盟',
                'acronyms' => 'LOL'
            ]
        );
    }

    /** @test **/
    public function a_guest_cannot_get_game_list()
    {
        $this->postJson('api/gamelist')->assertStatus(401);
    }

    /** @test **/
    public function a_user_can_get_game_list_empty_keyword()
    {
        Passport::actingAs(factory('App\User')->create());
        $this->postJson('api/gamelist', ['keyword' => ''])
              ->assertOk()
              ->assertJsonCount(0, 'data')
              ->assertJson(['data' => []]);
    }

    /** @test **/
    public function a_user_can_search_not_exist_keyword_and_gets_no_result()
    {
        Passport::actingAs(factory('App\User')->create());
        $this->withHeaders(['X-Localization' => 'zh-TW'])
              ->postJson('api/gamelist', ['keyword' => 'dota'])
              ->assertOk()
              ->assertJsonCount(0, 'data')
              ->assertJson( // json fragment does not take into account order.
                    [
                        'data' => []
                    ]
                );
    }

    /** @test **/
    public function a_user_can_search_en_complete_keyword_return_zhtw()
    {
        Passport::actingAs(factory('App\User')->create());
        $this->withHeaders(['X-Localization' => 'zh-TW'])
              ->postJson('api/gamelist', ['keyword' => 'World of Tanks'])
              ->assertOk()
              ->assertJsonCount(1, 'data')
              ->assertJson(
                  [
                        'data' => [
                            [
                                'GameName' => '戰車世界'
                            ]
                        ]
                    ]
                );
    }

    /** @test **/
    public function a_user_can_search_en_complete_keyword_return_en()
    {
        Passport::actingAs(factory('App\User')->create());
        $this->withHeaders(['X-Localization' => 'en'])
              ->postJson('api/gamelist', ['keyword' => 'World of Tanks'])
              ->assertOk()
              ->assertJsonCount(1, 'data')
              ->assertJson(
                  [
                      'data' => [
                          [
                              'GameName' => 'World of Tanks'
                          ]
                      ]
                  ]
                );
    }

    /** @test **/
    public function a_user_can_search_zhtw_complete_keyword_return_zhtw()
    {
        Passport::actingAs(factory('App\User')->create());
        $this->postJson('api/gamelist', ['keyword' => '魔獸世界'])
              ->assertOk()
              ->assertJsonCount(1, 'data')
              ->assertJson(
                  [
                      'data' => [
                          [
                              'GameName' => '魔獸世界'
                          ]
                      ]
                  ]
                );
    }

    /** @test **/
    public function a_user_can_search_zhtw_complete_keyword_return_en()
    {
        Passport::actingAs(factory('App\User')->create());
        $this->withHeaders(['X-Localization' => 'en'])
              ->postJson('api/gamelist', ['keyword' => '魔獸世界'])
              ->assertOk()
              ->assertJsonCount(1, 'data')
              ->assertJson(
                  [
                      'data' => [
                          [
                              'GameName' => 'World of warcraft'
                          ]
                      ]
                  ]
                );
    }

    /** @test **/
    public function a_user_can_search_en_partial_keyword_ignore_caps()
    {
        Passport::actingAs(factory('App\User')->create());
        $this->withHeaders(['X-Localization' => 'en'])
              ->postJson('api/gamelist', ['keyword' => 'worl'])
              ->assertOk()
              ->assertJsonCount(3, 'data')
              ->assertJsonFragment( // json fragment does not take into account order.
                    [
                        'data' => [
                            [
                                'GameName' => 'World of Warships'
                            ],
                            [
                                'GameName' => 'World of Tanks'
                            ],
                            [
                                'GameName' => 'World of warcraft'
                            ]
                        ]
                    ]
                );

        $this->withHeaders(['X-Localization' => 'en'])
              ->postJson('api/gamelist', ['keyword' => 'war'])
              ->assertOk()
              ->assertJsonCount(2, 'data')
              ->assertJsonFragment( // json fragment does not take into account order.
                    [
                        'data' => [
                            [
                                'GameName' => 'World of Warships'
                            ],
                            [
                                'GameName' => 'World of warcraft'
                            ]
                        ]
                    ]
                );
    }

    /** @test **/
    public function a_user_can_search_zhtw_partial_keyword()
    {
        Passport::actingAs(factory('App\User')->create());
        $this->withHeaders(['X-Localization' => 'zh-TW'])
              ->postJson('api/gamelist', ['keyword' => '世界'])
              ->assertOk()
              ->assertJsonCount(3, 'data')
              ->assertJsonFragment( // json fragment does not take into account order.
                    [
                        'data' => [
                            [
                                'GameName' => '魔獸世界'
                            ],
                            [
                                'GameName' => '戰車世界'
                            ],
                            [
                                'GameName' => '戰艦世界'
                            ]
                        ]
                    ]
                );

        $this->withHeaders(['X-Localization' => 'zh-TW'])
              ->postJson('api/gamelist', ['keyword' => '戰'])
              ->assertOk()
              ->assertJsonCount(2, 'data')
              ->assertJsonFragment( // json fragment does not take into account order.
                    [
                        'data' => [
                            [
                                'GameName' => '戰車世界'
                            ],
                            [
                                'GameName' => '戰艦世界'
                            ]
                        ]
                    ]
                );
    }
}
