<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Passport\Passport;

class AuthControllerTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_guest_cannot_log_out()
    {
        $this->postJson('/api/auth/logout')
              ->assertStatus(401);
    }

    /** @test */
    public function a_guest_cannot_get_user_data()
    {
        $this->postJson('/api/auth/user')
              ->assertStatus(401);
    }

    /** @test */
    public function a_user_can_log_out()
    {
        Passport::actingAs(factory('App\User')->create());
        $this->postJson('/api/auth/logout')
              ->assertOk()
              ->assertJson(['message' => 'Successfully logged out']);
    }

    /** @test */
    public function a_user_can_get_user_data()
    {
        Passport::actingAs(factory('App\User')->create());
        $this->postJson('/api/auth/user')
              ->assertOk()
              ->assertJsonStructure(
                    [
                        'data' =>
                        [
                            'user_id',
                            'nick_name',
                            'gender',
                            'age',
                            'location',
                            'photo',
                            'background',
                            'introduction',
                            'game_list',
                            'Fans',
                            'isFb',
                            'isGoogle',
                            'Issubscriber'
                        ]
                    ]
                );
    }

}
