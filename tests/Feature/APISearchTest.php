<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Passport\Passport;

class APISearchTest extends TestCase
{
    use RefreshDatabase;
    /**
     * @test
     */
    public function a_guest_cannot_perform_search()
    {
        // If user is log in, will redirect to login.
        $this->json('POST', '/api/search', [])->assertStatus(401);
    }

    /**
     * @test
     */
    public function a_user_can_perform_search_and_see_result()
    {
        // This will create a new user and log him in.
        Passport::actingAs(factory('App\User')->create());
        $user = factory('App\User')->create();
        factory('App\Models\UserImages')->create(['user_id' => $user->id]);

        $attributes = [
                          // 'username' => 'gamerbox0',
                          // 'gender' => '',
                          // 'minAge' => '',
                          // 'maxAge' => '',
                          // 'location' => ''
                      ];

        $this->json('POST', '/api/search', $attributes)
               ->assertOk()
               ->assertJsonStructure(
                   [
                        'data' =>
                        [
                            [
                                'user_id',
                                'nick_name',
                                'gender',
                                'age',
                                'location',
                                'photo',
                                'background',
                                'introduction',
                                'game_list',
                                'Fans',
                                'isFb',
                                'isGoogle',
                                'Issubscriber',
                                'IsFriend',
                                'IsFan',
                                'IsFollowing',
                                'IsRequest',
                                'game_name'
                            ]
                        ],
                        'links',
                        'meta'
                    ]
               );
    }

    /**
     * @test
     */
    public function a_user_can_perform_search_and_see_no_result()
    {
        // This will create a new user and log him in.
        Passport::actingAs(factory('App\User')->create());
        $user = factory('App\User')->create();
        factory('App\Models\UserImages')->create(['user_id' => $user->id]);

        $attributes = [
                          'username' => 'ssqomgomomgomgfdsiifus'
                          // 'gender' => '',
                          // 'minAge' => '99',
                          // 'maxAge' => '',
                          // 'location' => ''
                      ];

        $this->json('POST', '/api/search', $attributes)
               ->assertOk()
               ->assertJsonStructure(
                   [
                        'data' =>[],
                        'links',
                        'meta'
                    ]
               )
               ->assertJsonCount(0, 'data');
    }

    /**
     * @test
     */
    public function a_user_can_search_with_numeric_min_age()
    {
        Passport::actingAs(factory('App\User')->create());
        $params = ['minAge' => 99];
        $this->json('POST', '/api/search', $params)
                    ->assertOk();
        $params = ['minAge' => '99'];
        $this->json('POST', '/api/search', $params)
                    ->assertOk();
    }

    /**
     * @test
     */
    public function a_user_cannot_search_with_non_numeric_min_age()
    {
        Passport::actingAs(factory('App\User')->create());
        $params = ['minAge' => 'tn'];
        $this->json('POST', '/api/search', $params)
              ->assertJsonValidationErrors(['minAge'])
              ->assertJsonStructure(['message']);
    }

    /**
     * @test
     */
    public function a_user_cannot_search_users_younger_than_zero()
    {
        Passport::actingAs(factory('App\User')->create());
        $params = ['minAge' => -1];
        $this->json('POST', '/api/search', $params)
             ->assertJsonValidationErrors(['minAge'])
             ->assertJsonStructure(['message']);
        $params = ['minAge' => '-1'];
        $this->json('POST', '/api/search', $params)
              ->assertJsonValidationErrors(['minAge'])
              ->assertJsonStructure(['message']);
    }

    /**
     * @test
     */
    public function a_user_can_search_users_who_are_at_least_zero_years_old()
    {
        Passport::actingAs(factory('App\User')->create());
        $params = ['minAge' => 0];
        $this->json('POST', '/api/search', $params)
             ->assertOk();
        $params = ['minAge' => '0'];
        $this->json('POST', '/api/search', $params)
              ->assertOk();
    }

    /**
     * @test
     */
    public function a_user_can_search_with_numeric_max_age()
    {
        Passport::actingAs(factory('App\User')->create());
        $params = ['maxAge' => 75];
        $this->json('POST', '/api/search', $params)
                          ->assertOk();
        $params = ['maxAge' => '75'];
        $this->json('POST', '/api/search', $params)
                          ->assertOk();
    }

    /**
     * @test
     */
    public function a_user_cannot_search_with_non_numberic_max_age()
    {
        Passport::actingAs(factory('App\User')->create());
        $params = ['maxAge' => 'ddd'];
        $this->json('POST', '/api/search', $params)
              ->assertJsonValidationErrors(['maxAge'])
              ->assertJsonStructure(['message']);
    }

    /**
     * @test
     */
    public function a_user_cannot_search_users_older_than_ninety_nine()
    {
        Passport::actingAs(factory('App\User')->create());
        $params = ['maxAge' => 100];
        $this->json('POST', '/api/search', $params)
              ->assertJsonValidationErrors(['maxAge'])
              ->assertJsonStructure(['message']);

        $params = ['maxAge' => '100'];
        $this->json('POST', '/api/search', $params)
              ->assertJsonValidationErrors(['maxAge'])
              ->assertJsonStructure(['message']);
    }

    /**
     * @test
     */
    public function a_user_can_search_users_who_are_at_most_ninety_nine()
    {
        Passport::actingAs(factory('App\User')->create());
        $params = ['maxAge' => 99];
        $this->json('POST', '/api/search', $params)
              ->assertOk();

        $params = ['maxAge' => '99'];
        $this->json('POST', '/api/search', $params)
              ->assertOk();
    }

    /**
     * @test
     */
    public function a_user_cannot_search_users_when_min_age_greater_than_max_age()
    {
        Passport::actingAs(factory('App\User')->create());
        $params = ['minAge' => 95, 'maxAge' => 91];
        $this->json('POST', '/api/search', $params)
              ->assertJsonValidationErrors(['maxAge'])
              ->assertJsonStructure(['message']);
    }

    /**
     * @test
     */
    public function a_user_can_search_users_when_max_age_greater_than_min_age()
    {
        Passport::actingAs(factory('App\User')->create());
        $params = ['minAge' => 90, 'maxAge' => 91];
        $this->json('POST', '/api/search', $params)
              ->assertOk();
    }

    /**
     * @test
     */
    public function a_user_can_search_users_when_min_age_equals_max_age()
    {
        Passport::actingAs(factory('App\User')->create());
        $params = ['minAge' => 90, 'maxAge' => 90];
        $this->json('POST', '/api/search', $params)
              ->assertOk();
    }

    /**
     * @test
     */
    public function a_user_can_search_users_who_are_unknown_gender()
    {
        Passport::actingAs(factory('App\User')->create());
        $params = ['gender' => 0];
        $this->json('POST', '/api/search', $params)
            ->assertOk();
        $params = ['gender' => '0'];
        $this->json('POST', '/api/search', $params)
                ->assertOk();
    }

    /**
     * @test
     */
    public function a_user_can_search_users_who_are_male()
    {
        Passport::actingAs(factory('App\User')->create());
        $params = ['gender' => 1];
        $this->json('POST', '/api/search', $params)
            ->assertOk();
        $params = ['gender' => '1'];
        $this->json('POST', '/api/search', $params)
                ->assertOk();
    }

    /**
     * @test
     */
    public function a_user_can_search_users_who_are_female()
    {
        Passport::actingAs(factory('App\User')->create());
        $params = ['gender' => 2];
        $this->json('POST', '/api/search', $params)
            ->assertOk();
        $params = ['gender' => '2'];
        $this->json('POST', '/api/search', $params)
                ->assertOk();
    }

    /**
     * @test
     */
    public function a_user_cannot_search_users_invalid_gender()
    {
        Passport::actingAs(factory('App\User')->create());
        $params = ['gender' => 3];
        $this->json('POST', '/api/search', $params)
              ->assertJsonValidationErrors(['gender'])
              ->assertJsonStructure(['message']);
        $params = ['gender' => '3'];
        $this->json('POST', '/api/search', $params)
              ->assertJsonValidationErrors(['gender'])
              ->assertJsonStructure(['message']);
    }

    /**
     * @test
     */
    public function a_user_can_search_valid_location()
    {
        //Edge cases, 0, 23.
        Passport::actingAs(factory('App\User')->create());
        $params = ['location' => 0];
        $this->json('POST', '/api/search', $params)
              ->assertOk();
        $params = ['location' => '0'];
        $this->json('POST', '/api/search', $params)
              ->assertOk();

        $params = ['location' => 23];
        $this->json('POST', '/api/search', $params)
                ->assertOk();
        $params = ['location' => '23'];
        $this->json('POST', '/api/search', $params)
                ->assertOk();
    }

    /**
     * @test
     */
    public function a_user_cannot_search_invalid_location()
    {
        Passport::actingAs(factory('App\User')->create());
        $params = ['location' => -1];
        $this->json('POST', '/api/search', $params)
              ->assertJsonValidationErrors(['location'])
              ->assertJsonStructure(['message']);
        $params = ['location' => '-1'];
        $this->json('POST', '/api/search', $params)
              ->assertJsonValidationErrors(['location'])
              ->assertJsonStructure(['message']);

        $params = ['location' => 24];
        $this->json('POST', '/api/search', $params)
              ->assertJsonValidationErrors(['location'])
              ->assertJsonStructure(['message']);
        $params = ['location' => '24'];
        $this->json('POST', '/api/search', $params)
              ->assertJsonValidationErrors(['location'])
              ->assertJsonStructure(['message']);
    }

    /**
     * @test
     */
    public function a_user_can_search_games()
    {
        Passport::actingAs(factory('App\User')->create());
        $params = ['game_name' => 'any'];
        $this->json('POST', '/api/search', $params)
              ->assertOk();
    }
}
