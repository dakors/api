<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Passport\Passport;

class CommentTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_guest_cannot_add_comment()
    {
        // If user is not logged in, will redirect to login.
        $this->postJson('api/comments/new')->assertStatus(401);
    }

    /** @test */
    public function a_guest_cannot_edit_comment()
    {
        // If user is not logged in, will redirect to login.
        $this->postJson('api/comments/update')->assertStatus(401);
    }

    /** @test */
    public function a_guest_cannot_delete_comment()
    {
        // If user is not logged in, will redirect to login.
        $this->postJson('api/comments/delete')->assertStatus(401);
    }

    /** @test */
    public function a_guest_cannot_get_comments()
    {
        // If user is not logged in, will redirect to login.
        $this->postJson('api/comments')->assertStatus(401);
    }

    /** @test **/
    public function a_user_can_add_comment_to_own_blog()
    {
        $user = factory('App\User')->create();

        $blog = factory('App\Models\UserBlog')->create(['user_id' => $user->id]);
        Passport::actingAs($user);
        $this->postJson(
            'api/comments/new',
            [
                'content' => 'this is a new comment',
                'blog_id' => $blog->blog_id
            ]
        )->assertOk()
          ->assertJson(['message' => __('comment.new_comment_success')]);

        $this->assertDatabaseHas(
            'blog_comment',
            [
                'content' => 'this is a new comment',
                'blog_id' => $blog->blog_id,
                'user_id' => $user->id
            ]
        );
    }

    /** @test **/
    public function a_user_can_add_comment_to_another_user_blog()
    {
        $user = factory('App\User')->create();
        $anotherUser = factory('App\User')->create();
        $blog = factory('App\Models\UserBlog')->create(['user_id' => $anotherUser->id]);
        Passport::actingAs($user);
        $this->postJson(
            'api/comments/new',
            [
                'content' => 'this is a new comment',
                'blog_id' => $blog->blog_id
            ]
        )->assertOk()
          ->assertJson(['message' => __('comment.new_comment_success')]);

        $this->assertDatabaseHas(
            'blog_comment',
            [
                'content' => 'this is a new comment',
                'blog_id' => $blog->blog_id,
                'user_id' => $user->id
            ]
        );
    }

    /** @test **/
    public function a_user_cannot_add_comment_without_content()
    {
        $user = factory('App\User')->create();

        $blog = factory('App\Models\UserBlog')->create(['user_id' => $user->id]);
        Passport::actingAs($user);
        $this->postJson(
            'api/comments/new',
            [
                'blog_id' => $blog->blog_id
            ]
        )->assertJsonValidationErrors('content')
          ->assertJsonMissingValidationErrors('blog_id')
          ->assertJsonStructure(['message']);

        $this->assertDatabaseMissing(
            'blog_comment',
            [
                'blog_id' => $blog->blog_id,
                'user_id' => $user->id
            ]
        );
    }

    /** @test **/
    public function a_user_cannot_add_comment_without_blog_id()
    {
        $user = factory('App\User')->create();

        $blog = factory('App\Models\UserBlog')->create(['user_id' => $user->id]);
        Passport::actingAs($user);
        $this->postJson(
            'api/comments/new',
            [
                'content' => 'this is content'
              ]
       )->assertJsonValidationErrors('blog_id')
         ->assertJsonMissingValidationErrors('content')
         ->assertJsonStructure(['message']);

        $this->assertDatabaseMissing(
            'blog_comment',
            [
              'content' => 'this is content',
              'user_id' => $user->id
            ]
        );
    }

    /** @test **/
    public function a_user_cannot_add_comment_to_a_blog_that_do_not_exist()
    {
        $user = factory('App\User')->create();

        $blog = factory('App\Models\UserBlog')->create(['user_id' => $user->id]);
        Passport::actingAs($user);
        $this->postJson(
            'api/comments/new',
            [
                'content' => 'this is a new comment',
                'blog_id' => 559
            ]
        )->assertNotFound()
          ->assertJson(['message' => __('blog.blog_not_found')]);

        $this->assertDatabaseMissing(
            'blog_comment',
            [
                'content' => 'this is a new comment',
                'user_id' => $user->id
            ]
        );
    }

    /** @test **/
    public function a_user_can_update_her_own_comment()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $blog = factory('App\Models\UserBlog')->create(['user_id' => $user->id]);
        $comment = factory('App\Models\BlogComment')->create(
            [
                'user_id' => $user->id,
                'blog_id' => $blog->blog_id,
                'content' => 'content to be edited'
            ]
        );

        $this->postJson(
            'api/comments/update',
            [
                'content' => 'edited content',
                'commentId' => $comment->comment_id
            ]
        )->assertOk()
          ->assertJson(['message' => __('comment.comment_updated')]);

        $this->assertDatabaseHas(
            'blog_comment',
            [
                'content' => 'edited content',
                'blog_id' => $blog->blog_id,
                'user_id' => $user->id
            ]
        );

        $this->assertDatabaseMissing(
            'blog_comment',
            [
                'content' => 'content to be edited',
                'blog_id' => $blog->blog_id,
                'user_id' => $user->id
            ]
        );
    }

    /** @test **/
    public function a_user_cannot_update_comment_without_content()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $comment = factory('App\Models\BlogComment')->create(
            [
                'user_id' => $user->id,
                'blog_id' => 1,
                'content' => 'content to be edited'
            ]
        );
        $this->postJson('api/comments/update', ['commentId' => $comment->comment_id])
              ->assertJsonValidationErrors(['content'])
              ->assertJsonMissingValidationErrors(['commentId'])
              ->assertJsonStructure(['message']);

        $this->assertDatabaseHas(
            'blog_comment',
            [
                'content' => 'content to be edited',
                'blog_id' => 1,
                'user_id' => $user->id
            ]
        );
    }

    /** @test **/
    public function a_user_cannot_update_comment_without_comment_id()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $comment = factory('App\Models\BlogComment')->create(
            [
                'user_id' => $user->id,
                'blog_id' => 1,
                'content' => 'content to be edited'
            ]
        );
        $this->postJson('api/comments/update', ['content' => 'content updated'])
              ->assertJsonValidationErrors('commentId')
              ->assertJsonMissingValidationErrors('content')
              ->assertJsonStructure(['message']);

        $this->assertDatabaseHas(
            'blog_comment',
            [
                'content' => 'content to be edited',
                'blog_id' => 1,
                'user_id' => $user->id
            ]
        );

        $this->assertDatabaseMissing(
            'blog_comment',
            [
                'content' => 'content updated',
                'blog_id' => 1,
                'user_id' => $user->id
            ]
        );
    }

    /** @test **/
    public function a_user_cannot_update_comment_not_exist()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $this->postJson(
            'api/comments/update',
            [
                'content' => 'edited content',
                'commentId' => 559
            ]
        )->assertNotFound()
          ->assertJson(['message' => __('comment.comment_not_found')]);

        $this->assertDatabaseMissing(
            'blog_comment',
            [
                'content' => 'edited content',
                'blog_id' => 559,
            ]
        );
    }

    /** @test **/
    public function a_user_cannot_update_comment_belonging_to_others()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $blog = factory('App\Models\UserBlog')->create(['user_id' => $user->id]);
        $comment = factory('App\Models\BlogComment')->create(
            [
                'user_id' => 559,
                'blog_id' => $blog->blog_id,
                'content' => 'content to be edited'
            ]
        );

        $this->postJson(
            'api/comments/update',
            [
                'content' => 'edited content',
                'commentId' => $comment->comment_id
            ]
        )->assertStatus(403)
          ->assertJson(['message' => __('comment.comment_not_owner')]);

        $this->assertDatabaseHas(
            'blog_comment',
            [
                'content' => 'content to be edited',
                'blog_id' => $blog->blog_id,
                'user_id' => 559
            ]
        );

        $this->assertDatabaseMissing(
            'blog_comment',
            [
                'content' => 'edited content',
                'blog_id' => $blog->blog_id,
                'user_id' => $user->id
            ]
        );
    }

    /** @test **/
    public function a_user_can_remove_her_own_comment()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $comment = factory('App\Models\BlogComment')->create(
            [
                'user_id' => $user->id,
                'blog_id' => 1,
                'content' => 'content to be deleted'
            ]
        );

        $this->postJson('api/comments/delete', ['commentId' => $comment->comment_id])
              ->assertOk()
              ->assertJson(['message' => __('comment.comment_removed')]);

        $this->assertDatabaseMissing(
            'blog_comment',
            [
                'user_id' => $user->id,
                'blog_id' => 1,
                'content' => 'content to be deleted'
            ]
        );
    }

    /** @test **/
    public function a_user_cannot_remove_comment_without_comment_id()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $this->postJson('api/comments/delete')
              ->assertJsonValidationErrors(['commentId'])
              ->assertJsonStructure(['message']);
    }

    /** @test **/
    public function a_user_cannot_remove_comment_not_found()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $comment = factory('App\Models\BlogComment')->create(
            [
                'user_id' => $user->id,
                'blog_id' => 1,
                'content' => 'content to be deleted'
            ]
        );

        $this->postJson('api/comments/delete', ['commentId' => 559])
              ->assertNotFound()
              ->assertJson(['message' => __('comment.comment_not_found')]);

        $this->assertDatabaseHas(
            'blog_comment',
            [
                'user_id' => $user->id,
                'blog_id' => 1,
                'content' => 'content to be deleted'
            ]
        );
    }

    /** @test **/
    public function a_user_cannot_remove_another_user_comment()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $comment = factory('App\Models\BlogComment')->create(
            [
                'user_id' => 559,
                'blog_id' => 1,
                'content' => 'content to be deleted'
            ]
        );

        $this->postJson('api/comments/delete', ['commentId' => $comment->comment_id])
              ->assertStatus(403)
              ->assertJson(['message' => __('comment.comment_not_owner')]);

        $this->assertDatabaseHas(
            'blog_comment',
            [
                'user_id' => 559,
                'blog_id' => 1,
                'content' => 'content to be deleted'
            ]
        );
    }

    /** @test **/
    public function a_user_can_get_comments_for_blog()
    {
        $user = factory('App\User')->create();
        $user2 = factory('App\User')->create();
        Passport::actingAs($user);
        $blog = factory('App\Models\UserBlog')->create(['user_id' => $user->id]);
        $comment = factory('App\Models\BlogComment')->create(
            [
                'user_id' => $user->id,
                'blog_id' => $blog->blog_id,
                'content' => 'content1'
            ]
        );
        $comment = factory('App\Models\BlogComment')->create(
            [
                'user_id' => $user2,
                'blog_id' => $blog->blog_id,
                'content' => 'content2'
            ]
        );
        $this->postJson('api/comments', ['blog_id' => $blog->blog_id])
              ->assertOk()
              ->assertJsonCount(2, 'data')
              ->assertJsonStructure([
                  'data' =>[
                      [
                          'comment_id',
                          'blog_id',
                          'content',
                          'user_id',
                          'nick_name',
                          'gender',
                          'photo',
                          'Issubscriber',
                          'IsCommentOwner',
                          'timestamp'
                      ]
                  ],
                  'links',
                  'meta',
                ]);
    }

    /** @test **/
    public function a_user_can_get_comments_for_blog_with_no_comments()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $blog = factory('App\Models\UserBlog')->create(['user_id' => $user->id]);
        $this->postJson('api/comments', ['blog_id' => $blog->blog_id])
              ->assertOk()
              ->assertJsonStructure([
                  'data' =>[],
                  'links',
                  'meta',
                ]);
    }

    /** @test **/
    public function a_user_cannot_get_comments_without_blog_id()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $this->postJson('api/comments')
              ->assertJsonValidationErrors('blog_id')
              ->assertJsonStructure(['message']);
    }

    /** @test **/
    public function a_user_cannot_get_comment_of_blog_that_is_not_found()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $blog = factory('App\Models\UserBlog')->create(['user_id' => $user->id]);
        $this->postJson('api/comments', ['blog_id' => 559])
              ->assertNotFound()
              ->assertJson(['message' => __('blog.blog_not_found')]);
    }
}
