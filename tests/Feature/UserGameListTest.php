<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Passport\Passport;

class UserGameListTest extends TestCase
{
    use RefreshDatabase;
    /** @test **/
    public function a_guest_cannot_add_game()
    {
        $this->postJson('/api/games/add')->assertStatus(401);
    }

    /** @test **/
    public function a_guest_cannot_remove_game()
    {
        $this->postJson('/api/games/remove')->assertStatus(401);
    }

    /** @test **/
    public function a_user_can_add_game()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $this->postJson('/api/games/add', ['game_name' => 'new game'])
              ->assertOk()
              ->assertJson(['message' => sprintf(__('userGameList.added'), 'new game')]);

        $this->assertDatabaseHas('user_game_list', ['user_id' => $user->id, 'game_name' => 'new game']);
    }

    /** @test **/
    public function a_user_cannot_add_interest_without_game_name()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $this->postJson('/api/games/add', ['game_name' => ''])
              ->assertJsonValidationErrors('game_name')
              ->assertJsonStructure(['message']);
    }

    /** @test **/
    public function a_user_can_remove_game()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $game = 'new game';
        factory('App\Models\UserGameList')->create(['game_name' => $game, 'user_id' => $user->id]);
        $this->postJson('/api/games/remove', ['game_name' => 'new game'])
              ->assertOk()
              ->assertJson(['message' => sprintf(__('userGameList.removed'), 'new game')]);

        $this->assertDatabaseMissing('user_game_list', ['user_id' => $user->id, 'game_name' => 'new game']);
    }

    /** @test **/
    public function a_user_cannot_remove_game_without_game_name()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $this->postJson('/api/games/add', ['game_name' => ''])
              ->assertJsonValidationErrors('game_name')
              ->assertJsonStructure(['message']);
    }

    /** @test **/
    public function a_user_cannot_remove_game_not_exist()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $game = 'new game';
        $this->postJson('/api/games/remove', ['game_name' => 'new game'])
              ->assertNotFound()
              ->assertJson(['message' => sprintf(__('userGameList.not_found'), 'new game')]);
    }
}
