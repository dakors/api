<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SocialAuthTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_guest_cannot_social_login_without_valid_inputs()
    {
        $this->postJson('api/auth/socialauth')
              ->assertJsonValidationErrors(['access_token', 'provider'])
              ->assertJsonStructure(['message']);
    }

    /** @test */
    public function a_guest_cannot_social_login_without_valid_providers()
    {
        $this->postJson('api/auth/socialauth', ['access_token' => 'faketokenstring', 'provider' => 'Twitch'])
              ->assertJsonValidationErrors(['provider'])
              ->assertJsonStructure(['message']);
    }
}
