<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Passport\Passport;

class FanControllerTest extends TestCase
{
    use RefreshDatabase;
    /** @test */
    public function a_guest_cannot_get_following()
    {
        // Request without valid token will return 401.
        $this->json('POST', '/api/following')->assertStatus(401);
    }

    /** @test */
    public function a_guest_cannot_get_fans()
    {
        // Request without valid token will return 401.
        $this->json('POST', '/api/fans')->assertStatus(401);
    }

    /** @test */
    public function a_guest_cannot_follow_other_user()
    {
        // Request without valid token will return 401.
        $this->json('POST', '/api/following/follow')->assertStatus(401);
    }

    /** @test */
    public function a_guest_cannot_unfollow_other_user()
    {
        // Request without valid token will return 401.
        $this->json('POST', '/api/following/unfollow')->assertStatus(401);
    }

    /** @test */
    public function a_user_can_get_following()
    {
        $user = factory('App\User')->create();

        Passport::actingAs($user);
        factory('App\User', 14)->create()->each(function ($newUser) use (&$user) {
            $fan = factory('App\Models\Fan')->create(
                [
                    'user_id' => $newUser->id,
                    'fans_id' => $user->id
                ]
            );
            factory('App\Models\UserImages')->create(['user_id' => $newUser->id]);
        });

        $this->withHeaders(['X-Localization' => 'en'])
                ->json('POST', '/api/following')
                ->assertOk()
                ->assertJsonStructure(
                    [
                          'data' =>
                          [
                              [
                                  'user_id',
                                  'nick_name',
                                  'gender',
                                  'age',
                                  'location',
                                  'photo',
                                  'background',
                                  'introduction',
                                  'game_list',
                                  'Fans',
                                  'isFb',
                                  'isGoogle',
                                  'Issubscriber',
                                  'IsFriend',
                                  'IsFan',
                                  'IsFollowing',
                                  'IsRequest',
                                  'IsMuted', // FollowingUserResource
                                  'game_name' // SearchUserResource
                              ]
                          ],
                          'links',
                          'meta'
                      ]
                  )
                ->assertJsonCount(14, 'data'); //assert page of 30
    }

    /** @test */
    public function a_user_can_get_max_30_following_per_page()
    {
        $user = factory('App\User')->create();

        Passport::actingAs($user);
        factory('App\User', 31)->create()->each(function ($newUser) use (&$user) {
            $fan = factory('App\Models\Fan')->create(
                [
                    'user_id' => $newUser->id,
                    'fans_id' => $user->id
                ]
            );
            factory('App\Models\UserImages')->create(['user_id' => $newUser->id]);
        });

        $this->withHeaders(['X-Localization' => 'en'])
                ->json('POST', '/api/following')
                ->assertOk()
                ->assertJsonStructure(
                    [
                          'data' =>
                          [
                              [
                                  'user_id',
                                  'nick_name',
                                  'gender',
                                  'age',
                                  'location',
                                  'photo',
                                  'background',
                                  'introduction',
                                  'game_list',
                                  'Fans',
                                  'isFb',
                                  'isGoogle',
                                  'Issubscriber',
                                  'IsFriend',
                                  'IsFan',
                                  'IsFollowing',
                                  'IsRequest',
                                  'IsMuted', //FollowingUserResource
                                  'game_name' //SearchUserResource
                              ]
                          ],
                          'links',
                          'meta'
                      ]
                  )
                ->assertJsonCount(30, 'data'); //assert page of 30
    }

    /** @test */
    public function a_user_can_sort_following_name_asc()
    {
        $user = factory('App\User')->create();

        Passport::actingAs($user);
        $user1 = factory('App\User')->create(['nick_name' => 'aaron']);
        factory('App\Models\UserImages')->create(['user_id' => $user1->id]);
        factory('App\Models\Fan')->create(
            [
                'user_id' => $user1->id,
                'fans_id' => $user->id
            ]
        );

        $user2 = factory('App\User')->create(['nick_name' => 'bryan']);
        factory('App\Models\UserImages')->create(['user_id' => $user2->id]);
        factory('App\Models\Fan')->create(
            [
                'user_id' => $user2->id,
                'fans_id' => $user->id
            ]
        );

        $user3 = factory('App\User')->create(['nick_name' => 'charlie']);
        factory('App\Models\UserImages')->create(['user_id' => $user3->id]);
        factory('App\Models\Fan')->create(
            [
                'user_id' => $user3->id,
                'fans_id' => $user->id
            ]
        );

        $this->withHeaders(['X-Localization' => 'en'])
                ->json('POST', '/api/following', ['orderby' => 'NameAsc'])
                ->assertOk()
                ->assertJson( // Here we assert the order.
                    [
                          'data' =>
                          [
                              [
                                  'nick_name' => $user1->nick_name
                              ],
                              [
                                  'nick_name' => $user2->nick_name
                              ],
                              [
                                  'nick_name' => $user3->nick_name
                              ]
                          ],
                      ]
                  )
                ->assertJsonCount(3, 'data'); //assert page of 30
    }

    /** @test */
    public function a_user_can_sort_following_name_desc()
    {
        $user = factory('App\User')->create();

        Passport::actingAs($user);
        $user1 = factory('App\User')->create(['nick_name' => 'aaron']);
        factory('App\Models\UserImages')->create(['user_id' => $user1->id]);
        factory('App\Models\Fan')->create(
            [
                'user_id' => $user1->id,
                'fans_id' => $user->id
            ]
        );

        $user2 = factory('App\User')->create(['nick_name' => 'bryan']);
        factory('App\Models\UserImages')->create(['user_id' => $user2->id]);
        factory('App\Models\Fan')->create(
            [
                'user_id' => $user2->id,
                'fans_id' => $user->id
            ]
        );

        $user3 = factory('App\User')->create(['nick_name' => 'charlie']);
        factory('App\Models\UserImages')->create(['user_id' => $user3->id]);
        factory('App\Models\Fan')->create(
            [
                'user_id' => $user3->id,
                'fans_id' => $user->id
            ]
        );

        $this->withHeaders(['X-Localization' => 'en'])
                ->json('POST', '/api/following', ['orderby' => 'NameDesc'])
                ->assertOk()
                ->assertJson( // Here we assert the order.
                    [
                          'data' =>
                          [
                              [
                                  'nick_name' => $user3->nick_name
                              ],
                              [
                                  'nick_name' => $user2->nick_name
                              ],
                              [
                                  'nick_name' => $user1->nick_name
                              ]
                          ],
                      ]
                  )
                ->assertJsonCount(3, 'data'); //assert page of 30
    }

    /** @test */
    public function a_user_can_filter_following()
    {
        $user = factory('App\User')->create();

        Passport::actingAs($user);
        factory('App\User', 31)->create()->each(function ($newUser) use (&$user) {
            $fan = factory('App\Models\Fan')->create(
                [
                    'user_id' => $newUser->id,
                    'fans_id' => $user->id
                ]
            );
            factory('App\Models\UserImages')->create(['user_id' => $newUser->id]);
        });

        $user1 = factory('App\User')->create(['nick_name' => 'coco']);
        factory('App\Models\UserImages')->create(['user_id' => $user1->id]);
        factory('App\Models\Fan')->create(
            [
                'user_id' => $user1->id,
                'fans_id' => $user->id
            ]
        );

        $user2 = factory('App\User')->create(['nick_name' => 'cococo']);
        factory('App\Models\UserImages')->create(['user_id' => $user2->id]);
        factory('App\Models\Fan')->create(
            [
                'user_id' => $user2->id,
                'fans_id' => $user->id
            ]
        );

        $user3 = factory('App\User')->create(['nick_name' => 'cococobo']);
        factory('App\Models\UserImages')->create(['user_id' => $user3->id]);
        factory('App\Models\Fan')->create(
            [
                'user_id' => $user3->id,
                'fans_id' => $user->id
            ]
        );

        $this->withHeaders(['X-Localization' => 'en'])
                ->json('POST', '/api/following', ['filter' => 'coco'])
                ->assertOk()
                ->assertJson( // Here we assert the order.
                    [
                          'data' =>
                          [
                              [
                                  'nick_name' => $user1->nick_name
                              ],
                              [
                                  'nick_name' => $user2->nick_name
                              ],
                              [
                                  'nick_name' => $user3->nick_name
                              ]
                          ],
                      ]
                  )
                ->assertJsonCount(3, 'data');

        $this->withHeaders(['X-Localization' => 'en'])
                ->json('POST', '/api/following', ['filter' => 'cococo'])
                ->assertOk()
                ->assertJson( // Here we assert the order.
                    [
                          'data' =>
                          [
                              [
                                  'nick_name' => $user2->nick_name
                              ],
                              [
                                  'nick_name' => $user3->nick_name
                              ]
                          ],
                      ]
                  )
                ->assertJsonCount(2, 'data');

        $this->withHeaders(['X-Localization' => 'en'])
                ->json('POST', '/api/following', ['filter' => 'cococobo'])
                ->assertOk()
                ->assertJson( // Here we assert the order.
                    [
                          'data' =>
                          [
                              [
                                  'nick_name' => $user3->nick_name
                              ]
                          ],
                      ]
                  )
                ->assertJsonCount(1, 'data'); //assert page of 30

        $this->withHeaders(['X-Localization' => 'en'])
                ->json('POST', '/api/following', ['filter' => 'cocococobo'])
                ->assertOk()
                ->assertJson( // Here we assert the order.
                    [
                          'data' =>[]
                      ]
                  )
                ->assertJsonCount(0, 'data'); //assert page of 30
    }

    /** @test */
    public function a_user_can_get_empty_following()
    {
        Passport::actingAs(factory('App\User')->create());

        $this->withHeaders(['X-Localization' => 'en'])
                ->json('POST', '/api/following')
                ->assertOk()
                ->assertJsonStructure(['data', 'links', 'meta'])
                ->assertJsonCount(0, 'data');
    }

    /** @test */
    public function a_user_can_get_fans()
    {
        $user = factory('App\User')->create();

        Passport::actingAs($user);
        factory('App\User', 20)->create()->each(function ($newUser) use (&$user) {
            $fan = factory('App\Models\Fan')->create(
                [
                    'user_id' => $user->id,
                    'fans_id' => $newUser->id
                ]
            );
            factory('App\Models\UserImages')->create(['user_id' => $newUser->id]);
        });

        $this->withHeaders(['X-Localization' => 'en'])
                ->json('POST', '/api/fans')
                ->assertOk()
                ->assertJsonStructure(
                    [
                          'data' =>
                          [
                              [
                                  'user_id',
                                  'nick_name',
                                  'gender',
                                  'age',
                                  'location',
                                  'photo',
                                  'background',
                                  'introduction',
                                  'game_list',
                                  'Fans',
                                  'isFb',
                                  'isGoogle',
                                  'Issubscriber',
                                  'IsFriend',
                                  'IsFan',
                                  'IsFollowing',
                                  'IsRequest',
                                  'game_name' //SearchUserResource
                              ]
                          ],
                          'meta',
                          'links'
                      ]
                  )
                ->assertJsonCount(20, 'data'); // Assert page of 30
    }

    /** @test */
    public function a_user_can_get_max_30_fans_per_page()
    {
        $user = factory('App\User')->create();

        Passport::actingAs($user);
        factory('App\User', 41)->create()->each(function ($newUser) use (&$user) {
            $fan = factory('App\Models\Fan')->create(
                [
                    'user_id' => $user->id,
                    'fans_id' => $newUser->id
                ]
            );
            factory('App\Models\UserImages')->create(['user_id' => $newUser->id]);
        });

        $this->withHeaders(['X-Localization' => 'en'])
                ->json('POST', '/api/fans')
                ->assertOk()
                ->assertJsonStructure(
                    [
                          'data' =>
                          [
                              [
                                  'user_id',
                                  'nick_name',
                                  'gender',
                                  'age',
                                  'location',
                                  'photo',
                                  'background',
                                  'introduction',
                                  'game_list',
                                  'Fans',
                                  'isFb',
                                  'isGoogle',
                                  'Issubscriber',
                                  'IsFriend',
                                  'IsFan',
                                  'IsFollowing',
                                  'IsRequest',
                                  'game_name' //SearchUserResource
                              ]
                          ],
                          'meta',
                          'links'
                      ]
                  )
                ->assertJsonCount(30, 'data'); // Assert page of 30
    }

    /** @test */
    public function a_user_can_sort_fan_name_asc()
    {
        $user = factory('App\User')->create();

        Passport::actingAs($user);
        $user1 = factory('App\User')->create(['nick_name' => 'aaron']);
        factory('App\Models\UserImages')->create(['user_id' => $user1->id]);
        factory('App\Models\Fan')->create(
            [
                'user_id' => $user->id,
                'fans_id' => $user1->id
            ]
        );

        $user2 = factory('App\User')->create(['nick_name' => 'bryan']);
        factory('App\Models\UserImages')->create(['user_id' => $user2->id]);
        factory('App\Models\Fan')->create(
            [
                'user_id' => $user->id,
                'fans_id' => $user2->id
            ]
        );

        $user3 = factory('App\User')->create(['nick_name' => 'charlie']);
        factory('App\Models\UserImages')->create(['user_id' => $user3->id]);
        factory('App\Models\Fan')->create(
            [
                'user_id' => $user->id,
                'fans_id' => $user3->id
            ]
        );

        $this->withHeaders(['X-Localization' => 'en'])
                ->json('POST', '/api/fans', ['orderby' => 'NameAsc'])
                ->assertOk()
                ->assertJson( // Here we assert the order.
                    [
                          'data' =>
                          [
                              [
                                  'nick_name' => $user1->nick_name
                              ],
                              [
                                  'nick_name' => $user2->nick_name
                              ],
                              [
                                  'nick_name' => $user3->nick_name
                              ]
                          ],
                      ]
                  )
                ->assertJsonCount(3, 'data'); //assert page of 30
    }

    /** @test */
    public function a_user_can_sort_fan_name_desc()
    {
        $user = factory('App\User')->create();

        Passport::actingAs($user);
        $user1 = factory('App\User')->create(['nick_name' => 'aaron']);
        factory('App\Models\UserImages')->create(['user_id' => $user1->id]);
        factory('App\Models\Fan')->create(
            [
                'user_id' => $user->id,
                'fans_id' => $user1->id
            ]
        );

        $user2 = factory('App\User')->create(['nick_name' => 'bryan']);
        factory('App\Models\UserImages')->create(['user_id' => $user2->id]);
        factory('App\Models\Fan')->create(
            [
                'user_id' => $user->id,
                'fans_id' => $user2->id
            ]
        );

        $user3 = factory('App\User')->create(['nick_name' => 'charlie']);
        factory('App\Models\UserImages')->create(['user_id' => $user3->id]);
        factory('App\Models\Fan')->create(
            [
                'user_id' => $user->id,
                'fans_id' => $user3->id
            ]
        );

        $this->withHeaders(['X-Localization' => 'en'])
                ->json('POST', '/api/fans', ['orderby' => 'NameDesc'])
                ->assertOk()
                ->assertJson( // Here we assert the order.
                    [
                          'data' =>
                          [
                              [
                                  'nick_name' => $user3->nick_name
                              ],
                              [
                                  'nick_name' => $user2->nick_name
                              ],
                              [
                                  'nick_name' => $user1->nick_name
                              ]
                          ],
                      ]
                  )
                ->assertJsonCount(3, 'data'); //assert page of 30
    }

    /** @test */
    public function a_user_can_filter_fans()
    {
        $user = factory('App\User')->create();

        Passport::actingAs($user);
        factory('App\User', 31)->create()->each(function ($newUser) use (&$user) {
            $fan = factory('App\Models\Fan')->create(
                [
                    'user_id' => $user->id,
                    'fans_id' => $newUser->id
                ]
            );
            factory('App\Models\UserImages')->create(['user_id' => $newUser->id]);
        });

        $user1 = factory('App\User')->create(['nick_name' => 'coco']);
        factory('App\Models\UserImages')->create(['user_id' => $user1->id]);
        factory('App\Models\Fan')->create(
            [
                'user_id' => $user->id,
                'fans_id' => $user1->id
            ]
        );

        $user2 = factory('App\User')->create(['nick_name' => 'cococo']);
        factory('App\Models\UserImages')->create(['user_id' => $user2->id]);
        factory('App\Models\Fan')->create(
            [
                'user_id' => $user->id,
                'fans_id' => $user2->id
            ]
        );

        $user3 = factory('App\User')->create(['nick_name' => 'cococobo']);
        factory('App\Models\UserImages')->create(['user_id' => $user3->id]);
        factory('App\Models\Fan')->create(
            [
                'user_id' => $user->id,
                'fans_id' => $user3->id
            ]
        );

        $this->withHeaders(['X-Localization' => 'en'])
                ->json('POST', '/api/fans', ['filter' => 'coco'])
                ->assertOk()
                ->assertJson( // Here we assert the order.
                    [
                          'data' =>
                          [
                              [
                                  'nick_name' => $user1->nick_name
                              ],
                              [
                                  'nick_name' => $user2->nick_name
                              ],
                              [
                                  'nick_name' => $user3->nick_name
                              ]
                          ],
                      ]
                  )
                ->assertJsonCount(3, 'data');

        $this->withHeaders(['X-Localization' => 'en'])
                ->json('POST', '/api/fans', ['filter' => 'cococo'])
                ->assertOk()
                ->assertJson( // Here we assert the order.
                    [
                          'data' =>
                          [
                              [
                                  'nick_name' => $user2->nick_name
                              ],
                              [
                                  'nick_name' => $user3->nick_name
                              ]
                          ],
                      ]
                  )
                ->assertJsonCount(2, 'data');

        $this->withHeaders(['X-Localization' => 'en'])
                ->json('POST', '/api/fans', ['filter' => 'cococobo'])
                ->assertOk()
                ->assertJson( // Here we assert the order.
                    [
                          'data' =>
                          [
                              [
                                  'nick_name' => $user3->nick_name
                              ]
                          ],
                      ]
                  )
                ->assertJsonCount(1, 'data'); //assert page of 30

        $this->withHeaders(['X-Localization' => 'en'])
                ->json('POST', '/api/fans', ['filter' => 'cocococobo'])
                ->assertOk()
                ->assertJson( // Here we assert the order.
                    [
                          'data' =>[]
                      ]
                  )
                ->assertJsonCount(0, 'data');
    }

    /** @test */
    public function a_user_can_get_empty_fans()
    {
        Passport::actingAs(factory('App\User')->create());

        $this->withHeaders(['X-Localization' => 'en'])
                ->json('POST', '/api/fans')
                ->assertOk()
                ->assertJsonStructure(['data', 'links', 'meta'])
                ->assertJsonCount(0, 'data');
    }

    /** @test */
    public function a_user_can_follow_user()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $otherUser = factory('App\User')->create();
        $this->withHeaders(['X-Localization' => 'en'])
                ->json('POST', 'api/following/follow', ['user_id' => $otherUser->id])
                ->assertOk()
                ->assertJson(['message' => sprintf(__('fan.follow_success'), $otherUser->nick_name)]);

        $this->assertDatabaseHas('fans_list', [
                    'user_id' => $otherUser->id,
                    'fans_id' => $user->id,
                    'isMuted' => 'false'
                ]);
    }

    /** @test */
    public function a_user_cannot_follow_invalid_user()
    {
        Passport::actingAs(factory('App\User')->create());
        $this->withHeaders(['X-Localization' => 'en'])
                ->json('POST', 'api/following/follow', ['user_id' => 559])
                ->assertNotFound()
                ->assertJson(['message' => __('User not found')]);
    }

    /** @test */
    public function a_user_cannot_follow_without_valid_inputs()
    {
        Passport::actingAs(factory('App\User')->create());
        $this->withHeaders(['X-Localization' => 'en'])
                ->json('POST', 'api/following/follow')
                ->assertJsonValidationErrors('user_id')
                ->assertJsonStructure(['message']);
    }

    /** @test */
    public function a_user_cannot_follow_a_user_who_he_already_followed()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $otherUser = factory('App\User')->create();
        factory('App\Models\Fan')->create([
            'user_id' => $otherUser->id,
            'fans_id' => $user->id
        ]);
        $this->withHeaders(['X-Localization' => 'en'])
                ->json('POST', 'api/following/follow', ['user_id' => $otherUser->id])
                ->assertStatus(500)
                ->assertJson(['message' => sprintf(__('fan.follow_fail_duplicate'), $otherUser->nick_name)]);
    }

    /** @test */
    public function a_user_can_unfollow_user()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $otherUser = factory('App\User')->create();
        factory('App\Models\Fan')->create([
            'user_id' => $otherUser->id,
            'fans_id' => $user->id
        ]);
        $this->withHeaders(['X-Localization' => 'en'])
                ->json('POST', 'api/following/unfollow', ['user_id' => $otherUser->id])
                ->assertStatus(200)
                ->assertJson(['message' => sprintf(__('fan.unfollow_success'), $otherUser->nick_name)]);
    }

    /** @test */
    public function a_user_cannot_unfollow_invalid_user()
    {
        Passport::actingAs(factory('App\User')->create());
        $this->withHeaders(['X-Localization' => 'en'])
                ->json('POST', 'api/following/unfollow', ['user_id' => 559])
                ->assertNotFound()
                ->assertJson(['message' => __('User not found')]);
    }

    /** @test */
    public function a_user_cannot_unfollow_without_valid_inputs()
    {
        Passport::actingAs(factory('App\User')->create());
        $this->withHeaders(['X-Localization' => 'en'])
                ->json('POST', 'api/following/follow')
                ->assertJsonValidationErrors('user_id')
                ->assertJsonStructure(['message']);
    }

    /** @test */
    public function a_user_cannot_unfollow_user_who_is_not_followed()
    {
        $this->withoutExceptionHandling();
        Passport::actingAs(factory('App\User')->create());
        $otherUser = factory('App\User')->create();
        $this->withHeaders(['X-Localization' => 'en'])
                ->json('POST', 'api/following/unfollow', ['user_id' => $otherUser->id])
                ->assertStatus(404)
                ->assertJson(['message' => sprintf(__('fan.unfollow_fail_not_following'), $otherUser->nick_name)]);
    }
}
