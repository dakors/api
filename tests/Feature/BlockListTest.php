<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Passport\Passport;

class BlockListTest extends TestCase
{
    /** @test **/
    public function a_guest_cannot_get_block_list()
    {
        $this->postJson('api/blocklist')->assertStatus(401);
    }

    /** @test **/
    public function a_guest_cannot_block_another_user()
    {
        $this->postJson('api/blocklist/block')->assertStatus(401);
    }

    /** @test **/
    public function a_guest_cannot_unblock_another_user()
    {
        $this->postJson('api/blocklist/unblock')->assertStatus(401);
    }

    /** @test **/
    public function a_user_can_get_block_list()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $blockUser1 = factory('App\User')->create();
        $blockUser2 = factory('App\User')->create();
        factory('App\Models\UserBlockList')
              ->create(
                  [
                      'user_id' => $user->id,
                      'block_id' => $blockUser1->id
                  ]
              );

        factory('App\Models\UserBlockList')
              ->create(
                  [
                      'user_id' => $user->id,
                      'block_id' => $blockUser2->id
                  ]
              );
        $this->postJson('api/blocklist')
              ->assertJsonStructure(
                  [
                        'data' =>
                        [
                            [
                                'user_id',
                                'nick_name',
                                'gender',
                                'age',
                                'location',
                                'photo',
                                'background',
                                'introduction',
                                'game_list',
                                'Fans',
                                'isFb',
                                'isGoogle',
                                'Issubscriber',
                                'IsFriend',
                                'IsFan',
                                'IsFollowing',
                                'IsRequest',
                            ]
                        ]
                    ]
                )
              ->assertJsonCount(2, 'data');
    }

    /** @test **/
    public function a_user_can_block_another_user()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $blockUser = factory('App\User')->create();
        $this->postJson('api/blocklist/block', ['user_id' => $blockUser->id])
              ->assertStatus(200)
              ->assertJson(['message' => sprintf(__('blockList.add_block_success'), $blockUser->nick_name)]);

        $this->assertDatabaseHas('user_block_list', ['user_id' => $user->id, 'block_id' => $blockUser->id]);
    }

    /** @test **/
    public function a_user_cannot_block_without_user_id()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $blockUser = factory('App\User')->create();
        $this->postJson('api/blocklist/block')
              ->assertJsonValidationErrors('user_id')
              ->assertJsonStructure(['message']);

        $this->assertDatabaseMissing('user_block_list', ['user_id' => $user->id, 'block_id' => $blockUser->id]);
    }

    /** @test **/
    public function a_user_cannot_block_user_who_is_not_found()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $this->postJson('api/blocklist/block', ['user_id' => 559])
              ->assertNotFound()
              ->assertJson(['message' => __('User not found')]);

        $this->assertDatabaseMissing('user_block_list', ['user_id' => $user->id, 'block_id' => 559]);
    }

    /** @test **/
    public function a_user_cannot_block_a_blocked_user()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $blockUser = factory('App\User')->create();
        factory('App\Models\UserBlockList')
              ->create(
                  [
                      'user_id' => $user->id,
                      'block_id' => $blockUser->id
                  ]
              );
        $this->postJson('api/blocklist/block', ['user_id' => $blockUser->id])
              ->assertStatus(500)
              ->assertJson(['message' => sprintf(__('blockList.block_fail_duplicate'), $blockUser->nick_name)]);
    }

    /** @test **/
    public function a_user_can_unblock_a_user()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $blockUser = factory('App\User')->create();
        factory('App\Models\UserBlockList')
              ->create(
                  [
                      'user_id' => $user->id,
                      'block_id' => $blockUser->id
                  ]
              );
        $this->postJson('api/blocklist/unblock', ['user_id' => $blockUser->id])
              ->assertOk()
              ->assertJson(['message' => sprintf(__('blockList.unblock_success'), $blockUser->nick_name)]);

        $this->assertDatabaseMissing('user_block_list', ['user_id' => $user->id, 'block_id' => $blockUser->id]);
    }

    /** @test **/
    public function a_user_cannot_unblock_without_user_id()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $blockUser = factory('App\User')->create();

        factory('App\Models\UserBlockList')
              ->create(
                  [
                      'user_id' => $user->id,
                      'block_id' => $blockUser->id
                  ]
              );

        $this->postJson('api/blocklist/unblock')
              ->assertJsonValidationErrors('user_id')
              ->assertJsonStructure(['message']);

        $this->assertDatabaseHas('user_block_list', ['user_id' => $user->id, 'block_id' => $blockUser->id]);
    }

    /** @test */
    public function a_user_cannot_unblock_user_not_found()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $this->postJson('api/blocklist/unblock', ['user_id' => 559])
              ->assertNotFound()
              ->assertJson(['message' => __('User not found')]);
    }

    /** @test */
    public function a_user_cannot_unblock_user_who_is_not_blocked()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $blockUser = factory('App\User')->create();
        $this->postJson('api/blocklist/unblock', ['user_id' => $blockUser->id])
              ->assertNotFound()
              ->assertJson(['message' => sprintf(__('blockList.unblock_fail_not_blocking'), $blockUser->nick_name)]);
    }
}
