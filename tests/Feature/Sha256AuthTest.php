<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class Sha256AuthTest extends TestCase
{
    use RefreshDatabase;

    protected function createUser()
    {
        factory('Laravel\Passport\Client')
          ->create(
                      [
                          'id' => 1,
                          'name' => 'Gamerbox Personal Access Client',
                          'personal_access_client' => 1,
                          'password_client' => 0
                      ]
                  );
        factory('Laravel\Passport\Client')
          ->create(
                      [
                          'id' => 2,
                          'name' => 'Gamerbox Password Grant Client',
                          'personal_access_client' => 0,
                          'password_client' => 1
                      ]
                  );
        $this->postJson('/api/auth/register',
                    ['name' => 'newtester2',
                     'email' => 'test@trendian.com',
                     'password' => '123456',
                     'password_confirmation' => '123456'])
              ->assertOk();
    }

    /** @test */
    public function a_guest_can_login()
    {
        // $this->withoutExceptionHandling();
        $this->createUser();
        $this->postJson('api/auth/login', ['email' => 'test@trendian.com', 'password' => '123456'])
             ->assertOk()
             ->assertJsonStructure(
                  [
                      'token_type',
                      'expires_in',
                      'access_token',
                      'refresh_token'
                  ]
               );
    }

    /** @test */
    public function a_guest_cannot_login_wrong_email()
    {
        // $this->withoutExceptionHandling();
        $this->createUser();
        $this->postJson('api/auth/login', ['email' => 'test@trendan.com', 'password' => '123456'])
             ->assertStatus(401)
             ->assertJson(['message' => 'The user credentials were incorrect.']);
    }

    /** @test */
    public function a_guest_cannot_login_wrong_password()
    {
        $this->createUser();
        $this->postJson('api/auth/login', ['email' => 'test@trendian.com', 'password' => '123457'])
             ->assertStatus(401)
             ->assertJson(['message' => 'The user credentials were incorrect.']);
    }

    /** @test */
    public function a_guest_cannot_login_without_valid_inputs()
    {
        $this->postJson('api/auth/login')
              ->assertJsonValidationErrors(['email', 'password'])
              ->assertJsonStructure(['message']);
    }

    /** @test */
    public function a_guest_cannot_login_without_proper_email_format()
    {
        $this->postJson('api/auth/login', ['email' => 'testerattrendian.com', '123456'])
              ->assertJsonValidationErrors('email')
              ->assertJsonStructure(['message']);

        $this->postJson('api/auth/login', ['email' => 'tester@trendian', '123456'])
              ->assertJsonValidationErrors('email')
              ->assertJsonStructure(['message']);
    }

    /** @test */
    public function a_guest_cannot_login_with_password_less_than_6_characters()
    {
        $this->postJson('api/auth/login', ['email' => 'tester@trendian.com', '12345'])
              ->assertJsonValidationErrors('password')
              ->assertJsonStructure(['message']);
    }
}
