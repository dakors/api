<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Passport\Passport;

class FriendControllerTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_guest_cannot_get_friends()
    {
        // Request without valid token will return 401.
        $this->json('POST', '/api/friends')->assertStatus(401);
    }

    /** @test */
    public function a_guest_cannot_get_requests()
    {
        // Request without valid token will return 401.
        $this->json('POST', '/api/requests')->assertStatus(401);
    }

    /** @test */
    public function a_guest_cannot_send_request()
    {
        // Request without valid token will return 401.
        $this->json('POST', '/api/requests/send')->assertStatus(401);
    }

    /** @test */
    public function a_guest_cannot_reject_request()
    {
        // Request without valid token will return 401.
        $this->json('POST', '/api/requests/reject')->assertStatus(401);
    }

    /** @test */
    public function a_guest_cannot_accept_request()
    {
        // Request without valid token will return 401.
        $this->json('POST', '/api/requests/accept')->assertStatus(401);
    }

    /** @test */
    public function a_guest_cannot_unfriend()
    {
        // Request without valid token will return 401.
        $this->json('POST', '/api/friends/unfriend')->assertStatus(401);
    }

    /** @test */
    public function a_user_can_get_friends()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        factory('App\User', 10)->create()->each(function ($newUser) use (&$user) {
            $friend = factory('App\Models\Friend')->create(
                [
                    'requester' => $newUser->id,
                    'receiver' => $user->id,
                    'status' => 'confirmed'
                ]
            );

            factory('App\Models\UserImages')->create(['user_id' => $newUser->id]);
        });
        $this->json('POST', '/api/friends')
                ->assertOk()
                ->assertJsonStructure(
                    [
                          'data' =>
                          [
                              [
                                  'user_id',
                                  'nick_name',
                                  'gender',
                                  'age',
                                  'location',
                                  'photo',
                                  'background',
                                  'introduction',
                                  'game_list',
                                  'Fans',
                                  'isFb',
                                  'isGoogle',
                                  'Issubscriber',
                                  'IsFriend',
                                  'IsFan',
                                  'IsFollowing',
                                  'IsRequest',
                                  'game_name' //SearchUserResource
                              ]
                          ],
                          'links',
                          'meta'
                      ]
                  )
                ->assertJsonCount(10, 'data');
    }

    /** @test */
    public function a_user_can_get_max_30_friends_per_page()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        factory('App\User', 40)->create()->each(function ($newUser) use (&$user) {
            $friend = factory('App\Models\Friend')->create(
                [
                        'requester' => $newUser->id,
                        'receiver' => $user->id,
                        'status' => 'confirmed'
                    ]
                );
            factory('App\Models\UserImages')->create(['user_id' => $newUser->id]);
        });
        $this->json('POST', '/api/friends')
                    ->assertOk()
                    ->assertJsonStructure(
                        [
                              'data' =>
                              [
                                  [
                                      'user_id',
                                      'nick_name',
                                      'gender',
                                      'age',
                                      'location',
                                      'photo',
                                      'background',
                                      'introduction',
                                      'game_list',
                                      'Fans',
                                      'isFb',
                                      'isGoogle',
                                      'Issubscriber',
                                      'IsFriend',
                                      'IsFan',
                                      'IsFollowing',
                                      'IsRequest',
                                      'game_name' //SearchUserResource
                                  ]
                              ],
                              'links',
                              'meta'
                          ]
                      )
                    ->assertJsonCount(30, 'data');
    }

    /** @test */
    public function a_user_can_sort_friend_name_asc()
    {
        $user = factory('App\User')->create();

        Passport::actingAs($user);
        $user1 = factory('App\User')->create(['nick_name' => 'aaron']);
        factory('App\Models\UserImages')->create(['user_id' => $user1->id]);
        factory('App\Models\Friend')->create(
            [
                'requester' => $user1->id,
                'receiver' => $user->id,
                'status' => 'confirmed'
            ]
        );

        $user2 = factory('App\User')->create(['nick_name' => 'bryan']);
        factory('App\Models\UserImages')->create(['user_id' => $user2->id]);
        factory('App\Models\Friend')->create(
            [
                'requester' => $user2->id,
                'receiver' => $user->id,
                'status' => 'confirmed'
            ]
        );

        $user3 = factory('App\User')->create(['nick_name' => 'charlie']);
        factory('App\Models\UserImages')->create(['user_id' => $user3->id]);
        factory('App\Models\Friend')->create(
            [
                'requester' => $user3->id,
                'receiver' => $user->id,
                'status' => 'confirmed'
            ]
        );

        $this->withHeaders(['X-Localization' => 'en'])
                ->json('POST', '/api/friends', ['orderby' => 'NameAsc'])
                ->assertOk()
                ->assertJson( // Here we assert the order.
                    [
                          'data' =>
                          [
                              [
                                  'nick_name' => $user1->nick_name
                              ],
                              [
                                  'nick_name' => $user2->nick_name
                              ],
                              [
                                  'nick_name' => $user3->nick_name
                              ]
                          ],
                      ]
                  )
                ->assertJsonCount(3, 'data'); //assert page of 3
    }

    /** @test */
    public function a_user_can_sort_friend_name_desc()
    {
        $user = factory('App\User')->create();

        Passport::actingAs($user);
        $user1 = factory('App\User')->create(['nick_name' => 'aaron']);
        factory('App\Models\UserImages')->create(['user_id' => $user1->id]);
        factory('App\Models\Friend')->create(
            [
                'requester' => $user1->id,
                'receiver' => $user->id,
                'status' => 'confirmed'
            ]
        );

        $user2 = factory('App\User')->create(['nick_name' => 'bryan']);
        factory('App\Models\UserImages')->create(['user_id' => $user2->id]);
        factory('App\Models\Friend')->create(
            [
                'requester' => $user2->id,
                'receiver' => $user->id,
                'status' => 'confirmed'
            ]
        );

        $user3 = factory('App\User')->create(['nick_name' => 'charlie']);
        factory('App\Models\UserImages')->create(['user_id' => $user3->id]);
        factory('App\Models\Friend')->create(
            [
                'requester' => $user3->id,
                'receiver' => $user->id,
                'status' => 'confirmed'
            ]
        );

        $this->withHeaders(['X-Localization' => 'en'])
                ->json('POST', '/api/friends', ['orderby' => 'NameDesc'])
                ->assertOk()
                ->assertJson( // Here we assert the order.
                    [
                          'data' =>
                          [
                              [
                                  'nick_name' => $user3->nick_name
                              ],
                              [
                                  'nick_name' => $user2->nick_name
                              ],
                              [
                                  'nick_name' => $user1->nick_name
                              ]
                          ],
                      ]
                  )
                ->assertJsonCount(3, 'data'); //assert page of 3
    }

    /** @test */
    public function a_user_can_filter_friends()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        factory('App\User', 40)->create()->each(function ($newUser) use (&$user) {
            $friend = factory('App\Models\Friend')->create(
                [
                        'requester' => $newUser->id,
                        'receiver' => $user->id,
                        'status' => 'confirmed'
                    ]
                );
            factory('App\Models\UserImages')->create(['user_id' => $newUser->id]);
        });

        $user1 = factory('App\User')->create(['nick_name' => 'coco']);
        factory('App\Models\UserImages')->create(['user_id' => $user1->id]);
        factory('App\Models\Friend')->create(
            [
                'requester' => $user1->id,
                'receiver' => $user->id,
                'status' => 'confirmed'
            ]
        );

        $user2 = factory('App\User')->create(['nick_name' => 'cococo']);
        factory('App\Models\UserImages')->create(['user_id' => $user2->id]);
        factory('App\Models\Friend')->create(
            [
                'requester' => $user2->id,
                'receiver' => $user->id,
                'status' => 'confirmed'
            ]
        );

        $user3 = factory('App\User')->create(['nick_name' => 'cococobo']);
        factory('App\Models\UserImages')->create(['user_id' => $user3->id]);
        factory('App\Models\Friend')->create(
            [
                'requester' => $user3->id,
                'receiver' => $user->id,
                'status' => 'confirmed'
            ]
        );

        $this->withHeaders(['X-Localization' => 'en'])
                ->json('POST', '/api/friends', ['filter' => 'coco'])
                ->assertOk()
                ->assertJson( // Here we assert the order.
                    [
                          'data' =>
                          [
                              [
                                  'nick_name' => $user1->nick_name
                              ],
                              [
                                  'nick_name' => $user2->nick_name
                              ],
                              [
                                  'nick_name' => $user3->nick_name
                              ]
                          ],
                      ]
                  )
                ->assertJsonCount(3, 'data');

        $this->withHeaders(['X-Localization' => 'en'])
                ->json('POST', '/api/friends', ['filter' => 'cococo'])
                ->assertOk()
                ->assertJson( // Here we assert the order.
                    [
                          'data' =>
                          [
                              [
                                  'nick_name' => $user2->nick_name
                              ],
                              [
                                  'nick_name' => $user3->nick_name
                              ]
                          ],
                      ]
                  )
                ->assertJsonCount(2, 'data');

        $this->withHeaders(['X-Localization' => 'en'])
                ->json('POST', '/api/friends', ['filter' => 'cococobo'])
                ->assertOk()
                ->assertJson( // Here we assert the order.
                    [
                          'data' =>
                          [
                              [
                                  'nick_name' => $user3->nick_name
                              ]
                          ],
                      ]
                  )
                ->assertJsonCount(1, 'data'); //assert page of 30

        $this->withHeaders(['X-Localization' => 'en'])
                ->json('POST', '/api/friends', ['filter' => 'cocococobo'])
                ->assertOk()
                ->assertJson( // Here we assert the order.
                    [
                          'data' =>[]
                      ]
                  )
                ->assertJsonCount(0, 'data');
    }

    /** @test */
    public function a_user_can_get_empty_friends()
    {
        Passport::actingAs(factory('App\User')->create());

        $this->json('POST', '/api/friends')
                ->assertOk()
                ->assertJsonStructure(['data', 'links', 'meta'])
                ->assertJsonCount(0, 'data');
    }

    /** @test */
    public function a_user_can_unfriend()
    {
        $user = factory('App\User')->create();
        $userReceiver = factory('App\User')->create();
        $userRequester = factory('App\User')->create();
        Passport::actingAs($user);
        factory('App\Models\Friend')->create([
                                'requester' => $user->id,
                                'receiver' => $userReceiver->id,
                                'status' => 'confirmed']);

        factory('App\Models\Friend')->create([
                                'requester' => $userRequester->id,
                                'receiver' => $user->id,
                                'status' => 'confirmed']);

        $this->json('POST', '/api/friends/unfriend', ['user_id' => $userReceiver->id])
              ->assertStatus(200)
              ->assertJson(['message' => sprintf(__('friend.unfriend_success'), $userReceiver->nick_name)]);

        $this->json('POST', '/api/friends/unfriend', ['user_id' => $userRequester->id])
              ->assertStatus(200)
              ->assertJson(['message' => sprintf(__('friend.unfriend_success'), $userRequester->nick_name)]);

        $this->assertDatabaseMissing(
            'friend_list',
            [
                'requester' => $user->id,
                'receiver' => $userReceiver->id,
                'status' => 'confirmed'
            ]
        );
        $this->assertDatabaseMissing(
            'friend_list',
            [
                'requester' => $userRequester->id,
                'receiver' => $user->id,
                'status' => 'confirmed'
            ]
        );
    }

    /** @test */
    public function a_user_cannot_unfriend_without_user_id()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $this->json('POST', '/api/friends/unfriend')
              ->assertJsonValidationErrors('user_id')
              ->assertJsonStructure(['message']);
    }

    /** @test */
    public function a_user_cannot_unfriend_user_not_exist()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $this->json('POST', '/api/friends/unfriend', ['user_id' => 559])
              ->assertNotFound()
              ->assertJson(['message' => __('User not found')]);
    }

    /** @test */
    public function a_user_cannot_unfriend_a_user_who_is_not_his_friend()
    {
        $user = factory('App\User')->create();
        $user2 = factory('App\User')->create();
        Passport::actingAs($user);
        $this->json('POST', '/api/friends/unfriend', ['user_id' => $user2->id])
              ->assertNotFound()
              ->assertJson(['message' => __('friend.not_friend')]);
    }

    /** @test */
    public function a_user_can_get_requests()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        factory('App\User', 8)->create()->each(function ($newUser) use (&$user) {
            $fan = factory('App\Models\Friend')->create(
                [
                    'requester' => $newUser->id,
                    'receiver' => $user->id,
                    'status' => 'requesting'
                ]
            );
            factory('App\Models\UserImages')->create(['user_id' => $newUser->id]);
        });
        $this->json('POST', '/api/requests')
                ->assertOk()
                ->assertJsonStructure(
                    [
                          'data' =>
                          [
                              [
                                  'user_id',
                                  'nick_name',
                                  'gender',
                                  'age',
                                  'location',
                                  'photo',
                                  'background',
                                  'introduction',
                                  'game_list',
                                  'Fans',
                                  'isFb',
                                  'isGoogle',
                                  'Issubscriber',
                                  'IsFriend',
                                  'IsFan',
                                  'IsFollowing',
                                  'IsRequest',
                                  'game_name' //SearchUserResource
                              ]
                          ],
                          'links',
                          'meta'
                      ]
                  )
                ->assertJsonCount(8, 'data');
    }

    /** @test */
    public function a_user_can_get_max_30_requests_per_page()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        factory('App\User', 58)->create()->each(function ($newUser) use (&$user) {
            $fan = factory('App\Models\Friend')->create(
                [
                    'requester' => $newUser->id,
                    'receiver' => $user->id,
                    'status' => 'requesting'
                ]
            );
            factory('App\Models\UserImages')->create(['user_id' => $newUser->id]);
        });
        $this->json('POST', '/api/requests')
                ->assertOk()
                ->assertJsonStructure(
                    [
                          'data' =>
                          [
                              [
                                  'user_id',
                                  'nick_name',
                                  'gender',
                                  'age',
                                  'location',
                                  'photo',
                                  'background',
                                  'introduction',
                                  'game_list',
                                  'Fans',
                                  'isFb',
                                  'isGoogle',
                                  'Issubscriber',
                                  'IsFriend',
                                  'IsFan',
                                  'IsFollowing',
                                  'IsRequest',
                                  'game_name' //SearchUserResource
                              ]
                          ],
                          'links',
                          'meta'
                      ]
                  )
                ->assertJsonCount(30, 'data');
    }

    /** @test */
    public function a_user_can_sort_requests_name_asc()
    {
        $user = factory('App\User')->create();

        Passport::actingAs($user);
        $user1 = factory('App\User')->create(['nick_name' => 'aaron']);
        factory('App\Models\UserImages')->create(['user_id' => $user1->id]);
        factory('App\Models\Friend')->create(
            [
                'requester' => $user1->id,
                'receiver' => $user->id,
                'status' => 'requesting'
            ]
        );

        $user2 = factory('App\User')->create(['nick_name' => 'bryan']);
        factory('App\Models\UserImages')->create(['user_id' => $user2->id]);
        factory('App\Models\Friend')->create(
            [
                'requester' => $user2->id,
                'receiver' => $user->id,
                'status' => 'requesting'
            ]
        );

        $user3 = factory('App\User')->create(['nick_name' => 'charlie']);
        factory('App\Models\UserImages')->create(['user_id' => $user3->id]);
        factory('App\Models\Friend')->create(
            [
                'requester' => $user3->id,
                'receiver' => $user->id,
                'status' => 'requesting'
            ]
        );

        $this->withHeaders(['X-Localization' => 'en'])
                ->json('POST', '/api/requests', ['orderby' => 'NameAsc'])
                ->assertOk()
                ->assertJson( // Here we assert the order.
                    [
                          'data' =>
                          [
                              [
                                  'nick_name' => $user1->nick_name
                              ],
                              [
                                  'nick_name' => $user2->nick_name
                              ],
                              [
                                  'nick_name' => $user3->nick_name
                              ]
                          ],
                      ]
                  )
                ->assertJsonCount(3, 'data'); //assert page of 3
    }

    /** @test */
    public function a_user_can_sort_requests_name_desc()
    {
        $user = factory('App\User')->create();

        Passport::actingAs($user);
        $user1 = factory('App\User')->create(['nick_name' => 'aaron']);
        factory('App\Models\UserImages')->create(['user_id' => $user1->id]);
        factory('App\Models\Friend')->create(
            [
                'requester' => $user1->id,
                'receiver' => $user->id,
                'status' => 'requesting'
            ]
        );

        $user2 = factory('App\User')->create(['nick_name' => 'bryan']);
        factory('App\Models\UserImages')->create(['user_id' => $user2->id]);
        factory('App\Models\Friend')->create(
            [
                'requester' => $user2->id,
                'receiver' => $user->id,
                'status' => 'requesting'
            ]
        );

        $user3 = factory('App\User')->create(['nick_name' => 'charlie']);
        factory('App\Models\UserImages')->create(['user_id' => $user3->id]);
        factory('App\Models\Friend')->create(
            [
                'requester' => $user3->id,
                'receiver' => $user->id,
                'status' => 'requesting'
            ]
        );

        $this->withHeaders(['X-Localization' => 'en'])
                ->json('POST', '/api/requests', ['orderby' => 'NameDesc'])
                ->assertOk()
                ->assertJson( // Here we assert the order.
                    [
                          'data' =>
                          [
                              [
                                  'nick_name' => $user3->nick_name
                              ],
                              [
                                  'nick_name' => $user2->nick_name
                              ],
                              [
                                  'nick_name' => $user1->nick_name
                              ]
                          ],
                      ]
                  )
                ->assertJsonCount(3, 'data'); //assert page of 3
    }

    /** @test */
    public function a_user_can_filter_requests()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        factory('App\User', 40)->create()->each(function ($newUser) use (&$user) {
            $friend = factory('App\Models\Friend')->create(
                [
                        'requester' => $newUser->id,
                        'receiver' => $user->id,
                        'status' => 'confirmed'
                    ]
                );
            factory('App\Models\UserImages')->create(['user_id' => $newUser->id]);
        });

        $user1 = factory('App\User')->create(['nick_name' => 'coco']);
        factory('App\Models\UserImages')->create(['user_id' => $user1->id]);
        factory('App\Models\Friend')->create(
            [
                'requester' => $user1->id,
                'receiver' => $user->id,
                'status' => 'requesting'
            ]
        );

        $user2 = factory('App\User')->create(['nick_name' => 'cococo']);
        factory('App\Models\UserImages')->create(['user_id' => $user2->id]);
        factory('App\Models\Friend')->create(
            [
                'requester' => $user2->id,
                'receiver' => $user->id,
                'status' => 'requesting'
            ]
        );

        $user3 = factory('App\User')->create(['nick_name' => 'cococobo']);
        factory('App\Models\UserImages')->create(['user_id' => $user3->id]);
        factory('App\Models\Friend')->create(
            [
                'requester' => $user3->id,
                'receiver' => $user->id,
                'status' => 'requesting'
            ]
        );

        $this->withHeaders(['X-Localization' => 'en'])
                ->json('POST', '/api/requests', ['filter' => 'coco'])
                ->assertOk()
                ->assertJson( // Here we assert the order.
                    [
                          'data' =>
                          [
                              [
                                  'nick_name' => $user1->nick_name
                              ],
                              [
                                  'nick_name' => $user2->nick_name
                              ],
                              [
                                  'nick_name' => $user3->nick_name
                              ]
                          ],
                      ]
                  )
                ->assertJsonCount(3, 'data');

        $this->withHeaders(['X-Localization' => 'en'])
                ->json('POST', '/api/requests', ['filter' => 'cococo'])
                ->assertOk()
                ->assertJson( // Here we assert the order.
                    [
                          'data' =>
                          [
                              [
                                  'nick_name' => $user2->nick_name
                              ],
                              [
                                  'nick_name' => $user3->nick_name
                              ]
                          ],
                      ]
                  )
                ->assertJsonCount(2, 'data');

        $this->withHeaders(['X-Localization' => 'en'])
                ->json('POST', '/api/requests', ['filter' => 'cococobo'])
                ->assertOk()
                ->assertJson( // Here we assert the order.
                    [
                          'data' =>
                          [
                              [
                                  'nick_name' => $user3->nick_name
                              ]
                          ],
                      ]
                  )
                ->assertJsonCount(1, 'data'); //assert page of 30

        $this->withHeaders(['X-Localization' => 'en'])
                ->json('POST', '/api/requests', ['filter' => 'cocococobo'])
                ->assertOk()
                ->assertJson( // Here we assert the order.
                    [
                          'data' =>[]
                      ]
                  )
                ->assertJsonCount(0, 'data');
    }

    /** @test */
    public function a_user_can_get_empty_requests()
    {
        Passport::actingAs(factory('App\User')->create());

        $this->json('POST', '/api/requests')
                ->assertOk()
                ->assertJsonStructure(['data'])
                ->assertJsonCount(0, 'data');
    }

    /** @test */
    public function a_user_can_send_friend_request()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $otherUser = factory('App\User')->create();

        $this->json('POST', '/api/requests/send', ['user_id' => $otherUser->id])
              ->assertOk()
              ->assertJson(['message' => __('friend.request_success')]);

        $this->assertDatabaseHas(
            'friend_list',
            [
                'requester' => $user->id,
                'receiver' => $otherUser->id,
                'status' => 'requesting'
            ]
        );
    }

    /** @test */
    public function a_user_cannot_send_friend_request_without_valid_inputs()
    {
        Passport::actingAs(factory('App\User')->create());
        $this->json('POST', '/api/requests/send')
              ->assertJsonValidationErrors('user_id')
              ->assertJsonStructure(['message']);
    }

    /** @test */
    public function a_user_cannot_send_friend_request_user_not_found()
    {
        Passport::actingAs(factory('App\User')->create());
        $this->json('POST', '/api/requests/send', ['user_id' => 559])
              ->assertNotFound()
              ->assertJson(['message' => __('User not found')]);
    }

    /** @test */
    public function a_user_cannot_send_duplicate_request()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $otherUser = factory('App\User')->create();
        factory('App\Models\Friend')->create([
            'receiver' => $otherUser->id,
            'requester' => $user->id,
            'status' => 'requesting'
        ]);

        $this->json('POST', '/api/requests/send', ['user_id' => $otherUser->id])
              ->assertStatus(500)
              ->assertJson(['message' => sprintf(__('friend.duplicate_request'), $otherUser->nick_name)]);
    }

    /** @test */
    public function a_user_can_reject_request()
    {
        $this->withoutExceptionHandling();
        $user = factory('App\User')->create();
        $otherUser = factory('App\User')->create();
        Passport::actingAs($user);
        $request = factory('App\Models\Friend')->create([
            'receiver' => $user->id,
            'requester' => $otherUser->id,
            'status' => 'requesting'
        ]);
        // Add an additional request sent by other user to a 3rd user to make test more robust.
        factory('App\Models\Friend')->create([
            'receiver' => 559,
            'requester' => $otherUser->id,
            'status' => 'requesting'
        ]);
        // Verify record is there before rejection
        $this->assertDatabaseHas(
            'friend_list',
            [
                'requester' => $otherUser->id,
                'receiver' => $user->id,
                'status' => 'requesting'
            ]
        );

        $this->json('POST', '/api/requests/reject', ['user_id' => $otherUser->id])
              ->assertOk()
              ->assertJson(['message' => sprintf(__('friend.reject_request'), $otherUser->nick_name)]);

        $this->assertDatabaseMissing(
            'friend_list',
            [
                'requester' => $otherUser->id,
                'receiver' => $user->id,
                'status' => 'requesting'
            ]
        );
    }

    /** @test */
    public function a_user_cannot_reject_request_without_valid_inputs()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $this->json('POST', '/api/requests/reject')
              ->assertJsonValidationErrors('user_id')
              ->assertJsonStructure(['message']);
    }

    /** @test */
    public function a_user_cannot_reject_request_not_found()
    {
        $user = factory('App\User')->create();
        $otherUser = factory('App\User')->create();
        Passport::actingAs($user);
        $this->withHeaders(['X-Localization' => 'en'])
              ->json('POST', '/api/requests/reject', ['user_id' => $otherUser->id])
              ->assertNotFound()
              ->assertJson(['message' => __('friend.request_not_found')]);
    }

    /** @test */
    public function a_user_cannot_reject_request_requester_not_found()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $request = factory('App\Models\Friend')->create([
            'receiver' => $user->id,
            'requester' => 321,
            'status' => 'requesting'
        ]);
        $this->withHeaders(['X-Localization' => 'en'])
              ->json('POST', '/api/requests/reject', ['user_id' => 321])
              ->assertNotFound()
              ->assertJson(['message' => __('User not found')]);
    }

    /** @test */
    public function a_user_cannot_reject_request_already_friend()
    {
        $user = factory('App\User')->create();
        $otherUser = factory('App\User')->create();
        Passport::actingAs($user);
        $request = factory('App\Models\Friend')->create([
            'receiver' => $user->id,
            'requester' => $otherUser->id,
            'status' => 'confirmed'
        ]);
        $this->withHeaders(['X-Localization' => 'en'])
              ->json('POST', '/api/requests/reject', ['user_id' => $otherUser->id])
              ->assertStatus(500)
              ->assertJson(['message' => sprintf(__('friend.reject_already_friend'), $otherUser->nick_name)]);
    }

    /** @test */
    public function a_user_can_accept_request()
    {
        $this->withoutExceptionHandling();
        $user = factory('App\User')->create();
        $otherUser = factory('App\User')->create();
        Passport::actingAs($user);
        $request = factory('App\Models\Friend')->create([
            'receiver' => $user->id,
            'requester' => $otherUser->id,
            'status' => 'requesting'
        ]);
        // Add an additional request sent by other user to a 3rd user to make test more robust.
        factory('App\Models\Friend')->create([
            'receiver' => 559,
            'requester' => $otherUser->id,
            'status' => 'requesting'
        ]);
        $this->json('POST', '/api/requests/accept', ['user_id' => $otherUser->id])
              ->assertOk()
              ->assertJson(['message' => sprintf(__('friend.accept_request'), $otherUser->nick_name)]);

        $this->assertDatabaseMissing(
            'friend_list',
            [
                'receiver' => $user->id,
                'requester' => $otherUser->id,
                'status' => 'requesting'
            ]
        );

        $this->assertDatabaseHas(
            'friend_list',
            [
                'receiver' => $user->id,
                'requester' => $otherUser->id,
                'status' => 'confirmed'
            ]
        );
    }

    /** @test */
    public function a_user_cannot_accept_request_without_valid_inputs()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $this->withHeaders(['X-Localization' => 'en'])
              ->json('POST', '/api/requests/accept')
              ->assertJsonValidationErrors('user_id')
              ->assertJsonStructure(['message']);
    }

    /** @test */
    public function a_user_cannot_accept_request_not_found()
    {
        $user = factory('App\User')->create();
        $otherUser = factory('App\User')->create();
        Passport::actingAs($user);
        $this->withHeaders(['X-Localization' => 'en'])
              ->json('POST', '/api/requests/accept', ['user_id' => $otherUser->id])
              ->assertNotFound()
              ->assertJson(['message' => __('friend.request_not_found')]);
    }

    /** @test */
    public function a_user_cannot_accept_request_requester_not_found()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $request = factory('App\Models\Friend')->create([
            'receiver' => $user->id,
            'requester' => 123,
            'status' => 'requesting'
        ]);
        $this->withHeaders(['X-Localization' => 'en'])
              ->json('POST', '/api/requests/accept', ['user_id' => 123])
              ->assertNotFound()
              ->assertJson(['message' => __('User not found')]);
    }

    /** @test */
    public function a_user_cannot_accept_request_already_friend()
    {
        $user = factory('App\User')->create();
        $otherUser = factory('App\User')->create();
        Passport::actingAs($user);
        $request = factory('App\Models\Friend')->create([
            'receiver' => $user->id,
            'requester' => $otherUser->id,
            'status' => 'confirmed'
        ]);
        $this->withHeaders(['X-Localization' => 'en'])
              ->json('POST', '/api/requests/accept', ['user_id' => $otherUser->id])
              ->assertStatus(500)
              ->assertJson(['message' => sprintf(__('friend.accept_already_friend'), $otherUser->nick_name)]);
    }

    /** @test */
    public function a_user_can_quick_search_friends()
    {
        $user = factory('App\User')->create();

        Passport::actingAs($user);
        $user1 = factory('App\User')->create(['nick_name' => 'aaron']);
        factory('App\Models\UserImages')->create(['user_id' => $user1->id]);
        factory('App\Models\Friend')->create(
            [
                'requester' => $user1->id,
                'receiver' => $user->id,
                'status' => 'confirmed'
            ]
        );

        $user2 = factory('App\User')->create(['nick_name' => 'bryan']);
        factory('App\Models\UserImages')->create(['user_id' => $user2->id]);
        factory('App\Models\Friend')->create(
            [
                'requester' => $user2->id,
                'receiver' => $user->id,
                'status' => 'confirmed'
            ]
        );

        $user3 = factory('App\User')->create(['nick_name' => 'cocobo']);
        factory('App\Models\UserImages')->create(['user_id' => $user3->id]);
        factory('App\Models\Friend')->create(
            [
                'requester' => $user3->id,
                'receiver' => $user->id,
                'status' => 'confirmed'
            ]
        );

        $user4 = factory('App\User')->create(['nick_name' => 'cocobox']);
        factory('App\Models\UserImages')->create(['user_id' => $user4->id]);
        factory('App\Models\Friend')->create(
            [
                'requester' => $user4->id,
                'receiver' => $user->id,
                'status' => 'confirmed'
            ]
        );

        $this->withHeaders(['X-Localization' => 'en'])
                ->json('POST', '/api/friends/quicksearch', ['keyword' => 'coco'])
                ->assertOk()
                ->assertJsonStructure(
                    [
                          'data' =>
                          [
                              [
                                  'user_id',
                                  'nick_name',
                                  'gender',
                                  'age',
                                  'location',
                                  'photo',
                                  'background',
                                  'introduction',
                                  'game_list',
                                  'Fans',
                                  'isFb',
                                  'isGoogle',
                                  'Issubscriber',
                                  'IsFriend',
                                  'IsFan',
                                  'IsFollowing',
                                  'IsRequest',
                                  'game_name' //SearchUserResource
                              ]
                          ],
                          'links',
                          'meta'
                      ]
                  )
                ->assertJson( // Here we assert the order.
                    [
                          'data' =>
                          [
                              [
                                  'nick_name' => $user3->nick_name
                              ],
                              [
                                  'nick_name' => $user4->nick_name
                              ]
                          ],
                      ]
                  )
                ->assertJsonCount(2, 'data'); //assert page of 3

        $this->withHeaders(['X-Localization' => 'en'])
                ->json('POST', '/api/friends/quicksearch', ['keyword' => 'cocobox'])
                ->assertOk()
                ->assertJson( // Here we assert the order.
                    [
                          'data' =>
                          [
                              [
                                  'nick_name' => $user4->nick_name
                              ]
                          ],
                      ]
                  )
                ->assertJsonCount(1, 'data');
    }

    /** @test */
    public function a_user_cannot_quick_search_friends_no_result()
    {
        $user = factory('App\User')->create();

        Passport::actingAs($user);

        // Test on empty database
        $this->withHeaders(['X-Localization' => 'en'])
                ->json('POST', '/api/friends/quicksearch', ['keyword' => 'cocobox'])
                ->assertNotFound()
                ->assertJson(['message' => __('User not found')]);

        $user1 = factory('App\User')->create(['nick_name' => 'aaron']);
        factory('App\Models\UserImages')->create(['user_id' => $user1->id]);
        factory('App\Models\Friend')->create(
            [
                'requester' => $user1->id,
                'receiver' => $user->id,
                'status' => 'confirmed'
            ]
        );
        $this->withHeaders(['X-Localization' => 'en'])
                ->json('POST', '/api/friends/quicksearch', ['keyword' => 'cocobox'])
                ->assertNotFound()
                ->assertJson(['message' => __('User not found')]);
    }
}
