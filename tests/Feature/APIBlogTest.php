<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Passport\Passport;
use App\Models\UserBlog;

class APIBlogTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    /** @test */
    public function a_guest_cannot_get_user_blogs()
    {
        // If user is not logged in, will redirect to login.
        $this->postJson('api/blog')->assertStatus(401);
    }

    /** @test */
    public function a_guest_cannot_submit_new_blog()
    {
        // If user is not logged in, will redirect to login.
        $this->postJson('api/blog/new')->assertStatus(401);
    }

    /** @test */
    public function a_guest_cannot_delete_blog()
    {
        // If user is not logged in, will redirect to login.
        $this->postJson('api/blog/delete')->assertStatus(401);
    }

    /** @test */
    public function a_guest_cannot_update_blog()
    {
        // If user is not logged in, will redirect to login.
        $this->postJson('api/blog/update')->assertStatus(401);
    }

    /** @test */
    public function a_user_can_get_user_blogs()
    {
        $user = factory('App\User')->create();
        $blog = factory('App\Models\UserBlog', 10)->create(
            [
                  'user_id' => $user->id,
              ]
        );
        Passport::actingAs($user);
        $this->postJson('api/blog', ['id' => $user->id])
              ->assertOk()
              ->assertJsonStructure(
                  [
                      'data' =>
                      [
                          [
                              'blog_id',
                              'user_id',
                              'event_id',
                              'content',
                              'preview',
                              'liked_count_amount',
                              'comment_count_amount',
                              'isLiked',
                              'timestamp'
                          ]
                      ],
                      'links',
                      'meta',
              ]
              );
    }

    /** @test */
    public function a_user_can_get_empty_user_blogs()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $this->postJson('api/blog', ['id' => $user->id])
              ->assertOk()
              ->assertJsonStructure(
                  [
                      'data' =>
                      [],
                      'links',
                      'meta',
                  ]
              );
    }

    /** @test */
    public function a_user_cannot_get_user_blogs_without_validation()
    {
        Passport::actingAs(factory('App\User')->create());
        $this->postJson('api/blog')
                ->assertJsonValidationErrors('id')
                ->assertJsonStructure(['message']);
    }

    /** @test */
    public function a_user_cannot_get_invalid_users_blog()
    {
        Passport::actingAs(factory('App\User')->create());
        $this->post('/blog', ['id' => 559])
              ->assertNotFound()
              ->assertJson(['message' => __('User not found')]);
    }

    /** @test */
    public function a_user_can_submit_new_blog()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $content = 'this is a new blog';
        $this->postJson('api/blog/new', ['blog_content' => $content])
                ->assertOk()
                ->assertJson(['message' => __('blog.new_blog_success')]);
        $this->assertDatabaseHas('user_blog', ['content' => $content, 'user_id' => $user->id]);
        $blog = UserBlog::first();
        $this->assertTrue($blog->preview == null);
    }

    /** @test */
    public function a_user_can_submit_new_blog_with_image()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $content = 'this is a new blog';
        /** 20x20 test image encoded in base 64 string. **/
        $image = '/9j/4AAQSkZJRgABAQEASABIAAD/4QDSRXhpZgAASUkqAAgAAAAFABIBAwABAAAAAQAAADEBAgALAAAASgAAADIBAgAUAAAAVgAAABMCAwABAAAAAQAAAGmHBAABAAAAagAAAAAAAABHSU1QIDIuNC41AAAyMDA5OjAxOjI3IDE5OjI1OjE2AAUAAJAHAAQAAAAwMjIwkJICAAQAAAA2ODMAAqAEAAEAAAAUAAAAA6AEAAEAAAAUAAAABaAEAAEAAACsAAAAAAAAAAIAAQACAAQAAABSOTgAAgAHAAQAAAAwMTAwAAAAAP/bAEMAAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAf/bAEMBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAf/AABEIABQAFAMBIQACEQEDEQH/xAAaAAACAgMAAAAAAAAAAAAAAAAACgIHBggJ/8QAIxAAAQUBAAEEAwEAAAAAAAAABAECAwUGBwAICRESExQhMf/EABgBAAMBAQAAAAAAAAAAAAAAAAEEBgUD/8QAJBEAAgICAQMEAwAAAAAAAAAAAQIDEQQSACExQQUUIlFigZH/2gAMAwEAAhEDEQA/AEweDcgz91i9Z1/d29XDQZQPUhYrElR2BB/T+g0WPstbHSOZXPHfX5DPACx3u+uyD61gtMotUEWy4vqxWsO9D9vb2xe6+mvjfIPTf3/Tk+4Ja4rWXho+15kNzvFdO6dlqMrQaHmuQWjoc5lc2WYNT2sHNqG1sipr2rGY0u6udiUELYynrHqubDkwe0x8iRYshgzIUMDwYsaPliVDKjKJg00MMqxzOmTiqugjmZ+aGLjwOHE0saExWgYsHLuaj01VgxDBCyMUUo7EtajioOio7PNXttn7oWQK2pjya6wElRWyQFiyOimjei/CoqOav+p/U+F8PKlGV1V1Nq6qyn7VgCD+weZ5FEg9x0PN2OLYK37pyXF47leyhqO0cp6Bu9OPkDyGgl6fL7qpxIkR2G/QHmKurymssc8S+z87JrS2r78GWqFMAqbdsN1Osb7lnQsBqNvBX5yk9NfRMPvbYepWCjJPJgwmM3efwmbq2foE2DUvzyqBk54JdlUZK2ppTTIwwyHnTs0qNJk4LKy5Mfu2UDbWVcyYjFbuSyB8sq7FdUdJKNKG4yAdopBZjOgJ6CjGqbjwAfjYvuCCT55ywsSGmWBxbPt9CSyZ2I5rGu+ksz3tRzY2sjaqNcny1jGsRf41qIiJ4eUaigB9AD+Di3Mz5fbEUe6oLYRrXk15MpQ7JJjYYvzxCkLE+RQCwp3sY5fs6FZ/wEN+0BUU4sk0EkujdA0XQL0mwvpo1c008pB4JDpYnHnPhSwsiCLM2xsTbA5BRIpijjiZIgQa2pDUWnqquvD4GJDkiYj5rFqDS3RY2Ntd6/HbW+pUkKQbNa30u6s9+njt4HjlfeHjHBz/2Q==';
        $this->postJson('api/blog/new', ['blog_content' => $content, 'blog_image' => $image])
                ->assertOk()
                ->assertJson(['message' => __('blog.new_blog_success')]);
        $this->assertDatabaseHas('user_blog', ['content' => $content, 'user_id' => $user->id]);
        $this->assertArrayHasKey('image', json_decode(UserBlog::first()->preview, true));
    }

    /** @test */
    public function a_user_can_submit_new_blog_with_url()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $content = 'this is a new blog';
        $mockUrlPreview = $this->faker->text(20);
        $this->postJson('api/blog/new', ['blog_content' => $content, 'blog_url_preview_json' => $mockUrlPreview])
                ->assertOk()
                ->assertJson(['message' => __('blog.new_blog_success')]);
        $this->assertDatabaseHas(
            'user_blog',
            [
                'content' => $content,
                'user_id' => $user->id,
                'preview' => $mockUrlPreview
            ]
        );
    }

    /** @test */
    public function a_user_can_submit_new_blog_with_event()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $content = 'this is a new blog';
        $this->postJson('api/blog/new', ['blog_content' => $content, 'blog_event' => 10])
                ->assertOk()
                ->assertJson(['message' => __('blog.new_blog_success')]);
        $this->assertDatabaseHas(
            'user_blog',
            [
                'content' => $content,
                'user_id' => $user->id,
                'event_id' => 10
            ]
        );
    }

    /** @test */
    public function a_user_cannot_submit_new_blog_with_invalid_image()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $content = 'this is a new blog';
        $image = $this->faker->text(100);
        $this->withHeaders(['X-Localization' => 'en'])->postJson('api/blog/new', ['blog_content' => $content, 'blog_image' => $image])
                ->assertStatus(500)
                ->assertJson(['message' => __('images.content_incorrect')]);
        $this->assertDatabaseMissing('user_blog', ['content' => $content, 'user_id' => $user->id]);
    }

    /** @test */
    public function a_user_cannot_submit_new_blog_without_valid_inputs()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $this->postJson('api/blog/new')
                ->assertJsonValidationErrors('blog_content')
                ->assertJsonStructure(['message']);
        $this->assertDatabaseMissing('user_blog', ['user_id' => $user->id]);
    }

    /** @test */
    public function a_user_can_delete_own_blog()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $content = 'this should not be seen';
        $blogToDelete = factory('App\Models\UserBlog')->create(
            [
                  'user_id' => $user->id,
                  'content' => $content
              ]
        );
        $this->postJson('api/blog/delete', ['blog_id' =>$blogToDelete->blog_id])
              ->assertOk()
              ->assertJson(['message' => __('blog.blog_removed')]);
        $this->assertDatabaseMissing('user_blog', ['content' => $content, 'user_id' => $user->id]);
    }

    /** @test */
    public function a_user_can_delete_own_blog_and_image_on_gcs()
    {
        // Need to manually verify that the image is deleted on GCS!!
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $content = 'this is a new blog';
        /** 20x20 test image encoded in base 64 string. **/
        $image = '/9j/4AAQSkZJRgABAQEASABIAAD/4QDSRXhpZgAASUkqAAgAAAAFABIBAwABAAAAAQAAADEBAgALAAAASgAAADIBAgAUAAAAVgAAABMCAwABAAAAAQAAAGmHBAABAAAAagAAAAAAAABHSU1QIDIuNC41AAAyMDA5OjAxOjI3IDE5OjI1OjE2AAUAAJAHAAQAAAAwMjIwkJICAAQAAAA2ODMAAqAEAAEAAAAUAAAAA6AEAAEAAAAUAAAABaAEAAEAAACsAAAAAAAAAAIAAQACAAQAAABSOTgAAgAHAAQAAAAwMTAwAAAAAP/bAEMAAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAf/bAEMBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAf/AABEIABQAFAMBIQACEQEDEQH/xAAaAAACAgMAAAAAAAAAAAAAAAAACgIHBggJ/8QAIxAAAQUBAAEEAwEAAAAAAAAABAECAwUGBwAICRESExQhMf/EABgBAAMBAQAAAAAAAAAAAAAAAAEEBgUD/8QAJBEAAgICAQMEAwAAAAAAAAAAAQIDEQQSACExQQUUIlFigZH/2gAMAwEAAhEDEQA/AEweDcgz91i9Z1/d29XDQZQPUhYrElR2BB/T+g0WPstbHSOZXPHfX5DPACx3u+uyD61gtMotUEWy4vqxWsO9D9vb2xe6+mvjfIPTf3/Tk+4Ja4rWXho+15kNzvFdO6dlqMrQaHmuQWjoc5lc2WYNT2sHNqG1sipr2rGY0u6udiUELYynrHqubDkwe0x8iRYshgzIUMDwYsaPliVDKjKJg00MMqxzOmTiqugjmZ+aGLjwOHE0saExWgYsHLuaj01VgxDBCyMUUo7EtajioOio7PNXttn7oWQK2pjya6wElRWyQFiyOimjei/CoqOav+p/U+F8PKlGV1V1Nq6qyn7VgCD+weZ5FEg9x0PN2OLYK37pyXF47leyhqO0cp6Bu9OPkDyGgl6fL7qpxIkR2G/QHmKurymssc8S+z87JrS2r78GWqFMAqbdsN1Osb7lnQsBqNvBX5yk9NfRMPvbYepWCjJPJgwmM3efwmbq2foE2DUvzyqBk54JdlUZK2ppTTIwwyHnTs0qNJk4LKy5Mfu2UDbWVcyYjFbuSyB8sq7FdUdJKNKG4yAdopBZjOgJ6CjGqbjwAfjYvuCCT55ywsSGmWBxbPt9CSyZ2I5rGu+ksz3tRzY2sjaqNcny1jGsRf41qIiJ4eUaigB9AD+Di3Mz5fbEUe6oLYRrXk15MpQ7JJjYYvzxCkLE+RQCwp3sY5fs6FZ/wEN+0BUU4sk0EkujdA0XQL0mwvpo1c008pB4JDpYnHnPhSwsiCLM2xsTbA5BRIpijjiZIgQa2pDUWnqquvD4GJDkiYj5rFqDS3RY2Ntd6/HbW+pUkKQbNa30u6s9+njt4HjlfeHjHBz/2Q==';
        $this->postJson('api/blog/new', ['blog_content' => $content, 'blog_image' => $image])
                ->assertOk()
                ->assertJson(['message' => __('blog.new_blog_success')]);

        $blog = UserBlog::first();
        $gcsUrl = json_decode(UserBlog::first()->preview, true)['image'];
        $this->postJson('api/blog/delete', ['blog_id' => $blog->blog_id])
              ->assertOk()
              ->assertJson(['message' => __('blog.blog_removed')]);
    }

    /** @test */
    public function a_user_cannot_delete_blog_without_valid_inputs()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $content = 'this should be seen';
        $blogToDelete = factory('App\Models\UserBlog')->create(
            [
                  'user_id' => $user->id,
                  'content' => $content
              ]
        );
        $this->postJson('api/blog/delete')
              ->assertJsonValidationErrors('blog_id')
              ->assertJsonStructure(['message']);

        $this->assertDatabaseHas('user_blog', ['content' => $content, 'user_id' => $user->id]);
    }

    /** @test */
    public function a_user_cannot_delete_blog_not_found()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $content = 'this should be seen';
        $blogToDelete = factory('App\Models\UserBlog')->create(
            [
                  'user_id' => $user->id,
                  'content' => $content
              ]
        );
        $this->postJson('api/blog/delete', ['blog_id' => 559])
              ->assertNotFound()
              ->assertJson(['message' => __('blog.blog_not_found')]);
        $this->assertDatabaseHas('user_blog', ['content' => $content, 'user_id' => $user->id]);
    }

    /** @test */
    public function a_user_cannot_delete_someone_else_blog()
    {
        $userBlogOwner = factory('App\User')->create();
        $userStranger = factory('App\User')->create();
        Passport::actingAs($userStranger);
        $content = 'this should not be deleted!';
        $blogToDelete = factory('App\Models\UserBlog')->create(
            [
                  'user_id' => $userBlogOwner->id,
                  'content' => $content
              ]
        );

        $this->postJson('api/blog/delete', ['blog_id' =>$blogToDelete->blog_id])
              ->assertStatus(403)
              ->assertJson(['message' => __('blog.blog_not_owner')]);
        $this->assertDatabaseHas('user_blog', ['content' => $content,'user_id' => $userBlogOwner->id]);
    }

    /** @test */
    public function a_user_can_update_blog_content()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $content = 'this is original blog content';
        $newContent = 'this is the updated content';
        $blogToUpdate = factory('App\Models\UserBlog')->create(
            [
                  'user_id' => $user->id,
                  'content' => $content
              ]
        );
        $this->postJson(
            'api/blog/update',
            [
                'blog_id' =>$blogToUpdate->blog_id,
                'blog_content' => $newContent
            ]
        )->assertOk()
        ->assertJson(['message' => __('blog.blog_updated')]);

        $this->assertDatabaseMissing('user_blog', ['content' => $content,'user_id' => $user->id]);
        $this->assertDatabaseHas('user_blog', ['content' => $newContent,'user_id' => $user->id]);
    }

    /** @test */
    public function a_user_can_update_blog_image()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $content = 'this is original blog content';
        $image = '/9j/4AAQSkZJRgABAQEASABIAAD/4QDSRXhpZgAASUkqAAgAAAAFABIBAwABAAAAAQAAADEBAgALAAAASgAAADIBAgAUAAAAVgAAABMCAwABAAAAAQAAAGmHBAABAAAAagAAAAAAAABHSU1QIDIuNC41AAAyMDA5OjAxOjI3IDE5OjI1OjE2AAUAAJAHAAQAAAAwMjIwkJICAAQAAAA2ODMAAqAEAAEAAAAUAAAAA6AEAAEAAAAUAAAABaAEAAEAAACsAAAAAAAAAAIAAQACAAQAAABSOTgAAgAHAAQAAAAwMTAwAAAAAP/bAEMAAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAf/bAEMBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAf/AABEIABQAFAMBIQACEQEDEQH/xAAaAAACAgMAAAAAAAAAAAAAAAAACgIHBggJ/8QAIxAAAQUBAAEEAwEAAAAAAAAABAECAwUGBwAICRESExQhMf/EABgBAAMBAQAAAAAAAAAAAAAAAAEEBgUD/8QAJBEAAgICAQMEAwAAAAAAAAAAAQIDEQQSACExQQUUIlFigZH/2gAMAwEAAhEDEQA/AEweDcgz91i9Z1/d29XDQZQPUhYrElR2BB/T+g0WPstbHSOZXPHfX5DPACx3u+uyD61gtMotUEWy4vqxWsO9D9vb2xe6+mvjfIPTf3/Tk+4Ja4rWXho+15kNzvFdO6dlqMrQaHmuQWjoc5lc2WYNT2sHNqG1sipr2rGY0u6udiUELYynrHqubDkwe0x8iRYshgzIUMDwYsaPliVDKjKJg00MMqxzOmTiqugjmZ+aGLjwOHE0saExWgYsHLuaj01VgxDBCyMUUo7EtajioOio7PNXttn7oWQK2pjya6wElRWyQFiyOimjei/CoqOav+p/U+F8PKlGV1V1Nq6qyn7VgCD+weZ5FEg9x0PN2OLYK37pyXF47leyhqO0cp6Bu9OPkDyGgl6fL7qpxIkR2G/QHmKurymssc8S+z87JrS2r78GWqFMAqbdsN1Osb7lnQsBqNvBX5yk9NfRMPvbYepWCjJPJgwmM3efwmbq2foE2DUvzyqBk54JdlUZK2ppTTIwwyHnTs0qNJk4LKy5Mfu2UDbWVcyYjFbuSyB8sq7FdUdJKNKG4yAdopBZjOgJ6CjGqbjwAfjYvuCCT55ywsSGmWBxbPt9CSyZ2I5rGu+ksz3tRzY2sjaqNcny1jGsRf41qIiJ4eUaigB9AD+Di3Mz5fbEUe6oLYRrXk15MpQ7JJjYYvzxCkLE+RQCwp3sY5fs6FZ/wEN+0BUU4sk0EkujdA0XQL0mwvpo1c008pB4JDpYnHnPhSwsiCLM2xsTbA5BRIpijjiZIgQa2pDUWnqquvD4GJDkiYj5rFqDS3RY2Ntd6/HbW+pUkKQbNa30u6s9+njt4HjlfeHjHBz/2Q==';
        $blogToUpdate = factory('App\Models\UserBlog')->create(
            [
                  'user_id' => $user->id,
                  'content' => $content
              ]
        );
        $this->postJson(
            'api/blog/update',
            [
                'blog_id' =>$blogToUpdate->blog_id,
                'blog_content' => $content,
                'blog_image' => $image
            ]
        )->assertOk()
        ->assertJson(['message' => __('blog.blog_updated')]);
        $this->assertArrayHasKey('image', json_decode(UserBlog::first()->preview, true));
    }

    /** @test */
    public function a_user_can_update_blog_preview()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $content = 'this is original blog content';
        $newContent = 'this is updated blog content';
        $mockUrlPreview = $this->faker->text(20);
        $blogToUpdate = factory('App\Models\UserBlog')->create(
            [
                  'user_id' => $user->id,
                  'content' => $content,
                  'preview' => $this->faker->text(20)
              ]
        );
        $this->postJson(
            'api/blog/update',
            [
                'blog_id' =>$blogToUpdate->blog_id,
                'blog_content' => $newContent,
                'blog_url_preview_json' => $mockUrlPreview
            ]
        )->assertOk()
        ->assertJson(['message' => __('blog.blog_updated')]);
        $this->assertDatabaseHas(
            'user_blog',
            [
                'content' => $newContent,
                'user_id' => $user->id,
                'preview' => $mockUrlPreview
            ]
        );
    }

    /** @test */
    public function a_user_cannot_update_blog_without_valid_inputs()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $content = 'this is original blog content';
        $newContent = 'this is the updated content';
        $blogToUpdate = factory('App\Models\UserBlog')->create(
            [
                  'user_id' => $user->id,
                  'content' => $content
              ]
        );
        $this->postJson(
            'api/blog/update',
            [
                'blog_content' => $newContent
            ]
        )->assertJsonValidationErrors('blog_id')
        ->assertJsonStructure(['message']);

        $this->postJson(
            'api/blog/update',
            [
                'blog_id' => $blogToUpdate->blog_id
            ]
        )->assertJsonValidationErrors('blog_content')
        ->assertJsonStructure(['message']);
    }

    /** @test */
    public function a_user_cannot_update_with_invalid_image()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $content = 'this is a new blog';
        $image = $this->faker->text(100);
        $this->withHeaders(['X-Localization' => 'en'])->postJson('api/blog/new', ['blog_content' => $content, 'blog_image' => $image])
                ->assertStatus(500)
                ->assertJson(['message' => __('images.content_incorrect')]);
        $this->assertDatabaseMissing('user_blog', ['content' => $content, 'user_id' => $user->id]);
    }

    /** @test **/
    public function a_user_cannot_update_blog_not_exist()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $newContent = 'this is the updated content';
        $this->postJson(
            'api/blog/update',
            [
                'blog_id' => 559,
                'blog_content' => $newContent
            ]
        )->assertNotFound()
        ->assertJson(['message' => __('blog.blog_not_found')]);

        $this->assertDataBaseMissing('user_blog', ['content' => $newContent]);
    }

    /** @test **/
    public function a_user_cannot_update_blog_not_owner()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $content = 'this is original blog content';
        $newContent = 'this is the updated content';
        $blogToUpdate = factory('App\Models\UserBlog')->create(
            [
                  'user_id' => 559,
                  'content' => $content
              ]
        );
        $this->postJson(
            'api/blog/update',
            [
                'blog_id' => $blogToUpdate->blog_id,
                'blog_content' => $newContent
            ]
        )->assertStatus(403)
        ->assertJson(['message' => __('blog.blog_not_owner')]);

        $this->assertDataBaseMissing('user_blog', ['content' => $newContent]);
    }
}
