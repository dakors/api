<?php

namespace Tests\Feature;

use Tests\TestCase;
use Laravel\Passport\Passport;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class APIHomeTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_guest_cannot_view_home()
    {
        // Request without valid token will return 401.
        $this->json('POST', '/api/home')->assertStatus(401);
    }

    /** @test */
    public function a_user_can_view_home()
    {
        $user = factory('App\User')->create(
            [
                'gender' => '1',
                'location' => 11
            ]
        );
        $otherUser = factory('App\User')->create(
            [
                'gender' => '2',
                'location' => 15
            ]
        );
        // Create a blog which belongs to this owner.
        $blog = factory('App\Models\UserBlog')->create(
            [
                  'user_id' => $user->id,
              ]
        );
        // Create a blog which belongs to other user.
        $blog2 = factory('App\Models\UserBlog')->create(
            [
                  'user_id' => $otherUser->id,
              ]
        );
        // Make user follow otherUser so that otherUser's post is in user's homepage.
        factory('App\Models\Fan')->create(['user_id' => $otherUser->id, 'fans_id' => $user->id]);
        // Login as user
        Passport::actingAs($user);
        factory('App\Models\UserImages')->create(
            [
                  'user_id' => $user->id
              ]
        );
        factory('App\Models\UserImages')->create(
            [
                  'user_id' => $otherUser->id
              ]
        );
        // Assert HTTP 200
        $this->json('POST', '/api/home')
        ->assertOk()
        ->assertJsonCount(2, 'data')
        ->assertJsonStructure(
            [
                // Assert there is a data element, but the next assertion will verify the data itself.
                'data' =>[],
                'links',
                'meta'
            ]
        )
        ->assertJson(
            [
                'data' =>
                [
                    [
                        'blog_id' => $blog->blog_id,
                        'user_id' => $blog->user_id,
                        'event_id' => $blog->event_id,
                        'content' => $blog->content,
                        'preview' => $blog->preview,
                        'liked_count_amount' => $blog->likes->count(),
                        'comment_count_amount' => $blog->comments->count(),
                        'isLiked' => $blog->isLiked(),
                        'nick_name' => $user->nick_name,
                        'photo' => $user->getAvatar(),
                        'gender' => $user->gender,
                        'loction' => $user->location,
                        'Issubscriber' => $user->isSub(),
                        'timestamp' => \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $blog->modify_date)->timestamp
                    ],
                    [
                        'blog_id' => $blog2->blog_id,
                        'user_id' => $blog2->user_id,
                        'event_id' => $blog2->event_id,
                        'content' => $blog2->content,
                        'preview' => $blog2->preview,
                        'liked_count_amount' => $blog2->likes->count(),
                        'comment_count_amount' => $blog2->comments->count(),
                        'isLiked' => $blog2->isLiked(),
                        'nick_name' => $otherUser->nick_name,
                        'photo' => $otherUser->getAvatar(),
                        'gender' => $otherUser->gender,
                        'loction' => $otherUser->location,
                        'Issubscriber' => $otherUser->isSub(),
                        'timestamp' => \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $blog2->modify_date)->timestamp
                    ]
                ]
            ]
        );
    }

    /** @test */
    public function a_user_can_view_home_with_attached_event()
    {
        $user = factory('App\User')->create(
            [
                'gender' => '1',
                'location' => 11
            ]
        );
        Passport::actingAs($user);
        $event = factory('App\Models\UserEvent')->create(
            [
                'user_id' => $user->id
            ]
          );
        // Create a blog which belongs to this owner.
        $blog = factory('App\Models\UserBlog')->create(
            [
                  'user_id' => $user->id,
                  'event_id' => $event->event_id
              ]
        );
        $this->json('POST', '/api/home')
        ->assertOk()
        ->assertJsonCount(1, 'data')
        ->assertJsonStructure(
            [
                // Assert there is a data element, but the next assertion will verify the data itself.
                'data' =>[],
                'links',
                'meta'
            ]
        )
        ->assertJson(
            [
                'data' =>
                [
                    [
                        'blog_id' => $blog->blog_id,
                        'user_id' => $blog->user_id,
                        'event_id' => $blog->event_id,
                        'content' => $blog->content,
                        'preview' => [
                            'url' => "",
                            'image' => $event->pic1,
                            'location' => $event->location,
                            'event_name' => $event->event_name,
                            'start_time' => $event->start_time,
                            'end_time' => $event->end_time,
                            'event_hoster' => $event->user->nick_name
                        ],
                        'liked_count_amount' => $blog->likes->count(),
                        'comment_count_amount' => $blog->comments->count(),
                        'isLiked' => $blog->isLiked(),
                        'nick_name' => $user->nick_name,
                        'photo' => $user->getAvatar(),
                        'gender' => $user->gender,
                        'loction' => $user->location,
                        'Issubscriber' => $user->isSub(),
                        'timestamp' => \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $blog->modify_date)->timestamp
                    ]
                ]
            ]
        );
    }

    /** @test */
    public function a_user_can_view_home_when_blog_is_empty()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $this->postJson('api/home')
              ->assertOk()
              ->assertJsonStructure(
                  [
                      'data' =>
                      [],
                      'links',
                      'meta',
                  ]
              );
    }
}
