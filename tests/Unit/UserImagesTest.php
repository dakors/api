<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserImagesTest extends TestCase
{
    use RefreshDatabase;
    /** @test **/
    public function user_can_get_photo_file_path()
    {
        $user = factory('App\User')->create();
        $photoUrl = 'https:/\/storage.googleapis.com\/development-gamerbox\/images\/1\/EUfZuhCnTEXN7LTjDDnkqP_t09VoT2zAb6voxAvGIg1OI.jpg';
        $photoPath = '1\/EUfZuhCnTEXN7LTjDDnkqP_t09VoT2zAb6voxAvGIg1OI.jpg';
        $userImage = factory('App\Models\UserImages')->create([
                'user_id' => $user->id,
                'photo' => $photoUrl
            ]);
        $this->assertTrue($user->userImages->getPhotoFilePath() === $photoPath);
    }

    /** @test **/
    public function user_get_empty_string_if_no_photo()
    {
        $user = factory('App\User')->create();
        $userImage = factory('App\Models\UserImages')->create([
                'user_id' => $user->id,
                'photo' => ''
            ]);
        $this->assertTrue($user->userImages->getPhotoFilePath() === '');
    }

    /** @test **/
    public function user_get_empty_string_if_photo_not_stored_on_gcs()
    {
        $user = factory('App\User')->create();
        $userImage = factory('App\Models\UserImages')->create([
                'user_id' => $user->id,
                'photo' => 'https://scontent.xx.fbcdn.net/v/t1.0-1/p200x200/14650649_1612112568815011_5669364999496782363_n.jpg?oh=c1dd4a49a5dc8745f353d6f77063645e&oe=59814EF1'
            ]);
        $this->assertTrue($user->userImages->getPhotoFilePath() === '');
    }

    /** @test **/
    public function user_can_get_background_file_path()
    {
        $user = factory('App\User')->create();
        $bgUrl = 'https:/\/storage.googleapis.com\/development-gamerbox\/images\/1\/EUfZuhCnTEXN7LTjDDnkqP_t09VoT2zAb6voxAvGIg1OI.jpg';
        $bgPath = '1\/EUfZuhCnTEXN7LTjDDnkqP_t09VoT2zAb6voxAvGIg1OI.jpg';
        $userImage = factory('App\Models\UserImages')->create([
                'user_id' => $user->id,
                'background' => $bgUrl
            ]);
        $this->assertTrue($user->userImages->getBackgroundFilePath() === $bgPath);
    }

    /** @test **/
    public function user_get_empty_string_if_no_background()
    {
        $user = factory('App\User')->create();
        $userImage = factory('App\Models\UserImages')->create([
                'user_id' => $user->id,
                'background' => ''
            ]);
        $this->assertTrue($user->userImages->getBackgroundFilePath() === '');
    }

    /** @test **/
    public function user_get_empty_string_if_background_not_stored_on_gcs()
    {
        $user = factory('App\User')->create();
        $userImage = factory('App\Models\UserImages')->create([
                'user_id' => $user->id,
                'background' => 'https://scontent.xx.fbcdn.net/v/t1.0-1/p200x200/14650649_1612112568815011_5669364999496782363_n.jpg?oh=c1dd4a49a5dc8745f353d6f77063645e&oe=59814EF1'
            ]);
        $this->assertTrue($user->userImages->getBackgroundFilePath() === '');
    }
}
