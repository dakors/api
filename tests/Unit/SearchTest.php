<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Http\Controllers\SearchController;
use Laravel\Passport\Passport;

class SearchTest extends TestCase
{
    use RefreshDatabase;

    public function setUp()
    {
        Parent::setUp();

        factory('App\User')->create(
            [
              'id' => 1,
              'nick_name' => 'gamerbox0',
              'age' => 28,
              'gender' => '0',
              'location' => '1'
          ]
        );

        factory('App\User')->create(
            [
              'id' => 2,
              'nick_name' => 'gamerbox1',
              'age' => 50,
              'gender' => '1',
              'location' => '2'
            ]
        );

        factory('App\User')->create(
            [
              'id' => 3,
              'nick_name' => 'gamerbox2',
              'age' => 18,
              'gender' => '2',
              'location' => '1'
            ]
        );

        factory('App\User')->create(
            [
              'id' => 4,
              'nick_name' => 'gamerbox3',
              'age' => 37,
              'gender' => '1',
              'location' => '3'
            ]
        );

        factory('App\User')->create(
            [
              'id' => 5,
              'nick_name' => 'gamerbox4',
              'age' => 37,
              'gender' => '2',
              'location' => '3'
            ]
        );

        factory('App\User')->create(
            [
              'id' => 6,
              'age' => 66,
              'gender' => '0',
              'location' => '17'
            ]
        );

        factory('App\User')->create(
            [
              'id' => 7,
              'age' => 48,
              'gender' => '0',
              'location' => '18'
            ]
        );

        factory('App\User')->create(
            [
              'id' => 8,
              'age' => 40,
              'gender' => '0',
              'location' => '18'
            ]
        );

        factory('App\Models\UserGameList')->create(
            [
                'user_id' => 1,
                'game_name' => 'wow'
            ]
        );

        factory('App\Models\UserGameList')->create(
            [
                'user_id' => 2,
                'game_name' => 'world of warcraft'
            ]
        );
        factory('App\Models\UserGameList')->create(
            [
                'user_id' => 2,
                'game_name' => 'world of Tank'
            ]
        );
        factory('App\Models\UserGameList')->create(
            [
                'user_id' => 2,
                'game_name' => 'LOL'
            ]
        );
        factory('App\Models\UserGameList')->create(
            [
                'user_id' => 3,
                'game_name' => 'world of tank'
            ]
        );
        factory('App\Models\UserGameList')->create(
            [
                'user_id' => 4,
                'game_name' => 'woshi87'
            ]
        );
        factory('App\Models\UserGameList')->create(
            [
                'user_id' => 5,
                'game_name' => 'Word of warships'
            ]
        );

        factory('App\Models\GameNameList')->create(
            [
                'enUS' => 'World of warcraft',
                'zhTW' => '魔獸世界',
                'acronyms' => 'WOW'
            ]
        );
    }

    /**
     * @test
     */
    public function a_user_can_search_any()
    {
        Passport::actingAs(factory('App\User')->create());
        $result = SearchController::searchForUsers([]);
        $this->assertTrue($result->count() == 8);
    }

    /**
     * @test
     */
    public function a_user_can_find_none()
    {
        Passport::actingAs(factory('App\User')->create());
        factory('App\User')->create();
        $params = ['username' => 'ssqomgomomgomgfdsiifus',
                     'gender' => '',
                     'minAge' => '',
                     'maxAge' => '',
                     'location' => ''
          ];
        $result = SearchController::searchForUsers($params);
        $this->assertTrue($result->count() == 0, "Actual count is ".$result->count());
    }

    /**
     * @test
     */
    public function a_user_can_search_specific_name()
    {
        Passport::actingAs(factory('App\User')->create());
        $params = ['username' => 'gamerbox0',
                     'gender' => '',
                     'minAge' => '',
                     'maxAge' => '',
                     'location' => ''
          ];
        $result = SearchController::searchForUsers($params);
        $this->assertTrue($result->count() == 1);
        $this->assertTrue($result->contains('id', 1));
    }

    /**
     * @test
     */
    public function a_user_can_search_generic_name()
    {
        Passport::actingAs(factory('App\User')->create());
        $params = ['username' => 'gamerbox',
                     'gender' => '',
                     'minAge' => '',
                     'maxAge' => '',
                     'location' => ''
          ];
        $result = SearchController::searchForUsers($params);
        $this->assertTrue($result->count() == 5);
        $this->assertTrue($result->contains('id', 1));
        $this->assertTrue($result->contains('id', 2));
        $this->assertTrue($result->contains('id', 3));
        $this->assertTrue($result->contains('id', 4));
        $this->assertTrue($result->contains('id', 5));
    }

    /**
     * @test
     */
    public function a_user_can_search_min_age()
    {
        Passport::actingAs(factory('App\User')->create());
        $params = ['minAge' => 40,
                     'gender' => '',
                     'username' => '',
                     'maxAge' => '',
                     'location' => ''
          ];
        $result = SearchController::searchForUsers($params);
        $this->assertTrue($result->count() == 4);
        $this->assertTrue($result->contains('id', 2));
        $this->assertTrue($result->contains('id', 6));
        $this->assertTrue($result->contains('id', 7));
        $this->assertTrue($result->contains('id', 8));
    }

    /**
     * @test
     */
    public function a_user_can_search_max_age()
    {
        Passport::actingAs(factory('App\User')->create());
        $params = ['maxAge' => 36,
                     'gender' => '',
                     'username' => '',
                     'minAge' => '',
                     'location' => ''
          ];
        $result = SearchController::searchForUsers($params);
        $this->assertTrue($result->count() == 2);
        $this->assertTrue($result->contains('id', 1));
        $this->assertTrue($result->contains('id', 3));
    }

    /**
     * @test
     */
    public function a_user_can_search_age_range()
    {
        Passport::actingAs(factory('App\User')->create());
        $params = ['minAge' => 40,
                     'maxAge' => 50,
                     'gender' => '',
                     'username' => '',
                     'location' => ''
          ];
        $result = SearchController::searchForUsers($params);
        $this->assertTrue($result->count() == 3);
        $this->assertTrue($result->contains('id', 2));
        $this->assertTrue($result->contains('id', 7));
        $this->assertTrue($result->contains('id', 8));
    }

    /**
     * @test
     */
    public function a_user_can_search_gender()
    {
        Passport::actingAs(factory('App\User')->create());
        $params = ['gender' => '0',
                     'username' => '',
                     'minAge' => '',
                     'maxAge' => '',
                     'location' => ''
          ];
        $result = SearchController::searchForUsers($params);
        $this->assertTrue($result->count() == 4);
        $this->assertTrue($result->contains('id', 1));
        $this->assertTrue($result->contains('id', 6));
        $this->assertTrue($result->contains('id', 7));
        $this->assertTrue($result->contains('id', 8));

        $params = ['gender' => '1',
                     'username' => '',
                     'minAge' => '',
                     'maxAge' => '',
                     'location' => ''
          ];
        $result = SearchController::searchForUsers($params);
        $this->assertTrue($result->count() == 2);
        $this->assertTrue($result->contains('id', 2));
        $this->assertTrue($result->contains('id', 4));

        $params = ['gender' => '2',
                     'username' => '',
                     'minAge' => '',
                     'maxAge' => '',
                     'location' => ''
          ];
        $result = SearchController::searchForUsers($params);
        $this->assertTrue($result->count() == 2);
        $this->assertTrue($result->contains('id', 3));
        $this->assertTrue($result->contains('id', 5));
    }

    /**
     * @test
     */
    public function a_user_can_search_location()
    {
        Passport::actingAs(factory('App\User')->create());
        $params = ['location' => '0',
                     'username' => '',
                     'minAge' => '',
                     'maxAge' => '',
                     'gender' => ''
          ];
        $result = SearchController::searchForUsers($params);
        $this->assertTrue($result->count() == 0);

        $params = ['location' => '1',
                     'username' => '',
                     'minAge' => '',
                     'maxAge' => '',
                     'gender' => ''
          ];
        $result = SearchController::searchForUsers($params);
        $this->assertTrue($result->count() == 2);
        $this->assertTrue($result->contains('id', 1));
        $this->assertTrue($result->contains('id', 3));

        $params = ['location' => '17',
                     'username' => '',
                     'minAge' => '',
                     'maxAge' => '',
                     'gender' => ''
          ];
        $result = SearchController::searchForUsers($params);
        $this->assertTrue($result->count() == 1);
        $this->assertTrue($result->contains('id', 6));
    }

    /**
     * @test
     */
    public function a_user_can_search_games()
    {
        Passport::actingAs(factory('App\User')->create());
        $params = ['location' => '',
                     'username' => '',
                     'minAge' => '',
                     'maxAge' => '',
                     'gender' => '',
                     'game_name' => 'LOL'
          ];
        $result = SearchController::searchForUsers($params);
        $this->assertTrue($result->count() == 1);
        $this->assertTrue($result->contains('id', 2));
    }

    /**
     * @test
     */
    public function a_user_can_search_games_through_game_list_lookup()
    {
        Passport::actingAs(factory('App\User')->create());

        $params = ['location' => '',
                     'username' => '',
                     'minAge' => '',
                     'maxAge' => '',
                     'gender' => '',
                     'game_name' => 'Wo'
          ];
        $result = SearchController::searchForUsers($params);
        $this->assertTrue($result->count() == 5);
        $this->assertTrue($result->contains('id', 1));
        $this->assertTrue($result->contains('id', 2));
        $this->assertTrue($result->contains('id', 3));
        $this->assertTrue($result->contains('id', 4));
        $this->assertTrue($result->contains('id', 5));

        $params = ['location' => '',
                     'username' => '',
                     'minAge' => '',
                     'maxAge' => '',
                     'gender' => '',
                     'game_name' => 'wow'
          ];
        $result = SearchController::searchForUsers($params);
        $this->assertTrue($result->count() == 2, $result->count());
        $this->assertTrue($result->contains('id', 1));
        $this->assertTrue($result->contains('id', 2));

        $params = ['location' => '',
                     'username' => '',
                     'minAge' => '',
                     'maxAge' => '',
                     'gender' => '',
                     'game_name' => '魔獸世界'
          ];
        $result = SearchController::searchForUsers($params);
        $this->assertTrue($result->count() == 2, $result->count());
        $this->assertTrue($result->contains('id', 1));
        $this->assertTrue($result->contains('id', 2));
    }

    /**
     * @test
     */
    public function a_user_can_specify_all_params()
    {
        Passport::actingAs(factory('App\User')->create());
        $params = ['username' => 'gamerbox',
                     'minAge' => 18,
                     'maxAge' => 30,
                     'gender' => '',
                     'location' => '1',
                     'game_name' => 'WO'
          ];
        $result = SearchController::searchForUsers($params);
        $this->assertTrue($result->count() == 2);
        $this->assertTrue($result->contains('id', 1));
        $this->assertTrue($result->contains('id', 3));
    }

    /**
     * @test
     */
    public function a_user_can_only_get_15_result()
    {
        $users = factory('App\User', 50)->create()->each(function ($user) {
            $user->gender = '0';
        });
        Passport::actingAs($users[0]);
        $result = SearchController::searchForUsers([]);
        $this->assertTrue($result->count() == 15);

        $result = SearchController::searchForUsers(['gender' => '0']);
        $this->assertTrue($result->count() == 15);
    }

    /**
     * @test
     */
    public function a_user_cannot_find_blocked_users()
    {
        $this->withoutExceptionHandling();
        $users = factory('App\User', 4)->create();
        Passport::actingAs($users[0]);

        factory('App\Models\UserBlockList')->create(
            [
                'user_id' => $users[0],
                'block_id' => $users[1]
            ]
        );
        factory('App\Models\UserBlockList')->create(
            [
                'user_id' => $users[0],
                'block_id' => $users[2]
            ]
        );
        factory('App\Models\UserBlockList')->create(
            [
                'user_id' => $users[0],
                'block_id' => $users[3]
            ]
        );
        $result = SearchController::searchForUsers([]);
        $this->assertTrue($result->count() == 8, 'Actual count is '.$result->count());
    }

    /**
     * @test
     */
    public function a_user_cannot_find_blocked_by_users()
    {
        $this->withoutExceptionHandling();
        $users = factory('App\User', 4)->create();
        Passport::actingAs($users[0]);
        factory('App\Models\UserBlockList')->create(
            [
                'user_id' => $users[1],
                'block_id' => $users[0]
            ]
        );
        factory('App\Models\UserBlockList')->create(
            [
                'user_id' => $users[2],
                'block_id' => $users[0]
            ]
        );
        factory('App\Models\UserBlockList')->create(
            [
                'user_id' => $users[3],
                'block_id' => $users[0]
            ]
        );
        $result = SearchController::searchForUsers([]);
        $this->assertTrue($result->count() == 8, 'Actual count is '.$result->count());
    }
}
