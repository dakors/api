<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Carbon\Carbon;
use App\Models\UserBlockList;

class UserTest extends TestCase
{
    use RefreshDatabase;

    protected static $users;

    public function setUp()
    {
        Parent::setUp();

        self::$users = factory('App\User', 5)->create();
    }

    private function seedFans()
    {
        // Create fans/followers relationship
        // User[0] has 4 fans, user[1,2,3,4]
        // User[1] has 3 fans, user[2,3,4]
        // User[2] has 2 fans, user[3,4]
        // User[3] has 1 fan, user[4]
        // User[4] is unpopular and sad
        for ($i = 0; $i < 5; $i++) {
            for ($j = $i+1; $j < 5; $j++) {
                factory('App\Models\Fan')->create(
                    [
                          'user_id' => self::$users[$i]->id,
                          'fans_id' => self::$users[$j]->id
                      ]
                );
            }
        }
    }

    private function seedFriends()
    {
        // Make user[0] friends with user[1], user[2] and user[3] and have a
        // pending friend request with user[4]
        factory('App\Models\Friend')->create(
            [
                'requester' => self::$users[0]->id,
                'receiver' => self::$users[1]->id,
                'status' => 'confirmed'
            ]
        );
        factory('App\Models\Friend')->create(
            [
                'requester' => self::$users[2]->id,
                'receiver' => self::$users[0]->id,
                'status' => 'confirmed'
            ]
        );
        factory('App\Models\Friend')->create(
            [
                'requester' => self::$users[3]->id,
                'receiver' => self::$users[0]->id,
                'status' => 'confirmed'
            ]
        );
        factory('App\Models\Friend')->create(
            [
                'requester' => self::$users[0]->id,
                'receiver' => self::$users[4]->id,
                'status' => 'requesting'
            ]
        );

        // Make user[4] have sent a friend request touser[2]. User[0] and user[1]
        // has a pending requesting with user[4] as created above.
        factory('App\Models\Friend')->create(
            [
                'requester' => self::$users[1]->id,
                'receiver' => self::$users[4]->id,
                'status' => 'requesting'
            ]
        );
        factory('App\Models\Friend')->create(
            [
                'requester' => self::$users[4]->id,
                'receiver' => self::$users[2]->id,
                'status' => 'requesting'
            ]
        );
    }

    protected function seedBlogs()
    {
        factory('App\Models\UserBlog')->create(
            [
                  'blog_id' => 1,
                  'user_id' => self::$users[0]->id
              ]
        );

        factory('App\Models\UserBlog')->create(
            [
                  'blog_id' => 2,
                  'user_id' => self::$users[1]->id
              ]
        );

        factory('App\Models\UserBlog')->create(
            [
                  'blog_id' => 3,
                  'user_id' => self::$users[2]->id
              ]
        );

        factory('App\Models\UserBlog')->create(
            [
                  'blog_id' => 4,
                  'user_id' => self::$users[3]->id
              ]
        );

        factory('App\Models\UserBlog')->create(
            [
                  'blog_id' => 5,
                  'user_id' => self::$users[4]->id
              ]
        );
    }

    /**
    * @test
    */
    public function user_can_get_fans()
    {
        $this->seedFans();
        // $this->withoutExceptionHandling();
        // User[0]
        // Assert user[0] have 4 fans
        $this->assertTrue(self::$users[0]->fans->count() == 4);
        for ($i = 1; $i < self::$users->count()-1; $i++) {
            $this->assertTrue(self::$users[0]->fans->contains('id', self::$users[$i]->id));
        }

        // User[2]
        // Assert user[2] have 2 fans
        $this->assertTrue(self::$users[2]->fans->count() == 2);
        // Assert user[3] is fan of user[2]
        $this->assertTrue(self::$users[2]->fans->contains('id', self::$users[3]->id));
        // Assert user[4] is fan of user[2]
        $this->assertTrue(self::$users[2]->fans->contains('id', self::$users[3]->id));

        // User[4]
        // Assert user[2] have 0 fans
        $this->assertTrue(self::$users[4]->fans->count() == 0);
    }

    /**
    * @test
    */
    public function user_cannot_get_blocked_and_blocked_by_fans()
    {
        $this->seedFans();
        factory('App\Models\UserBlockList')->create(
            [
                'user_id' => self::$users[0]->id,
                'block_id' => self::$users[1]->id
            ]
        );
        factory('App\Models\UserBlockList')->create(
            [
                'user_id' => self::$users[0]->id,
                'block_id' => self::$users[2]->id
            ]
        );
        factory('App\Models\UserBlockList')->create(
            [
                'user_id' => self::$users[3]->id,
                'block_id' => self::$users[0]->id
            ]
        );

        $this->assertTrue(self::$users[0]->fans->count() == 1);
        $this->assertFalse(self::$users[0]->fans->contains('id', self::$users[1]->id));
        $this->assertFalse(self::$users[0]->fans->contains('id', self::$users[2]->id));
        $this->assertFalse(self::$users[0]->fans->contains('id', self::$users[3]->id));
    }

    /**
    * @test
    */
    public function user_can_get_following()
    {
        $this->seedFans();
        // $this->withoutExceptionHandling();

        // Following is the inverse relationship with fans.

        // User[0]
        // Assert user[0] is not following anyone
        $this->assertTrue(self::$users[0]->following->count() == 0);

        // User[1]
        // Assert user[1] is following user[0]
        $this->assertTrue(self::$users[1]->following->count() == 1);
        $this->assertTrue(self::$users[1]->following->contains('id', self::$users[0]->id));

        // User[3]
        // Assert user[3] is following user[0], user[1] and user[2]
        $this->assertTrue(self::$users[3]->following->count() == 3);
        $this->assertTrue(self::$users[3]->following->contains('id', self::$users[0]->id));
        $this->assertTrue(self::$users[3]->following->contains('id', self::$users[1]->id));
        $this->assertTrue(self::$users[3]->following->contains('id', self::$users[2]->id));
    }

    /** @test **/
    public function user_cannot_get_blocked_and_blocked_by_following()
    {
        $this->seedFans();

        factory('App\Models\UserBlockList')->create(
            [
                'user_id' => self::$users[4]->id,
                'block_id' => self::$users[1]->id
            ]
        );
        factory('App\Models\UserBlockList')->create(
            [
                'user_id' => self::$users[4]->id,
                'block_id' => self::$users[2]->id
            ]
        );
        factory('App\Models\UserBlockList')->create(
            [
                'user_id' => self::$users[3]->id,
                'block_id' => self::$users[4]->id
            ]
        );

        // After blocking, should see 2 following
        $this->assertTrue(self::$users[4]->following->count() == 1);
        $this->assertFalse(self::$users[4]->following->contains('id', self::$users[1]->id));
        $this->assertFalse(self::$users[4]->following->contains('id', self::$users[2]->id));
        $this->assertFalse(self::$users[4]->following->contains('id', self::$users[3]->id));
    }

    /**
    * @test
    */
    public function user_can_get_friends()
    {
        $this->withoutExceptionHandling();
        $this->seedFriends();

        $friends = self::$users[0]->friends()->get();
        $this->assertTrue($friends->count() == 3);
        $this->assertTrue($friends->contains('id', self::$users[1]->id));
        $this->assertTrue($friends->contains('id', self::$users[2]->id));
        $this->assertTrue($friends->contains('id', self::$users[3]->id));
        // Assert status 'requesting' is not included
        $this->assertFalse($friends->contains('id', self::$users[4]->id));
    }

    /**
    * @test
    */
    public function user_cannot_get_blocked_and_blocked_by_friends()
    {
        $this->seedFriends();
        factory('App\Models\UserBlockList')->create(
            [
                'user_id' => self::$users[0]->id,
                'block_id' => self::$users[1]->id
            ]
        );
        factory('App\Models\UserBlockList')->create(
            [
                'user_id' => self::$users[2]->id,
                'block_id' => self::$users[0]->id
            ]
        );
        $friends = self::$users[0]->friends()->get();
        $this->assertTrue($friends->count() == 1);
        $this->assertTrue($friends->contains('id', self::$users[3]->id));

        // Assert blocked, blocked by
        $this->assertFalse($friends->contains('id', self::$users[1]->id));
        $this->assertFalse($friends->contains('id', self::$users[2]->id));
        // Assert status 'requesting' is not included
        $this->assertFalse($friends->contains('id', self::$users[4]->id));
    }

    /**
    * @test
    */
    public function user_can_get_friend_requests()
    {
        // $this->withoutExceptionHandling();

        $this->seedFriends();
        // Assert user[4] has friend request from user[0] and user[1] and not from user[2]
        $this->assertTrue(self::$users[4]->friendRequests->count() == 2);
        $this->assertTrue(self::$users[4]->friendRequests->contains('id', self::$users[0]->id));
        $this->assertTrue(self::$users[4]->friendRequests->contains('id', self::$users[1]->id));
        $this->assertFalse(self::$users[4]->friendRequests->contains('id', self::$users[2]->id));

        // Assert user[2] has 1 friend request from user[4]
        $this->assertTrue(self::$users[2]->friendRequests->count() == 1);
        $this->assertTrue(self::$users[2]->friendRequests->contains('id', self::$users[4]->id));
    }

    /**
    * @test
    */
    public function user_cannot_get_blocked_and_blocked_by_requests()
    {
        $this->seedFriends();
        factory('App\Models\UserBlockList')->create(
            [
                'user_id' => self::$users[4]->id,
                'block_id' => self::$users[1]->id
            ]
        );
        factory('App\Models\UserBlockList')->create(
            [
                'user_id' => self::$users[0]->id,
                'block_id' => self::$users[4]->id
            ]
        );
        // Assert user[4] has friend request from user[0] and user[1] but user[1] is blocked by user[4]
        // and user[4] is blocked by user[0] so no friend requests is returned.
        $this->assertTrue(self::$users[4]->friendRequests->count() == 0);
        $this->assertFalse(self::$users[4]->friendRequests->contains('id', self::$users[0]->id));
        $this->assertFalse(self::$users[4]->friendRequests->contains('id', self::$users[1]->id));
    }

    /**
    * @test
    */
    public function user_can_get_common_blog()
    {
        $this->seedFans();
        $this->seedFriends();
        $this->seedBlogs();

        $blogs = self::$users[0]->commonBlogs();
        //user[0] is friend with user[1,2,3] and does not follow anyone. So he should
        //see the blogs from user[1,2,3] and his own blog.
        $this->assertTrue($blogs->count() == 4);
        $this->assertTrue($blogs->contains('blog_id', 1));
        $this->assertTrue($blogs->contains('blog_id', 2));
        $this->assertTrue($blogs->contains('blog_id', 3));
        $this->assertTrue($blogs->contains('blog_id', 4));

        //user[1] is friend with user[0] and follows user[0]
        $blogs = self::$users[1]->commonBlogs();
        $this->assertTrue($blogs->count() == 2);
        $this->assertTrue($blogs->contains('blog_id', 1));
        $this->assertTrue($blogs->contains('blog_id', 2));

        //user[2] is friend with user[0] and follows user[0,1]
        $blogs = self::$users[2]->commonBlogs();
        $this->assertTrue($blogs->count() == 3);
        $this->assertTrue($blogs->contains('blog_id', 1));
        $this->assertTrue($blogs->contains('blog_id', 2));
        $this->assertTrue($blogs->contains('blog_id', 3));

        //user[3] is friend with user[0] and follows user[0,1,2]
        $blogs = self::$users[3]->commonBlogs();
        $this->assertTrue($blogs->count() == 4);
        $this->assertTrue($blogs->contains('blog_id', 1));
        $this->assertTrue($blogs->contains('blog_id', 2));
        $this->assertTrue($blogs->contains('blog_id', 3));
        $this->assertTrue($blogs->contains('blog_id', 4));

        //user[4] has no friends and follows user[0,1,2,3]
        $blogs = self::$users[4]->commonBlogs();
        $this->assertTrue($blogs->count() == 5);
        $this->assertTrue($blogs->contains('blog_id', 1));
        $this->assertTrue($blogs->contains('blog_id', 2));
        $this->assertTrue($blogs->contains('blog_id', 3));
        $this->assertTrue($blogs->contains('blog_id', 4));
        $this->assertTrue($blogs->contains('blog_id', 5));
    }

    /**
    * @test
    */
    public function user_cannot_get_blogs_of_blocked_and_block_by_users()
    {
        $this->seedFans();
        $this->seedFriends();
        $this->seedBlogs();

        // User[0] blocks User[4]
        factory('App\Models\UserBlockList')->create(
            [
                'user_id' => self::$users[0]->id,
                'block_id' => self::$users[4]->id
            ]
        );

        // User[1] blocks User[0]
        factory('App\Models\UserBlockList')->create(
            [
                'user_id' => self::$users[1]->id,
                'block_id' => self::$users[0]->id
            ]
        );

        // User[3] blocks User[0]
        factory('App\Models\UserBlockList')->create(
            [
                'user_id' => self::$users[3]->id,
                'block_id' => self::$users[0]->id
            ]
        );

        $blogs = self::$users[0]->commonBlogs();
        //user[0] is friend with user[1,2,3] and does not follow anyone.
        //user[0] is blocked by user[1] and user[3]. So he should
        //see the blogs from user[2] and his own blog.

        $this->assertTrue($blogs->count() == 2);
        $this->assertTrue($blogs->contains('blog_id', 1));
        $this->assertFalse($blogs->contains('blog_id', 2));
        $this->assertTrue($blogs->contains('blog_id', 3));
        $this->assertFalse($blogs->contains('blog_id', 4));

        //user[1] is friend with user[0] and follows user[0]
        //However user[1] blocks user[0] so can only see her own blog.
        $blogs = self::$users[1]->commonBlogs();
        $this->assertTrue($blogs->count() == 1);
        $this->assertFalse($blogs->contains('blog_id', 1));
        $this->assertTrue($blogs->contains('blog_id', 2));

        //user[3] is friend with user[0] and follows user[0,1,2]
        //user[3] blocks user[0] and so only see user[1,2] and her own blog.
        $blogs = self::$users[3]->commonBlogs();
        $this->assertTrue($blogs->count() == 3);
        $this->assertFalse($blogs->contains('blog_id', 1));
        $this->assertTrue($blogs->contains('blog_id', 2));
        $this->assertTrue($blogs->contains('blog_id', 3));
        $this->assertTrue($blogs->contains('blog_id', 4));

        //user[4] has no friends and follows user[0,1,2,3]
        //user[4] is blocked by user[0] so does not see her blog.
        $blogs = self::$users[4]->commonBlogs();
        $this->assertTrue($blogs->count() == 4);
        $this->assertFalse($blogs->contains('blog_id', 1));
        $this->assertTrue($blogs->contains('blog_id', 2));
        $this->assertTrue($blogs->contains('blog_id', 3));
        $this->assertTrue($blogs->contains('blog_id', 4));
        $this->assertTrue($blogs->contains('blog_id', 5));
    }

    /**
    * @test
    */
    public function user_can_get_own_blogs()
    {
        $this->seedBlogs();
        factory('App\Models\UserBlog')->create(
            [
                  'blog_id' => 6,
                  'user_id' => self::$users[0]->id
              ]
        );

        factory('App\Models\UserBlog')->create(
            [
                  'blog_id' => 7,
                  'user_id' => self::$users[0]->id
              ]
        );
        $blogs = self::$users[0]->getBlogs();
        $this->assertTrue($blogs->count() == 3);
        $this->assertTrue($blogs->contains('blog_id', 1));
        $this->assertTrue($blogs->contains('blog_id', 6));
        $this->assertTrue($blogs->contains('blog_id', 7));

        $blogs = self::$users[1]->getBlogs();
        $this->assertTrue($blogs->count() == 1);
        $this->assertTrue($blogs->contains('blog_id', 2));
    }

    /**
    * @test
    */
    public function user_can_get_latest_blog()
    {
        factory('App\Models\UserBlog')->create(
            [
                  'blog_id' => 4,
                  'user_id' => self::$users[0]->id
              ]
        );
        factory('App\Models\UserBlog')->create(
            [
                  'blog_id' => 2,
                  'user_id' => self::$users[0]->id
              ]
        );
        factory('App\Models\UserBlog')->create(
            [
                  'blog_id' => 3,
                  'user_id' => self::$users[0]->id
              ]
        );
        $this->assertTrue(self::$users[0]->getLatestBlog()->blog_id == 4);
        factory('App\Models\UserBlog')->create(
            [
                  'blog_id' => 150,
                  'user_id' => self::$users[0]->id
              ]
        );
        factory('App\Models\UserBlog')->create(
            [
                  'blog_id' => 160,
                  'user_id' => self::$users[0]->id
              ]
        );
        $this->assertTrue(self::$users[0]->getLatestBlog()->blog_id == 160);
    }

    /** @test **/
    public function user_can_get_subscription_status()
    {
        $yesterday = Carbon::now()->subDay()->toDateString();
        $tomorrow = Carbon::now()->addDay()->toDateString();
        $today = Carbon::now()->toDateString();

        self::$users[0]->subscription_expiration_date = $tomorrow;
        self::$users[1]->subscription_expiration_date = $yesterday;
        self::$users[2]->subscription_expiration_date = $today;

        $this->assertTrue(self::$users[0]->isSub());
        $this->assertFalse(self::$users[1]->isSub());
        $this->assertTrue(self::$users[2]->isSub());
    }
}
