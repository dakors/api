<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;
use App\Http\Resources\SearchUserResourceCollection;
use Laravel\Passport\Passport;

class SearchUserResourceTest extends TestCase
{
    use RefreshDatabase;

    public function setUp()
    {
        Parent::setUp();

        factory('App\User', 10)->create()->each(function ($user) {
            $num = mt_rand(1, 3);
            factory('App\Models\UserGameList', $num)->create(
                [
                    'user_id' => $user->id
                ]
              );
            factory('App\Models\UserImages')->create(
                [
                    'user_id' => $user->id
                ]
              );
        });
    }

    /**
     * @test
     */
    public function game_name_is_returned_correctly()
    {
        Passport::actingAs(User::find(1));
        $json = (new SearchUserResourceCollection(User::all()))->jsonSerialize();
        foreach ($json as $value) {
            $this->assertTrue($value['game_name'] === $value['game_list'][0]['game_name']);
        }
    }
}
