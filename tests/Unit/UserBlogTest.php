<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\UserBlog;

class UserBlogTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    protected static $users;

    public function setUp()
    {
        Parent::setUp();

        self::$users = factory('App\User', 5)->create();

        factory('App\Models\UserBlog')->create(
            [
                  'blog_id' => 1,
                  'user_id' => self::$users[0]->id
              ]
        );

        factory('App\Models\UserBlog')->create(
            [
                  'blog_id' => 2,
                  'user_id' => self::$users[1]->id
              ]
        );

        factory('App\Models\UserBlog')->create(
            [
                  'blog_id' => 3,
                  'user_id' => self::$users[0]->id
              ]
        );

        factory('App\Models\UserBlog')->create(
            [
                  'blog_id' => 4,
                  'user_id' => self::$users[0]->id
              ]
        );
    }

    private function seedLikes()
    {
        factory('App\Models\BlogLiked')->create(
            [
                  'blog_id' => 1,
                  'user_id' => self::$users[0]->id
              ]
        );

        factory('App\Models\BlogLiked')->create(
            [
                  'blog_id' => 1,
                  'user_id' => self::$users[1]->id
              ]
        );

        factory('App\Models\BlogLiked')->create(
            [
                  'blog_id' => 1,
                  'user_id' => self::$users[2]->id
              ]
        );

        factory('App\Models\BlogLiked')->create(
            [
                  'blog_id' => 1,
                  'user_id' => self::$users[3]->id
              ]
        );

        factory('App\Models\BlogLiked')->create(
            [
                  'blog_id' => 1,
                  'user_id' => self::$users[4]->id
              ]
        );

        factory('App\Models\BlogLiked')->create(
            [
                  'blog_id' => 2,
                  'user_id' => self::$users[0]->id
              ]
        );

        factory('App\Models\BlogLiked')->create(
            [
                  'blog_id' => 2,
                  'user_id' => self::$users[1]->id
              ]
        );

        factory('App\Models\BlogLiked')->create(
            [
                  'blog_id' => 2,
                  'user_id' => self::$users[3]->id
              ]
        );

        factory('App\Models\BlogLiked')->create(
            [
                  'blog_id' => 3,
                  'user_id' => self::$users[4]->id
              ]
        );
    }

    private function seedComments()
    {
        factory('App\Models\BlogComment')->create(
            [
                  'blog_id' => 4,
                  'user_id' => self::$users[4]->id
              ]
        );

        factory('App\Models\BlogComment')->create(
            [
                  'blog_id' => 4,
                  'user_id' => self::$users[3]->id
              ]
        );

        factory('App\Models\BlogComment')->create(
            [
                  'blog_id' => 4,
                  'user_id' => self::$users[2]->id
              ]
        );

        factory('App\Models\BlogComment')->create(
            [
                  'blog_id' => 3,
                  'user_id' => self::$users[2]->id
              ]
        );

        factory('App\Models\BlogComment')->create(
            [
                  'blog_id' => 3,
                  'user_id' => self::$users[0]->id
              ]
        );
    }

    /**
    * @test
    */
    public function can_get_the_user_who_create_a_blog()
    {
        $user = UserBlog::find(1)->user;
        $this->assertTrue($user->id == self::$users[0]->id);
        $user = UserBlog::find(2)->user;
        $this->assertTrue($user->id == self::$users[1]->id);
    }

    /**
    * @test
    */
    public function can_get_blog_likes()
    {
        $this->seedLikes();
        $likes = UserBlog::find(1)->likes;

        $this->assertTrue($likes->count() == 5);
        $this->assertTrue($likes->contains('user_id', self::$users[0]->id));
        $this->assertTrue($likes->contains('user_id', self::$users[1]->id));
        $this->assertTrue($likes->contains('user_id', self::$users[2]->id));
        $this->assertTrue($likes->contains('user_id', self::$users[3]->id));
        $this->assertTrue($likes->contains('user_id', self::$users[4]->id));

        $likes = UserBlog::find(2)->likes;
        $this->assertTrue($likes->count() == 3);
        $this->assertTrue($likes->contains('user_id', self::$users[0]->id));
        $this->assertTrue($likes->contains('user_id', self::$users[1]->id));
        $this->assertTrue($likes->contains('user_id', self::$users[3]->id));

        $likes = UserBlog::find(3)->likes;
        $this->assertTrue($likes->count() == 1);
        $this->assertTrue($likes->contains('user_id', self::$users[4]->id));
    }

    /**
    * @test
    */
    public function can_get_blog_comments()
    {
        $this->seedComments();
        $comments = UserBlog::find(4)->comments;
        $this->assertTrue($comments->count() == 3);
        $this->assertTrue($comments->contains('user_id', self::$users[2]->id));
        $this->assertTrue($comments->contains('user_id', self::$users[3]->id));
        $this->assertTrue($comments->contains('user_id', self::$users[4]->id));

        $comments = UserBlog::find(3)->comments;
        $this->assertTrue($comments->count() == 2);
        $this->assertTrue($comments->contains('user_id', self::$users[0]->id));
        $this->assertTrue($comments->contains('user_id', self::$users[2]->id));
    }

    /**
    * @test
    */
    public function can_check_if_user_liked_a_blog()
    {
        $this->seedLikes();
        $this->actingAs(self::$users[0]);
        $this->assertTrue(UserBlog::find(1)->isLiked());
        $this->assertTrue(UserBlog::find(2)->isLiked());
        $this->assertFalse(UserBlog::find(3)->isLiked());

        $this->actingAs(self::$users[4]);
        $this->assertTrue(UserBlog::find(1)->isLiked());
        $this->assertFalse(UserBlog::find(2)->isLiked());
        $this->assertTrue(UserBlog::find(3)->isLiked());
    }

    /**
    * @test
    */
    public function can_get_image_path_from_preview()
    {
        $blog = factory('App\Models\UserBlog')->create(
            [
                'user_id' => 1,
                'preview' => '{"image":"https:/\/storage.googleapis.com\/development-gamerbox\/images\/1\/EUfZuhCnTEXN7LTjDDnkqP_t09VoT2zAb6voxAvGIg1OI.jpg?W:20&H:20"}'
            ]
        );
        $this->assertTrue($blog->getImageFilePath() === '1/EUfZuhCnTEXN7LTjDDnkqP_t09VoT2zAb6voxAvGIg1OI.jpg');
    }

    /**
    * @test
    */
    public function cannot_get_image_path_from_url_preview()
    {
        $blog = factory('App\Models\UserBlog')->create(
            [
                'user_id' => 1,
                'preview' => '{
                                  "description":"It\u0027s good news for Arsenal.\n\nMesut Ozil will reportedly not leave Arsenal this summer.\n\nMore gossip: https://t.co/Ky4wQZCRpU",
                                  "embedUrl":"",
                                  "image":"https://pbs.twimg.com/media/D6WS2i2WsAApCTG.jpg:large",
                                  "title":"BBC Sport on Twitter",
                                  "url":"https://twitter.com/BBCSport/status/1127468759650848768?s\u003d09"
                              }'
            ]
        );
        $this->assertTrue($blog->getImageFilePath() == '');
    }

    /**
    * @test
    */
    public function cannot_get_image_path_from_youtube_preview()
    {
        $blog = factory('App\Models\UserBlog')->create(
            [
                'user_id' => 1,
                'preview' => '{
                                  "description":"Commentary by @ODPixel @BTSGoDz MarsMedia ►https://twitter.com/marsmedia ►https://www.facebook.com/MarsMedia.Esports/ LIQUID vs PSG.LGD - WORLD CLASS DOTA! -...",
                                  "embedUrl":"http://www.youtube.com/embed/a_ql8tw9DWU",
                                  "image":"https://i.ytimg.com/vi/a_ql8tw9DWU/maxresdefault.jpg",
                                  "title":"LIQUID vs LGD - WORLD CLASS DOTA!!! - MDL DISNEYLAND PARIS MAJOR DOTA 2",
                                  "url":"https://youtu.be/a_ql8tw9DWU"}'
            ]
        );
        $this->assertTrue($blog->getImageFilePath() == '');
    }

    /**
    * @test
    */
    public function cannot_get_image_file_path_if_preview_not_exist()
    {
        $this->assertTrue(UserBlog::find(1)->getImageFilePath() == '');
    }

    /**
    * @test
    */
    public function cannot_get_image_file_path_if_preview_not_json_string()
    {
        $blog = factory('App\Models\UserBlog')->create(
            [
                'user_id' => 1,
                'preview' => $this->faker->text(20)
            ]
        );
        $this->assertTrue($blog->getImageFilePath() == '');
    }
}
