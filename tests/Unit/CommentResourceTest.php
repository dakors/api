<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Http\Resources\BlogCommentResource;
use Laravel\Passport\Passport;
use Carbon\Carbon;

class CommentResourceTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    /** @test **/
    public function is_owner_correctly_return_true()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $blog = factory('App\Models\UserBlog')->create(['user_id' => $user->id]);
        $comment = factory('App\Models\BlogComment')->create(
            [
                'blog_id' => $blog->blog_id,
                'user_id' => $user->id,
                'modify_date' => $this->faker->date($format = 'Y-m-d H:i:s', $max = 'now')
            ]
        );
        $json = (new BlogCommentResource($comment))->jsonSerialize();
        $this->assertTrue($json['IsCommentOwner']);
    }

    /** @test **/
    public function is_owner_correctly_return_false()
    {
        $user = factory('App\User')->create();
        $user2 = factory('App\User')->create();
        Passport::actingAs($user);
        $blog = factory('App\Models\UserBlog')->create(['user_id' => $user->id]);
        $comment = factory('App\Models\BlogComment')->create(
            [
                'blog_id' => $blog->blog_id,
                'user_id' => $user2->id,
                'modify_date' => $this->faker->date($format = 'Y-m-d H:i:s', $max = 'now')
            ]
        );
        $json = (new BlogCommentResource($comment))->jsonSerialize();
        $this->assertFalse($json['IsCommentOwner']);
    }

    /** @test **/
    public function is_unix_timestamp_correctly_returned()
    {
        $user = factory('App\User')->create();
        Passport::actingAs($user);
        $blog = factory('App\Models\UserBlog')->create(['user_id' => $user->id]);
        $comment = factory('App\Models\BlogComment')->create(
            [
                'blog_id' => $blog->blog_id,
                'user_id' => $user->id,
                'modify_date' => $this->faker->date($format = 'Y-m-d H:i:s', $max = 'now')
            ]
        );
        $json = (new BlogCommentResource($comment))->jsonSerialize();
        $timestamp = $json['timestamp'];
        $this->assertTrue(Carbon::createFromTimestamp($timestamp)->toDateTimeString() === $comment->modify_date);
    }
}
