<?php

namespace App;

use Laravel\Socialite\Contracts\User as ProviderUser;
use App\User;

class SocialAccountService
{
    private $provider_name =['facebook'=>'fb_id','google' =>'google_id'];

    public function findOrCreate(ProviderUser $providerUser, $provider)
    {
        /*$account = LinkedSocialAccount::where('provider_name', $provider)
                   ->where('provider_id', $providerUser->getId())
                   ->first();*/
       //$Socialite_user = Socialite::driver('facebook')->user();
        //echo $this->provider_name[$provider];
        //echo $providerUser->id;
        $account = User::where( $this->provider_name[$provider], '=', $providerUser->id )
                            ->first();



        if ($account) {
            //var_dump($account);
            return $account;
        } else {
            //var_dump($providerUser->image);
            //exit();
            $user = User::create([
                'email' => $providerUser->getEmail(),
                'nick_name'  => $providerUser->getName(),
                $this->provider_name[$provider] => $providerUser->getId(),
            ]);


        return $user;

        }
    }


}
