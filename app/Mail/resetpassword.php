<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class resetpassword extends Mailable
{
    use Queueable, SerializesModels;

    
    public $user;
    public $newPassword;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user,$newPassword)
    {
        $this->user = $user;
        $this->newPassword = $newPassword;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject(__('passwords.reset'))->view('emails.resetpassword');
    }
}
