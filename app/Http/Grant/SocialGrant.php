<?php 

namespace App\Http\Grant;

use App\User as Account;
use DB;
use App\Models\UserImages;
use App\Models\UserOptions;
use App\Models\SocialAccounts;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Http\Request;
use Laravel\Passport\Bridge\User;
use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Entities\UserEntityInterface;
use League\OAuth2\Server\Exception\OAuthServerException;
use League\OAuth2\Server\Grant\AbstractGrant;
use League\OAuth2\Server\Repositories\RefreshTokenRepositoryInterface;
use League\OAuth2\Server\Repositories\UserRepositoryInterface;
use League\OAuth2\Server\RequestEvent;
use League\OAuth2\Server\ResponseTypes\ResponseTypeInterface;
use Psr\Http\Message\ServerRequestInterface;

class SocialGrant extends AbstractGrant{

    //private $provider_name =['facebook'=>'fb_id','google' =>'google_id'];

	/**
     * @param UserRepositoryInterface         $userRepository
     * @param RefreshTokenRepositoryInterface $refreshTokenRepository
     */
    public function __construct(
        UserRepositoryInterface $userRepository,
        RefreshTokenRepositoryInterface $refreshTokenRepository
    ) {
        $this->setUserRepository($userRepository);
        $this->setRefreshTokenRepository($refreshTokenRepository);

        $this->refreshTokenTTL = new \DateInterval('P1M');
    }

    /**
     * {@inheritdoc}
     */
    public function respondToAccessTokenRequest(
        ServerRequestInterface $request,
        ResponseTypeInterface $responseType,
        \DateInterval $accessTokenTTL
    ) {
        // Validate request
        $client = $this->validateClient($request);
        $scopes = $this->validateScopes($this->getRequestParameter('scope', $request));
        $user = $this->validateUser($request, $client);

        // Finalize the requested scopes
        $scopes = $this->scopeRepository->finalizeScopes($scopes, $this->getIdentifier(), $client, $user->getIdentifier());

        // Issue and persist new tokens
        $accessToken = $this->issueAccessToken($accessTokenTTL, $client, $user->getIdentifier(), $scopes);
        $refreshToken = $this->issueRefreshToken($accessToken);

        // Inject tokens into response
        $responseType->setAccessToken($accessToken);
        $responseType->setRefreshToken($refreshToken);

        return $responseType;
    }

    /**
     * @param ServerRequestInterface $request
     * @param ClientEntityInterface  $client
     *
     * @throws OAuthServerException
     *
     * @return UserEntityInterface
     */
    protected function validateUser(ServerRequestInterface $request, ClientEntityInterface $client)
    {
        $provider = $this->getRequestParameter('provider', $request);
        if (is_null($provider)) {
            throw OAuthServerException::invalidRequest('provider');
        }

        $access_token = $this->getRequestParameter('access_token', $request);
        if (is_null($access_token)) {
            throw OAuthServerException::invalidRequest('access_token');
        }

        //$providerUser = Socialite::driver($provider)->userFromToken($access_token);

        

        //dd($providerUser);


        $user = $this->getUserFromSocialNetwork(new Request($request->getParsedBody())); 

        if ($user instanceof UserEntityInterface === false) {
            $this->getEmitter()->emit(new RequestEvent(RequestEvent::USER_AUTHENTICATION_FAILED, $request));

            throw OAuthServerException::invalidCredentials();
        }

        return $user;
    }

    private function getUserFromSocialNetwork(Request $request){

        try{
            $providerUser = Socialite::driver($request->provider)->userFromToken($request->access_token);
        }catch(\Exception $e){
            return;
        }
        
        $socialaccount = SocialAccounts::where('provider','=',$request->provider)->where('provider_user_id','=',$providerUser->id )->first();
        
        //dd($providerUser->getEmail());
        //dd($socialaccount);
       
        if (!$socialaccount == null) {
            $User = Account::where('id','=',$socialaccount->user_id)->first();
            //dd($User);
            return new User($User->getAuthIdentifier());
        } else {
            $DataCenterId = config('app.DataCenterId');
            $MachineId = config('app.MachineId');
            $IdWorker = \wantp\Snowflake\IdWorker::getIns($DataCenterId,$MachineId);
            $uuid = strval($IdWorker->id());
            try{$NewAccount = Account::create([
                'uuid' => $uuid,
                'email' => is_null($providerUser->getEmail()) ? '' : $providerUser->getEmail(),
                'nick_name'  => is_null($providerUser->getName()) ? '':$providerUser->getName(),
                //$this->provider_name[$request->provider] => $providerUser->getId(),
                ]);
            }catch(\Exception $e){
                return response("An Error Occured, please retry later", 422);
            }    
            
        }
        if($NewAccount->wasRecentlyCreated === True){
            SocialAccounts::create([
                'user_id' => $NewAccount->id,
                'provider'=> $request->provider,
                'provider_user_id' => $providerUser->id,
            ]);
            UserImages::create([
              'user_id' => $NewAccount->id,
              'photo' => '',
              'background' => '',
            ]);
            UserOptions::create([
                'user_id' => $NewAccount->id,
                'isStrangerAllowed' => true,
                'reminder_time' => -1,
            ]);

            $RecommendedUser =DB::table('recommend_user')->select(DB::raw("$NewAccount->id as fans_id , user_id"))->get();
            $RecommendedUser=collect($RecommendedUser)->map(function($x){ return (array) $x; })->toArray();
            DB::table('fans_list')->insert($RecommendedUser);
          }

        return new User($NewAccount->getAuthIdentifier());

    }

    /**
     * {@inheritdoc}
     */
    public function getIdentifier()
    {
        return 'social';
    }

}