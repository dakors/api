<?php

namespace App\Http\Resources;

use App\Http\Resources\SearchUserResource;

class FollowingUserResource extends SearchUserResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $attributes = parent::toArray($request);
        $attributes['IsMuted'] = $this->pivot->IsMuted;
        return $attributes;
    }
}
