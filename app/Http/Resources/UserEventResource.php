<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Auth;
class UserEventResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        //dd(Auth::user()->id);
        if($this->user->id == Auth::user()->id){
            return [
                'event_id' => $this->event_id,
                //'user_id' => $this->user->uuid,
                'nick_name' => $this->user->nick_name,
                'event_name' => $this->event_name,
                'registration_start_time' => $this->registration_start_time,
                'registration_end_time' => $this->registration_end_time,
                'start_time' => $this->start_time,
                'end_time' => $this->end_time,
                'location' => $this->location,
                'detail' => $this->detail,
                'pic1' => $this->pic1,
                'pic2' => $this->pic2,
                'pic3' => $this->pic3,
                'isRegistrable' => $this->isRegistrable(),
                'isDuringRegistration' => $this->isDuringRegistration(),
                'registration_limit_amount' => $this->registration_limit_amount,
                'interested_user_amount' => $this->Interested->count(),
                //'isRegistered' => $this->isRegistered(),
                'registered_user_amount' => $this->Registered->count(),
                'timestamp' =>  \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $this->modify_time)->timestamp,
            ];
        }else{
            return [
                'event_id' => $this->event_id,
                'user_id' => $this->user->uuid,
                'nick_name' => $this->user->nick_name,
                'event_name' => $this->event_name,
                'registration_start_time' => $this->registration_start_time,
                'registration_end_time' => $this->registration_end_time,
                'start_time' => $this->start_time,
                'end_time' => $this->end_time,
                'location' => $this->location,
                'detail' => $this->detail,
                'pic1' => $this->pic1,
                'pic2' => $this->pic2,
                'pic3' => $this->pic3,
                'isRegistrable' => $this->isRegistrable(),
                'isDuringRegistration' => $this->isDuringRegistration(),
                'registration_limit_amount' => $this->registration_limit_amount,
                 //'interested_user_amount' => $this->Interested->count(),
                'isRegistered' => $this->isRegistered(),
                // 'registered_user_amount' => $this->Registered->count(),
                'isFull' => ($this->Registered->count() >= $this->registration_limit_amount) && ($this->registration_limit_amount !== -1)  ,
                'timestamp' =>  \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $this->modify_time)->timestamp,
            ];
        }

    }
}
