<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class NotificationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return [
            'data' => $this->items(),
            'meta' => [ 
                'current_page' =>   $this->currentPage(),            
                'from' => $this->firstItem(),
                'last_page' => $this->lastPage(),
                'path' =>$request->url(),
                'per_page' => $this->perPage(),
                'to' => $this->lastItem(),
                'total' => $this->total(),
                        ],
            'links' => [
                'first' => $request->url()."?page=1",
                'last' => $request->url()."?page=".$this->lastPage(),
                'next' => $this->nextPageUrl(),
                'prev' => $this->previousPageUrl(),
            ],
        ];
    }
}
