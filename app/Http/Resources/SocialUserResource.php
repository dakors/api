<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Auth;

class SocialUserResource extends UserResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //dd($this->beRequests);
        $myId = Auth::user()->id;
        $attributes = parent::toArray($request);
        $attributes['IsBlocked'] = $this->blockByList()->pluck('user_block_list.user_id')->contains($myId);
        $attributes['IsFriend'] = $this->friends()->pluck('id')->contains($myId);
        $attributes['IsFan'] = $this->fans->pluck('id')->contains($myId);
        $attributes['IsFollowing'] = $this->following->pluck('id')->contains($myId);
        $attributes['IsRequest'] = $this->friendRequests->pluck('id')->contains($myId);
        $attributes['BeRequest'] = $this->beRequests->pluck('id')->contains($myId);
        return $attributes;
    }
}
