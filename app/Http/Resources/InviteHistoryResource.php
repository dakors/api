<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Auth;

class InviteHistoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        $user = Auth::user();
        if($user->id == $this->sender_id){
            return [
                'invite_history_id' => $this->id,
                'user_id' => $this->receiver()->uuid,
                'nick_name' => $this->receiver()->nick_name,
                'gender' => $this->receiver()->gender,
                'photo' => $this->receiver()->userImages->photo,
                'Issubscriber' => $this->receiver()->isSub(),
                'start_time' => $this->start_time,
                'end_time' => $this->end_time,
                'game_name' => $this->game_name,
                'location' => $this->location,
                'status' => $this->status,
                'request_time' => $this->request_time,
                'respond_time' => $this->respond_time,
                //'canceled_by' => $this->canceled_by ? $this->canceled_by_user()->uuid:$this->canceled_by ,
                'type' => 'send',
            ];
        }else{
            return [
                'invite_history_id' => $this->id,
                'user_id' => $this->sender()->uuid,
                'nick_name' => $this->sender()->nick_name,
                'gender' => $this->sender()->gender,
                'photo' => $this->sender()->userImages->photo,
                'Issubscriber' => $this->sender()->isSub(),
                'start_time' => $this->start_time,
                'end_time' => $this->end_time,
                'game_name' => $this->game_name,
                'location' => $this->location,
                'status' => $this->status,
                'request_time' => $this->request_time,
                'respond_time' => $this->respond_time,
                //'canceled_by' => $this->canceled_by ? $this->canceled_by_user()->uuid:$this->canceled_by,
                'type' => 'receive',
            ];

        }

    }
}
