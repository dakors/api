<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Auth;
use Carbon\Carbon;

class BlogCommentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'comment_id' => $this->comment_id,
            'blog_id' => $this->blog_id,
            'content' => strip_tags($this->content),
            'user_id' => $this->user->uuid,
            'nick_name' => $this->user->nick_name,
            'gender' => $this->user->gender,
            'photo' => $this->user->getAvatar(),
            'Issubscriber' => $this->user->isSub(),
            'IsCommentOwner' => $this->isOwner(),
            'timestamp' => $this->getTimestamp()
        ];
    }

    private function getTimestamp()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->modify_date)->timestamp;
    }

    private function isOwner()
    {
        return Auth::user()->id === $this->user_id;
    }
}
