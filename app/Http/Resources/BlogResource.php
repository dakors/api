<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BlogResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if ($this->event_id !== null) {
            if ($this->event !== null) {
                $preview['url']="";
                $preview['image']=$this->event->pic1;
                $preview['location']=$this->event->location;
                $preview['event_name']=$this->event->event_name;
                $preview['start_time']=$this->event->start_time;
                $preview['end_time']=$this->event->end_time;
                $preview['event_hoster']=$this->event->user->nick_name;
            } else {
                $preview['url']="";
                $preview['image']="";
                $preview['location']="";
                $preview['event_name']=0;
                $preview['start_time']=0;
                $preview['event_name']= __('blog.event_deleted');
                $preview['event_hoster']='';
            }
            return [
                'blog_id' => $this->blog_id,
                'user_id' => $this->user->uuid,
                'nick_name' => $this->user->nick_name,
                'photo' => $this->user->getAvatar(),
                'gender' => $this->user->gender,
                'loction' => $this->user->location,
                'Issubscriber' => $this->user->isSub(),
                'event_id' => $this->event_id,
                'content' => strip_tags($this->content),
                'preview' => $preview,
                'liked_count_amount' => $this->likes->count(),
                'comment_count_amount' => $this->comments->count(),
                'timestamp' => \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $this->modify_date)->timestamp
            ];
        } else {
            return [
                'blog_id' => $this->blog_id,
                'user_id' => $this->user->uuid,
                'nick_name' => $this->user->nick_name,
                'photo' => $this->user->getAvatar(),
                'gender' => $this->user->gender,
                'loction' => $this->user->location,
                'Issubscriber' => $this->user->isSub(),
                'event_id' => $this->event_id,
                'content' => strip_tags($this->content),
                'preview' => $this->preview,
                'liked_count_amount' => $this->likes->count(),
                'comment_count_amount' => $this->comments->count(),
                'timestamp' => \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $this->modify_date)->timestamp
            ];
        }
    }
}
