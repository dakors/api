<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class RecommendedUserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return [
            //'user_id' => $this->uuid,
            'nick_name' => $this->nick_name,
            'gender' => $this->gender,
            //'age' => $this->age,
            //'location' => $this->location,
            'photo' => $this->userImages->photo,
            //'background' => $this->userImages->background,
            //'introduction' => $this->introduction,
            //'game_list' => $this->userGameList,
            //'Fans' => $this->fans->count(),
            //'Friends' => $this->friends()->count(),
            //'isFb' => $this->socialaccounts()->pluck('provider')->contains('facebook'),
            //'isGoogle' => $this->socialaccounts()->pluck('provider')->contains('google'),
            'Issubscriber' => $this->isSub(),
            //'isStrangerAllowed' => $this->options->isStrangerAllowed,
            //'reminder_time' => $this->options->reminder_time,
        ];
    }
}
