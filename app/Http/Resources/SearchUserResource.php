<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SearchUserResource extends SocialUserResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $attributes = parent::toArray($request);
        $attributes['game_name'] = $this->getGameName();
        return $attributes;
    }

    public function getGameName()
    {
        $gameList = $this->userGameList;
        $gameName = '';
        if($gameList!== null && $gameList->isNotEmpty())
            $gameName = $this->userGameList[0]['game_name'];
        return $gameName;
    }
}
