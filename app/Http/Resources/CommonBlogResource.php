<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CommonBlogResource extends UserBlogResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $attributes = parent::toArray($request);
        $attributes['nick_name'] = $this->user->nick_name;
        $attributes['photo'] = $this->user->getAvatar();
        $attributes['gender'] = $this->user->gender;
        $attributes['loction'] = $this->user->location;
        $attributes['Issubscriber'] = $this->user->isSub();
        return $attributes;
    }
}
