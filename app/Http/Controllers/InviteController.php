<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Models\Friend;
use App\Models\DeviceInfo;
use App\Models\InviteHistory;
use App\Models\UserGameList;
use App\Models\InviteReminder;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use App\Http\Resources\InviteHistoryResource;
use App\Http\Resources\InviteHistoryResourceCollection;
use Carbon\Carbon;
use DB;
use FCM;

class InviteController extends Controller
{
    public function request(Request $request){
        $this->validate($request, [      
            'user_id' => 'required',          
            'game_id' => 'required',
            'start_time' => 'required',
            'end_time' => 'required',
            'location' => 'required',
        ]);

        $sender = Auth::user();
        $receiver = User::where('uuid',$request->user_id)->firstorfail();
        $game = UserGameList::where(['user_id' => $receiver->id ,'id' => $request->game_id])->firstorfail();
        if($request->start_time >= $request->end_time){
            return response(['message' => __('invite.start_time_big_then_end_time')], 422);
        }
        $now = \Carbon\Carbon::now()->timestamp * 1000;
        if($request->start_time <= $now || $request->end_time <= $now ){
            return response(['message' => __('invite.start_time_or_end_time_is_invalidate')], 422);
        }
        $invite = InviteHistory::Create([
            'sender_id' => $sender->id,
            'receiver_id' => $receiver->id,
            'start_time' => $request->start_time,
            'end_time' => $request->end_time,
            'game_name' => $game->game_name,
            'location' => $request->location,
            'request_time' => $now,
        ]);

        if($invite->save()){
            if(count($receiver->FcmToken())>=1){
                
                $fcm_data= ['type' => 'invite'
                ,'nick_name' => $sender->nick_name
                ,'photo' => $sender->userImages['photo']
                ,'user_id' => $sender->uuid
                ,'target_id' => $receiver->uuid
                ,'game_name' => $game->game_name
                ,'location' => $request->location
                ,'start_time' => $request->start_time
                ,'end_time' => $request->end_time
                ,'timestamp' => $now
                ]; 

                $title = __('invite.new_invite_request');
                $body = __('invite.who_invite_you',['Name' => $sender->nick_name,'Game' => $game->game_name]);
                
                $this->fcm($fcm_data,$receiver->FcmToken(),$title,$body);
            } 
            return response(['message' => __('invite.send_new_invite_success')], 200);
        }else{
            return response(['message' => __('An unexpected error has occured. Please try again')], 500);
        }
    }

    public function accept(Request $request){
        $this->validate($request, [      
            'invite_history_id' => 'required',
        ]);
        $receiver = Auth::user();
        $invite = InviteHistory::where(['receiver_id' => $receiver->id,'id' => $request->invite_history_id,'status' => 'requesting'])->firstorfail();
        
        $now = \Carbon\Carbon::now()->timestamp * 1000;
        if($invite->start_time <= $now || $invite->end_time <= $now ){
            return response(['message' => __('invite.start_time_or_end_time_is_passed')], 422);
        }
        $invite->update(['status' => 'accept','respond_time' => $now]);

        if($invite->save()){
            $sender = User::where('id',$invite->sender_id)->firstorfail();
            if(count($sender->FcmToken())>=1){
                $reminder_time = $this->getReminderTime($sender,$invite->start_time);
                $fcm_data= ['type' => 'respond'
                ,'nick_name' => $receiver->nick_name
                ,'photo' => $receiver->userImages['photo']
                ,'user_id' => $receiver->uuid
                ,'target_id' => $sender->uuid
                ,'game_name' => $invite->game_name           
                ,'timestamp' => $now
                ,'respond_type' => 'accept'
                ,'location' => $invite->location
                ,'invite_history_id' => $invite->id
                ,'start_time' => $invite->start_time
                ,'reminder_time' => $reminder_time
                ]; 

                $title = __('invite.accept_invite_request');
                $body = sprintf(__('invite.who_accept_invite'),$receiver->nick_name);
                
                $this->fcm($fcm_data,$sender->FcmToken(),$title,$body);
            } 
            return response(['message' => __('invite.accept_invite_success')], 200);
        }else{
            return response(['message' => __('An unexpected error has occured. Please try again')], 500);
        }
    }

    public function reject(Request $request){
        $this->validate($request, [      
            'invite_history_id' => 'required',
        ]);
        $receiver = Auth::user();
        //dd($receiver->id);
        $invite = InviteHistory::where(['receiver_id' => $receiver->id,'id' => $request->invite_history_id,'status' => 'requesting'])->firstorfail();
        
        $now = \Carbon\Carbon::now()->timestamp * 1000;
        if($invite->start_time <= $now || $invite->end_time <= $now ){
            return response(['message' => __('invite.start_time_or_end_time_is_passed')], 422);
        }
        $invite->update(['status' => 'reject','respond_time' => $now]);

        if($invite->save()){
            $sender = User::where('id',$invite->sender_id)->firstorfail();
            //$reminder_time = $this->getReminderTime($sender,$invite->start_time);
            $fcm_data= ['type' => 'respond'
            ,'nick_name' => $receiver->nick_name
            ,'photo' => $receiver->userImages['photo']
            ,'user_id' => $receiver->uuid
            ,'target_id' => $sender->uuid
            ,'game_name' => $invite->game_name           
            ,'timestamp' => $now
            ,'respond_type' => 'reject'
            ,'location' => $invite->location
            ,'invite_history_id' => $invite->id
            ,'start_time' => $invite->start_time
            ,'reminder_time' => -1
            ]; 

            $title = __('invite.reject_invite_request');
            $body = sprintf(__('invite.who_reject_invite'),$receiver->nick_name);
            
            $this->fcm($fcm_data,$sender->FcmToken(),$title,$body);
            return response(['message' => __('invite.reject_invite_success')], 200);
        }else{
            return response(['message' => __('An unexpected error has occured. Please try again')], 500);
        }
    }

    public function cancel(Request $request){
        $this->validate($request, [      
            'invite_history_id' => 'required',
        ]);
        $user = Auth::user();
        $userId = $user->id;
        $invite = InviteHistory::where(['id' => $request->invite_history_id,'status' => 'accept'])
                    ->where(function($query) use ($userId){
                        $query->where('sender_id',$userId)->orwhere('receiver_id',$userId);
                    })->firstorfail();
        //dd($invite->toArray());
        $now = \Carbon\Carbon::now()->timestamp * 1000;
        if($invite->start_time <= $now || $invite->end_time <= $now ){
            return response(['message' => __('invite.start_time_or_end_time_is_passed')], 422);
        }
        $invite->update(['status' => 'canceled','respond_time' => $now,'canceled_by' => $userId]);

        if($invite->save()){
            return response(['message' => __('invite.cancel_invite_success')], 200);
        }else{
            return response(['message' => __('An unexpected error has occured. Please try again')], 500);
        }
    }

    public function getList(Request $request){
        $user =Auth::user();
        $user->last_time_history_list =  Carbon::now()->toDateTimeString();
        $invite = InviteHistory::where('sender_id',$user->id)->orwhere('receiver_id',$user->id)->orderbyRaw("case respond_time when 0 then request_time else respond_time end desc")->Paginate(15);
        $user->save();
        return new InviteHistoryResourceCollection($invite);
    }

    public function get(Request $request){
        $this->validate($request, [      
            'invite_history_id' => 'required',
        ]);
        $user = Auth::user();
        $userId = $user->id;
        $invite = InviteHistory::where(['id' => $request->invite_history_id])
                                ->where(function($query) use ($userId){
                                    $query->orwhere('sender_id',$userId)->orwhere('receiver_id',$userId);
                                })->firstorfail();
        
        return new InviteHistoryResource($invite);
    }

    public function reminder(Request $request){
        $this->validate($request, [      
            'invite_history_id' => 'required',
            'reminder_time' => 'required',
        ]);
        $user = Auth::user();
        $userId = $user->id;
        $invite = InviteHistory::where(['id' => $request->invite_history_id,'status' => 'accept'])
                    ->where(function($query) use ($user){
                        $query->where('sender_id',$user->id)->orwhere('receiver_id',$user->id);
                    })->firstorfail();

        $reminder = InviteReminder::firstornew(['invite_history_id' => $request->invite_history_id , 'user_id' => $user->id]);
        $reminder->reminder_time = $request->reminder_time;
        if($reminder->save()){
            return response(['message' => __('invite.set_invite_reminder_time_success')], 200);
        }else{
            return response(['message' => __('An unexpected error has occured. Please try again')], 500);
        }
    }

    public function getReminderList(){
        $user = Auth::user();
        //dd($user->id);
        $get_invite_reminder_sql="select tb1.invite_history_id,tb1.reminder_time,tb3.nick_name as target_name,tb2.location,tb2.game_name,tb2.start_time from invite_reminder as tb1 join invite_history as tb2 on tb1.invite_history_id = tb2.id and tb2.sender_id = tb1.user_id  join users as tb3 on tb2.receiver_id = tb3.id where tb2.status = 'accept' and tb1.user_id = $user->id and (tb2.start_time+tb1.reminder_time*1000) >= unix_timestamp(now())*1000  
                                    union
                                    select tb1.invite_history_id,tb1.reminder_time,tb3.nick_name as target_name,tb2.location,tb2.game_name,tb2.start_time from invite_reminder as tb1 join invite_history as tb2 on tb1.invite_history_id = tb2.id and tb2.receiver_id = tb1.user_id  join users as tb3 on tb2.sender_id = tb3.id where tb2.status = 'accept' and tb1.user_id = $user->id and (tb2.start_time+tb1.reminder_time*1000) >= unix_timestamp(now())*1000  ;";
        
        $result1 = DB::select($get_invite_reminder_sql);
        //dd($user->options->reminder_time);
        //dd($result1);
        if($user->options->reminder_time != -1){
            $get_invite_reminder_sql2="select tb1.id as invite_history_id, ".$user->options->reminder_time." as reminder_time,tb2.nick_name as target_name,tb1.location,tb1.game_name,tb1.start_time from invite_history as tb1 join users as tb2 on tb1.receiver_id = tb2.id where start_time >=(unix_timestamp(now())+(".$user->options->reminder_time." * 60))*1000 and sender_id = $user->id  and status = 'accept' "
                    . "union all "
                    . "select tb1.id as invite_history_id, ".$user->options->reminder_time." as reminder_time,tb2.nick_name as target_name,tb1.location,tb1.game_name,tb1.start_time from invite_history as tb1 join users as tb2 on tb1.sender_id = tb2.id where start_time >=(unix_timestamp(now())+(".$user->options->reminder_time." * 60))*1000 and receiver_id = $user->id and status = 'accept' ";
        $result2 = DB::select($get_invite_reminder_sql2);
        }else{
        $result2 = [];    
        }
        
        $result2 = collect($result2)->wherenotin('invite_history_id',collect($result1)->pluck('invite_history_id'))->toArray();
        //dd($result2);
        $result = collect(array_merge($result1,$result2))->where('reminder_time','<>','-1');
        
        return response(['data' => ['Invite_Remiinder_List' => $result]], 200);
    }
    private function fcm($fcm_data,$tokens,$title,$body){
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);

        $notificationBuilder = new PayloadNotificationBuilder($title);
        $notificationBuilder->setBody($body)
                            ->setIcon('myicon')
                            ->setSound('default');

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData($fcm_data);

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

        //$tokens = $request->token;

        $downstreamResponse = FCM::sendTo($tokens, $option, $notification, $data);

        $downstreamResponse->numberSuccess();
        
        if(count($downstreamResponse->numberFailure()) >= 1 ){
            $device = DeviceInfo::wherein('device_id',$downstreamResponse->tokensToDelete());
            $device->delete();
        }

        $downstreamResponse->numberModification();

        // return Array - you must remove all this tokens in your database
        //$downstreamResponse->tokensToDelete();
        
        // return Array (key : oldToken, value : new token - you must change the token in your database)
        $downstreamResponse->tokensToModify();
        
        // return Array - you should try to resend the message to the tokens in the array
        $downstreamResponse->tokensToRetry();

        // return Array (key:token, value:error) - in production you should remove from your database the tokens
        $downstreamResponse->tokensWithError();
    }


    private function getReminderTime($user,$start_time){
        if($user->options->reminder_time !== -1){
            $reminder_time = $start_time - ($user->options->reminder_time * 60 * 1000);
        }else{
            $start_time = -1;
        }
        return $reminder_time;
    }
}



