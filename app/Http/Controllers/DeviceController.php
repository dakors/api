<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use DB;
use Auth;
use App\User;
use App\Models\DeviceInfo;

class DeviceController extends Controller
{
    public function add(Request $request){
        $this->validate($request, [
            'device_id' => 'required',
            'os' => 'required',
        ]);
        $user = Auth::user();
        
        $device = DeviceInfo::firstornew(['device_id' => $request->device_id]);
        //dd($device);
        $device->device_id = $request->device_id;
        $device->os = $request->os;
        $device->user_id = $user->id;
        $device->isNotificationForMessageEnabled = $request->isNotificationForMessageEnabled ?? 'false';
        $device->isNotificationForFriendAddEnabled = $request->isNotificationForFriendAddEnabled ?? 'false';
        $device->isSoundEnabled = $request->isSoundEnabled ?? 'false';
        //dd($device);
        if($device->save()){
            return response(['message' => __('device.add_device_success')], 200);
        }else{
            return response(['message' => __('An unexpected error has occured. Please try again')], 500);
        }

    }

    public function remove(Request $request){
        $this->validate($request, [
            'device_id' => 'required',
            'os' => 'required',
        ]);
        $user = Auth::user();
        $device = DeviceInfo::where('device_id',$request->device_id)->where('user_id',$user->id)->where('os',$request->os)->firstorfail();

        if($device->delete()){
            return response(['message' => __('device.remove_device_success')], 200);
        }else{
            return response(['message' => __('An unexpected error has occured. Please try again')], 500);
        }
    }
}

