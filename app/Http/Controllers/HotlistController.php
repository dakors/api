<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Auth;
use App\Models\HotList;
use App\Http\Resources\HotListResourceCollection;

class HotlistController extends Controller
{
    public function getList(Request $request){
        $this->validate($request, ['catalog_id' => 'required']);

        $list =HotList::where('catalog_id','=',$request->catalog_id)->orderby('rank','asc')->get();
        if(count($list)>=1){
            return new HotListResourceCollection($list);
        }else{
            return response(['message' => __('Hot List Not Found')], 404);
        }
    }
}
