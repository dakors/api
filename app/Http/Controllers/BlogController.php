<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use app\User;
use Auth;
use DB;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Illuminate\Support\Facades\Storage;
use App\Models\UserBlog;
use App\Models\RecommendedUser;
use App\Http\Resources\UserBlogResourceCollection;
use App\Http\Resources\UserBlogResource;
use App\Http\Resources\BlogResourceCollection;
use App\Http\Resources\BlogResource;
use App\Models\HashtagSearchLog;

class BlogController extends Controller
{


    public function create(Request $request)
    {
        $this->validate($request, ['blog_content' => 'max:3000']);

        $userID = Auth::user()->id;
        $uuid = Auth::user()->uuid;
        $preview = $this->generatePreview($request, $uuid);
        if ($preview instanceof Response) {
            return $preview;
        }
       
        if(empty($request->blog_content) && empty($preview)){
            return response(['message' => __('blog.content_cant_empty')], 500);
        }
        $blog = new UserBlog;
        $blog->user_id = $userID;
        $blog->content = strip_tags(str_replace("&nbsp;"," ",($request->blog_content ?? '')));//trim(($request->blog_content ?? ''), " \t\n\r\0\x0B\xC2\xA0");
        $blog->preview = $preview ?? null;
        $blog->event_id = $request->blog_event;

        if ($blog->save()) {
            
            $new_blog = $blog = UserBlog::where('blog_id',$blog->blog_id)
            ->with(['user.userImages', 'likes', 'comments','event'])->first();
            
            return response(['message' => __('blog.new_blog_success'),'data' => new UserBlogResource($new_blog)], 200);
        } else {
            return response(['message' => __('An unexpected error has occured. Please try again')], 500);
        }
    }

    public function getLastOne(Request $request)
    {
       
        $posts[] = Auth::user()->getLatestBlog();
        if ($request->ajax()) {
            return [
            'posts' => view('ajax.home')->with(compact('posts'))->render()
        ];
        }
    }

    public function delete(Request $request)
    {
        $userID = Auth::user()->id;

        $data = $this->validate($request, ['blog_id' => 'required']);

        // $result = UserBlog::where('blog_id', $request->blog_id)->where('user_id', $userID)->delete();
        $blogToDelete = UserBlog::find($data['blog_id']);

        if ($blogToDelete === null) {
            return response(['message' => __('blog.blog_not_found')], 404);
        }

        if ($blogToDelete->user_id !== $userID) {
            return response(['message' => __('blog.blog_not_owner')], 403);
        }
        $path = $blogToDelete->getImageFilePath();
        if ($blogToDelete->delete()) {
            if ($path != '') {
                Storage::disk('gcs')->delete($path);
            }
            return response(['message' => __('blog.blog_removed')], 200);
        } else {
            return response(['message' => __('An unexpected error has occured. Please try again')], 500);
        }
    }

    public function update(Request $request)
    {
        $data = $this->validate($request, ['blog_id' => 'required']);

        $blog = UserBlog::find($data['blog_id']);

        if ($blog === null) {
            return response(['message' => __('blog.blog_not_found')], 404);
        }
        $userID = Auth::user()->id;
        $uuid = Auth::user()->uuid;
        if ($blog->user_id !== $userID) {
            return response(['message' => __('blog.blog_not_owner')], 403);
        }
        
        $old_image_url = isset($blog->preview->image) ? $blog->preview->image : '';

        $path = $blog->getImageFilePath();
        // Check if the original blog has an image attach. If it does, get the current image path before
        // generating new preview.

        $preview = $this->generatePreview($request, $uuid);
        if ($preview instanceof Response) {
            return $preview;
        }

        if(gettype($preview) == 'array'){
            $new_image_url = isset($preview['image']) ? $preview['image'] : '';
        }else if (gettype($preview) == 'object'){
            $new_image_url = isset($preview->image) ? $preview->image : '';
        }



        if(empty($request->blog_content) && empty($preview)){
            return response(['message' => __('blog.content_cant_empty')], 500);
        }
        $blog->content = str_replace("&nbsp;"," ",($request->blog_content ?? ''));//trim(($request->blog_content ?? ''), " \t\n\r\0\x0B\xC2\xA0");
        $blog->preview = $preview ?? null;
        $blog->event_id = $request->blog_event ?? null;
        
        if ($blog->save()) {
            // If have existing image, delete it from GCS.
            if ($path !== '' && $old_image_url !== $new_image_url) {
                Storage::disk('gcs')->delete($path);
            }
            $new_blog = $blog = UserBlog::where('blog_id',$blog->blog_id)
            ->with(['user.userImages', 'likes', 'comments','event'])->first();
            return response(['message' => __('blog.blog_updated'),'data' => new UserBlogResource($new_blog)], 200);
        } else {
            return response(['message' => __('An unexpected error has occured. Please try again')], 500);
        }
    }

    public function getBlogs(Request $request)
    {
        $this->validate($request, [
        'id'   => 'required'
      ]);
        
        $user = User::where('uuid', $request->id)
                ->wherenotin('id',Auth::user()->blockAndBlockByList())->first();
        if ($user === null) {
            return response(['message' => __('User not found')], 404);
        }        
        $blogs = $user->getBlogs();
        //dd($blogs);
        return new UserBlogResourceCollection($blogs);
    }

    public function getBlog(Request $request)
    {
        $this->validate($request, [
            'blog_id'   => 'required'
          ]);

        switch(AUTH::check()){
            case true:
            $blog = UserBlog::where('blog_id',$request->blog_id)
            ->wherenotin('user_id',Auth::user()->blockAndBlockByList())
            ->with(['user.userImages', 'likes', 'comments','event'])->firstorfail();
    
            if ($blog === null){
                return response(['message' => __('Blog not found')], 404);
            }
    
            return new UserBlogResource($blog);
            break;

            case false:
            $blog = UserBlog::where('blog_id',$request->blog_id)
            ->with(['user.userImages', 'likes', 'comments','event'])->firstorfail();
            if ($blog === null){
                return response(['message' => __('Blog not found')], 404);
            }
    
            return new BlogResource($blog);
            break;
        }

    }

    public function hashtagSearch(Request $request){
        $this->validate($request, [
            'keyword'   => 'required'
          ]);
        $user = Auth::user();
        
        $blogs = UserBlog::whereRaw("MATCH (content) AGAINST ('$request->keyword')")->where('content','like','%'.$request->keyword.'%')
                            ->with(['user.userImages', 'likes', 'comments','event'])
                            ->whereNotIn('user_id',$user->blockAndBlockByList())
                            ->orderby('modify_date','desc')->get();

        $log = new HashtagSearchLog;
        $log->user_id = $user->id ;
        $log->keyword = $request->keyword;
        $log->save();

        if(count($blogs) >=1 ){
            return new UserBlogResourceCollection($blogs);
        }else{
            return response(['message' => __('Blog not found')], 404);
        }


    }
    
    public function homepage(Request $request){
        /*$users = RecommendedUser::get()->pluck('user_id')->toArray();

        $blog = UserBlog::wherein('user_id',$users)
        ->with(['user.userImages', 'likes', 'comments','event'])
        ->latest('modify_date')
        ->Paginate(15);*/
        $blog = UserBlog::whereRaw('CHAR_LENGTH(content) > 300')
        ->with(['user.userImages', 'likes', 'comments','event'])
        ->latest('modify_date')
        ->Paginate(15);
        return new BlogResourceCollection($blog);
    }

    private function generatePreview($request, $uuid)
    {
        if (isset($request->blog_image)) {
            $imageName = str_random(22).'_'.str_random(22);
            $image_str=explode(',', $request->blog_image);

            $imagedata = base64_decode($image_str[count($image_str)-1]);

            if (strlen($imagedata)>=1) {
                $image_info = getimagesizefromstring($imagedata);
            } else {
                return response(['message' => __('images.content_incorrect')], 500);
            }
            if (!$image_info) {
                return response(['message' => __('images.content_incorrect')], 500);
            }
            $image_type = explode('/', $image_info['mime'])[0];
            $subname = explode('/', $image_info['mime'])[1];
            
            if ($image_type !== 'image') {
                return response(['message' => __('images.type_incorrect')], 500);
            }

            $imageProperty = sprintf("?W=%d&H=%d", $image_info[0], $image_info[1]);
            
            $source = imagecreatefromstring($imagedata);
            ob_start();
            if($subname == 'png'){
                imagepng($source);
            }else{
                imagejpeg($source,null,100);
            }
            $image_contents = ob_get_clean();


            $savepath = "$uuid"."/"."$imageName.$subname";

            $disk = Storage::disk('gcs');
            if (!$disk->put($savepath, $image_contents)) {
                return response(['message' => __('An unexpected error has occured. Please try again')], 500);
            }
            $image_url = $disk->url($savepath).$imageProperty;

            $preview = ['image'=>$image_url];
        /*}else if(!is_array(json_decode($request->blog_url_preview_json,true))){            
            $preview = '';*/
        }elseif (!is_array($request->blog_url_preview_json)) {
            $preview = $request->blog_url_preview_json ? json_decode($request->blog_url_preview_json) :null;            
            //dd($preview);
        } else{
            $preview = $request->blog_url_preview_json ?? null;  
        }
        return $preview;
    }
    
    public function hotKeywords() {
        $blog = collect(UserBlog::get()->toArray())->pluck('content')->toArray();

        foreach($blog as $content){
            preg_match_all('/'.$this->getPattern().'/u', $content, $matches);
            //var_dump($matches);
            if(count($matches[3])>=1){
                $content_keyword = array_unique($matches[3]);
                //$content_keyword = $matches[3];
                foreach($content_keyword as $keyword){
                    $keywords[] = $keyword;
                }
            }
            
        }

        $keywords = array_count_values($keywords);
        arsort($keywords);
        $i=0;
        foreach($keywords as $keyword => $value){
            $data[$i]['keyword'] = $keyword;
            $data[$i]['amount'] = $value;
            $i++;
        }
        return response(['data' => collect($data)->take(30)], 200);
    }

    private function getPattern() {
        $a = '\xc0-\xd6'.'\xd8-\xf6'.'\xf8-\xff'.'\x{0100}-\x{024f}'.'\x{0253}-\x{0254}'.'\x{0256}-\x{0257}'.'\x{0259}'.'\x{025b}'.'\x{0263}'.'\x{0268}'.'\x{026f}'.'\x{0272}'.'\x{0289}'.'\x{028b}'.'\x{02bb}'.'\x{0300}-\x{036f}'.'\x{1e00}-\x{1eff}';
        $b = '\x{0400}-\x{04ff}'.'\x{0500}-\x{0527}'.'\x{2de0}-\x{2dff}'.'\x{a640}-\x{a69f}'.'\x{0591}-\x{05bf}'.'\x{05c1}-\x{05c2}'.'\x{05c4}-\x{05c5}'.'\x{05c7}'.'\x{05d0}-\x{05ea}'.'\x{05f0}-\x{05f4}'.'\x{fb12}-\x{fb28}'.'\x{fb2a}-\x{fb36}'.'\x{fb38}-\x{fb3c}'.'\x{fb3e}'.'\x{fb40}-\x{fb41}'.'\x{fb43}-\x{fb44}'.'\x{fb46}-\x{fb4f}'.'\x{0610}-\x{061a}'.'\x{0620}-\x{065f}'.'\x{066e}-\x{06d3}'.'\x{06d5}-\x{06dc}'.'\x{06de}-\x{06e8}'.'\x{06ea}-\x{06ef}'.'\x{06fa}-\x{06fc}'.'\x{06ff}'.'\x{0750}-\x{077f}'.'\x{08a0}'.'\x{08a2}-\x{08ac}'.'\x{08e4}-\x{08fe}'.'\x{fb50}-\x{fbb1}'.'\x{fbd3}-\x{fd3d}'.'\x{fd50}-\x{fd8f}'.'\x{fd92}-\x{fdc7}'.'\x{fdf0}-\x{fdfb}'.'\x{fe70}-\x{fe74}'.'\x{fe76}-\x{fefc}'.'\x{200c}-\x{200c}'.'\x{0e01}-\x{0e3a}'.'\x{0e40}-\x{0e4e}'.'\x{1100}-\x{11ff}'.'\x{3130}-\x{3185}'.'\x{A960}-\x{A97F}'.'\x{AC00}-\x{D7AF}'.'\x{D7B0}-\x{D7FF}'.'\x{FFA1}-\x{FFDC}';
        $c = '\x{30A1}-\x{30FA}\x{30FC}-\x{30FE}'.'\x{FF66}-\x{FF9F}'.'\x{FF10}-\x{FF19}\x{FF21}-\x{FF3A}'.'\x{FF41}-\x{FF5A}'.'\x{3041}-\x{3096}\x{3099}-\x{309E}'.'\x{3400}-\x{4DBF}'.'\x{4E00}-\x{9FFF}'.$this->unichr(173824).'-'.$this->unichr(177983).$this->unichr(177984).'-'.$this->unichr(178207).$this->unichr(194560).'-'.$this->unichr(195103).'\x{3003}\x{3005}\x{303B}';
        $d = $a.$b.$c;
        $e = '\x{0041}-\x{005A}\x{0061}-\x{007A}\x{00AA}\x{00B5}\x{00BA}\x{00C0}-\x{00D6}\x{00D8}-\x{00F6}'.'\x{00F8}-\x{0241}\x{0250}-\x{02C1}\x{02C6}-\x{02D1}\x{02E0}-\x{02E4}\x{02EE}\x{037A}\x{0386}'.'\x{0388}-\x{038A}\x{038C}\x{038E}-\x{03A1}\x{03A3}-\x{03CE}\x{03D0}-\x{03F5}\x{03F7}-\x{0481}'.'\x{048A}-\x{04CE}\x{04D0}-\x{04F9}\x{0500}-\x{050F}\x{0531}-\x{0556}\x{0559}\x{0561}-\x{0587}'.'\x{05D0}-\x{05EA}\x{05F0}-\x{05F2}\x{0621}-\x{063A}\x{0640}-\x{064A}\x{066E}-\x{066F}'.'\x{0671}-\x{06D3}\x{06D5}\x{06E5}-\x{06E6}\x{06EE}-\x{06EF}\x{06FA}-\x{06FC}\x{06FF}\x{0710}'.'\x{0712}-\x{072F}\x{074D}-\x{076D}\x{0780}-\x{07A5}\x{07B1}\x{0904}-\x{0939}\x{093D}\x{0950}'.'\x{0958}-\x{0961}\x{097D}\x{0985}-\x{098C}\x{098F}-\x{0990}\x{0993}-\x{09A8}\x{09AA}-\x{09B0}'.'\x{09B2}\x{09B6}-\x{09B9}\x{09BD}\x{09CE}\x{09DC}-\x{09DD}\x{09DF}-\x{09E1}\x{09F0}-\x{09F1}'.'\x{0A05}-\x{0A0A}\x{0A0F}-\x{0A10}\x{0A13}-\x{0A28}\x{0A2A}-\x{0A30}\x{0A32}-\x{0A33}'.'\x{0A35}-\x{0A36}\x{0A38}-\x{0A39}\x{0A59}-\x{0A5C}\x{0A5E}\x{0A72}-\x{0A74}\x{0A85}-\x{0A8D}'.'\x{0A8F}-\x{0A91}\x{0A93}-\x{0AA8}\x{0AAA}-\x{0AB0}\x{0AB2}-\x{0AB3}\x{0AB5}-\x{0AB9}\x{0ABD}'.'\x{0AD0}\x{0AE0}-\x{0AE1}\x{0B05}-\x{0B0C}\x{0B0F}-\x{0B10}\x{0B13}-\x{0B28}\x{0B2A}-\x{0B30}'.'\x{0B32}-\x{0B33}\x{0B35}-\x{0B39}\x{0B3D}\x{0B5C}-\x{0B5D}\x{0B5F}-\x{0B61}\x{0B71}\x{0B83}'.'\x{0B85}-\x{0B8A}\x{0B8E}-\x{0B90}\x{0B92}-\x{0B95}\x{0B99}-\x{0B9A}\x{0B9C}\x{0B9E}-\x{0B9F}'.'\x{0BA3}-\x{0BA4}\x{0BA8}-\x{0BAA}\x{0BAE}-\x{0BB9}\x{0C05}-\x{0C0C}\x{0C0E}-\x{0C10}'.'\x{0C12}-\x{0C28}\x{0C2A}-\x{0C33}\x{0C35}-\x{0C39}\x{0C60}-\x{0C61}\x{0C85}-\x{0C8C}'.'\x{0C8E}-\x{0C90}\x{0C92}-\x{0CA8}\x{0CAA}-\x{0CB3}\x{0CB5}-\x{0CB9}\x{0CBD}\x{0CDE}'.'\x{0CE0}-\x{0CE1}\x{0D05}-\x{0D0C}\x{0D0E}-\x{0D10}\x{0D12}-\x{0D28}\x{0D2A}-\x{0D39}'.'\x{0D60}-\x{0D61}\x{0D85}-\x{0D96}\x{0D9A}-\x{0DB1}\x{0DB3}-\x{0DBB}\x{0DBD}\x{0DC0}-\x{0DC6}'.'\x{0E01}-\x{0E30}\x{0E32}-\x{0E33}\x{0E40}-\x{0E46}\x{0E81}-\x{0E82}\x{0E84}\x{0E87}-\x{0E88}'.'\x{0E8A}\x{0E8D}\x{0E94}-\x{0E97}\x{0E99}-\x{0E9F}\x{0EA1}-\x{0EA3}\x{0EA5}\x{0EA7}'.'\x{0EAA}-\x{0EAB}\x{0EAD}-\x{0EB0}\x{0EB2}-\x{0EB3}\x{0EBD}\x{0EC0}-\x{0EC4}\x{0EC6}'.'\x{0EDC}-\x{0EDD}\x{0F00}\x{0F40}-\x{0F47}\x{0F49}-\x{0F6A}\x{0F88}-\x{0F8B}\x{1000}-\x{1021}'.'\x{1023}-\x{1027}\x{1029}-\x{102A}\x{1050}-\x{1055}\x{10A0}-\x{10C5}\x{10D0}-\x{10FA}\x{10FC}'.'\x{1100}-\x{1159}\x{115F}-\x{11A2}\x{11A8}-\x{11F9}\x{1200}-\x{1248}\x{124A}-\x{124D}'.'\x{1250}-\x{1256}\x{1258}\x{125A}-\x{125D}\x{1260}-\x{1288}\x{128A}-\x{128D}\x{1290}-\x{12B0}'.'\x{12B2}-\x{12B5}\x{12B8}-\x{12BE}\x{12C0}\x{12C2}-\x{12C5}\x{12C8}-\x{12D6}\x{12D8}-\x{1310}'.'\x{1312}-\x{1315}\x{1318}-\x{135A}\x{1380}-\x{138F}\x{13A0}-\x{13F4}\x{1401}-\x{166C}'.'\x{166F}-\x{1676}\x{1681}-\x{169A}\x{16A0}-\x{16EA}\x{1700}-\x{170C}\x{170E}-\x{1711}'.'\x{1720}-\x{1731}\x{1740}-\x{1751}\x{1760}-\x{176C}\x{176E}-\x{1770}\x{1780}-\x{17B3}\x{17D7}'.'\x{17DC}\x{1820}-\x{1877}\x{1880}-\x{18A8}\x{1900}-\x{191C}\x{1950}-\x{196D}\x{1970}-\x{1974}'.'\x{1980}-\x{19A9}\x{19C1}-\x{19C7}\x{1A00}-\x{1A16}\x{1D00}-\x{1DBF}\x{1E00}-\x{1E9B}'.'\x{1EA0}-\x{1EF9}\x{1F00}-\x{1F15}\x{1F18}-\x{1F1D}\x{1F20}-\x{1F45}\x{1F48}-\x{1F4D}'.'\x{1F50}-\x{1F57}\x{1F59}\x{1F5B}\x{1F5D}\x{1F5F}-\x{1F7D}\x{1F80}-\x{1FB4}\x{1FB6}-\x{1FBC}'.'\x{1FBE}\x{1FC2}-\x{1FC4}\x{1FC6}-\x{1FCC}\x{1FD0}-\x{1FD3}\x{1FD6}-\x{1FDB}\x{1FE0}-\x{1FEC}'.'\x{1FF2}-\x{1FF4}\x{1FF6}-\x{1FFC}\x{2071}\x{207F}\x{2090}-\x{2094}\x{2102}\x{2107}'.'\x{210A}-\x{2113}\x{2115}\x{2119}-\x{211D}\x{2124}\x{2126}\x{2128}\x{212A}-\x{212D}'.'\x{212F}-\x{2131}\x{2133}-\x{2139}\x{213C}-\x{213F}\x{2145}-\x{2149}\x{2C00}-\x{2C2E}'.'\x{2C30}-\x{2C5E}\x{2C80}-\x{2CE4}\x{2D00}-\x{2D25}\x{2D30}-\x{2D65}\x{2D6F}\x{2D80}-\x{2D96}'.'\x{2DA0}-\x{2DA6}\x{2DA8}-\x{2DAE}\x{2DB0}-\x{2DB6}\x{2DB8}-\x{2DBE}\x{2DC0}-\x{2DC6}'.'\x{2DC8}-\x{2DCE}\x{2DD0}-\x{2DD6}\x{2DD8}-\x{2DDE}\x{3005}-\x{3006}\x{3031}-\x{3035}'.'\x{303B}-\x{303C}\x{3041}-\x{3096}\x{309D}-\x{309F}\x{30A1}-\x{30FA}\x{30FC}-\x{30FF}'.'\x{3105}-\x{312C}\x{3131}-\x{318E}\x{31A0}-\x{31B7}\x{31F0}-\x{31FF}\x{3400}-\x{4DB5}'.'\x{4E00}-\x{9FBB}\x{A000}-\x{A48C}\x{A800}-\x{A801}\x{A803}-\x{A805}\x{A807}-\x{A80A}'.'\x{A80C}-\x{A822}\x{AC00}-\x{D7A3}\x{F900}-\x{FA2D}\x{FA30}-\x{FA6A}\x{FA70}-\x{FAD9}'.'\x{FB00}-\x{FB06}\x{FB13}-\x{FB17}\x{FB1D}\x{FB1F}-\x{FB28}\x{FB2A}-\x{FB36}\x{FB38}-\x{FB3C}'.'\x{FB3E}\x{FB40}-\x{FB41}\x{FB43}-\x{FB44}\x{FB46}-\x{FBB1}\x{FBD3}-\x{FD3D}\x{FD50}-\x{FD8F}'.'\x{FD92}-\x{FDC7}\x{FDF0}-\x{FDFB}\x{FE70}-\x{FE74}\x{FE76}-\x{FEFC}\x{FF21}-\x{FF3A}'.'\x{FF41}-\x{FF5A}\x{FF66}-\x{FFBE}\x{FFC2}-\x{FFC7}\x{FFCA}-\x{FFCF}\x{FFD2}-\x{FFD7}'.'\x{FFDA}-\x{FFDC}';
        $f = '\x{0300}-\x{036F}\x{0483}-\x{0486}\x{0591}-\x{05B9}\x{05BB}-\x{05BD}\x{05BF}'.'\x{05C1}-\x{05C2}\x{05C4}-\x{05C5}\x{05C7}\x{0610}-\x{0615}\x{064B}-\x{065E}\x{0670}'.'\x{06D6}-\x{06DC}\x{06DF}-\x{06E4}\x{06E7}-\x{06E8}\x{06EA}-\x{06ED}\x{0711}\x{0730}-\x{074A}'.'\x{07A6}-\x{07B0}\x{0901}-\x{0903}\x{093C}\x{093E}-\x{094D}\x{0951}-\x{0954}\x{0962}-\x{0963}'.'\x{0981}-\x{0983}\x{09BC}\x{09BE}-\x{09C4}\x{09C7}-\x{09C8}\x{09CB}-\x{09CD}\x{09D7}'.'\x{09E2}-\x{09E3}\x{0A01}-\x{0A03}\x{0A3C}\x{0A3E}-\x{0A42}\x{0A47}-\x{0A48}\x{0A4B}-\x{0A4D}'.'\x{0A70}-\x{0A71}\x{0A81}-\x{0A83}\x{0ABC}\x{0ABE}-\x{0AC5}\x{0AC7}-\x{0AC9}\x{0ACB}-\x{0ACD}'.'\x{0AE2}-\x{0AE3}\x{0B01}-\x{0B03}\x{0B3C}\x{0B3E}-\x{0B43}\x{0B47}-\x{0B48}\x{0B4B}-\x{0B4D}'.'\x{0B56}-\x{0B57}\x{0B82}\x{0BBE}-\x{0BC2}\x{0BC6}-\x{0BC8}\x{0BCA}-\x{0BCD}\x{0BD7}'.'\x{0C01}-\x{0C03}\x{0C3E}-\x{0C44}\x{0C46}-\x{0C48}\x{0C4A}-\x{0C4D}\x{0C55}-\x{0C56}'.'\x{0C82}-\x{0C83}\x{0CBC}\x{0CBE}-\x{0CC4}\x{0CC6}-\x{0CC8}\x{0CCA}-\x{0CCD}\x{0CD5}-\x{0CD6}'.'\x{0D02}-\x{0D03}\x{0D3E}-\x{0D43}\x{0D46}-\x{0D48}\x{0D4A}-\x{0D4D}\x{0D57}\x{0D82}-\x{0D83}'.'\x{0DCA}\x{0DCF}-\x{0DD4}\x{0DD6}\x{0DD8}-\x{0DDF}\x{0DF2}-\x{0DF3}\x{0E31}\x{0E34}-\x{0E3A}'.'\x{0E47}-\x{0E4E}\x{0EB1}\x{0EB4}-\x{0EB9}\x{0EBB}-\x{0EBC}\x{0EC8}-\x{0ECD}\x{0F18}-\x{0F19}'.'\x{0F35}\x{0F37}\x{0F39}\x{0F3E}-\x{0F3F}\x{0F71}-\x{0F84}\x{0F86}-\x{0F87}\x{0F90}-\x{0F97}'.'\x{0F99}-\x{0FBC}\x{0FC6}\x{102C}-\x{1032}\x{1036}-\x{1039}\x{1056}-\x{1059}\x{135F}'.'\x{1712}-\x{1714}\x{1732}-\x{1734}\x{1752}-\x{1753}\x{1772}-\x{1773}\x{17B6}-\x{17D3}\x{17DD}'.'\x{180B}-\x{180D}\x{18A9}\x{1920}-\x{192B}\x{1930}-\x{193B}\x{19B0}-\x{19C0}\x{19C8}-\x{19C9}'.'\x{1A17}-\x{1A1B}\x{1DC0}-\x{1DC3}\x{20D0}-\x{20DC}\x{20E1}\x{20E5}-\x{20EB}\x{302A}-\x{302F}'.'\x{3099}-\x{309A}\x{A802}\x{A806}\x{A80B}\x{A823}-\x{A827}\x{FB1E}\x{FE00}-\x{FE0F}'.'\x{FE20}-\x{FE23}';
        $g = '\x{0030}-\x{0039}\x{0660}-\x{0669}\x{06F0}-\x{06F9}\x{0966}-\x{096F}\x{09E6}-\x{09EF}'.'\x{0A66}-\x{0A6F}\x{0AE6}-\x{0AEF}\x{0B66}-\x{0B6F}\x{0BE6}-\x{0BEF}\x{0C66}-\x{0C6F}'.'\x{0CE6}-\x{0CEF}\x{0D66}-\x{0D6F}\x{0E50}-\x{0E59}\x{0ED0}-\x{0ED9}\x{0F20}-\x{0F29}'.'\x{1040}-\x{1049}\x{17E0}-\x{17E9}\x{1810}-\x{1819}\x{1946}-\x{194F}\x{19D0}-\x{19D9}'.'\x{FF10}-\x{FF19}';
        $h = $e.$f.$d;
        $i = $g.'_';
        $j = $h.$i;
        $k = '['.$h.']';
        $l = '['.$j.']';
        $m = '^|$|[^&\/'.$j.']';
        $n = '[#\x{FF03}]';
        $result = '('.$m.')('.$n.')('.$l.'*'.$k.$l.'*)';
        return $result;
    }

    private function unichr($u) {
        return mb_convert_encoding('&#'.intval($u).';', 'UTF-8', 'HTML-ENTITIES');
    }
}
