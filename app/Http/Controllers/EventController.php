<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use DB;
use Auth;
use App\User;
use App\Models\UserEvent;
use App\Models\UserInterestedEvent;
use App\Models\UserRegisteredEvent;
use App\Http\Resources\UserEventResource;
use App\Http\Resources\UserEventResourceCollection;
use App\Http\Resources\SocialUserResourceCollection;
use Illuminate\Support\Facades\Storage;
use App\Models\DeviceInfo;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

class EventController extends Controller
{
    public function create(Request $request){
        $this->validate($request, [
            'event_name' => 'max:128',
            'location' => 'max:128',
            'detail' => 'max:256',
            'start_time' => 'required',
            'end_time' => 'required',
        ]);

        if(filter_var($request->isRegistrable,FILTER_VALIDATE_BOOLEAN)){
            $this->validate($request, [                
                'registration_start_time' => 'required',
                'registration_end_time' => 'required',
            ]);
        }
        $user = Auth::user();
        $content = collect($request->request)->toArray();   
        $content['user_id'] = $user->id;
        $uuid = $user->uuid;
        if($request->pic1_image !== null && !empty($request->pic1_image)){
            $content['pic1']= $this->generateImage($request->pic1_image,$uuid);
        }else{
            $content['pic1'] = '';
        }
        
        if($request->pic2_image !== null && !empty($request->pic2_image)){
            $content['pic2'] = $this->generateImage($request->pic2_image,$uuid);
        }else{
            $content['pic2'] = '';
        }

        if($request->pic3_image !== null && !empty($request->pic3_image)){
            $content['pic3'] = $this->generateImage($request->pic3_image,$uuid);
        }else{
            $content['pic3'] = '';
        }
      
        //dd($content);
        $newEvent = UserEvent::Create($content);

        if($newEvent->save()){
            $userFriendList = $user->friends()->pluck('id')->toArray();
            $userFansList = $user->fans()->where('IsMuted','false')->pluck('users.id')->toArray();
            $device = DeviceInfo::wherein('user_id',$userFansList)->orwherein('user_id',$userFansList)->pluck('device_id')->toArray();
            if(count($device)>=1){
                
                $fcm_data= ['type' => 'Event_Notification'
                ,'nick_name' => $user->nick_name
                ,'photo' =>  $user->userImages['photo']
                ,'user_id' => $user->uuid
                ,'event_id' => $newEvent->event_id
                ,'pic1' => $newEvent->pic1                
                ,'event_name' => $newEvent->event_name
                ];

                $title = __('event.new_event_notification');
                $body = sprintf(__('event.who_create_new_event_notification'),$user->nick_name);
                
                $this->fcm($fcm_data,$device,$title,$body);
            } 
            
            return response(['message' => __('event.new_event_success')], 200);
        }else{
            return response(['message' => __('An unexpected error has occured. Please try again')], 500);
        }

        
        
    }

    public function getList(Request $request){
        $this->validate($request, [
            'id'   => 'required'
          ]);
        $user = User::where('uuid','=',$request->id)->firstorfail();
        
        //dd($user->event()->toArray());
        //dd($user->getBlogs()->toArray());
        return new UserEventResourceCollection($user->event());
        
    }
    
    public function get(Request $request){
        $this->validate($request, [
            'event_id'   => 'required'
          ]);

        $event = UserEvent::where('event_id','=',$request->event_id)->firstorfail();
        
        return new UserEventResource($event);
    }

    public function delete(Request $request){
        $this->validate($request, [
            'event_id'   => 'required',
          ]);
          $user = Auth::user();
          $event = UserEvent::findorfail($request->event_id);
          if($user->id !== $event->user_id){
            return response(['message' => __('Not Event Owner')],401);
          }
          if($event->delete()){
            return response(['message' => __('event.delete_event_success')], 200);
          }else{
            return response(['message' => __('An unexpected error has occured. Please try again')], 500);
          }
    }

    public function update(Request $request){
        $this->validate($request, [
            'event_id'   => 'required',
            'event_name' => 'max:128',
            'location' => 'max:128',
            'detail' => 'max:256',
            'start_time' => 'required',
            'end_time' => 'required',
          ]);
        if(filter_var($request->isRegistrable,FILTER_VALIDATE_BOOLEAN)){
            $this->validate($request, [                
                'registration_start_time' => 'required',
                'registration_end_time' => 'required',
            ]);
        }

        $user = Auth::user();
        $event = UserEvent::findorfail($request->event_id);
        $content = collect($request->request)->toArray();
        $uuid = $user->uuid;
        if($user->id !== $event->user_id){
            return response(['message' => __('Not Event Owner')],401);
        }

        if($request->pic1_image !== null && !empty($request->pic1_image)){
            $content['pic1']= $this->generateImage($request->pic1_image,$uuid);
        }else{
            $content['pic1'] =  isset($request->pic1) ? $request->pic1 : '' ;
        }
        
        if($request->pic2_image !== null && !empty($request->pic2_image)){
            $content['pic2'] = $this->generateImage($request->pic2_image,$uuid);
        }else{
            $content['pic2'] = isset($request->pic2) ? $request->pic2 : '';
        }

        if($request->pic3_image !== null && !empty($request->pic3_image)){
            $content['pic3'] = $this->generateImage($request->pic3_image,$uuid);
        }else{
            $content['pic3'] = isset($request->pic3) ? $request->pic3 : '';
        }
        //dd($content);
        $event->update($content);
        if($event->save()){
            return response(['message' => __('event.update_event_success')], 200);
        }else{
            return response(['message' => __('An unexpected error has occured. Please try again')], 500);
        }
    }
    
    public function addinterestedEvent(Request $request){
        $this->validate($request, [
            'event_id'   => 'required'
          ]);
        $event = UserEvent::findorfail($request->event_id);
        //dd($event);
        $user = Auth::user();
        if($event->user_id == $user->id){
            return response(['message' => __('Cant Add Own Event to Interested')],403);
        }

        if($event->isInterested()){
            return response(['message' => __('Already Add This Event to Interested')],403);
        }
        
        $InterestedEvent = UserInterestedEvent::Create([
            'event_id' => $event->event_id,
            'user_id' => $user->id,
        ]);
    
        if($InterestedEvent->save()){ 
            //dd($InterestedEvent->event()->get());          
            return response(['message' => __('event.add_to_interested'),'data' => new UserEventResourceCollection($InterestedEvent->event()->get())], 200);
        }else{
            return response(['message' => __('An unexpected error has occured. Please try again')], 500);
        }
    }

    public function removeinterestedEvent(Request $request){
        $this->validate($request, [
            'event_id'   => 'required'
          ]);
        $event = UserEvent::findorfail($request->event_id);
        //dd($event);
        $user = Auth::user();
        if($event->user_id == $user->id){
            return response(['message' => __('Event owner cant add or remove Interested')],403);
        }

        if(!$event->isInterested()){
            return response(['message' => __('Already Removed This Event From Interested')],403);
        }

        $InterestedEvent = UserInterestedEvent::where('user_id','=',$user->id)->where('event_id','=',$event->event_id)->firstorfail();
        if($InterestedEvent->delete()){
            return response(['message' => __('event.removed_from_interested'),'data' => new UserEventResource($event)], 200);
        }else{
            return response(['message' => __('An unexpected error has occured. Please try again')], 500);
        }
    }

    public function registerevent(Request $request){
        $this->validate($request, [
            'event_id'   => 'required'
          ]);
        $event = UserEvent::findorfail($request->event_id);
        $user = Auth::user();
            //dd($event->isRegistrable());
        if(!$event->isRegistrable()){
            return response(['message' => __('This Event Cannot Regist or Unregist'),'data' => new UserEventResource($event)],403);
        }

        if($event->user_id == $user->id){
            return response(['message' => __('Cant Regist Own Event')],403);
        }
        
        if($event->isRegistered()){
            return response(['message' => __('Already Registered This Event')],403);
        }

        if(($event->Registered->count() >= $event->registration_limit_amount) && ($event->registration_limit_amount !== -1)){
            return response(['message' => __('活動報名名額已滿!!'),'data' => new UserEventResource($event)],403);
        }
        
        try{
            DB::beginTransaction();
            DB::table('user_registered_event')->insert([
                'event_id' => $event->event_id,
                'user_id' => $user->id, 
            ]);            
        }catch(\Exception $e){ 
                
            DB::rollback();            
            return response(['message' => __('An unexpected error has occured. Please try again')], 500);
        }

        if(($event->Registered->count() >= $event->registration_limit_amount) && ($event->registration_limit_amount !== -1)){
            DB::rollback();
            return response(['message' => __('活動報名名額已滿!!'),'data' => new UserEventResource($event)],403);            
        }else{
            DB::commit();
            $registeredevent = UserEvent::findorfail($request->event_id);
            return response(['message' => __('活動報名成功'),'data' => new UserEventResource($registeredevent)],200);   
        }
    }

    public function unregisterevent(Request $request){
        $this->validate($request, [
            'event_id'   => 'required'
          ]);
        $event = UserEvent::findorfail($request->event_id);
        $user = Auth::user();
            
        if(!$event->isRegistrable()){
            return response(['message' => __('This Event Cannot Regist or Unregist'),'data' => new UserEventResource($event)],403);
        }

        if($event->user_id == $user->id){
            return response(['message' => __('Cant Unregist Own Event')],401);
        }
        
        if(!$event->isRegistered()){
            return response(['message' => __('Did Not Registered This Event')],401);
        }

        $Unregisterevent = UserRegisteredEvent::where('event_id' , '=', $event->event_id)->where('user_id', '=' , $user->id)->firstorfail();
        //dd($Unregisterevent);
        if($Unregisterevent->delete()){
            $Unregisteredevent = UserEvent::findorfail($request->event_id);
            return response(['message' => __('event.unregistered_event'),'data' => new UserEventResource($Unregisteredevent)], 200);
        }else{
            return response(['message' => __('An unexpected error has occured. Please try again')], 500);
        }

    }


    public function getRegistered(Request $request){
        $this->validate($request, [
            'event_id'   => 'required'
          ]);
        $user = Auth::user();  
        $event = UserEvent::where('event_id' ,'=',$request->event_id)->firstorfail();
        if($user->id !== $event->user_id){
            return response(['message' => __('Not Event Owner')],401);
        }
        $RegisteredUserId = $event->Registered->pluck('user_id');
        
        $RegisteredUser = User::wherein('id',$RegisteredUserId)->Paginate(15);
        
        return new SocialUserResourceCollection($RegisteredUser);

    }

    public function getInterested(Request $request){
        $this->validate($request, [
            'event_id'   => 'required'
          ]);
        $user = Auth::user();  
        $event = UserEvent::where('event_id' ,'=',$request->event_id)->firstorfail();
        if($user->id !== $event->user_id){
            return response(['message' => __('Not Event Owner')],401);
        }
        $InterestedUserId = $event->Interested->pluck('user_id');
        
        $InterestedUser = User::wherein('id',$InterestedUserId)->Paginate(15);
        
        return new SocialUserResourceCollection($InterestedUser);

    }

    private function generateImage($image, $uuid)
    {
        if (isset($image)) {
            $imageName = str_random(22).'_'.str_random(22);
            $image_str=explode(',', $image);

            $imagedata = base64_decode($image_str[count($image_str)-1]);

            if (strlen($imagedata)>=1) {
                $image_info = getimagesizefromstring($imagedata);
            } else {
                return response(['message' => __('images.content_incorrect')], 500);
            }
            if (!$image_info) {
                return response(['message' => __('images.content_incorrect')], 500);
            }
            $image_type = explode('/', $image_info['mime'])[0];
            $subname = explode('/', $image_info['mime'])[1];
            
            if ($image_type !== 'image') {
                return response(['message' => __('images.type_incorrect')], 500);
            }

            $imageProperty = sprintf("?W=%d&H=%d", $image_info[0], $image_info[1]);
            
            $source = imagecreatefromstring($imagedata);
            ob_start();
            if($subname == 'png'){
                imagepng($source);
            }else{
                imagejpeg($source,null,100);
            }
            $image_contents = ob_get_clean();

            $savepath = "$uuid"."/"."$imageName.$subname";

            $disk = Storage::disk('gcs');
            if (!$disk->put($savepath, $image_contents)) {
                return response(['message' => __('An unexpected error has occured. Please try again')], 500);
            }
            $image_url = $disk->url($savepath).$imageProperty;

            
        }
        return $image_url;
    }

    private function fcm($fcm_data,$tokens,$title,$body){
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);

        $notificationBuilder = new PayloadNotificationBuilder($title);
        $notificationBuilder->setBody($body)
                            ->setIcon('myicon')
                            ->setSound('default');

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData($fcm_data);

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

        //$tokens = $request->token;

        $downstreamResponse = FCM::sendTo($tokens, $option, $notification, $data);

        $downstreamResponse->numberSuccess();
        
        if(count($downstreamResponse->numberFailure()) >= 1 ){
            $device = DeviceInfo::wherein('device_id',$downstreamResponse->tokensToDelete());
            $device->delete();
        }

        $downstreamResponse->numberModification();

        // return Array - you must remove all this tokens in your database
        //$downstreamResponse->tokensToDelete();
        
        // return Array (key : oldToken, value : new token - you must change the token in your database)
        $downstreamResponse->tokensToModify();
        
        // return Array - you should try to resend the message to the tokens in the array
        $downstreamResponse->tokensToRetry();

        // return Array (key:token, value:error) - in production you should remove from your database the tokens
        $downstreamResponse->tokensWithError();
    }
}
