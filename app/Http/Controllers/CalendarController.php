<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use DB;
use Auth;
use App\User;
use App\Models\UserEvent;
use App\Models\UserInterestedEvent;
use App\Models\UserRegisteredEvent;
use App\Models\InviteHistory;

class CalendarController extends Controller
{
    public function get(Request $request){
            $this->validate($request, [
                'month' => 'required'
            ]);
            $user = Auth::user();
            $month_start=strtotime($request->month)*1000;
            $month_end=strtotime(($request->month."+1 month"))*1000;
            $accept_list_sql="select tb1.id as invite_history_id, tb2.uuid as sender_id,tb2.nick_name as sender_name,tb2.gender as sender_gender,tb4.photo as sender_photo,tb3.uuid as receiver_id,tb3.nick_name as receiver_name,tb3.gender as receiver_gender,tb5.photo as receiver_photo,game_name,tb1.location,start_time,end_time from invite_history as tb1 join users as tb2 on tb1.sender_id = tb2.id join users as tb3 on tb1.receiver_id = tb3.id left join user_images as tb4 on tb1.sender_id = tb4.user_id left join user_images as tb5 on tb1.receiver_id = tb5.user_id "
                    . "where ((start_time <= $month_start and start_time >= $month_end) or "
                    . "($month_start <= start_time and $month_end >= start_time) or"
                    . "($month_start <= end_time and $month_end >= start_time))"                
                    . "and (receiver_id = $user->id or sender_id = $user->id) and status = 'accept'";
            $result['data']['accept_list']=collect(DB::select(DB::Raw($accept_list_sql)));
            
            $canceled_list_sql="select tb1.id as invite_history_id,tb2.uuid as sender_id,tb2.nick_name as sender_name,tb2.gender as sender_gender,tb5.photo as sender_photo,tb3.uuid as receiver_id,tb3.nick_name as receiver_name,tb3.gender as receiver_gender,tb6.photo as receiver_photo,game_name,tb1.location,start_time,end_time,tb4.nick_name as canceled_by from invite_history as tb1 join users as tb2 on tb1.sender_id = tb2.id join users as tb3 on tb1.receiver_id = tb3.id join users as tb4 on tb1.canceled_by = tb4.id left join user_images as tb5 on tb1.sender_id = tb5.user_id left join user_images as tb6 on tb1.receiver_id = tb6.user_id "
            . "where ((start_time <= $month_start and start_time >= $month_end) or "
            . "($month_start <= start_time and $month_end >= start_time) or"
            . "($month_start <= end_time and $month_end >= start_time))"     
            . "and (receiver_id = $user->id or sender_id = $user->id) and status = 'canceled'"; 
            $result['data']['canceled_list'] = collect(DB::select(DB::Raw($canceled_list_sql)));
            
            $event_list_sql="select event_id as id,tb2.uuid as user_id,event_name,start_time,end_time,tb1.location,detail,pic1,pic2,pic3,tb1.create_time,tb1.modify_time from user_event as tb1 join users as tb2 on tb1.user_id = tb2.id "
            . "where ((start_time <= $month_start and start_time >= $month_end) or "
            . "($month_start <= start_time and $month_end >= start_time) or"
            . "($month_start <= end_time and $month_end >= start_time))"                
            . "and user_id = $user->id";
            $result['data']['event_list'] = collect(DB::select(DB::Raw($event_list_sql)));
            
            $Interested_List_sql="SELECT tb1.id ,tb3.nick_name as event_hoster,tb3.uuid as hoster_id ,tb2.event_id,tb2.event_name,tb2.location,tb2.start_time,tb2.end_time,tb2.detail,tb2.pic1,tb2.pic2,tb2.pic3 FROM user_interested_event as tb1 left join user_event as tb2 on tb1.event_id = tb2.event_id left join users as tb3 on tb2.user_id = tb3.id "
            . " where ((start_time <= $month_start and start_time >= $month_end) or "
            . "($month_start <= start_time and $month_end >= start_time) or"
            . "($month_start <= end_time and $month_end >= start_time))"                
            . "and tb1.user_id = $user->id";
            $result['data']['interested_event_list'] = collect(DB::select(DB::Raw($Interested_List_sql)));
            
            $Registered_List_sql="SELECT tb1.id ,tb3.nick_name as event_hoster,tb3.uuid as hoster_id ,tb2.event_id,tb2.event_name,tb2.location,tb2.start_time,tb2.end_time,tb2.detail,tb2.pic1,tb2.pic2,tb2.pic3 FROM user_registered_event as tb1 left join user_event as tb2 on tb1.event_id = tb2.event_id left join users as tb3 on tb2.user_id = tb3.id "
            . " where ((start_time <= $month_start and start_time >= $month_end) or "
            . "($month_start <= start_time and $month_end >= start_time) or"
            . "($month_start <= end_time and $month_end >= start_time))"                
            . "and tb1.user_id = $user->id";
            $result['data']['registered_event_list'] = collect(DB::select(DB::Raw($Registered_List_sql)));

            //$result['data'] = $accept_list->merge($canceled_list);

            return response(['data' => $result['data']], 200);


    }
}
