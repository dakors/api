<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Illuminate\Support\Facades\Storage;
use App;
use App\Models\GameNameList;
use App\Models\UserGameList;

//use App\Models\UserBlog;
//use App\Http\Resources\UserBlogResourceCollection;

class GameListController extends Controller
{
    public $lang =['en' => 'enUs','zh-TW' => 'zhTW'];

    public function __construct(Request $request)
    {
        $this->locale = ($request->hasHeader('X-localization')) ? $request->header('X-localization') : 'zh-TW';
    }

    public function getList(Request $request)
    {
        $gamelist['data']=[];
        if (!empty($request['keyword'])) {
            $keyword = $request['keyword'];
            //$gamelist['data']=[];
            $nameFromUsers = collect(UserGameList::select('game_name as GameName')
                                        ->where('game_name','like','%'.$keyword.'%')
                                        ->groupBy('GameName')
                                        ->get()->toArray());
            //DD($nameFromUsers);                                    
            $nameFromData = collect(GameNameList::select($this->lang[$this->locale].' as GameName')
                                      ->where('enUs', 'like', '%'.$keyword.'%')
                                      ->orwhere('zhTw', 'like', '%'.$keyword.'%')
                                      ->orwhere('acronyms', 'like', '%'.$keyword.'%')
                                      ->get()->toArray());
            
            $gameNames = $nameFromData->merge($nameFromUsers)->unique('GameName')->values()->all();

            //dd($gameNames);
            $gamelist['data']= $gameNames;
            return response()->json($gamelist);
        } else {
            return response()->json($gamelist);
        }
    }
}
