<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use DB;
use Auth;
use App\User;
use App\Models\UserBlog;
use App\Models\UserReport;
use App\Models\BlogComment;

class ReportController extends Controller
{
    public function comment(Request $request){
        $this->validate($request, [
            'commentId'   => 'required'
          ]);

        $comment = BlogComment::findorfail($request['commentId']);
        //dd($comment->user_id);
        $report = UserReport::create([
            'user_id' => Auth::user()->id,
            'accused_person_id' => $comment->user_id,
            'content' => Json_encode($comment->toArray(),JSON_UNESCAPED_SLASHES),
            'type' => 'comment',
        ]);

        if($report->save()){
            return response(['message' => __('Reported')], 200);
        }else{
            return response(['message' => __('An unexpected error has occured. Please try again')], 500);
        }
    }

    public function blog(Request $request){
        $this->validate($request, [
            'blog_id'   => 'required'
          ]);
        
          $blog = UserBlog::findorfail($request->blog_id);
          //dd($blog->toArray());
          $report = UserReport::create([
            'user_id' => Auth::user()->id,
            'accused_person_id' => $blog->user_id,
            'content' => Json_encode($blog->toArray(),JSON_UNESCAPED_SLASHES),
            'type' => 'blog',
        ]);

        if($report->save()){
            return response(['message' => __('Reported')], 200);
        }else{
            return response(['message' => __('An unexpected error has occured. Please try again')], 500);
        }
    }

    public function profile(Request $request){
        $this->validate($request, [
            'user_id'   => 'required'
          ]);
        $user = User::where('uuid','=',$request->user_id)->firstorfail();
        
        $content = $user->toArray();
        $content['photo'] = $user->userImages->toArray()['photo'];
        $content['background'] = $user->userImages->toArray()['background'];
        $content['game_list'] = $user->userGameList->toArray();
        $report = UserReport::create([
            'user_id' => Auth::user()->id,
            'accused_person_id' => $user->id,
            'content' => Json_encode($content,JSON_UNESCAPED_SLASHES),
            'type' => 'profile',
        ]);

        if($report->save()){
            return response(['message' => __('Reported')], 200);
        }else{
            return response(['message' => __('An unexpected error has occured. Please try again')], 500);
        }
    }

    public function chat(Request $request){

    }
}
