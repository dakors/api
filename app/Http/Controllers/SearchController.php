<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\User;
use App\Models\GameNameList;
use App\Models\SearchLog;
use App\Http\Resources\SearchUserResourceCollection;
use Auth;
use Carbon\Carbon;
use DB;

class SearchController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    //
    public function search()
    {
        $attributes = $this->validate(
            request(),
            [
                'minAge' => ['integer', 'min:0', 'max:99'],
                'maxAge' => ['integer', 'min:0', 'max:99',
                                function ($attribute, $value, $fail) {
                                    if (Input::has('minAge')) {
                                        $minAge = Input::get('minAge');
                                        if ($minAge > $value) {
                                            return $fail($attribute.' is invalid.');
                                        }
                                    }
                                }
                            ],
                'gender' => ['in:0,1,2'],
                'location' => ['integer', 'gte:0', 'lte:9999'],
                'username' => [],
                'game_name' => []
            ]
        );

        $log = new SearchLog;
        $log->user_id = Auth::user()->id;
        $log->gender = $attributes['gender'];
        $log->minAge = $attributes['minAge'];
        $log->maxAge = $attributes['maxAge'];
        $log->location = $attributes['location'];
        $log->name = $attributes['username'];
        $log->game_name = $attributes['game_name'];
        $log->save();

        $result = self::searchForUsers($attributes);
        return new SearchUserResourceCollection($result);
    }

    public static function searchForUsers($searchParams)
    {
        $userId = Auth::user()->id;
        //Ensure that user search do not contain herself.
        $query = User::where('id', '!=', $userId);

        if (array_key_exists('username', $searchParams) && $searchParams['username'] != '') {
            $query->where('nick_name', 'LIKE', '%'.$searchParams['username'].'%');
        }

        if (array_key_exists('gender', $searchParams) && $searchParams['gender'] != '' && $searchParams['gender'] != 0) {
            $query->where('gender', $searchParams['gender']);
        }

        if (array_key_exists('minAge', $searchParams) && $searchParams['minAge'] != '') {
            $query->where('age', '>=', $searchParams['minAge']);
        }

        if (array_key_exists('maxAge', $searchParams) && $searchParams['maxAge'] != '') {
            $query->where('age', '<=', $searchParams['maxAge']);
        }

        if (array_key_exists('location', $searchParams) && $searchParams['location'] != '' && $searchParams['location'] != 0) {
            $query->where('location', $searchParams['location']);
        }

        if (array_key_exists('game_name', $searchParams) && $searchParams['game_name'] != '') {
            $query->whereHas(
                'userGameList',
                function ($q) use (&$searchParams) {
                    $gamelist = GameNameList::where('enUs', 'like', '%'.$searchParams['game_name'].'%')
                                            ->orwhere('zhTw', 'like', '%'.$searchParams['game_name'].'%')
                                            ->orwhere('acronyms', 'like', '%'.$searchParams['game_name'].'%')
                                            ->get()->toArray();

                    $q->where('game_name', 'like', '%'.$searchParams['game_name'].'%');
                    if (count($gamelist) !== 0) {
                        foreach ($gamelist as $game) {
                            $q->orwhere('game_name', 'like', '%'.$game['enUS'].'%')
                              ->orWhere('game_name', 'like', '%'.$game['zhTW'].'%')
                              ->orWhere('game_name', 'like', '%'.$game['acronyms'].'%');
                        }
                    }
                }
            );
        }

        //$blockUserIds = Auth::user()->blockAndBlockByList();
        $query->whereNotIn('id', Auth::user()->blockAndBlockByList());
        //$result = $query->inRandomOrder(Carbon::now()->timestamp)->Paginate(15);
        $result = $query->inRandomOrder('1234')->Paginate(15);
        return $result;


        // Shuffle after random because it is ordered by id.
        // if ($result->count() < 15) {
        //     return $result->random($result->count())->shuffle();
        // } else {
        //     return $result->random(15)->shuffle();
        // }
    }
}
