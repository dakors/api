<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\User;
use App\Models\SocialAccounts;
use App\Http\Resources\SocialUserResourceCollection;
use App\Http\Resources\SocialUserResource;
use App\Http\Resources\SearchUserResourceCollection;
use App\Http\Resources\RecommendedUserResourceCollection;
use App\Models\Friend;
use App\Models\GameNameList;
use App\Models\UserGameList;
use App\Models\UserBlog;
use App\Models\UserOptions;
use App\Models\RecommendedUser;
use Carbon\Carbon;
use App\Http\Resources\UserResource;
use Laravel\Socialite\Facades\Socialite;

class UserController extends Controller
{
    private $provider_name =['facebook'=>'fb_id','google' =>'google_id'];

    /*public function __construct()
    {
        $this->middleware('auth');
    }*/

    public function view()
    {
        
        $data = $this->validate(request(), [
                'user_id'   => 'required'
              ]);

        $user =User::where('uuid', '=', $data['user_id'])
                ->wherenotin('id',Auth::user()->blockAndBlockByList())->firstorfail();
        
        if (count($user) === 0) {
            return response(['message' => __('User not found')], 404);
        }

        return new SocialUserResource($user);
    }

    public function update(Request $request)
    {
        $this->validate($request, [
                    'nick_name' => ['required','max:36'],
                    'gender' => ['required', 'in:0,1,2'],
                    'age' => ['required', 'integer', 'min:0', 'max:99'],
                    'location' => ['required', 'integer', 'gte:0', 'lte:9999'],
                    'introduction' => 'max:250',
                ]);

        $user = Auth::user();
        if ($user === null) {
            return response(['message' => __('User not found')], 404);
        }

        $user->nick_name = $request->nick_name;
        $user->gender = $request->gender;
        $user->age = $request->age;
        $user->location = $request->location;
        $user->introduction = $request->introduction ? $request->introduction : '';

        if ($user->save()) {
            return response(['message' => __('user.updated')], 200);
        } else {
            return response(['message' => __('An unexpected error has occured. Please try again')], 500);
        }
    }

    /*public function getRecommendedlist()
    {
        $user =Auth::user();

        $userId = $user->id;

        $userGameList = $user->userGameList()->pluck('game_name')->toArray();
        $userBlockList = $user->blockList()->pluck('block_id')->toArray();
        $userFriendList = $user->friends()->pluck('id')->toArray();
        $userRequestList = $user->beRequests()->pluck('users.id')->toArray();
        

        $userFollowingList = $user->following()->pluck('user_id')->toArray();

        $columns = array('enUs','zhTw','acronyms');
        $recommededusers = collect(RecommendedUser::select('user_id')
                            ->wherenotIN('user_id', $userBlockList)
                            ->wherenotIN('user_id', $userFriendList)
                            ->wherenotIN('user_id', $userFollowingList)
                            ->wherenotIN('user_id', $userRequestList)
                            ->where('user_id', '!=', $userId)
                            ->get()->toArray())->flatten()->all();
        if (!empty($userGameList)) {
            $games = GameNameList::select('enUs', 'zhTw', 'acronyms')->where(function ($query) use ($columns, $userGameList) {
                foreach ($columns as $column) {
                    foreach ($userGameList as $game) {
                        $query->orwhere($column, 'like', '%'.$game.'%');
                    }
                }
            });

            $userGames=collect(collect($games->get()->toArray())->flatten()->all());

            $gamelist=$userGames->merge($userGameList)->unique()->toArray();

            $gameUsers_query= UserGameList::select('user_id', DB::raw('count(user_id) as total'))
                ->wherenotIN('user_id', $userBlockList)
                ->wherenotIN('user_id', $userFriendList)
                ->wherenotIN('user_id', $userFollowingList)
                ->wherenotIN('user_id', $userRequestList)
                ->where('user_id', '!=', $userId)
                ->orderby('total', 'desc')->limit(10)->whereNotIn('user_id', $recommededusers)->inRandomOrder(Carbon::now()->timestamp)->groupBy('user_id')->where(function ($query) use ($gamelist) {
                foreach ($gamelist as $game) {
                    $query->orwhere('game_name', 'like', $game);
                }
            });
            
            $gameUsers = $gameUsers_query->get()->pluck('user_id')->toArray();
            $maxRecommended = 5;
            if (count($recommededusers) < 5) {
                $maxRecommended = count($recommededusers);
            }
            $users_id = collect(collect($recommededusers)->random($maxRecommended)->flatten()->all())->merge($gameUsers)->unique()->shuffle()->toArray();

            $users = User::whereIn('id', $users_id)->with('UserImages');


        } else {
            $maxRecommended = 5;
            if (count($recommededusers) < 5) {
                $maxRecommended = count($recommededusers);
            }
            $users_id = collect(collect($recommededusers)->random($maxRecommended)->flatten()->all());
            $users = User::whereIn('id', $users_id)->inRandomOrder(Carbon::now()->timestamp)->with('UserImages');


        }
        if(count($users->get()) >= 1){
            return new SocialUserResourceCollection($users->get());
        }else{
            return response(['message' => __('user.recommended_error')], 500);
        }
    }*/

    public function getRecommendedlist(){
        switch(AUTH::check()){
        case true:
            $user =Auth::user();

            $userId = $user->id;
            $userBlockList = $user->blockList()->pluck('block_id')->toArray();
            $rank = collect(DB::select("select user_id,sum(s) as total from (select tb1.user_id,count(*) * 1 as s from user_blog as tb1 left join blog_liked as tb2 on tb1.blog_id = tb2.blog_id and tb1.user_id != tb2.user_id  where tb1.create_date >= DATE_SUB(NOW(),INTERVAL 30 day) group by tb1.user_id
            UNION ALL
            select user_id,count(*) * 2 as s from user_blog as tb1 where create_date >= DATE_SUB(NOW(),INTERVAL 30 day) group by tb1.user_id) as rank_s group by rank_s.user_id order by total desc limit 50"))
            ;
            //dd($userId);
            //dd($recommededusers->pluck('user_id')->toArray());

            $recommededusers = User::where('id','<>',$userId)
                                ->wherenotin('id',$userBlockList)
                                ->wherein('id',$rank->pluck('user_id')
                                ->toArray())->inRandomOrder(Carbon::now()->timestamp)->take(20)->get();
            //dd($recommededusers);

            if(count($recommededusers) >= 1){
                    return new SocialUserResourceCollection($recommededusers);
                }else{
                    return response(['message' => __('user.recommended_error')], 500);
                }
        case false:{
            $recommendeduser_id = RecommendedUser::get()->pluck('user_id')->toArray();
            $recommededusers = User::wherein('id',$recommendeduser_id)->get();            
            return new RecommendedUserResourceCollection($recommededusers);
        }

        }
    }

    public function socialBind(request $request)
    {
        $this->validate($request, [
                'access_token' => 'required',
                'provider' => 'required|in:facebook,google',
        ]);

        $user = Auth::user();

        if ($user === null) {
            return response(['message' => __('User not found')], 404);
        }

        /*if (!empty($user->fb_id) || !empty($user->google_id)) {
            return response(['message' => __('user.social_auth_already_exist')], 500);
        }*/
        //dd(count(SocialAccounts::where('user_id','=',$user->id)->get()));
        if(count(SocialAccounts::where('user_id','=',$user->id)->get()) >= 1){
            return response(['message' => __('user.social_auth_already_exist')], 500);
        }

        try {
            $providerUser = Socialite::driver($request->provider)->userFromToken($request->access_token);
        } catch (\Exception $e) {
            return response(['message' => __('user.social_auth_error')], 500);
        }

        if (SocialAccounts::where('provider','=',$request->provider)->where('provider_user_id','=',$providerUser->id)->first() !== null) {
            return response(['message' => __('user.social_auth_already_has_account')], 500);
        }

        $SocialAccount =SocialAccounts::create([
                'user_id' => $user->id,
                'provider'=> $request->provider,
                'provider_user_id' => $providerUser->id,
            ]);


        if ($SocialAccount->wasRecentlyCreated) {
            return response(['message' => __('user.social_auth_binded')], 200);
        } else {
            return response(['message' => __('An unexpected error has occured. Please try again')], 500);
        }
    }

    public function options(Request $request){
        $this->validate($request, [
            'isStrangerAllowed' => 'required|in:true,false',
            'reminder_time' => 'required',
        ]);
        $user = Auth::user();
        
        $user->options->isStrangerAllowed = $request->isStrangerAllowed;
        $user->options->reminder_time = $request->reminder_time;
        if($user->options->save()){
            return response(['message' => __('user.options_updated')], 200);
        }else{
            return response(['message' => __('An unexpected error has occured. Please try again')], 500);
        }

    }
    
}
