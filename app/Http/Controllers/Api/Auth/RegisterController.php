<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\User;
use DB; 
use App\Models\UserImages;
Use App\Models\UserOptions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Laravel\Passport\Client;


class RegisterController extends Controller
{
    use IssueTokenTrait;

	private $client;

	public function __construct(){
		$this->client = Client::find(1);
	}

    public function register(Request $request){

    	$this->validate($request, [
    		'name' => 'required',
    		'email' => 'required|email|unique:users,email',
    		'password' => 'required|min:6|confirmed'
      ]);
      
      $url = 'https://www.google.com/recaptcha/api/siteverify';

      $data = [
              'secret' => '6LcOcrMUAAAAAOvG86YXCzxJNgIH11_fjW5CBau3',
              'response' => request('recaptcha')   
              ];
      //dd($data);
      $options = [
                  'http' => [
                            'header' => 'Content-type: application/x-www-form-urlencoded\r\n',
                            'method' => 'POST',
                            'content' => http_build_query($data)
                            ]
                  ];

      $context = stream_context_create($options);
      $result = json_decode(file_get_contents($url,false,$context));
      //dd($result);
      if(!$result->success && $result <= 0.5){
        return response(['message' => __('Recaptcha Error')], 401);
      }
      $DataCenterId = config('app.DataCenterId');
      $MachineId = config('app.MachineId');
      $IdWorker = \wantp\Snowflake\IdWorker::getIns($DataCenterId,$MachineId);
      $uuid = strval($IdWorker->id());
      //dd($user_id);
      //dd(bcrypt($request['password']));
    	$user = User::create([
            'uuid' => $uuid,
            'nick_name' => $request['name'],
            'email' => $request['email'],
            'gb_id' => $request['email'],
            'password' => bcrypt($request['password']),            
        ]);
        
      if($user->wasRecentlyCreated === True){
          UserImages::create([
            'user_id' => $user->id,
            'photo' => '',
            'background' => '',
          ]);
          UserOptions::create([
            'user_id' => $user->id,
            'isStrangerAllowed' => true,
            'reminder_time' => -1,
          ]);
          //return $this->issueToken($request, 'password');
          $RecommendedUser =DB::table('recommend_user')->select(DB::raw("$user->id as fans_id , user_id"))->get();
          $RecommendedUser=collect($RecommendedUser)->map(function($x){ return (array) $x; })->toArray();
          DB::table('fans_list')->insert($RecommendedUser);
          return response(['message' => __('user.registration_success')], 200);
      } else{
        return response(['message' => __('An unexpected error has occured. Please try again')], 500);
      } 
          
    }

    public function mobileregister(Request $request){

    	$this->validate($request, [
    		'name' => 'required',
    		'email' => 'required|email|unique:users,email',
    		'password' => 'required|min:6|confirmed'
      ]);
            
      $DataCenterId = config('app.DataCenterId');
      $MachineId = config('app.MachineId');
      $IdWorker = \wantp\Snowflake\IdWorker::getIns($DataCenterId,$MachineId);
      $uuid = strval($IdWorker->id());
      //dd($user_id);
      //dd(bcrypt($request['password']));
    	$user = User::create([
            'uuid' => $uuid,
            'nick_name' => $request['name'],
            'email' => $request['email'],
            'gb_id' => $request['email'],
            'password' => bcrypt($request['password']),            
        ]);
        
      if($user->wasRecentlyCreated === True){
          UserImages::create([
            'user_id' => $user->id,
            'photo' => '',
            'background' => '',
          ]);
          UserOptions::create([
            'user_id' => $user->id,
            'isStrangerAllowed' => true,
            'reminder_time' => -1,
          ]);
          $RecommendedUser =DB::table('recommend_user')->select(DB::raw("$user->id as fans_id , user_id"))->get();
          $RecommendedUser=collect($RecommendedUser)->map(function($x){ return (array) $x; })->toArray();
          DB::table('fans_list')->insert($RecommendedUser);
          return response(['message' => __('user.registration_success')], 200);
      } else{
        return response(['message' => __('An unexpected error has occured. Please try again')], 500);
      } 
          
    }
}