<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;
use App\User;
use Mail;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\Client;
use App\Mail\resetpassword;

class PasswordController extends Controller
{
    public function reset(Request $request){
        $this->validate($request, [
    		'email' => 'required|email'
        ]);
        
        $user = User::where('gb_id', $request->email)->first();
        if($user === Null){
            return response(['message' => __('passwords.email_incorrect')], 404);
        }
        $newPassword=str_random(6);
        //$newPassword_hash=Hash::make($request['password']);

        if(!$user->update(['password' => bcrypt($newPassword)])){
            return response(['message' => __('An unexpected error has occured. Please try again')], 500);
        }

        Mail::to($request->email)->send(new resetpassword($user,$newPassword));
        if(Mail::failures()){
            return response(['message' => __('An unexpected error has occured. Please try again')], 500);
        }else{            
            return response(['message' => __('passwords.sent')], 200);
        }
        

        

    }

    public function change(Request $request){
        $this->validate($request, [
            'oldpassword' => 'required|min:6',
    		'password' => 'required|min:6|confirmed'
        ]);
        
        $user = Auth::user();
        //dd($user->password);
        $oldPassword = $request['oldpassword'];
        $newPassword = $request['password'];
        //dd(Hash::check($oldPassword,$user->password));
        if(!Hash::check($oldPassword,$user->password)){
            return response(['message' => __('passwords.old_password_incorrect')], 500);
        }
        
        //$newPassword = bcrypt($request['password']);
        if(empty($user->password)){            
            return response(['message' => __('passwords.cannot_use')], 500);
        }

        if(Hash::check($newPassword,$user->password)){
            return response(['message' => __('passwords.same')], 500);
        }

        if($user->update(['password'=> Hash::make($newPassword)])){
            return response(['message' => __('passwords.change')], 200);
        }else{
            return response(['message' => __('An unexpected error has occured. Please try again')], 500);
        }

    }
}
