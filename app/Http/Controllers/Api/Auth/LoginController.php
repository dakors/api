<?php
namespace App\Http\Controllers\Api\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Laravel\Passport\Client;
use App\User;
class LoginController extends Controller
{
    use IssueTokenTrait;
	private $client;
	public function __construct(){
		$this->client = Client::find(1);
	}
    public function login(Request $request){
    	$this->validate($request, [
    		'email' => 'required',
    		'password' => 'required'
		]);
		$user = User::where('gb_id','=',$request->email)->first();
		if($user === null){
			return response(['message' => __('auth.failed')], 401);
		}
		//dd(filter_var($user->activated,FILTER_VALIDATE_BOOLEAN));
		//dd(config('app.EmailActivated'));
		if(filter_var($user->activated,FILTER_VALIDATE_BOOLEAN) || !config('app.EmailActivated') ){
			return $this->issueToken($request, 'password');
		}else{
			return response(['message' => __('user.not_activated')], 401);
		}
        
	}
	
    public function refresh(Request $request){
    	$this->validate($request, [
    		'refresh_token' => 'required'
    	]);
    	return $this->issueToken($request, 'refresh_token');
	}
	
    public function logout(Request $request){
    	$accessToken = Auth::user()->token();
    	DB::table('oauth_refresh_tokens')
    		->where('access_token_id', $accessToken->id)
    		->update(['revoked' => true]);
    	$accessToken->revoke();
    	return response()->json([], 204);
    }
}