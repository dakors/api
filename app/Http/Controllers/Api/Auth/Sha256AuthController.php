<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Api\Auth\IssueTokenTrait;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Laravel\Passport\Client;

class Sha256AuthController extends Controller
{
	use IssueTokenTrait;

	private $client;

	public function __construct(){
		$this->client = Client::find(1);
	}

  public function sha256Auth(Request $request){

			$this->validate($request, [
				'email'   => 'required|email',
				'password' => 'required|min:6'
			  ]);

	    	return $this->issueToken($request, 'sha256');
  }
}
