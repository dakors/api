<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Api\Auth\IssueTokenTrait;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Laravel\Passport\Client;

class SocialAuthController extends Controller
{

	use IssueTokenTrait;

	private $client;

	public function __construct(){
		$this->client = Client::find(1);
	}

    public function socialAuth(Request $request){

    	$this->validate($request, [
			'access_token' => 'required',
			'provider' => 'required|in:facebook,google',
		]);

    	return $this->issueToken($request, 'social');
    }
	
}