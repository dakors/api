<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Http\Resources\SocialUserResourceCollection;
use App\Models\UserBlockList;
use App\Models\Fan;
use App\Models\Friend;

class BlockListController extends Controller
{

    private $relationship;

    public function getList(request $request){
        $orderby =  isset($request->orderby) ? $request->orderby : 'NameAsc';  
        $filter = isset($request->filter) ? $request->filter : '';

        $user = Auth::user();
        $blockList=$user->blockList()->where(function($query) use ($filter){
            if(!empty($filter)){
                $query->where('nick_name','like','%'.$filter.'%');                    
            }
        });             
        switch($orderby){
            case 'NameDesc':                
            return new SocialUserResourceCollection($blockList->orderby('nick_name','DESC')->Paginate(30));
            break;
            default:
            case 'NameAsc':                
            return new SocialUserResourceCollection($blockList->orderby('nick_name','ASC')->Paginate(30));
            break;
        }

    }

    public function block(){
        $data = $this->validate(request(), [
            'user_id' => 'required'
        ]);

        $blockId = $data['user_id'];
        //if(!User::all()->contains($blockId))
        //    return response(['message' => __('User not found')], 404);

        $userToBlock = User::where('uuid' ,'=', $blockId)->first();
        //dd($userToBlock->id);
        if($userToBlock === null){
            return response(['message' => __('User not found')], 404);
        }
        $userId = Auth::user()->id;
        
        if($this->isBlocked($userId,$userToBlock->id)){
            return response(['message' => sprintf(__('blockList.block_fail_duplicate'), $userToBlock->nick_name)], 500);
        }

        $result = UserBlockList::firstorCreate([
            'user_id' => $userId,
            'block_id' => $userToBlock->id
        ]);
        
        if($result->wasRecentlyCreated === True){
            $unfollow = Fan::where([['user_id', '=', $userToBlock->id],['fans_id' ,'=' ,$userId]])->delete();
            $unfriend = Friend::where([['requester','=',  $userToBlock->id],['receiver' , '=', $userId]])->orWhere([['requester', '=', $userId],['receiver' , '=', $userToBlock->id]])->update(['status' => 'canceled']);
            return response(['message' => sprintf(__('blockList.add_block_success'), $userToBlock->nick_name)], 200);
        }else{
            return response(['message' => __('An unexpected error has occured. Please try again')], 500);
        }
    }

    public function unblock(){
        $data = $this->validate(request(), [
            'user_id' => 'required'
        ]);

        $unblockId = $data['user_id'];
        //if(!User::all()->contains($unblockId))
        //    return response(['message' => __('User not found')], 404);
    
        $userToUnblock = User::where('uuid','=',$unblockId)->first();
        if($userToUnblock === null){
            return response(['message' => sprintf(__('blockList.block_fail_duplicate'), $userToUnBlock->nick_name)], 500);
        } 
        $userId = Auth::user()->id;
        
        if(!$this->isBlocked($userId,$userToUnblock->id)){            
            return response(['message' => sprintf(__('blockList.unblock_fail_not_blocking'), $userToUnblock->nick_name)], 404);
        }
        //$Unblock = UserBlockList::where([['user_id' => $userId],['block_id' => $userToUnblock->id]])->first();
        if($this->relationship->delete())
            return response(['message' => sprintf(__('blockList.unblock_success'), $userToUnblock->nick_name)], 200);
        else
            return response(['message' => __('An unexpected error has occured. Please try again')], 500);

    }

    public function isBlocked($userId,$blockId){
       
        $this->relationship = UserBlockList::where([['user_id','=',$userId],['block_id', '=', $blockId]])->first();
        
        return $this->relationship !== Null;
        
    }
}
