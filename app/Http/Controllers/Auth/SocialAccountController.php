<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Socialite;
use Session;
use Auth;
use App\User;
use App\Models\UserImages;
use App\SocialAccountService;

class SocialAccountController {
    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return Response
     */
    public function redirectToProvider($provider)
    {
        return \Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information
     *
     * @return Response
     */
    public function handleProviderCallback(\App\SocialAccountService $accountService, $provider)
    {

        try {
            $user = \Socialite::with($provider)->user();
        } catch (\Exception $e) {
            return redirect('/login');
        }

        $authUser = $accountService->findOrCreate(
            $user,
            $provider
        );
        if($authUser->wasRecentlyCreated === True){
          UserImages::create([
            'user_id' => $authUser->id,
            'photo' => 'http://api.gamerbox.com.tw/images/default_photo.png',
            'background' => '',
          ]);
        }
        //dd($authUser);
        Auth::guard('web')->login($authUser, true);
        return redirect('/');
    }
}
