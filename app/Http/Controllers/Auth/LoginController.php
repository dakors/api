<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Auth;
use App\User;


class LoginController extends Controller
{
    public function __construct(){
      $this->middleware('guest:web')->except('logout');
    }

    public function showLoginForm()
    {
      return view('auth.login');
    }

    public function login(Request $request){
      // Validate the form data
      $this->validate($request, [
        'email'   => 'required|email',
        'password' => 'required|min:6'
      ]);
      $password=strtoupper(hash('sha256',$request->password));
      $remember_me = $request->has('remember') ? true : false;
      // Attempt to log the user in
      /*if (Auth::guard('web')->attempt(['email' => $request->email, 'password' => $password], $request->remember)) {
        // if successful, then redirect to their intended location
        return redirect()->intended(route('/'));
      }else {
        return redirect()->back()->withInput($request->only('email', 'remember'));
      }*/





      $Account=User::where('gb_id','=',$request->email)->where('password','=',$password)->first();
      if($Account){
        $images = $Account->userImages->toArray();
        Session::put('Avatar',$images['photo']);
        Auth::guard('web')->login($Account, $remember_me);
        return redirect('/');
      }else{
          return redirect()->back()->withInput($request->only('email', 'remember'));
      }



      // if unsuccessful, then redirect back to the login with the form data

    }

    public function logout(Request $request){
      Auth::logout();
      return redirect('/');
    }



}
