<?php

namespace App\Http\Controllers;

use App\Models\UserGameList;
use Illuminate\Http\Request;
use Auth;

class UserGameListController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = $this->validate(request(), [
            'game_name' => 'required|max:36'
        ]);
        
        $user = Auth::user();
        //dd(collect($user->userGameList->toArray())->firstwhere('game_name','666'));
        if ($user == null) {
            return response(['message' => __('User not found')], 404);
        }
        if(count($user->userGameList)>=10){
            return response(['message' => sprintf(__('userGameList.Exceeding_quantity_limit'), $data['game_name'])], 400);
        } 
        if(collect($user->userGameList->toArray())->firstwhere('game_name',$data['game_name']) !== null){
            return response(['message' => sprintf(__('userGameList.already_exist'), $data['game_name'])], 400);
        }
        $game = new UserGameList;
        $game->user_id = $user->id;
        $game->game_name = $data['game_name'];

        if ($game->save()) {
            return response(['message' => sprintf(__('userGameList.added'), $data['game_name'])], 200);
        } else {
            return response(['message' => __('An unexpected error has occured. Please try again')], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserGameList  $userGameList
     * @return \Illuminate\Http\Response
     */
    public function delete()
    {
        $data = $this->validate(request(), [
            'game_name' => 'required'
        ]);

        $user = Auth::user();
        if ($user == null) {
            return response(['message' => __('User not found')], 404);
        }

        $game = UserGameList::where('game_name', $data['game_name'])
                    ->where('user_id', $user->id)
                    ->first();
        if ($game === null) {
            return response(['message' => sprintf(__('userGameList.not_found'), $data['game_name'])], 404);
        }

        if ($game->delete()) {
            return response(['message' => sprintf(__('userGameList.removed'), $data['game_name'])], 200);
        } else {
            return response(['message' => __('An unexpected error has occured. Please try again')], 500);
        }
    }
}
