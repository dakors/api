<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use app\User;
use Auth;
use DB;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Illuminate\Support\Facades\Storage;
use App\Models\UserImages;
use App\Http\Resources\UserImagesResource;

class ImagesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function upload(request $request)
    {
        $this->validate($request, ['type' => 'required|in:photo,background',
                                    'image' => 'required']);

        $userImages =Auth::user()->userImages;
        if ($request->type === 'photo') {
            $path = $userImages->getPhotoFilePath();
        } else {
            $path = $userImages->getBackgroundFilePath();
        }

        $imageName = str_random(22).'_'.str_random(22);

        $image_str=explode(',', $request->image);

        $imagedata = base64_decode($image_str[count($image_str)-1]);

        if (strlen($imagedata)>=1) {
            $image_info = getimagesizefromstring($imagedata);
        } else {
            return response(['message' => __('images.content_incorrect')], 500);
        }
        if (!$image_info) {
            return response(['message' => __('images.content_incorrect')], 500);
        }
        $image_type = explode('/', $image_info['mime'])[0];
        $subname = explode('/', $image_info['mime'])[1];

        if ($image_type !== 'image') {
            return response(['message' => __('images.type_incorrect')], 500);
        }

        $source = imagecreatefromstring($imagedata);
        ob_start();
        if ($subname == 'png') {
            imagepng($source);
        } else {
            imagejpeg($source, null, 100);
        }
        $image_contents = ob_get_clean();


        $uuid = Auth::user()->uuid;

        $savepath = "$uuid"."/"."$imageName.$subname";

        $disk = Storage::disk('gcs');


        if (!$disk->put($savepath, $image_contents)) {
            return response(['message' => __('An unexpected error has occured. Please try again')], 500);
        }
        $image_url = $disk->url($savepath);


        if ($userImages === null) {
            return response(['message' => __('An unexpected error has occured. Please try again')], 500);
        }

        if (!$userImages->update([$request->type => $image_url])) {
            return response(['message' => __('An unexpected error has occured. Please try again')], 500);
        }

        // The image has since been updated successfully, so we can delete the old file.
        if ($path !== '') {
            $disk->delete($path);
        }

        return response(['message' => __('images.new_image_success'), 'data' => new UserImagesResource(Auth::user()->userImages)], 200);
    }
}
