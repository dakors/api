<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Http\Resources\SearchUserResourceCollection;
use App\Models\Friend;
use App\Models\DeviceInfo;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

class FriendController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getFriendList(request $request)
    {
        $orderby =  isset($request->orderby) ? $request->orderby : 'NameAsc';
        $filter = isset($request->filter) ? $request->filter : '';
        $user = Auth::user();
        $friends = $user->friends()->where(function ($query) use ($filter) {
            if (!empty($filter)) {
                $query->where('nick_name', 'like', '%'.$filter.'%');
            }
        });
        switch ($orderby) {
            case 'NameDesc':
            return new SearchUserResourceCollection($friends->orderby('nick_name', 'DESC')->Paginate(30));
            break;
            default:
            case 'NameAsc':
            return new SearchUserResourceCollection($friends->orderby('nick_name', 'ASC')->Paginate(30));
            break;
        }
    }

    public function getRequestList(request $request)
    {
        $orderby =  isset($request->orderby) ? $request->orderby : 'NameAsc';
        $filter = isset($request->filter) ? $request->filter : '';

        $user = Auth::user();
        $friendRequests = $user->friendRequests()->where(function ($query) use ($filter) {
            if (!empty($filter)) {
                $query->where('nick_name', 'like', '%'.$filter.'%');
            }
        });
        $user->update(['last_time_friend_list' => date("Y-m-d H:i:s")]);
        $user->save();
        switch ($orderby) {
            case 'NameDesc':
            return new SearchUserResourceCollection($friendRequests->orderby('nick_name', 'DESC')->Paginate(30));
            break;
            default:
            case 'NameAsc':
            return new SearchUserResourceCollection($friendRequests->orderby('nick_name', 'ASC')->Paginate(30));
            break;
        }
    }

    public function unfriend()
    {
        $user = Auth::user();
        $data = $this->validate(request(), [
            'user_id' => 'required'
        ]);

        $otherUser = User::where('uuid','=',$data['user_id'])->first();
        if ($otherUser === null) {
            return response(['message' => __('User not found')], 404);
        }

        $caseReceiver = ['status' => 'confirmed','requester' => $otherUser->id, 'receiver' => $user->id];
        $caseRequester = ['status' => 'confirmed','requester' => $user->id, 'receiver' => $otherUser->id];

        $friend = Friend::where($caseReceiver)->orwhere(function ($q) use ($caseRequester){
            $q->where($caseRequester);
        })->first();
        
        
        if ($friend === null) {
            return response(['message' => __('friend.not_friend')], 404);
        }
        
        $friend->status = 'canceled';
        
        if ($friend->save()) {
            return response(['message' => sprintf(__('friend.unfriend_success'), $otherUser->nick_name)], 200);
        } else {
            return response(['message' => __('An unexpected error has occured. Please try again')], 500);
        }
    }

    public function sendFriendRequest()
    {
        $data = $this->validate(request(), [
            'user_id' => 'required'
        ]);

        $friendRequestId = $data['user_id'];

        /*if (!User::all()->contains('id', $friendRequestId)) {
            return response(['message' => __('User not found')], 404);
        }*/

        $userToRequest = User::where('uuid','=',$friendRequestId)->firstorfail();

        /*if($userToRequest === null){
            return response(['message' => __('User not found')], 404);
        }*/

        $user = Auth::user();
        //$otherUser = User::find($friendRequestId);
        if($user->id === $userToRequest->id){
            return response(['message' => sprintf(__('friend.cant_request_self'), $userToRequest->nick_name)], 401);
        }
        
        $caseReceiver = ['requester' => $userToRequest->id, 'receiver' => $user->id];
        $caseRequester = ['requester' => $user->id, 'receiver' => $userToRequest->id];

        $relationship = Friend::where($caseReceiver)->orwhere(function ($q) use ($caseRequester){
            $q->where($caseRequester);
        })->where('status','!=','confirmed')->firstornew(['requester' => $user->id,'receiver' => $userToRequest->id]);

        if ($relationship->exists && ($relationship->status == 'requesting' || $relationship->status == 'confirmed' )) {
            return response(['message' => sprintf(__('friend.request_success'), $userToRequest->nick_name)], 200);
        }
        $relationship->requester = $user->id ;
        $relationship->receiver = $userToRequest->id ;
        $relationship->status = 'requesting';
        $relationship->modify_date = date("Y-m-d H:i:s");
        if ($relationship->save()) {           
            if(count($userToRequest->FcmToken())>=1){
                
                $fcm_data= ['type' => 'friend_request'
                ,'nick_name' => $user->nick_name
                ,'target_id' => $user->uuid
                ,'user_id' => $userToRequest->uuid];

                $title = __('friend.new_friend_request');
                $body = sprintf(__('friend.want_be_your_friend'),$user->nick_name);
                $icon = !empty($user->userImages->photo) ? $user->userImages->photo : 'https://www.gamerbox.com.tw/images/default_photo.png';
                $this->fcm($fcm_data,$userToRequest->FcmToken(),$title,$body,$icon);
            } 
            return response(['message' => sprintf(__('friend.request_success'), $userToRequest->nick_name)], 200);
        } else {
            return response(['message' => __('An unexpected error has occured. Please try again')], 500);
        }
    }

    public function rejectFriendRequest()
    {
        $data = $this->validate(request(), [
            'user_id' => 'required'
        ]);
        $requesterId = $data['user_id'];

        $requester = User::where('uuid','=',$requesterId)->first();
        if ($requester === null) {
            return response(['message' => __('User not found')], 404);
        }

        $friendRequest = Friend::where('requester', $requester->id)->where('receiver', Auth::user()->id)->first();

        if ($friendRequest === null) {
            return response(['message' => __('friend.request_not_found')], 404);
        }

        if ($friendRequest->status === 'confirmed') {
            return response(['message' => sprintf(__('friend.reject_already_friend'), $requester->nick_name)], 200);
        }

        $friendRequest->status = 'rejected';
        if ($friendRequest->save()) {
            return response(['message' => sprintf(__('friend.reject_request'), $requester->nick_name)], 200);
        } else {
            return response(['message' => __('An unexpected error has occured. Please try again')], 500);
        }
    }

    public function acceptFriendRequest()
    {
        $data = $this->validate(request(), [
            'user_id' => 'required'
        ]);
        $user = Auth::user();
        $requesterId = $data['user_id'];
        $requester = User::where('uuid','=',$requesterId)->first();
        if ($requester === null) {
            return response(['message' => __('User not found')], 404);
        }

        $friendRequest = Friend::where('requester','=', $requester->id)->where('receiver','=', $user->id)->first();

        if ($friendRequest === null) {
            return response(['message' => __('friend.request_not_found')], 404);
        }

        if ($friendRequest->receiver !== $user->id) {
            return response(['message' => __('friend.wrong_receiver')], 403);
        }

        if ($friendRequest->status === 'confirmed') {
            return response(['message' => sprintf(__('friend.accept_already_friend'), $requester->nick_name)], 500);
        }        
      
        $friendRequest->status = 'confirmed';
        if ($friendRequest->save()) {
            if(count($requester->FcmToken())>=1){
                $fcm_data= ['type' => 'friend_confirm'
                ,'nick_name' => $user->nick_name
                ,'target_id' => $user->uuid
                ,'user_id' => $requester->uuid];

                $title = __('friend.friend_confirm');
                $body = sprintf(__('friend.is_your_new_friend'),$user->nick_name);
                $icon = !empty($user->userImages->photo) ? $user->userImages->photo : 'https://www.gamerbox.com.tw/images/default_photo.png';
                $this->fcm($fcm_data,$requester->FcmToken(),$title,$body,$icon);
            }


            $newfriend=$user->friends()->where('uuid',$requesterId)->get();
            return response(['message' => sprintf(__('friend.accept_request'), $requester->nick_name),'data' => new SearchUserResourceCollection($newfriend)], 200);
        } else {
            return response(['message' => __('An unexpected error has occured. Please try again')], 500);
        }
    }


    public function quicksearch(request $request)
    {
        $keyword = isset($request->keyword) ? $request->keyword : '';
        $user = Auth::user();
        $friends = $user->friends()->where(function ($query) use ($keyword) {
            if (!empty($keyword)) {
                $query->where('nick_name', 'like', '%'.$keyword.'%');
            }
        });

        if (count($friends->get())>=1) {
            return new SearchUserResourceCollection($friends->orderby('nick_name', 'ASC')->Paginate(30));
        } else {
            return response(['message' => __('User not found')], 404);
        }
    }

    private function fcm($fcm_data,$tokens,$title,$body,$icon){
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);

        $notificationBuilder = new PayloadNotificationBuilder($title);
        $notificationBuilder->setBody($body)
                            ->setIcon($icon)
                            ->setSound('default');

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData($fcm_data);

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

        //$tokens = $request->token;

        $downstreamResponse = FCM::sendTo($tokens, $option, $notification, $data);

        $downstreamResponse->numberSuccess();
        
        if(count($downstreamResponse->numberFailure()) >= 1 ){
            $device = DeviceInfo::wherein('device_id',$downstreamResponse->tokensToDelete());
            $device->delete();
        }

        $downstreamResponse->numberModification();

        // return Array - you must remove all this tokens in your database
        //$downstreamResponse->tokensToDelete();
        
        // return Array (key : oldToken, value : new token - you must change the token in your database)
        $downstreamResponse->tokensToModify();
        
        // return Array - you should try to resend the message to the tokens in the array
        $downstreamResponse->tokensToRetry();

        // return Array (key:token, value:error) - in production you should remove from your database the tokens
        $downstreamResponse->tokensWithError();
    }
}
