<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use App\User;
use App\Http\Resources\FollowingUserResourceCollection;
use App\Http\Resources\SearchUserResourceCollection;
use App\Models\Fan;

class FanController extends Controller
{
    public function getFollowingList(Request $request)
    {
        $orderby =  isset($request->orderby) ? $request->orderby : 'NameAsc';
        $filter = isset($request->filter) ? $request->filter : '';
        $type = isset($request->type) ? strtolower($request->type) : 'non-subscriber';

        $user = Auth::user();

        $followings=$user->following()->where(function ($query) use ($filter,$type) {
            if (!empty($filter)) {
                $query->where('nick_name', 'like', '%'.$filter.'%');
            }
            
            switch ($type) {     
                case 'all':    
                                 
                break;

                case 'subscriber':
                $query->where('subscription_expiration_date','>=',date("Y-m-d"));
                break;
                
                case 'non-subscriber':
                default:
                $query->where('subscription_expiration_date','<',date("Y-m-d"));
                break;
                
            }
            
        });

        if($type === 'all') {
            switch ($orderby) {
                case 'NameDesc':
                return new FollowingUserResourceCollection($followings->orderby('nick_name', 'DESC')->Paginate(30));
                break;
                default:
                case 'NameAsc':
                return new FollowingUserResourceCollection($followings->orderby('nick_name', 'ASC')->Paginate(30));
                break;
            }
        }else{
            switch ($orderby) {
                case 'NameDesc':
                return new FollowingUserResourceCollection($followings->orderby('nick_name', 'DESC')->Paginate(30));
                break;
                default:
                case 'NameAsc':
                return new FollowingUserResourceCollection($followings->orderby('nick_name', 'ASC')->Paginate(30));
                break;
            }
        }
    }

    public function getFanList(Request $request)
    {
        $orderby =  isset($request->orderby) ? $request->orderby : 'NameAsc';
        $filter = isset($request->filter) ? $request->filter : '';

        $user = Auth::user();
        $fans = $user->fans()->where(function ($query) use ($filter) {
            if (!empty($filter)) {
                $query->where('nick_name', 'like', '%'.$filter.'%');
            }
        });
        switch ($orderby) {
            case 'NameDesc':
            return new SearchUserResourceCollection($fans->orderby('nick_name', 'DESC')->Paginate(30));
            break;
            default:
            case 'NameAsc':
            return new SearchUserResourceCollection($fans->orderby('nick_name', 'ASC')->Paginate(30));
            break;
        }
    }

    public function follow()
    {
        $data = $this->validate(request(), [
            'user_id' => 'required'
        ]);

        $followId = $data['user_id'];

       
        
        $userToFollow = User::where('uuid','=',$followId)->first();
        
        if($userToFollow === null){
            return response(['message' => __('User not found')], 404);
        }
        
        $userId = Auth::user()->id;
        if($userId === $userToFollow->id){
            return response(['message' => sprintf(__('fan.cannt_follow_your_self'), $userToFollow->nick_name)], 401); 
        }
        $fan = Fan::firstOrNew([
            'user_id' => $userToFollow->id,
            'fans_id' => $userId,
            'IsMuted' => 'false'
        ]);

        // Check if this is a duplicate record
        if ($fan->exists) {
            return response(['message' => sprintf(__('fan.follow_success'), $userToFollow->nick_name)], 200);
        }

        if ($fan->save()) {
            return response(['message' => sprintf(__('fan.follow_success'), $userToFollow->nick_name)], 200);
        } else {
            return response(['message' => __('An unexpected error has occured. Please try again')], 500);
        }
    }

    public function unfollow()
    {
        $data = $this->validate(request(), [
            'user_id' => 'required'
        ]);

        $unfollowId = $data['user_id'];
        $userToUnFollow = User::where('uuid','=',$unfollowId)->first();

        if ($userToUnFollow === null) {
            return response(['message' => __('User not found')], 404);
        }

        $userId = Auth::user()->id;
        $relationship = Fan::where(['user_id' => $userToUnFollow->id, 'fans_id' => $userId])->first();

        // Relationship not found, user is not following.
        if ($relationship === null) {
            return response(['message' => sprintf(__('fan.unfollow_fail_not_following'), $userToUnFollow->nick_name)], 404);
        }


        if ($relationship->delete()) {
            return response(['message' => sprintf(__('fan.unfollow_success'), $userToUnFollow->nick_name)], 200);
        } else {
            return response(['message' => __('An unexpected error has occured. Please try again')], 500);
        }
    }

    public function mute(){
        $data = $this->validate(request(), [
            'user_id' => 'required'
        ]);

        $muteId = $data['user_id'];
        $userToUnFollow = User::where('uuid','=',$muteId)->first();

        if ($userToUnFollow === null) {
            return response(['message' => __('User not found')], 404);
        }

        $userId = Auth::user()->id;
        $relationship = Fan::where(['user_id' => $userToUnFollow->id, 'fans_id' => $userId])->first();

        if ($relationship === null) {
            return response(['message' => sprintf(__('fan.not_following'), $userToUnFollow->nick_name)], 404);
        }

        //dd($relationship);
        $relationship->IsMuted = 'true';

        if ($relationship->save()) {
            return response(['message' => sprintf(__('fan.mute_following_success'), $userToUnFollow->nick_name)], 200);
        } else {
            return response(['message' => __('An unexpected error has occured. Please try again')], 500);
        }
    }

    public function unmute(){
        $data = $this->validate(request(), [
            'user_id' => 'required'
        ]);

        $muteId = $data['user_id'];
        $userToUnFollow = User::where('uuid','=',$muteId)->first();

        if ($userToUnFollow === null) {
            return response(['message' => __('User not found')], 404);
        }

        $userId = Auth::user()->id;
        $relationship = Fan::where(['user_id' => $userToUnFollow->id, 'fans_id' => $userId])->first();

        if ($relationship === null) {
            return response(['message' => sprintf(__('fan.not_following'), $userToUnFollow->nick_name)], 404);
        }

        //dd($relationship);
        $relationship->IsMuted = 'false';

        if ($relationship->save()) {
            return response(['message' => sprintf(__('fan.unmute_following_success'), $userToUnFollow->nick_name)], 200);
        } else {
            return response(['message' => __('An unexpected error has occured. Please try again')], 500);
        }
    }
}
