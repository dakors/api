<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Dusterio\LinkPreview\Client;
use romanzipp\Twitch\Twitch;
class UrlController extends Controller
{
    public function preview(request $request){
        $this->validate($request, [
    		'url' => 'required'
        ]);
        $url_link = strip_tags(html_entity_decode($request->url));
        $twitch_clip_regex = '/(?:https:\/\/)?clips\.twitch\.tv\/([A-Za-a]+)($|\?)/i'; //[1]
        $twitch_clip_regex2 = '/(?:https?:\/\/)?(?:www\.|go\.)?twitch\.tv\/([a-z0-9_]+)\/(clip)\/([A-Za-a]+)($|\?)/i';//[2]
        $twitch_channel_regex = '/(?:https?:\/\/)?(?:www\.|go\.)?twitch\.tv\/([a-z0-9_]+)($|\?)/i';//[1]
        $twitch_video_regex = '/(?:https?:\/\/)?(?:www\.|go\.)?twitch\.tv\/(videos)\/([0-9_]+)($|\?)/i';//[2]
        $twitch = new Twitch;
        $twitch->setClientId('8c3btmu742xmk9tiubmv35eh0meq41');
        $searchVal = array("%{width}", "%{height}");
        $replaceVal = array("1024", "768");
        if(preg_match($twitch_video_regex, $url_link, $matches)){
            
            $vedioId = (int) $matches[2];
            $data = $twitch->getVideosById($vedioId);
            //dd($data);
            if($data->success()){
                $video = $data->shift();
                $result['data']['title'] = $video->title;
                $result['data']['description'] = $video->user_name." Video";
                $result['data']['image'] = str_replace ( $searchVal, $replaceVal, $video->thumbnail_url);
                $result['data']['embedUrl'] = 'https://player.twitch.tv/?video='.$video->id.'&autoplay=false';
                $result['data']['url'] = $video->url;
                return response(['data' => $result['data']], 200);

            }
        }
        if(preg_match($twitch_clip_regex, $url_link, $matches)){
            $clipId = $matches[1];
            //dd($clipId);
            $data = $twitch->getClip($clipId);
            //dd($data);
            if($data->success()){
                $clip = $data->shift();
                $result['data']['title'] = $clip->title;
                $result['data']['description'] = $clip->broadcaster_name." Clip";
                $result['data']['image'] = $clip->thumbnail_url;
                $result['data']['embedUrl'] = $clip->embed_url.'&autoplay=false';
                $result['data']['url'] = $clip->url;
                return response(['data' => $result['data']], 200);

            }
        }
        if(preg_match($twitch_clip_regex2, $url_link, $matches)){
            //dd($matches);
            $clipId = $matches[3];
            $data = $twitch->getClip($clipId);
            //dd($data);
            if($data->success()){
                $clip = $data->shift();
                $result['data']['title'] = $clip->title;
                $result['data']['description'] = $clip->broadcaster_name." Clip";
                $result['data']['image'] = $clip->thumbnail_url;
                $result['data']['embedUrl'] = $clip->embed_url.'&autoplay=false';
                $result['data']['url'] = $clip->url;
                return response(['data' => $result['data']], 200);

            }
        }
        if(preg_match($twitch_channel_regex, $url_link, $matches)){
            $channelId = $matches[1];
            //dd($channelId);
            $data = $twitch->getUserByName($channelId);
            //dd($data);
            if($data->success()){
                $user = $data->shift();
                $result['data']['title'] = $user->display_name;
                $result['data']['description'] = $user->description;
                $result['data']['image'] = $user->offline_image_url;
                $result['data']['embedUrl'] = 'https://player.twitch.tv/?channel='.$channelId.'&autoplay=false';
                $result['data']['url'] = $url_link;
                return response(['data' => $result['data']], 200);
            }
        }
        $previewClient = new Client($url_link);

        try {
            $previews = $previewClient->getPreviews();

        } catch (ClientException $ex) {
            //$result['data']=['image' => '','title' => $url_link,'description' => $url_link,'embedUrl' => '','url' => $url_link];        
            return response(['message' => __('An unexpected error has occured. Please try again')], 500);    
        }

        $data= $previews['general']->toArray();
        //dd(($data['cover']));        
        //dd($data);
        if(!empty($data['cover'])){
            if(gettype($data['cover']) !== 'string'){
                $result['data']['image']=$url_link;
            }else{
                $result['data']['image']=$data['cover'];
            }            
        }else if(!empty($data['image'])){
            dd($$data['images']);
            $result['data']['image']=$data['images'][0];
        }else{
            $result['data']['image']='';
        }

        if(!empty($data['title'])){
            $result['data']['title']=$data['title'];
        }else{
            $result['data']['title']='';
        }

        if(!empty($data['description'])){
            $result['data']['description']=$data['description'];
        }else{
            $result['data']['description']='';
        }

        if(isset($previews['youtube'])){
            $youtube = $previews['youtube']->toArray();
            $result['data']['embedUrl'] = 'https://www.youtube.com/embed/'.$youtube['id'] ;
            $url_link = 'https://www.youtube.com/watch?v='.$youtube['id'];
        }else{
            $result['data']['embedUrl'] = '';
        } 
        $result['data']['url'] = $url_link;
        return response(['data' => $result['data']], 200);       
    }
}