<?php

namespace App\Http\Controllers;

use App\Models\BlogLiked;
use App\Models\UserBlog;
use App\Http\Resources\BlogLikedResourceCollection;
use Illuminate\Http\Request;
use app\User;
use Auth;

class LikeController extends Controller
{
    public function create(Request $request)
    {
        $data = $this->validate($request, ['blog_id' => 'required']);

        $userID = Auth::user()->id;

        if(!UserBlog::where('blog_id', $request->blog_id)->exists()) {
            return response(['message' => __('blog.blog_not_found')], 404);
        }
        
        if (BlogLiked::where('blog_id', $request->blog_id)->where('user_id', $userID)->exists()) {
            return response(['message' => __('blog.blog_already_like')], 500);
        }

        $BlogLiked = new BlogLiked;
        $BlogLiked->user_id = $userID;
        $BlogLiked->blog_id = $data['blog_id'];

        if ($BlogLiked->save()) {
            return response(['message' => __('blog.blog_liked')], 200);
        } else {
            return response(['message' => __('An unexpected error has occured. Please try again')], 500);
        }
    }

    public function delete(Request $request)
    {
        $data = $this->validate($request, ['blog_id' => 'required']);

        $userID = Auth::user()->id;

        $like = BlogLiked::where('blog_id', '=', $data['blog_id'])->where('user_id', $userID)->first();

        if ($like === null) {
            return response(['message' => __('blog.like_not_found')], 404);
        }

        if ($like->delete()) {
            return response(['message' => __('blog.blog_unliked')], 200);
        } else {
            return response(['message' => __('An unexpected error has occured. Please try again')], 500);
        }
    }

    public function list(Request $request)
    {
        $attributes = $this->validate($request, ['blog_id' => 'required']);

        $blog = UserBlog::find($attributes['blog_id']);
        if ($blog === null) {
            return response(['message' => __('blog.blog_not_found')], 404);
        }

        $likes = $blog->likes()->Paginate(15);
        return new BlogLikedResourceCollection($likes);
    }
}
