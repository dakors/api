<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Input;
use App\Http\Resources\NotificationResource;
use App\Models\UserFriends;
use App\Models\InviteHistory;
use App\User;
use Auth;
use DB;
use Carbon\Carbon;

class NotificationController extends Controller
{
    public function unreadCount(){
        $user=Auth::user();
        $userId=$user->id;
        $LastTimeNoticeList=$user->last_time_notice_list;

        $count=DB::select("select blog_id from (SELECT tb1.blog_id FROM user_blog as tb1 join blog_comment as tb2 on tb1.blog_id = tb2.blog_id "
        ."where tb1.user_id = $userId and tb2.user_id <> $userId and tb2.create_date > '$LastTimeNoticeList' group by tb1.blog_id "
        ."union all "
        ."SELECT tb1.blog_id FROM user_blog as tb1 join blog_liked as tb2 on tb1.blog_id = tb2.blog_id "
        ."where tb1.user_id = $userId and tb2.user_id <> $userId and tb2.create_date > '$LastTimeNoticeList' group by tb1.blog_id) as tb4");

        $last_time_friend_list = $user->last_time_friend_list;
        $friendRequests = $user->friendRequests()->where(function ($query) use ($last_time_friend_list) {
            if (!empty($filter)) {
                $query->where('modify_date' >= $last_time_friend_list);
            }
        });
        
        //$count_sql='select count(id) as amount from invite_history where (respond_time>= :last_timestamp and sender_id = :user_id) or (request_time >= :last_timestamp and receiver_id = :user_id);';
        $invitehistory = InviteHistory::where(function ($query) use ($user){
            //dd(Carbon::createFromFormat('Y-m-d H:i:s', $user->last_time_history_list)->timestamp *1000);
            $query->where('respond_time' ,'>=', Carbon::createFromFormat('Y-m-d H:i:s', $user->last_time_history_list)->timestamp *1000)->where('sender_id' , $user->id);
        })->orwhere(function ($query) use ($user){
            $query->where('request_time' ,'>=', Carbon::createFromFormat('Y-m-d H:i:s', $user->last_time_history_list)->timestamp *1000)->where('receiver_id' , $user->id);
        });
        //dd(count($invitehistory));
        if($count!==null){
            return response(['data' => ['unreadNotification' => count($count),'unreadRequesting' => $friendRequests->count(),'unradInvite' => $invitehistory->count(),'total' => (count($count) + $friendRequests->count() + $invitehistory->count()) ]], 200);
        }else{
            return response(['message' => __('An unexpected error has occured. Please try again')], 500);
        }
    }

    public function getList(Request $request){
        $user=Auth::user();
        $userId=$user->id;

        $list=DB::select("select * from (select tb3.blog_id,tb5.uuid as user_id,unix_timestamp(tb4.create_date)*1000 as timestamp ,IFNULL(tb7.liked_count_amount,0) liked_count_amount ,tb5.nick_name,gender,IF(subscription_expiration_date >= CURRENT_DATE,true,false) as Issubscriber,photo,other_users,'blog_comment' as type "
        ."from (SELECT tb1.blog_id,MAX(tb2.comment_id) as comment_id,count(DISTINCT tb2.user_id)-1 as other_users  from user_blog as tb1 join blog_comment as tb2 on tb1.blog_id = tb2.blog_id where tb1.user_id = $userId and tb2.user_id <> $userId group by tb1.blog_id) "
        ."as tb3 join blog_comment as tb4 on tb3.comment_id = tb4.comment_id join users as tb5 on tb4.user_id = tb5.id join user_images as tb6 on tb4.user_id = tb6.user_id left join like_count as tb7 on tb3.blog_id = tb7.blog_id "
        ."union all "
        ."select tb3.blog_id,tb5.uuid as user_id,unix_timestamp(tb3.create_date)*1000 as timestamp ,IFNULL(tb7.liked_count_amount,0) liked_count_amount ,tb5.nick_name,gender,IF(subscription_expiration_date >= CURRENT_DATE,true,false) as Issubscriber,photo,other_users,'blog_liked' as type "
        ."from (select tb1.blog_id,count(tb2.user_id)-1 as other_users,max(tb2.create_date) as create_date from user_blog as tb1 join blog_liked as tb2 on tb1.blog_id = tb2.blog_id where tb1.user_id = $userId and tb2.user_id <> $userId group by tb1.blog_id) "
        ."as tb3 join blog_liked as tb4 on tb3.blog_id = tb4.blog_id and tb3.create_date = tb4.create_date join users as tb5 on tb4.user_id = tb5.id join user_images as tb6 on tb6.user_id = tb4.user_id left join like_count as tb7 on tb3.blog_id = tb7.blog_id) as notification order by notification.timestamp desc ");

        $list = $this->arrayPaginator($list,$request);
        $user->update(['last_time_notice_list' => date("Y-m-d H:i:s")]);
        $user->save(); 
        return response(new NotificationResource($list), 200);
    }

    public function arrayPaginator($array, $request)
    {
        $page = Input::get('page', 1);
        $perPage = 15;
        $offset = ($page * $perPage) - $perPage;

        return new LengthAwarePaginator(array_slice($array, $offset, $perPage, true), count($array), $perPage, $page,
            ['path' => $request->url(), 'query' => $request->query()]);
    }
}
