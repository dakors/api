<?php

namespace App\Http\Controllers;

use App\Models\BlogComment;
use App\Models\UserBlog;
use Illuminate\Http\Request;
use App\Http\Resources\BlogCommentResourceCollection;
use App\Http\Resources\BlogCommentResource;
use Auth;

class CommentController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $attributes = $this->validate(
            $request,
            [
                'content' => 'required|max:2000',
                'blog_id' => 'required'
            ]
        );

        $blog = UserBlog::find($attributes['blog_id']);
        if ($blog === null) {
            return response(['message' => __('blog.blog_not_found')], 404);
        }

        $comment = new BlogComment;
        $comment->blog_id = $attributes['blog_id'];
        $comment->content = $attributes['content'];
        $comment->user_id = Auth::user()->id;
        
        if ($comment->save()) {
            //DD($comment->comment_id);
            $new_comment = BlogComment::where('comment_id',$comment->comment_id)->first();
            //return new BlogCommentResourceCollection($new_comment);
            return response(['message' => __('comment.new_comment_success'),'data' => new BlogCommentResource($new_comment)], 200);
        } else {
            return response(['message' => __('An unexpected error has occured. Please try again')], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $attributes = $this->validate($request, [
                'content'   => 'required',
                'commentId' => 'required'
            ]);

        $comment = BlogComment::find($attributes['commentId']);

        if ($comment === null) {
            return response(['message' => __('comment.comment_not_found')], 404);
        }

        if (Auth::user()->id != $comment->user_id) {
            return response(['message' => __('comment.comment_not_owner')], 403);
        }

        $comment->content = $attributes['content'];
        if ($comment->save()) {
            $new_comment = BlogComment::where('comment_id',$comment->comment_id)->first();
            return response(['message' => __('comment.comment_updated'),'data' => new BlogCommentResource($new_comment)], 200);
        } else {
            return response(['message' => __('An unexpected error has occured. Please try again')], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\BlogComment  $blogComment
     * @return \Illuminate\Http\Response
     */
    public function delete()
    {
        $attributes = $this->validate(request(), ['commentId' => 'required']);

        $comment = BlogComment::find($attributes['commentId']);
        if ($comment === null) {
            return response(['message' => __('comment.comment_not_found')], 404);
        }
        
        $blog = UserBlog::where('blog_id', '=' ,$comment->blog_id)->first();
        
        if ($comment->user_id != Auth::user()->id && $blog->user_id != Auth::user()->id ) {
            return response(['message' => __('comment.comment_not_owner')], 403);
        }

        if ($comment->delete()) {
            return response(['message' => __('comment.comment_removed')], 200);
        } else {
            return response(['message' => __('An unexpected error has occured. Please try again')], 500);
        }
    }

    public function list()
    {
        $attributes = $this->validate(request(), ['blog_id' => 'required']);

        $blog = UserBlog::find($attributes['blog_id']);
        if ($blog === null) {
            return response(['message' => __('blog.blog_not_found')], 404);
        }
        
        //return new BlogCommentResourceCollection($blog->comments()->Paginate(15));
        return new BlogCommentResourceCollection($blog->comments()->wherenotin('blog_comment.user_id',AUTH::user()->blockAndBlockByList())->Paginate(15));
    }
}
