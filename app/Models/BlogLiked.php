<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use App\User;

class BlogLiked extends Model
{
    //
    use Notifiable;
    //使用資料表users
    protected $table = 'blog_liked';
    //自訂時間欄位
    public $timestamps = false;

    public function blog()
    {
        return $this->belongsTo(UserBlog::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function delete()
    {
        return DB::delete('delete from blog_liked where blog_id = ? and user_id = ?', [$this->blog_id, $this->user_id]);
    }
}
