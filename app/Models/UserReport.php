<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class UserReport extends Model
{
    protected $primaryKey = 'report_id';

    protected $table = 'user_report';

    public $timestamps = false;

    protected $fillable = [
        'user_id',
        'accused_person_id',
        'content',
        'type',
        'status',
        'create_date',
        'modify_date',
    ];
}
