<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use App\User;
use App\Models\UserRegisteredEvent;
use App\Models\UserInterestedEvent;
use Auth;

class UserEvent extends Model
{   
    use Notifiable;

    protected $primaryKey = 'event_id';

    protected $fillable = [        
        'user_id',
        'event_name',
        'registration_start_time',
        'registration_end_time',
        'start_time',
        'end_time',
        'location',
        'detail',
        'pic1',
        'pic2',
        'pic3',
        'create_time',
        'modify_time',
        'isRegistrable',
        'registration_limit_amount',
    ];


    protected $table = 'user_event';
    //自訂時間欄位
    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function blog(){
        return $this->belongsTo(UserBlog::class);
    }
    
    public function Interested(){
        return $this->hasMany(UserInterestedEvent::class,'event_id')->with(['user']);
    }

    public function Registered(){
        return $this->hasMany(UserRegisteredEvent::class,'event_id')->with(['user']);
    }

    public function isRegistered(){
        $id = Auth::user()->id;
        return $this->Registered->contains('user_id', $id);
    }

    public function isInterested(){
        $id = Auth::user()->id;
        return $this->Interested->contains('user_id', $id);
    }

    public function isRegistrable(){
        return filter_var($this->isRegistrable, FILTER_VALIDATE_BOOLEAN);
    }

    public function isDuringRegistration(){
        $now = \Carbon\Carbon::now()->timestamp * 1000;
        return ($this->registration_start_time <= $now && $now <= $this->registration_end_time );
    }
}
