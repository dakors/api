<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use App\User;
use App\Models\UserEvent;
use Auth;

class UserRegisteredEvent extends Model
{
    protected $table = 'user_registered_event';

    protected $fillable = [
        'event_id',
        'user_id',        
    ];

    public $timestamps = false;

    public function event(){
        return $this->belongsTo(UserEvent::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
}
