<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class RecommendedUser extends Model
{
    use Notifiable;

    protected $fillable = [
        'id','user_id'
    ];

    protected $table = 'recommend_user';

    public $timestamps = false;
}
