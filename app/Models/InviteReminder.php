<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class InviteReminder extends Model
{
    protected $fillable = [
        'invite_history_id',
        'user_id',
        'reminder_time',
    ];

    protected $table = 'invite_reminder';

    public $timestamps = false;
}
