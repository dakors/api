<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SearchLog extends Model
{
    protected $table = 'search_log';

    public $timestamps = false;

    protected $fillable = ['user_id', 
                            'minAge',
                            'maxAge',
                            'gender',
                            'location',
                            'name',
                            'game_name',
                        ];
}
