<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class HashtagSearchLog extends Model
{
    protected $table = 'hashtag_search_log';

    public $timestamps = false;

    protected $fillable = ['user_id', 'keyword'];
}
