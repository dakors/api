<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Models\HotListCatalog;

class HotList extends Model
{
    use Notifiable;

    //protected $primaryKey = 'device_id';

    //使用資料表users
    protected $table = 'hot_list';
    //自訂時間欄位
    public $timestamps = false;

    public function Catalog()
    {
        return $this->belongsTo(HotlistCatalog::class);
    }

    
}
