<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class InviteHistory extends Model
{


    protected $table = 'invite_history';

    protected  $fillable = [
        'sender_id',
        'receiver_id',
        'start_time',
        'end_time',
        'game_name',
        'location',
        'status',
        'request_time',
        'respond_time',
        'canceled_by',
    ];

    public $timestamps = false;
    
    public function sender(){
        return User::where('id',$this->sender_id)->first();
    }

    public function receiver(){
        return User::where('id',$this->receiver_id)->first();
    }

    public function canceled_by_user(){
            return User::where('id',$this->canceled_by)->first();
    }
}
