<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class UserImages extends Model
{
    //
    use Notifiable;
    protected $fillable = [
        'user_id','photo','background',
    ];

    //使用資料表users
    protected $table = 'user_images';
    //自訂時間欄位
    public $timestamps = false;

    protected function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getImages($userID)
    {
        return $this->where('user_id', $userID)->first();
    }

    public function getPhotoFilePath()
    {
        if ($this->photo == null) {
            return '';
        }
        if (strpos($this->photo, 'storage.googleapis.com') == false) {
            return '';
        }

        $tokens = explode("/", $this->photo);
        return $tokens[count($tokens)-2].'/'.$tokens[count($tokens)-1];
    }

    public function getBackgroundFilePath()
    {
        if ($this->background == null) {
            return '';
        }
        if (strpos($this->background, 'storage.googleapis.com') == false) {
            return '';
        }

        $tokens = explode("/", $this->background);
        return $tokens[count($tokens)-2].'/'.$tokens[count($tokens)-1];
    }
}
