<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use App\User;
use App\Models\UserEvent;
use Auth;

class UserInterestedEvent extends Model
{   
    protected $table = 'user_interested_event';

    protected $fillable = [
        'event_id',
        'user_id',        
    ];

    public $timestamps = false;

    public function event(){
        return UserEvent::where('event_id','=',$this->event_id);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
}
