<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class SocialAccounts extends Model
{
    use Notifiable;

    protected $fillable = [
        'user_id','provider','provider_user_id'
    ];

    protected $table = 'social_accounts';

    public $timestamps = true;
    
    public function user() {
      return $this->belongsTo(User::class,'id','user_id');
    }

    public function toArray()
    {
        $attributes = $this->attributesToArray();

        return array_merge($attributes, $this->relationsToArray());
    }
}
