<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use App\User;
use Auth;

class DeviceInfo extends Model
{
    use Notifiable;

    protected $primaryKey = 'device_id';
    public $incrementing = false;
    protected $fillable =[  'device_id',
                            'os',
                            'user_id',
                            'isNotificationForMessageEnabled',
                            'isNotificationForFriendAddEnabled',
                            'isSoundEnabled',
                        ];
    //使用資料表users
    protected $table = 'device_info';
    //自訂時間欄位
    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo(User::class);
    }


}
