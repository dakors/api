<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class UserGameList extends Model
{
    use Notifiable;

    protected $fillable = [
        'id','user_id','game_name',
    ];

    protected $table = 'user_game_list';

    public $timestamps = false;
    
    public function user() {
      return $this->belongsTo(User::class,'id','user_id');
    }
}
