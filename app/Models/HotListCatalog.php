<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Models\HotList;

class HotListCatalog extends Model
{
    use Notifiable;

    //protected $primaryKey = 'device_id';

    //使用資料表users
    protected $table = 'hot_list_catalog';
    //自訂時間欄位
    public $timestamps = false;

    public function likes()
    {
        return $this->hasMany(Hotlist::class, 'catalog_id');
    }
}
