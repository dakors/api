<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GameNameList extends Model
{
    protected $table = 'game_name_list';

    public $timestamps = false;

    protected $fillable = ['zhTW', 'enUs', 'acronyms'];
}
