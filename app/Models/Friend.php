<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Friend extends Model
{
    protected $table = 'friend_list';

    public $timestamps = false;

    protected $fillable = ['requester', 'receiver', 'status'];
}
