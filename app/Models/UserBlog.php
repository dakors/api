<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use App\User;
use Auth;

class UserBlog extends Model
{
    //
    use Notifiable;

    protected $primaryKey = 'blog_id';

    //使用資料表users
    protected $table = 'user_blog';
    //自訂時間欄位
    public $timestamps = false;

    protected $casts = [
        'preview' => 'object',
    ];
    // Example method to get return 1 is to 1 relationship using Eloquent.
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function likes()
    {
        return $this->hasMany(BlogLiked::class, 'blog_id');
    }

    public function comments()
    {
        return $this->hasMany(BlogComment::class, 'blog_id')->orderBy('modify_date');
    }

    public function isLiked()
    {
        $id = Auth::user()->id;
        return $this->likes->contains('user_id', $id);
    }

    public function event()
    {
        return $this->hasOne(UserEvent::class, 'event_id', 'event_id')->with(['user']);
    }

    public function getImageFilePath()
    {
        if ($this->preview == null) {
            return '';
        }
        $jsonArray = (array) $this->preview;
        if (!is_array($jsonArray)) {
            return '';
        }
        if (!array_key_exists('image', $jsonArray)) {
            return '';
        }
        if (array_key_exists('embedUrl', $jsonArray)) {
            return '';
        }
        $temp = explode("?", $jsonArray['image']);
        $tokens = explode("/", $temp[0]);
        
        // Prevent array out of bounds.
        if (count($tokens)-2 < 0) {
            return '';
        }
        return $tokens[count($tokens)-2].'/'.$tokens[count($tokens)-1];
    }
}
