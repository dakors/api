<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;


class UserFans extends Model
{
    //
    use Notifiable;


    //使用資料表users
    protected $table = 'fans_list';
    //自訂時間欄位
    public $timestamps = false;

}
