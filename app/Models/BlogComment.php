<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use App\User;

class BlogComment extends Model
{
    //
    use Notifiable;

    protected $primaryKey = 'comment_id';
    //使用資料表users
    protected $table = 'blog_comment';
    //自訂時間欄位
    public $timestamps = false;

    public function blog()
    {
        return $this->belongsTo(UserBlog::class, 'blog_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
