<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserBlockList extends Model
{
    protected $table = 'user_block_list';

    public $timestamps = false;

    protected $fillable = ['user_id','block_id'];

}
