<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Fan extends Model
{
    protected $table = 'fans_list';

    public $timestamps = false;

    protected $fillable = ['user_id', 'fans_id', 'IsMuted'];
}
