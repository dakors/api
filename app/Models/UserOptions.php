<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class UserOptions extends Model
{
    use Notifiable;
    
    protected $primaryKey = 'user_id';
    public $incrementing = false;

    protected $fillable = [
        'user_id','isStrangerAllowed','reminder_time',
    ];

    //使用資料表
    protected $table = 'user_options';
    //自訂時間欄位
    public $timestamps = false;

    protected function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getOptions(){
        return $this->where('user_id', $userID)->first();
    }
}
