<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\UserImages;
use App\Models\UserGameList;
use App\Models\UserOptions;
use App\Models\UserBlog;
use App\Models\Friend;
use App\Models\UserBlockList;
use App\Models\SocialAccounts;
use App\Models\DeviceInfo;
use Carbon\Carbon;
use App\Models\UserEvent;

class User extends Authenticatable
{
    use HasApiTokens,Notifiable;



    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uuid',
        'gb_id',
        //'fb_id',
        //'google_id',
        'nick_name',
        'email',
        'password',
        'last_time_notice_list',
        'last_time_friend_list',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password','remember_token',
    ];

    public function getLatestBlog()
    {
        return UserBlog::where('user_id', $this->id)
            ->with(['user.userImages', 'likes', 'comments','event'])
            ->orderBy('blog_id', 'desc')->first();
    }

    public function commonBlogs()
    {
        $friendIds = $this->friends()->pluck('id');
        $followingIds = $this->following->pluck('id');
        $OfficialAccountId = intval(config('app.OfficialAccountId')); //get Official Account Id from config;
        $userIds = $friendIds->merge($followingIds)->push($this->id)->push($OfficialAccountId)->unique();        
        return UserBlog::wherein('user_id', $userIds)
            ->with(['user.userImages', 'likes', 'comments'])
            ->latest('modify_date')
            ->Paginate(15);
    }

    public function getBlogs()
    {
        return UserBlog::where('user_id', $this->id)
          ->with(['user.userImages', 'likes', 'comments','event'])
          ->latest('modify_date')
          ->Paginate(15);
    }

    /**
    * All fans of this user.
    **/
    public function fans()
    {
        return $this->belongsToMany(User::class, 'fans_list', 'user_id', 'fans_id')
                    ->whereNotIn('fans_id', $this->blockAndBlockByList())
                    ->withPivot('IsMuted', 'create_date');
    }

    /**
    * All user that this user is following.
    **/
    public function following()
    {
        return $this->belongsToMany(User::class, 'fans_list', 'fans_id', 'user_id')
                    ->whereNotIn('user_id', $this->blockAndBlockByList())
                    ->withPivot('IsMuted', 'create_date');
    }

    /**
    * Note this is a method! So use $user->friends() and not $user->friends
    */
    public function friends()
    {
        $requestedFriends = Friend::where('status', 'confirmed')
                        ->whereNotIn('receiver', $this->blockAndBlockByList())
                        ->where('requester', $this->id)
                        ->get();

        $receivedFriends = Friend::where('status', 'confirmed')
                        ->whereNotIn('requester', $this->blockAndBlockByList())
                        ->where('receiver', $this->id)
                        ->get();

        return User::whereIn('id', $requestedFriends->pluck('receiver')->merge($receivedFriends->pluck('requester')));
    }

    public function friendRequests()
    {
        return $this->belongsToMany(User::class, 'friend_list', 'receiver', 'requester')
                    ->whereNotIn('requester', $this->blockAndBlockByList())
                    ->where('status', 'requesting');
    }

    public function beRequests()
    {
        return $this->belongsToMany(User::class, 'friend_list', 'requester','receiver')
                    ->whereNotIn('receiver', $this->blockAndBlockByList())
                    ->where('status', 'requesting');
    }
    public function recommendedusers()
    {
        return $this->belongsToMany(User::class);
    }

    public function userGameList()
    {
        return $this->hasMany(UserGameList::class);
    }

    public function userImages()
    {
        return $this->hasOne(UserImages::class);
    }

    public function blockList()
    {
        return $this->belongsToMany(User::class, 'user_block_list', 'user_id', 'block_id');
    }

    public function blockByList()
    {
        return $this->belongsToMany(User::class, 'user_block_list', 'block_id', 'user_id');
    }

    public function blockAndBlockByList()
    {
        return collect($this->blockList->merge($this->blockByList))->pluck('id')->toArray();
    }

    public function FcmToken(){
        return $this->hasMany(DeviceInfo::class,'user_id','id')->pluck('device_id')->toArray();
    }
    public function getAvatar()
    {
        $Images=$this->userImages;
        return $Images['photo'];
    }

    public function isSub()
    {
        if ($this->subscription_expiration_date === '') {
            return false;
        }
        $expiryDate = Carbon::parse($this->subscription_expiration_date);
        return $expiryDate->greaterThanOrEqualTo(Carbon::now()->toDateString());
    }

    public function socialaccounts(){
        return SocialAccounts::where('user_id' , $this->id)->get();
    }

    public function event(){
        
        return UserEvent::where('user_id',$this->id)
        ->with('Interested')
        ->Paginate(15);

    }

    public function options(){
        return $this->hasOne(Useroptions::class);
    }

    public function isFriend(){
        $myId = Auth::user()->id;
        return $this->friends()->pluck('id')->contains($myId);
    }
}
