<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
Route::group(['middleware' => ['web']], function () {

    Route::get('/', 'HomeController@index')->name('home');

});
//Route::get('/', 'HomeController@index')->name('home');
Route::get('/login','Auth\LoginController@showLoginForm')->name('login');
Route::post('/login','Auth\LoginController@login')->name('login.submit');
Route::get('/logout','Auth\LoginController@logout');
//Route::get('/login/facebook', 'SocialAuthFacebookController@redirect');
//Route::get('/login/facebook/callback', 'SocialAuthFacebookController@callback');
Route::post('/register','Auth\RegisterController@register');

Route::get('login/{provider}',          'Auth\SocialAccountController@redirectToProvider');
Route::get('login/{provider}/callback', 'Auth\SocialAccountController@handleProviderCallback');

//Route::get('/profile/{id}','ProfileController@view');

Route::post('/blog/submit', 'BlogController@submit')->name('blog.submit');
Route::post('/blog/getLastOne', 'BlogController@getLastOne');
Route::post('/blog/delete', 'BlogController@delete');
Route::post('/blog/GetCommentList','BlogController@GetCommentList');
Route::post('/blog/like','BlogController@like');
Route::post('/blog/unlike','BlogController@unlike');
Route::post('/blog/comment', 'BlogController@addComment');
Route::post('/blog/comment/edit', 'BlogController@editComment');
Route::post('/blog/comment/delete', 'BlogController@deleteComment');
Route::post('/blog', 'BlogController@getBlogs');

Route::post('/search', 'SearchController@search')->name('search');
Route::get('/search/result', 'SearchController@result')->name('search.result');

/*Route::post('/social/follow', 'SocialController@follow')->name('social.follow');
Route::post('/social/sendfriendrequest', 'SocialController@sendFriendRequest')->name('social.sendfriendrequest');
Route::post('/social/acceptfriendrequest', 'SocialController@acceptFriendRequest')->name('social.acceptfriendrequest');
Route::post('/social/rejectfriendrequest', 'SocialController@rejectFriendRequest')->name('social.acceptfriendrequest');*/
