<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group([
    'prefix' => 'auth'
    ], function () {
        Route::post('login', 'Api\Auth\LoginController@login');
        Route::post('socialauth', 'Api\Auth\SocialAuthController@socialAuth');
        Route::post('register', 'Api\Auth\RegisterController@register');
        Route::post('mobileregister', 'Api\Auth\RegisterController@mobileregister');
        Route::post('resetpassword', 'Api\Auth\PasswordController@reset');

        Route::group([
      'middleware' => 'auth:api'
    ], function () {
        Route::post('changepassword', 'Api\Auth\PasswordController@change');
        Route::post('logout', 'AuthController@logout');
        Route::post('user', 'AuthController@user');
        Route::post('user/bind','UserController@socialBind');
    });
    });

Route::group([
    'middleware' => 'auth:api'
  ], function () {
        Route::post('/home', 'HomeController@index');
        Route::post('/search', 'SearchController@search')->name('search');
        Route::post('/following', 'FanController@getFollowingList');
        Route::post('/following/mute', 'FanController@mute');
        Route::post('/following/unmute', 'FanController@unmute');
        Route::post('/fans', 'FanController@getFanList');
        Route::post('/following/follow', 'FanController@follow');
        Route::post('/following/unfollow', 'FanController@unfollow');
        Route::post('/friends', 'FriendController@getFriendList');
        Route::post('/friends/unfriend', 'FriendController@unfriend');
        Route::post('/friends/quicksearch', 'FriendController@quicksearch');
        Route::post('/requests', 'FriendController@getRequestList');
        Route::post('/requests/send', 'FriendController@sendFriendRequest');
        Route::post('/requests/reject', 'FriendController@rejectFriendRequest');
        Route::post('/requests/accept', 'FriendController@acceptFriendRequest');
        Route::post('/blog', 'BlogController@getBlog');
        Route::post('/user/blogs', 'BlogController@getBlogs');
        Route::post('/blog/new', 'BlogController@create');
        Route::post('/blog/delete', 'BlogController@delete');
        Route::post('/blog/update', 'BlogController@update');
        Route::post('/blog/like', 'LikeController@create');
        Route::post('/blog/unlike', 'LikeController@delete');
        Route::post('/blog/hashtagSearch', 'BlogController@hashtagSearch');
        Route::post('/likes', 'LikeController@list');
        Route::post('/comments', 'CommentController@list');
        Route::post('/comments/new', 'CommentController@create');
        Route::post('/comments/delete', 'CommentController@delete');
        Route::post('/comments/update', 'CommentController@update');
        Route::post('/blocklist', 'BlockListController@getList');
        Route::post('/blocklist/block', 'BlockListController@block');
        Route::post('/blocklist/unblock', 'BlockListController@unblock');
        Route::post('/gamelist', 'GameListController@getList');
        Route::post('/games/add', 'UserGameListController@create');
        Route::post('/games/remove', 'UserGameListController@delete');
        Route::post('/preview', 'UrlController@preview');
        Route::post('/images/upload', 'ImagesController@upload');
        Route::post('/user', 'UserController@view');
        Route::post('/user/recommended', 'UserController@getRecommendedlist');
        Route::post('/user/update', 'UserController@update');
        Route::post('/user/options','UserController@options');
        Route::post('/notification/unreadCount','NotificationController@unreadCount');
        Route::post('/notifications','NotificationController@getList');
        Route::post('/hotlist','HotlistController@getList');
        Route::Post('/report/profile','ReportController@profile');
        Route::Post('/report/comment','ReportController@comment');
        Route::Post('/report/blog','ReportController@blog');
        Route::Post('/event/create','EventController@create');
        Route::Post('/user/events','EventController@getList');
        Route::Post('/event','EventController@get');
        Route::Post('/event/update','EventController@update');
        Route::Post('/event/delete','EventController@delete');
        Route::Post('/event/addinterested','EventController@addinterestedEvent');
        Route::Post('/event/removeinterested','EventController@removeinterestedEvent');
        Route::Post('/event/register','EventController@registerevent');
        Route::Post('/event/unregister','EventController@unregisterevent');
        Route::Post('/event/getRegisteredList','EventController@getRegistered');
        Route::Post('/event/getInterestedList','EventController@getInterested');
        Route::Post('/device/add','DeviceController@add');
        Route::Post('/device/remove','DeviceController@remove');
        Route::Post('/invite/request','InviteController@request');
        Route::Post('/invite/accept','InviteController@accept');
        Route::Post('/invite/cancel','InviteController@cancel');
        Route::Post('/invite/reject','InviteController@reject');
        Route::Post('/invite/reminder','InviteController@reminder');
        Route::Post('/invite/getreminderList','InviteController@getReminderList');
        Route::Post('/user/invitehistory','InviteController@getList');
        Route::Post('/user/invite','InviteController@get');
        Route::Post('/calendar','CalendarController@get');
        Route::Post('hotkeywords','BlogController@hotKeywords');
  });

  Route::group(['middleware' => 'api','prefix' => 'guest'], function () {
    Route::post('blog', 'BlogController@getBlog');
    Route::post('home', 'BlogController@homepage');
    Route::post('recommended','UserController@getRecommendedlist');
  });
/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/
