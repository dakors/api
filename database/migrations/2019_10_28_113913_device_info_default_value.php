<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeviceInfoDefaultValue extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('device_info', function (Blueprint $table) {
            DB::statement("ALTER TABLE `device_info` CHANGE `create_date` `create_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP;");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('device_info', function (Blueprint $table) {
            DB::statement("ALTER TABLE `device_info` CHANGE `create_date` `create_date` DATETIME NOT NULL DEFAULT '';");
        });
    }
}
