<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserFans extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('user_fans')){
        DB::statement("CREATE
        ALGORITHM = UNDEFINED
        SQL SECURITY DEFINER
        VIEW user_fans as 
        select `fans_list`.`user_id` AS `user_id`,count(`fans_list`.`user_id`) AS `amount` from `fans_list` where 1 group by `fans_list`.`user_id`;");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW user_fans");
    }
}
