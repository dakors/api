<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSearchLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('search_log', function (Blueprint $table) {
            DB::statement("CREATE TABLE `search_log` ( `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT , `user_id` BIGINT UNSIGNED NOT NULL , `minAge` TINYINT UNSIGNED NULL , `maxAge` TINYINT UNSIGNED NULL , `location` VARCHAR(128) NULL , `gender` ENUM('0','1','2') NULL DEFAULT '0' , `name` VARCHAR(36) NULL , `game_name` VARCHAR(36) NULL , `create_dt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP , PRIMARY KEY (`id`)) ENGINE = InnoDB;");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('search_log');
    }
}
