<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDefaultValueToFriendListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('friend_list', function (Blueprint $table) {
            DB::statement("ALTER TABLE `friend_list` CHANGE `create_date` `create_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, CHANGE `modify_date` `modify_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP;");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('friend_list', function (Blueprint $table) {
            DB::statement("ALTER TABLE `friend_list` CHANGE `create_date` `create_date` DATETIME NOT NULL, CHANGE `modify_date` `modify_date` DATETIME NOT NULL ;");
        });
    }
}
