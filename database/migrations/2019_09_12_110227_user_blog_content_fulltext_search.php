<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserBlogContentFulltextSearch extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_blog', function (Blueprint $table) {
            DB::statement("ALTER TABLE `user_blog` ADD FULLTEXT(`content`);");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_blog', function (Blueprint $table) {
            DB::statement("ALTER TABLE user_blog DROP INDEX content;");
        });
    }
}
