<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCandyTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{	
		if (!Schema::hasTable('candy')) {
			Schema::create('candy', function(Blueprint $table)
			{
				$table->bigIncrements('id', true);
				$table->unsignedBigInteger('user_id');
				$table->integer('amount');
				$table->dateTime('modify_date');
				$table->index(['user_id','amount','modify_date'], 'user_id');
			});
		}
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('candy');
	}

}
