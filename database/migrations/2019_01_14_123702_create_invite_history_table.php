<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInviteHistoryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!Schema::hasTable('invite_history')) {
			Schema::create('invite_history', function(Blueprint $table)
			{
				$table->bigIncrements('id', true);
				$table->unsignedBigInteger('sender_id')->index('sender_id');
				$table->unsignedBigInteger('receiver_id')->index('receiver_id');
				$table->string('game_name');
				$table->string('location', 128)->nullable()->default('');
				$table->unsignedBigInteger('start_time')->default(0);
				$table->unsignedBigInteger('end_time')->default(0);
				$table->enum('status', array('requesting','responded','canceled','accept','reject'))->default('requesting')->index('status');
				$table->unsignedBigInteger('request_time')->unsigned()->default(0);
				$table->unsignedBigInteger('respond_time')->unsigned()->default(0);
				$table->unsignedBigInteger('canceled_by')->default(0);
				$table->index(['sender_id','receiver_id','start_time','end_time','status'], 'sender_id_2');
				$table->index(['start_time','end_time'], 'start_time');
			});
		}
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('invite_history');
	}

}
