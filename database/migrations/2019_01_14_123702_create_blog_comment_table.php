<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBlogCommentTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{	
		if (!Schema::hasTable('blog_comment')) {
			Schema::create('blog_comment', function(Blueprint $table)
			{
				$table->bigIncrements('id');;
				$table->unsignedBigInteger('user_id');
				$table->unsignedBigInteger('blog_id');
				$table->string('content', 2000);
				$table->dateTime('create_date');
				$table->dateTime('modify_date');
			});
		}
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('blog_comment');
	}

}
