<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HotListCatalog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('hot_list_catalog')) {
			Schema::create('hot_list_catalog', function(Blueprint $table)
			{
				$table->Increments('id', true);
				$table->string('catalog_name', 36)->default('');
			});
		}
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('hot_list_catalog');
    }
}
