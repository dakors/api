<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBigBannerTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{	
		if (!Schema::hasTable('big_banner')) {
			Schema::create('big_banner', function(Blueprint $table)
			{
				$table->bigIncrements('id');
				$table->string('description');
				$table->string('url');
				$table->string('img');
				$table->dateTime('start_date');
				$table->dateTime('end_date');
				$table->dateTime('create_date');
			});
		}
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('big_banner');
	}

}
