<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDeviceInfoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!Schema::hasTable('device_info')) {
			Schema::create('device_info', function(Blueprint $table)
			{
				$table->string('device_id')->primary();
				$table->unsignedBigInteger('user_id')->index('user_id');
				$table->enum('os', array('ios','android'))->index('os');
				$table->enum('isNotificationForMessageEnabled', array('true','false'))->default('true');
				$table->enum('isNotificationForFriendAddEnabled', array('true','false'))->default('true');
				$table->enum('isSoundEnabled', array('true','false'))->default('true');
				$table->dateTime('create_date');
			});
		}
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('device_info');
	}

}
