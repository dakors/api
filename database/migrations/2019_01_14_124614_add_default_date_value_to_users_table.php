<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDefaultDateValueToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
          DB::statement('ALTER TABLE `users` CHANGE `create_date` `create_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, CHANGE `modify_date` `modify_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, CHANGE `last_time_friend_list` `last_time_friend_list` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, CHANGE `last_time_history_list` `last_time_history_list` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, CHANGE `last_time_notice_list` `last_time_notice_list` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP;');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
          DB::statement("ALTER TABLE `users` CHANGE `create_date` `create_date` DATETIME NOT NULL , CHANGE `modify_date` `modify_date` DATETIME NOT NULL , CHANGE `last_time_friend_list` `last_time_friend_list` DATETIME NOT NULL , CHANGE `last_time_history_list` `last_time_history_list` DATETIME NOT NULL , CHANGE `last_time_notice_list` `last_time_notice_list` DATETIME NOT NULL ;");
        });
    }
}
