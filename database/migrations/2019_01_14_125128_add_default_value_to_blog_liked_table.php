<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDefaultValueToBlogLikedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('blog_liked', function (Blueprint $table) {
            DB::statement("ALTER TABLE `blog_liked` CHANGE `create_date` `create_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP;");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('blog_liked', function (Blueprint $table) {
            DB::statement("ALTER TABLE `blog_liked` CHANGE `create_date` `create_date` DATETIME NOT NULL ;");
        });
    }
}
