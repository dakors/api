<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDefaultValueToFansListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fans_list', function (Blueprint $table) {
            DB::statement("ALTER TABLE `fans_list` CHANGE `create_date` `create_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP;");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fans_list', function (Blueprint $table) {
            DB::statement("ALTER TABLE `fans_list` CHANGE `create_date` `create_date` DATETIME NOT NULL ;");
        });
    }
}
