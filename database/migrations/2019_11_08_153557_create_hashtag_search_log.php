<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHashtagSearchLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hashtag_search_log', function (Blueprint $table) {
            DB::statement("CREATE TABLE `hashtag_search_log` ( `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT , `user_id` BIGINT UNSIGNED NOT NULL , `keyword` VARCHAR(255) NOT NULL , `create_dt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP , PRIMARY KEY (`id`), INDEX (`keyword`), INDEX (`create_dt`)) ENGINE = InnoDB;");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hashtag_search_log');
    }
}
