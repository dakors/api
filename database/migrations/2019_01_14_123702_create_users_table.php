<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!Schema::hasTable('users')){
			Schema::create('users', function(Blueprint $table)
			{				
				$table->bigIncrements('id');
				$table->string('uuid',128)->default('')->index('uuid');
				$table->string('gb_id', 128)->default('')->index('gb_id');
				//$table->string('fb_id', 128)->default('')->index('fb_id');
				//$table->string('google_id', 128)->default('')->index('gmail_id');
				$table->string('password', 256)->default('');
				$table->string('first_name', 36)->default('');
				$table->string('last_name', 36)->default('');
				$table->string('nick_name', 36)->default('')->index('nick_name');
				$table->boolean('age')->default(0)->index('age');
				$table->enum('gender', array('0','1','2'))->default('0')->index('gender');
				$table->string('location', 128)->default('0')->index('location');
				$table->string('phone_number', 36)->default('');
				$table->string('email', 64)->default('')->index('email');
				$table->string('introduction')->default('');
				$table->date('subscription_expiration_date');
				$table->dateTime('create_date');
				$table->dateTime('modify_date');
				$table->enum('activated', array('true','false'))->default('false');
				$table->dateTime('last_time_friend_list');
				$table->dateTime('last_time_history_list');
				$table->dateTime('last_time_notice_list');
				$table->index(['id','nick_name'], 'id_2');
			});
		}
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
