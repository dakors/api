<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFriendListTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{	
		if (!Schema::hasTable('friend_list')) {
			Schema::create('friend_list', function(Blueprint $table)
			{
				$table->bigIncrements('id');
				$table->unsignedBigInteger('requester')->index('requester');
				$table->unsignedBigInteger('receiver')->index('receiver');
				$table->enum('status', array('requesting','confirmed','canceled'))->index('status');
				$table->dateTime('create_date');
				$table->dateTime('modify_date');
				$table->unique(['requester','receiver'], 'requester_2');
			});
		}
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('friend_list');
	}

}
