<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserGameListTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!Schema::hasTable('user_game_list')){
			Schema::create('user_game_list', function(Blueprint $table)
			{
				$table->bigIncrements('id');
				$table->unsignedBigInteger('user_id');
				$table->string('game_name', 36)->index('game_name');
				$table->unique(['user_id','game_name'], 'user_id_2');
				$table->index(['user_id','game_name'], 'user_id');
			});
		}
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_game_list');
	}

}
