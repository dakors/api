<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TopUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {   
        if (!Schema::hasTable('top_users')){
        DB::statement("CREATE
        ALGORITHM = UNDEFINED
        SQL SECURITY DEFINER
        VIEW top_users as 
        select `tb1`.`id` AS `user_id`,`tb1`.`nick_name` AS `nick_name`,`tb1`.`age` AS `age`,`tb1`.`gender` AS `gender`,`tb3`.`photo` AS `photo`,if((`tb1`.`subscription_expiration_date` >= curdate()),'true','false') AS `Issubscriber`,ifnull(`tb2`.`amount`,0) AS `fans` from ((`users` `tb1` left join `user_fans` `tb2` on((`tb1`.`id` = `tb2`.`user_id`))) left join `user_images` `tb3` on((`tb1`.`id` = `tb3`.`user_id`))) where 1 order by ifnull(`tb2`.`amount`,0) desc,`tb1`.`id` limit 15;");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW top_users");
    }
}
