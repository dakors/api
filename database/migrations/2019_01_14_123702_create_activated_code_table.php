<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateActivatedCodeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!Schema::hasTable('activated_code')) {
			Schema::create('activated_code', function(Blueprint $table)
			{
				$table->bigIncrements('id');
				$table->unsignedInteger('user_id')->unique('user_id');
				$table->string('code')->unique('code');
				$table->dateTime('create_date');
				$table->dateTime('expiry_date');
			});
		}
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('activated_code');
	}

}
