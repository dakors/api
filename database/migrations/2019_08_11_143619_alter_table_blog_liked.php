<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableBlogLiked extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('blog_liked', function (Blueprint $table) {
            DB::statement("ALTER TABLE `blog_liked` DROP PRIMARY KEY, ADD INDEX (`blog_id`) USING BTREE;");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('blog_liked', function (Blueprint $table) {
            DB::statement("ALTER TABLE `blog_liked` DROP INDEX `blog_id`, ADD PRIMARY KEY (`blog_id`) USING BTREE;;");
        });
    }
}
