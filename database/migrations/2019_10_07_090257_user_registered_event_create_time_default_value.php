<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserRegisteredEventCreateTimeDefaultValue extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_registered_event', function (Blueprint $table) {
            DB::statement("ALTER TABLE `user_registered_event` CHANGE `create_time` `create_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP;");
            DB::statement("ALTER TABLE `user_registered_event` ADD INDEX(`event_id`);");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_registered_event', function (Blueprint $table) {
            DB::statement("ALTER TABLE `user_registered_event` CHANGE `create_time` `create_time` DATETIME NOT NULL;");
            DB::statement("ALTER TABLE `user_registered_event` DROP INDEX `event_id`;");
        });
    }
}
