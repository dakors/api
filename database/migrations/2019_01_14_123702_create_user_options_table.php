<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserOptionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!Schema::hasTable('user_options')){
			Schema::create('user_options', function(Blueprint $table)
			{
				$table->unsignedBigInteger('user_id')->primary();
				$table->enum('isStrangerAllowed', array('true','false'))->default('true');
				$table->boolean('reminder_time')->default(-1);
			});
		}
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_options');
	}

}
