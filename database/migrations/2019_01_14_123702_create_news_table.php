<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNewsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!Schema::hasTable('news')) {
			Schema::create('news', function(Blueprint $table)
			{
				$table->integer('id', true);
				$table->string('title', 128);
				$table->string('description');
				$table->string('url');
				$table->string('img');
				$table->enum('type', array('big','small'))->index('type');
				$table->dateTime('start_date')->index('start_date');
				$table->dateTime('end_date')->index('end_date');
				$table->dateTime('create_date');
			});
		}
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('news');
	}

}
