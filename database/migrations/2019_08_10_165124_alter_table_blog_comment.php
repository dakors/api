<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableBlogComment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('comment_id', function (Blueprint $table) {
            DB::statement("ALTER TABLE `blog_comment` CHANGE `id` `comment_id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT;");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('comment_id', function (Blueprint $table) {
            DB::statement("ALTER TABLE `blog_comment` CHANGE `comment_id` `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT;");
        });
    }
}
