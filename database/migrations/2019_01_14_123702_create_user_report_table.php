<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserReportTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{	
		if (!Schema::hasTable('user_report')){
			Schema::create('user_report', function(Blueprint $table)
			{
				$table->bigIncrements('report_id');
				$table->unsignedBigInteger('user_id');
				$table->unsignedBigInteger('accused_person_id');
				$table->text('content');
				$table->enum('type', array('profile','comment','blog','chat'));
				$table->enum('status', array('pending','solved'));
				$table->dateTime('create_date');
				$table->dateTime('modify_date');
			});
		}
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_report');
	}

}
