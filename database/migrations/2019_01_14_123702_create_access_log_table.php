<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAccessLogTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{	if (!Schema::hasTable('access_log')) {
			Schema::create('access_log', function(Blueprint $table)
			{
				$table->unsignedInteger('id', true);
				$table->string('function');
				$table->string('log');
				$table->dateTime('date');
			});
		}
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('access_log');
	}

}
