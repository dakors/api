<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUserEventDefaultValues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_event', function (Blueprint $table) {
            DB::statement("ALTER TABLE `user_event` CHANGE `event_name` `event_name` VARCHAR(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '', CHANGE `registration_start_time` `registration_start_time` BIGINT(20) UNSIGNED NOT NULL DEFAULT 0, CHANGE `registration_end_time` `registration_end_time` BIGINT(20) UNSIGNED NOT NULL DEFAULT 0, CHANGE `location` `location` VARCHAR(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '', CHANGE `pic1` `pic1` VARCHAR(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '', CHANGE `pic2` `pic2` VARCHAR(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '', CHANGE `pic3` `pic3` VARCHAR(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '', CHANGE `registration_limit_amount` `registration_limit_amount` INT(11) NOT NULL DEFAULT '-1',CHANGE `detail` `detail` VARCHAR(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '';");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_event', function (Blueprint $table) {
            DB::statement("ALTER TABLE `user_event` CHANGE `event_name` `event_name` VARCHAR(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL , CHANGE `registration_start_time` `registration_start_time` BIGINT(20) UNSIGNED NOT NULL , CHANGE `registration_end_time` `registration_end_time` BIGINT(20) UNSIGNED NOT NULL , CHANGE `location` `location` VARCHAR(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL , CHANGE `pic1` `pic1` VARCHAR(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL , CHANGE `pic2` `pic2` VARCHAR(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL , CHANGE `pic3` `pic3` VARCHAR(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL , CHANGE `registration_limit_amount` `registration_limit_amount` INT(11) NOT NULL DEFAULT '0',CHANGE `detail` `detail` VARCHAR(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL ;");
        });
    }
}
