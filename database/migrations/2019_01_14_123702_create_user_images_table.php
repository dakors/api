<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserImagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!Schema::hasTable('user_images')){
			Schema::create('user_images', function(Blueprint $table)
			{
				$table->bigIncrements('id');
				$table->unsignedBigInteger('user_id')->index('user_id');
				$table->string('photo');
				$table->string('background');
				$table->dateTime('modify_date');
				$table->index(['user_id','photo'], 'user_id_2');
			});
		}
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_images');
	}

}
