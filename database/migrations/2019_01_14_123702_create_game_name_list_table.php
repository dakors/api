<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGameNameListTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!Schema::hasTable('game_name_list')) {
			Schema::create('game_name_list', function(Blueprint $table)
			{
				$table->bigIncrements('id', true);
				$table->string('enUS', 64)->default('')->index('enUS');
				$table->string('zhTW', 64)->default('')->index('zhTW');
				$table->string('acronyms', 64)->index('acronyms');
				$table->boolean('corporation')->nullable()->index('corporation');
				$table->index(['enUS','zhTW','acronyms'], 'keyword');
			});
		}
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('game_name_list');
	}

}
