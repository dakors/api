<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EventRegisteredCount extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('event_registered_count')){
        DB::statement("CREATE
        ALGORITHM = UNDEFINED
        SQL SECURITY DEFINER
        VIEW event_registered_count as 
        SELECT tb1.event_id,count(tb2.id) as amount  FROM user_event as tb1 left join user_registered_event as tb2 on tb1.event_id = tb2.event_id where 1 GROUP by tb1.event_id ;");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW event_registered_count");
    }
}
