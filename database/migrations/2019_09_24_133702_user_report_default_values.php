<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserReportDefaultValues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_report', function (Blueprint $table) {
            DB::statement("ALTER TABLE `user_report` CHANGE `status` `status` ENUM('pending','solved') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending';");
            DB::statement("ALTER TABLE `user_report` CHANGE `create_date` `create_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP;");
            DB::statement("ALTER TABLE `user_report` CHANGE `modify_date` `modify_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP;");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_report', function (Blueprint $table) {
            DB::statement("ALTER TABLE `user_report` CHANGE `status` `status` ENUM('pending','solved') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL;");
            DB::statement("ALTER TABLE `user_report` CHANGE `modify_date` `modify_date` DATETIME NOT NULL;");
            DB::statement("ALTER TABLE `user_report` CHANGE `create_date` `create_date` DATETIME NOT NULL;");
        });
    }
}
