<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ALTERTABLESocialAccountsDROPPRIMARYKEY extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('social_accounts', function (Blueprint $table) {
            DB::statement("ALTER TABLE social_accounts DROP PRIMARY KEY, ADD INDEX (`user_id`) USING BTREE;");
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('social_accounts', function (Blueprint $table) {
            DB::statement("ALTER TABLE social_accounts DROP INDEX `user_id`, ADD PRIMARY KEY (`user_id`) USING BTREE;");
        });
    }
}
