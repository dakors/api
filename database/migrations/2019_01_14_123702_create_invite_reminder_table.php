<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInviteReminderTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!Schema::hasTable('invite_reminder')) {
			Schema::create('invite_reminder', function(Blueprint $table)
			{
				$table->bigIncrements('id', true);
				$table->unsignedBigInteger('invite_history_id')->index('invite_history_id');
				$table->unsignedBigInteger('user_id')->index('user_id');
				$table->Integer('reminder_time')->default(0);
				$table->unique(['invite_history_id','user_id'], 'unique_key');
			});
		}
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('invite_reminder');
	}

}
