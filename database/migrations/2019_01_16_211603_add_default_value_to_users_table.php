<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDefaultValueToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
       DB::statement("ALTER TABLE `users` CHANGE `subscription_expiration_date` `subscription_expiration_date` DATE NOT NULL DEFAULT '2000-01-01' ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       DB::statement('ALTER TABLE `users` CHANGE `subscription_expiration_date` `subscription_expiration_date` DATE NOT NULL');
    }
}
