<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserEventTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{	
		if (!Schema::hasTable('user_event')){
			Schema::create('user_event', function(Blueprint $table)
			{
				$table->bigIncrements('event_id');
				$table->integer('user_id')->index('user_id');
				$table->string('event_name', 128);
				$table->unsignedBigInteger('registration_start_time');
				$table->unsignedBigInteger('registration_end_time');
				$table->unsignedBigInteger('start_time');
				$table->unsignedBigInteger('end_time');
				$table->string('location', 128);
				$table->string('detail', 256);
				$table->string('pic1', 256);
				$table->string('pic2', 256);
				$table->string('pic3', 256);
				$table->dateTime('create_time');
				$table->dateTime('modify_time');
				$table->enum('isRegistrable', array('true','false'))->default('false');
				$table->integer('registration_limit_amount')->default(0);
				$table->index(['start_time','end_time'], 'time');
			});
		}
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_event');
	}

}
