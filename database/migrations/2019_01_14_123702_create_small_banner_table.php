<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSmallBannerTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!Schema::hasTable('small_banner')){
			Schema::create('small_banner', function(Blueprint $table)
			{
				$table->integer('id', true);
				$table->string('description');
				$table->string('url');
				$table->string('img');
				$table->dateTime('start_date');
				$table->dateTime('end_date');
				$table->dateTime('create_date');
			});
		}
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('small_banner');
	}

}
