<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDefaultDateValueToUserImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_images', function (Blueprint $table) {
            DB::statement("ALTER TABLE `user_images` CHANGE `modify_date` `modify_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP;");//
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_images', function (Blueprint $table) {
            DB::statement("ALTER TABLE `user_images` CHANGE `modify_date` `modify_date` DATETIME NOT NULL;");//
        });
    }
}
