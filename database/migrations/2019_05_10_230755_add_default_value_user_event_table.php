<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDefaultValueUserEventTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_event', function (Blueprint $table) {
            DB::statement("ALTER TABLE `user_event` CHANGE `create_time` `create_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
              CHANGE `modify_time` `modify_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_event', function (Blueprint $table) {
            DB::statement("ALTER TABLE `user_event` CHANGE `create_time` `create_time` DATETIME NOT NULL,
              CHANGE `modify_time` `modify_time` DATETIME NOT NULL;");
        });
    }
}
