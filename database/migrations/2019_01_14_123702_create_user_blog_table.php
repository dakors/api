<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserBlogTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{	
		if (!Schema::hasTable('user_blog')){
			Schema::create('user_blog', function(Blueprint $table)
			{
				$table->bigIncrements('blog_id');
				$table->unsignedBigInteger('user_id')->index('user_id');
				$table->unsignedBigInteger('event_id')->nullable();
				$table->string('content', 3000);
				$table->string('preview', 4096)->default('');
				$table->dateTime('create_date');
				$table->dateTime('modify_date');
			});
		}
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_blog');
	}

}
