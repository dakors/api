<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFansListTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!Schema::hasTable('fans_list')) {
			Schema::create('fans_list', function(Blueprint $table)
			{
				$table->bigIncrements('id');
				$table->unsignedBigInteger('fans_id')->index('fans_id');
				$table->unsignedBigInteger('user_id')->index('user_id');
				$table->enum('IsMuted', array('false','true'))->default('false');
				$table->dateTime('create_date');
				$table->unique(['fans_id','user_id'], 'fans_id_2');
			});
		}
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('fans_list');
	}

}
