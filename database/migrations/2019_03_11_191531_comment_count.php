<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CommentCount extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {   
        if (!Schema::hasTable('comment_count')){
            DB::statement("CREATE
            ALGORITHM = UNDEFINED
            SQL SECURITY DEFINER
            VIEW comment_count as 
            select `blog_comment`.`blog_id` AS `blog_id`,count(`blog_comment`.`blog_id`) AS `comment_count_amount` from `blog_comment` where 1 group by `blog_comment`.`blog_id` ;");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW comment_count");
    }
}
