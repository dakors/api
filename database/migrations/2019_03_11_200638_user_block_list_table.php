<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserBlockListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('user_block_list')) {
			Schema::create('user_block_list', function(Blueprint $table)
			{
				$table->bigIncrements('id', true);
				$table->unsignedBigInteger('user_id')->index('user_id');
				$table->unsignedBigInteger('block_id')->index('block_id');				
				$table->dateTime('create_date')->default(DB::raw('CURRENT_TIMESTAMP'));
				$table->unique(['user_id','block_id'], 'block_id_2');
			});
		}
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_block_list');
    }
}
