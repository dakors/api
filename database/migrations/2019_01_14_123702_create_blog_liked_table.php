<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBlogLikedTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{	
		if (!Schema::hasTable('blog_liked')) {
			Schema::create('blog_liked', function(Blueprint $table)
			{
				$table->bigIncrements('blog_id')->index('blog_id_2');
				$table->unsignedBigInteger('user_id');
				$table->dateTime('create_date');
				$table->unique(['blog_id','user_id'], 'blog_id');
				$table->index(['blog_id','create_date'], 'blog_id_3');
			});
		}
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('blog_liked');
	}

}
