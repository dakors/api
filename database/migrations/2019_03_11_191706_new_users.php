<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('new_users')){
        DB::statement("CREATE
        ALGORITHM = UNDEFINED
        SQL SECURITY DEFINER
        VIEW new_users as 
        select sql_no_cache `tb1`.`id` AS `user_id`,`tb1`.`nick_name` AS `nick_name`,`tb1`.`age` AS `age`,`tb1`.`gender` AS `gender`,`tb2`.`photo` AS `photo`,if((`tb1`.`subscription_expiration_date` >= curdate()),'true','false') AS `Issubscriber` from (`users` `tb1` left join `user_images` `tb2` on((`tb1`.`id` = `tb2`.`user_id`))) where 1 order by `tb1`.`id` desc limit 15;");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW new_users");
    }
}
