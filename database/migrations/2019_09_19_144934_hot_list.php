<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HotList extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('hot_list')) {
			Schema::create('hot_list', function(Blueprint $table)
			{
				$table->Increments('id', true);
                $table->string('name', 36)->default('');
                $table->integer('catalog_id')->index('catalog_id');
                $table->integer('rank');
			});
		}        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('hot_list');
    }
}
