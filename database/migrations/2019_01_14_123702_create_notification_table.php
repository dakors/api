<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNotificationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!Schema::hasTable('notification')) {
			Schema::create('notification', function(Blueprint $table)
			{
				$table->bigIncrements('id', true);
				$table->unsignedBigInteger('user_id')->index('user_id');
				$table->text('content');
				$table->string('title', 128);
				$table->string('message', 128);
				$table->dateTime('create_date');
			});
		}
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('notification');
	}

}
