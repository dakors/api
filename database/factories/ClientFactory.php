<?php

use Faker\Generator as Faker;

$factory->define(Laravel\Passport\Client::class, function (Faker $faker) {
    return [
        //
        'secret' => $faker->sha256(),
        'redirect' => 'locahost',
        'revoked' => 0,
        'created_at' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'updated_at' => $faker->date($format = 'Y-m-d', $max = 'now')
    ];
});
