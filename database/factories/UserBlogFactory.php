<?php

use Faker\Generator as Faker;

$factory->define(App\Models\UserBlog::class, function (Faker $faker) {
    return [
        //
        'content' => $faker->sentence,
        'modify_date' => $faker->date($format = 'Y-m-d H:i:s', $max = 'now')
    ];
});
