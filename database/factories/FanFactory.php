<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Fan::class, function (Faker $faker) {
    return [
        //
        'IsMuted' => 'false',
        'create_date' => $faker->date($format = 'Y-m-d', $max = 'now')
    ];
});
