<?php

use Faker\Generator as Faker;

$factory->define(App\Models\BlogComment::class, function (Faker $faker) {
    return [
        //
        'content' => $faker->sentence,
        'create_date' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'modify_date' => $faker->date($format = 'Y-m-d', $max = 'now')
    ];
});
