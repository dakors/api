<?php

use Faker\Generator as Faker;

$factory->define(App\Models\UserEvent::class, function (Faker $faker) {
    return [
        //
        'event_name' => $faker->text($maxNbChars = 128),
        'start_time' => $faker->unixTime($max = 'now'),
        'end_time' => $faker->unixTime($max = 'now'),
        'registration_start_time' => 0,
        'registration_end_time' => 0,
        'location' => $faker->streetAddress,
        'pic1' => $faker->imageUrl($width = 640, $height = 480),
        'pic2' => $faker->imageUrl($width = 640, $height = 480),
        'pic3' => $faker->imageUrl($width = 640, $height = 480),
        'detail' => $faker->sentence
    ];
});
