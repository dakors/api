<?php

use Faker\Generator as Faker;

$factory->define(App\Models\UserBlockList::class, function (Faker $faker) {
    return [
        //
        'create_date' => $faker->date($format = 'Y-m-d', $max = 'now'),
    ];
});
