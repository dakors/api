<?php

use Faker\Generator as Faker;

$factory->define(App\Models\UserImages::class, function (Faker $faker) {
    return [
        //
        'photo' => $faker->imageUrl(400, 400, 'business'),
        'background' => $faker->imageUrl(1080, 640, 'business')
    ];
});
