<?php

use Faker\Generator as Faker;

$factory->define(App\Models\UserGameList::class, function (Faker $faker) {
    return [
        //
        'game_name' => $faker->word
    ];
});
