<?php

use Faker\Generator as Faker;

$factory->define(App\Models\GameNameList::class, function (Faker $faker) {
    return [
        'corporation' => $faker->randomDigit
    ];
});
