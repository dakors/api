<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Password Reminder Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => '密碼至少要有六個字元且與密碼確認欄位一致。',
    'reset'    => '密碼已成功重設！',
    'sent'     => '密碼重設郵件已發送！',
    'token'    => '密碼重設隨機碼 (token) 無效。',
    'user'     => '找不到該電子郵件信箱對應的使用者。',
    'cannot_use' => "這個帳號使用第三方登入,無法修改密碼。",
    'same' => "新舊密碼相同,請用不同的密碼。",
    'change' => '密碼已成功修改！',
    'new' => "嗨 %s ,這裡是你的新密碼 : %s",
    'email_incorrect' => 'Email不正確',
    'old_password_incorrect' => '舊密碼不正確',
];
