<?php
return [
    'new_invite_request' => '新邀請',
    'who_invite_you' => ':Name 邀請你一起 :Game',
    'accept_invite_request' => '邀請回覆',
    'who_accept_invite' => '%s 接受了你的邀請',
    'reject_invite_request' => '邀請回覆',
    'who_reject_invite' => '%s 拒絕了你的邀請',
    'invite_not_found' => '該邀請不存在,或已回覆.',
    'start_time_or_end_time_is_invalidate' => '開始或結束時間不正確',    
    'send_new_invite_success' => '發送邀請成功',
];
