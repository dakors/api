<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Comment Controller Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for any messages returned in the
    | CommentController class.
    |
    */

    'new_image_success' => '成功上傳新圖片!',
    'content_incorrect' => '圖片內容不正確!',
    'type_incorrect' => '上傳檔案錯誤!',
];