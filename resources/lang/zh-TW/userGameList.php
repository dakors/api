<?php
return [
    'added' => '%s 新增成功',
    'removed' => '%s 刪除成功',
    'not_found' => '%s 不存在,無法刪除',
    'already_exist' => '%s 已存在,無法新增',
    'Exceeding_quantity_limit' => '超出數量上限',
];
