<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Blog Controller Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for any messages returned in the
    | BlogController class.
    |
    */

    'new_blog_success' => '你成功的發布新動態',
    'blog_removed' => '動態已被移除',
    'blog_not_found' => '找不到該動態',
    'blog_not_owner' => '不能執行該動作,只有擁有者可以',
    'blog_already_like' => '你已經YO了該動態',
    'blog_liked' => '你已經YO了',
    'blog_unliked' => '你已經不YO了',
    'like_not_found' => '不能移除YO,因為你沒有YO該動態',
    'event_deleted' => '該活動已被擁有者刪除',
    'blog_updated' => '動態更新完成',
    'content_cant_empty' => '內容不得空白',
];
