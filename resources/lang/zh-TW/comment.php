<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Comment Controller Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for any messages returned in the
    | CommentController class.
    |
    */

    'new_comment_success' => 'You have successfully posted a new comment',
    'comment_not_found' => 'Comment not found',
    'comment_not_owner' => 'Unable to remove comment, only the owner can do so',
    'comment_updated' => 'Comment has been updated',
    'comment_removed' => 'Your comment have been removed'
];
