<?php
return [
    'invalid_credentials' => [ // key from error type
        'error'   => '無效證件',
        'message' => '用戶憑據不正確。'
    ]
];