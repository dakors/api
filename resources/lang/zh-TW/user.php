<?php
return [
    'location' => '{0}  不限地區|{1}  台北市|{2}  新北市|{3}  桃園市|{4}  台中市|{5}  台南市|{6}  高雄市|{7}  基隆市|{8}  新竹市|{9}  嘉義市|{10}  新竹縣|{11}  苗栗縣|{12}  彰化縣|{13}  南投縣|{14}  雲林縣|{15}  嘉義縣|{16}  屏東縣|{17}  宜蘭縣|{18}  花蓮縣|{19}  台東縣|{20}  澎湖縣|{21}  金門縣|{22}  連江縣|{23}  馬祖縣',
    'Age' => '年齡: :Age',
    'Fans' => '粉絲: :Fans',
    'updated' => '你的個人資料已更新',
    'recommended_error' => '請新增遊戲',
    'social_auth_already_exist' => '此帳號已有第三方登入,無法再綁定登入.',
    'social_auth_already_has_account' => '此第三方登入已有帳號,無法再綁定登入.',
    'social_auth_error' => '第三方登入驗證錯誤.',
    'social_auth_binded' => '此帳號綁定第三方登入完成.',
    'registration_success' => '註冊成功!!',
    'not_activated' => '帳號尚未啟用!!',
    'not_found' => 'User not found',
    'options_updated' => '設定更新完成',
];
