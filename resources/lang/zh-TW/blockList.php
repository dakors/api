<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Blog Controller Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for any messages returned in the
    | BlogController class.
    |
    */

    'block_fail_duplicate' => 'You are already blocking %s',
    'add_block_success' => 'You are now blocking %s',
    'unblock_fail_not_blocking' => 'You have unblocked %s',
    'unblock_success' => 'Cannot unblock because you are not blocking %s',
];