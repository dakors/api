<?php
return [
    'event_not_found' => '找不到該活動',
    'new_event_notification' => '新活動通知',
    'who_create_new_event_notification' => '%s 建立了新的活動',
];
