<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Fan Controller Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for any messages returned in the
    | FanController class.
    |
    */

    'follow_success' => 'You are now following %s',
    'follow_fail_duplicate' => 'You are alreadt following %s',
    'unfollow_success' => 'You have unfollowed %s',
    'unfollow_fail_not_following' => 'Cannot unfollow because you are not following %s',
    'not_following' => 'You are not following %s',
    'mute_following_success' => 'You are muted %s',
    'unmute_following_success' => 'You are unmuted %s',
];
