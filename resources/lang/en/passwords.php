<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Passwords must be at least six characters and match the confirmation.',
    'reset' => 'Your password has been reset!',
    'sent' => 'We have e-mailed your password reset link!',
    'token' => 'This password reset token is invalid.',
    'user' => "We can't find a user with that e-mail address.",
    'cannot_use' => "This account cannot use passowrd",
    'same' => "New password same as old password",
    'change' => 'Your password has been change!',
    'new' => 'HI %s ,Here is the new password : %s',
    'email_incorrect' => 'Email is incorrect',
    'old_password_incorrect' => 'Old password is incorrect',
];
