<?php
return [
    'event_not_found' => 'Event not found',
    //'cant_add_own_event_to_interested' => 'Cant Add Own Event to Interested',
    'already_add_this_event_to_interested' => 'Already Add This Event to Interested',
    'add_to_interested' => 'add to interested',
    'event_owner_cant_add_or_remove_interested' => 'Event owner cant add or remove Interested',
    'already_removed_this_event_from_interested' => 'Already Removed This Event From Interested',
    'removed_from_interested' => 'removed_from_interested',
];
