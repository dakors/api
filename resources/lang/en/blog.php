<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Blog Controller Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for any messages returned in the
    | BlogController class.
    |
    */

    'new_blog_success' => 'You have successfully posted a new blog',
    'blog_removed' => 'The blog have been removed',
    'blog_not_found' => 'Blog not found',
    'blog_not_owner' => 'Unable to perform action, only the owner can do so',
    'blog_already_like' => 'You already Yo this blog',
    'blog_liked' => 'You like this blog',
    'blog_unliked' => 'You unlike this blog',
    'like_not_found' => 'Cannot unlike because you did not like this blog',
    'event_deleted' => 'This event deleted by owner.',
    'blog_updated' => 'Blog has been updated',
    'content_cant_empty' => 'Content cant be empty',
];
