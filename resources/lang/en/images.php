<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Comment Controller Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for any messages returned in the
    | CommentController class.
    |
    */

    'new_image_success' => 'You have successfully upload a new image',
    'content_incorrect' => 'Image content is incorrect',
    'type_incorrect' => 'Upload conent is not Image',
];
