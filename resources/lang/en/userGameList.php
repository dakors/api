<?php
return [
    'added' => '%s added',
    'removed' => '%s removed',
    'not_found' => '%s not found',
    'already_exist' => '%s is already exist',
    'Exceeding_quantity_limit' => 'Exceeding quantity limit',
];
