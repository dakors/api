<?php
return [
    'location' => '{0}  不限地區|{1}  台北市|{2}  新北市|{3}  桃園市|{4}  台中市|{5}  台南市|{6}  高雄市|{7}  基隆市|{8}  新竹市|{9}  嘉義市|{10}  新竹縣|{11}  苗栗縣|{12}  彰化縣|{13}  南投縣|{14}  雲林縣|{15}  嘉義縣|{16}  屏東縣|{17}  宜蘭縣|{18}  花蓮縣|{19}  台東縣|{20}  澎湖縣|{21}  金門縣|{22}  連江縣|{23}  馬祖縣',
    'Age' => '年齡: :Age',
    'Fans' => '粉絲: :Fans',
    'updated' => 'Your profile has been updated',
    'recommended_error' => 'Unable to find recommended users. Please add more games',
    'social_auth_already_exist' => 'This account had social auth already !!',
    'social_auth_already_has_account' => 'This social auth already has account !!',
    'social_auth_error' => 'The user credentials were incorrect.',
    'social_auth_binded' => 'This account binded with social auth',
    'registration_success' => 'Registration success!!',
    'not_activated' => 'Account not activated',
    'not_found' => 'User not found',
    'options_updated' => 'options updated',
];
