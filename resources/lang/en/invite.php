<?php
return [
    'new_invite_request' => 'New invitation',
    'who_invite_you' => ':Name invite you together :Game',
    'accept_invite_request' => 'Reply to invitation',
    'who_accept_invite' => '%s accepted your invitation',
    'reject_invite_request' => 'Reply to invitation',
    'who_reject_invite' => '%s declined your invitation',
    'invite_not_found' => 'The invitation does not exist or has been responded to.',
    'start_time_or_end_time_is_invalidate' => 'Incorrect start or end time',    
    'send_new_invite_success' => 'Invitation sent',
];
