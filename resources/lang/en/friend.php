<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Fan Controller Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for any messages returned in the
    | FriendController class.
    |
    */

    'request_success' => 'Friend request sent',
    'duplicate_request' => 'You have already sent a friend request to %s',
    'request_not_found' => 'Friend request not found',
    'wrong_receiver' => 'You are not the recipient of this friend request',
    'reject_request' => 'You have rejected a friend request from %s',
    'accept_request' => '%s is now your friend',
    'reject_already_friend' => 'Unable to reject request, %s is already your friend',
    'accept_already_friend' => 'Unable to accept request, %s is already your friend',
    'not_friend' => 'User is not your friend',
    'unfriend_success' => 'You have unfriend %s',
    'new_friend_request' => 'New friend request',
    'want_be_your_friend' => '%s want be your friend',
    'friend_confirm' => 'New friend request confirmed',
    'is_your_new_friend' => '%s is your new friend',
];
