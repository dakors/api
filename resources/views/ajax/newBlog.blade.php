@foreach($posts as $post)

      <div class="blog" id="{{$post->blog_id}}">
        <div class="dropdown" style="float: right;">
          <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          </button>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu2">
            @if($post->user_id == Auth::user()->id)
            <button class="blog_Delete dropdown-item" type="button" data-id="{{$post->blog_id}}">{{ __('Delete') }}</button>
            <button class="blog_Edit dropdown-item" type="button" data-id="{{$post->blog_id}}">{{ __('Edit') }}</button>
            @else
            <button class="blog_Report dropdown-item" type="button" data-id="{{$post->blog_id}}">{{ __('Report') }}</button>
            @endif
          </div>
        </div>
        <div class="blog_info">
          <div class="blog_avatar">
            <img src="{{$post->photo}}" alt="Avatar" class="blog_avatar"/>
          </div>
          <div class ="blog_user_name" alt="Name">
            <span><h3>{{ $post->nick_name }}</h3></span>
          </div>
          <div class="blog_time">
            @if($post->create_date !== $post->modify_date)
              <span><h4>編輯於:{{$post->create_date}}</h4></span>
            @else
              <span><h4>發布於:{{$post->modify_date}}</h4></span>
            @endif
          </div>
        </div>

        <div class="blog_content">
          <a><h6>{{ $post->content }}<h6></a>
          <?php
          if($post->event_id){
            $image = $post->pic1;
            $url = "event/$post->event_id";
            $title = $post->event_name;
            $description = "發起人:$post->event_hoster 地點:$post->loction" ;
            echo "<div class='preview'>";
            if($image){echo "<img src=$image class='blog'/>";}
            echo "<a href=$url target='_blank'><h4>$title</h4></a>";
            echo "<a><h5>$description</h5></a>";
            echo "</div>";
          }else if($post->preview){
              $preview=json_decode($post->preview,true);
              if(isset($preview['url'])&& !empty($preview['url'])){
                $image = isset($preview['image']) ? $preview['image'] : '';
                $url = $preview['url'];
                $title = $preview['title'];
                $description = $preview['description'];
                echo "<div class='preview'>";
                echo "<img src=$image class='blog'/>";
                echo "<a href=$url target='_blank'><h4>$title</h4></a>";
                echo "<a><h5>$description</h5></a>";
                echo "</div>";
              }else{
                $image = isset($preview['image']) ? $preview['image'] : '';
                  echo "<img src=$image class='blog'/>";
              }
            }
          ?>
      </div>
      <div>
        <div class="YO">
          <span><h5>YO:{{$post->liked_count_amount}}</h5></span>
        </div>
        <div class="comment_count">
          <span><h5>留言:{{$post->comment_count_amount}}</h5></span>
        </div>
      </div>
      </div>
@endforeach
