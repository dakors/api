@foreach($posts as $post)

<div class="blog" id="{{$post->blog_id}}">
  <div class="dropdown" style="float: right;">
    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    </button>
    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu2">
      @if($post->user_id == Auth::user()->id)
      <button class="blog_Delete dropdown-item" type="button" data-id="{{$post->blog_id}}">{{ __('Delete') }}</button>
      <button class="blog_Edit dropdown-item" type="button" data-id="{{$post->blog_id}}">{{ __('Edit') }}</button>
      @else
      <button class="blog_Report dropdown-item" type="button" data-id="{{$post->blog_id}}">{{ __('Report') }}</button>
      @endif
    </div>
  </div>
  <div class="blog_info">
    <div class="blog_avatar">
      <img src="{{$post->user->getAvatar()}}" alt="Avatar" class="blog_avatar"/>
    </div>
    <div class ="blog_user_name" alt="Name">
      <span><h3>{{ $post->user->nick_name }}</h3></span>
    </div>
    <div class="blog_time">
      @if($post->create_date !== $post->modify_date)
        <span><h4>編輯於:{{$post->create_date}}</h4></span>
      @else
        <span><h4>發布於:{{$post->modify_date}}</h4></span>
      @endif
    </div>
  </div>

  <div class="blog_content">
    <a><h6>{{ $post->content }}<h6></a>
    <?php
    if($post->event_id){
      $image = $post->pic1;
      $url = "event/$post->event_id";
      $title = $post->event_name;
      $description = "發起人:$post->event_hoster 地點:$post->loction" ;
      echo "<div class='preview'>";
      if($image){echo "<img src=$image class='blog'/>";}
      echo "<a href=$url target='_blank'><h4>$title</h4></a>";
      echo "<a><h5>$description</h5></a>";
      echo "</div>";
    }else if($post->preview){
        $preview=json_decode($post->preview,true);
        if(isset($preview['url'])&& !empty($preview['url'])){
          $image = isset($preview['image']) ? $preview['image'] : '';
          $url = $preview['url'];
          $title = $preview['title'];
          $description = $preview['description'];
          echo "<div class='preview'>";
          echo "<img src=$image class='blog'/>";
          echo "<a href=$url target='_blank'><h4>$title</h4></a>";
          echo "<a><h5>$description</h5></a>";
          echo "</div>";
        }else{
          $image = isset($preview['image']) ? $preview['image'] : '';
            echo "<img src=$image class='blog'/>";
        }
      }
    ?>
</div>
<div class='blog_detail' data-id="${{$post->blog_id}}">
  <div class="YO">
    <span><h5>YO:{{$post->likes->count()}}</h5></span>
  </div>
  <div class="comment_count">
    <span><h5>留言:{{$post->comments->count()}}</h5></span>
  </div>
</div>
<div class="row button-group blog" data-id='{{$post->blog_id}}'>
    @if($post->isLiked() == 'TRUE')

      <button type="button" class="btn btn-outline-warning center-block blog_unYO_button" data-id='{{$post->blog_id}}'><i class='icon icon-unYO'></i></button>

    @else

      <button type="button" class="btn btn-outline-warning center-block blog_YO_button" data-id='{{$post->blog_id}}'><i class='icon icon-YO'></i></i></button>

    @endif

      <button type="button" class="btn btn-outline-warning center-block blog_comment_button" data-id='{{$post->blog_id}}' title="{{ __('Comment') }}"><i class='icon icon-comment'></i></button>

</div>
</div>
@endforeach
