@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row home">


  <div class="col-md-9">
        <div class="form-group blog">
          @if(isset(Auth::user()->UserImages))
          <img src={{url(Auth::user()->UserImages->photo)}} class ="avatar">
          @endif
          <div class="shadow p-3 mb-5 bg-white rounded" role="alert" data-toggle="modal" data-target="#BlogModal">
            <b style="color:#000000">新增動態</b>
          </div>
        </div>
      <div class="modal fade" id="emptyModal" role="dialog">
      </div>
      <div class="modal fade" id="BlogModal" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">新增動態</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" id="blog_post">
              <form id="post_blog" method="POST" action="{{ route('blog.submit') }}">
                <textarea class="form-control noresize" rows="5" id="blog_post_content" name="blog_content" placeholder="Say something!!"></textarea>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input class="form-control " rows="5" id="blog_preview_data" name="blog_url_preview_json" hidden>
                <input type="file" accept="image/*" class="form-control " name="blog_image" rows="5" id="blog_image"  hidden>
                <input type="submit" class="form-control " rows="5" id="blog_submit"  hidden>
              </form>
              <div class="col-md-12" id="blog_post_preview">
              <button type="button" class="close" id="blog_post_preview_close">&times;</button>
              <div class="card mb-4 shadow-sm">
                <img class="card-img-top"  alt="Card image cap" id="blog_post_preview_image">
                <div class="card-body">
                  <h5 class="card-title" id="blog_post_preview_title"></h5>
                  <p class="card-text" id="blog_post_preview_desc"></p>
                </div>
              </div>
            </div>
              <div class="col-md-12" id="blog_image_preview">
                <button type="button" class="close" id="post_image_preview_close">&times;</button>
                <div class="card mb-4 shadow-sm">
                  <img class="card-img-top"  alt="Card image cap" id="post_image_preview">
                </div>
              </div>
              <button type="button" class="btn btn-info" id="blog_image_input_button"><span class="glyphicon glyphicon-picture"></span> {{ __('Picture') }}</button>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary" disabled id="blog_post_submit_button"><span class="glyphicon glyphicon-picture"></span> {{ __('Submit') }}</button>
            </div>
          </div>
        </div>
      </div>
      <div class="modal fade" id="BlogDeleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">刪除動態提醒</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              你確定要刪除這個動態?
              <form id='delete_blog'>
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <input type="hidden" name="delete_blog_id" id="delete_blog_id">
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary" id="BlogDeleteConfirm">Save changes</button>
            </div>
          </div>
      </div>
    </div>
      <section class="posts endless-pagination" data-next-page="{{ $posts->nextPageUrl() }}">
        <div class="blog start here d-none"></div>
  @foreach($posts as $post)
      <div class="blog" id="{{$post->blog_id}}">
        <div class="dropdown" style="float: right;">
          <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          </button>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu2">
            @if($post->user_id == Auth::user()->id)
            <button class="blog_Delete dropdown-item" type="button" data-id="{{$post->blog_id}}">{{ __('Delete') }}</button>
            <button class="blog_Edit dropdown-item" type="button" data-id="{{$post->blog_id}}">{{ __('Edit') }}</button>
            @else
            <button class="blog_Report dropdown-item" type="button" data-id="{{$post->blog_id}}">{{ __('Report') }}</button>
            @endif
          </div>
        </div>
        <div class="blog_info">
          <div class="blog_avatar">
            <a href="{{'profile/'.$post->user->id}}">
            <img src="{{$post->user->getAvatar()}}" alt="Avatar" class="blog_avatar"/>
            </a>
          </div>
          <div class ="blog_user_name" alt="Name">
            <a href="{{'profile/'.$post->user->id}}">
              <span><h3>{{ $post->user->nick_name }}</h3></span>
            </a>
          </div>
          <div class="blog_time">
            @if($post->create_date !== $post->modify_date)
              <span><h4>編輯於:{{$post->create_date}}</h4></span>
            @else
              <span><h4>發布於:{{$post->modify_date}}</h4></span>
            @endif
          </div>
        </div>

        <div class="blog_content">
          <a><h6>{{ $post->content }}<h6></a>
          <?php
          if($post->event_id){
            $image = $post->pic1;
            $url = "event/$post->event_id";
            $title = $post->event_name;
            $description = "發起人:$post->event_hoster 地點:$post->loction" ;
            echo "<div class='preview'>";
            if($image){echo "<img src=$image class='blog'/>";}
            echo "<a href=$url target='_blank'><h4>$title</h4></a>";
            echo "<a><h5>$description</h5></a>";
            echo "</div>";
          }else if($post->preview){
              $preview=json_decode($post->preview,true);
              if(isset($preview['url'])&& !empty($preview['url'])){
                $image = isset($preview['image']) ? $preview['image'] : '';
                $url = $preview['url'];
                $title = $preview['title'];
                $description = $preview['description'];
                echo "<div class='preview'><a href=$url target='_blank' class='preview'>";
                echo "<img src=$image class='blog'/></a>";
                echo "<h4>$title</h4>";
                echo "<h5>$description</h5>";
                echo "</div>";
              }else{
                $image = isset($preview['image']) ? $preview['image'] : '';
                  echo "<img src=$image class='blog'/>";
              }
            }
          ?>
      </div>
      <div class='blog_detail' data-id="{{$post->blog_id}}">
        <div class="YO">
          <span>YO:</span><span class='YO-count' id='{{$post->blog_id}}'>{{$post->likes->count()}}</span>
        </div>
        <div class="comment_count">
          <span>留言:</span><span class='comment-count'>{{$post->comments->count()}}</span>
        </div>
      </div>

        <div class="row button-group blog" data-id='{{$post->blog_id}}'>
            @if($post->isLiked() == 'TRUE')

              <button type="button" class="btn btn-outline-warning center-block blog_unYO_button" data-id='{{$post->blog_id}}'><i class='icon icon-unYO'></i></button>

            @else

              <button type="button" class="btn btn-outline-warning center-block blog_YO_button" data-id='{{$post->blog_id}}'><i class='icon icon-YO'></i></i></button>

            @endif

              <button type="button" class="btn btn-outline-warning center-block blog_comment_button" data-id='{{$post->blog_id}}' title="{{ __('Comment') }}"><i class='icon icon-comment'></i></button>

        </div>

    </div>
  @endforeach

{{--{!! $posts->render() !!}--}}

    </section>
    </div>
	</div>
</div>

@stop
