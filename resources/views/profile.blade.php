@extends('layouts.app')

@section('category')


@endsection

@section('content')
<div id="app">
  <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
          <profile :profile="{{$profile}}" :images="{{$images}}"></profile>
          <div class="card card-default">
          <nav>
            <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
              <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-blog" role="tab" aria-controls="nav-blog" aria-selected="true">Blog</a>
              <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-event" role="tab" aria-controls="nav-event" aria-selected="false">Event</a>
              <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Contact</a>
            </div>
          </nav>
          <profiletabs :profile="{{$profile}}"></profiletabs>

  </div>
</div>
</div>
</div>

@endsection
