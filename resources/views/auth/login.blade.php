@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card blog">
                <div class="card-header blog">{{ __('Login') }}</div>

                <div class="card-body blog">
                    <form method="POST" action="{{ route('login.submit') }}" aria-label="{{ __('Login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-sm-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                            </div>
                        </div>
                        <hr>
                        <!--<div class="form-group row">
                            <div class="btn-group ">
                          		<a class='btn btn-primary disabled'><i class="fa fa-facebook" style="width:16px; height:20px"></i></a>
                          		<a class='btn btn-primary ' href='{{url('login/facebook/')}}' style="width:12em"> {{__('Sign in with ',['name' => 'Facebook'])}}</a>
                          	</div>
                        </div>
                        <div class="form-group row">
                            <div class="btn-group ">
                          		<a class='btn btn-danger disabled'><i class="fa fa-google-plus" style="width:16px; height:20px"></i></a>
                          		<a class='btn btn-danger' href='{{url('login/google/')}}' style="width:12em;"> {{__('Sign in with ',['name' => 'Google'])}}</a>
                          	</div>
                        </div>-->
                        <div class="form-group row">
                            <div class="social-login-button col-md-6 btn">
                          		<a class='btn social-login' href='{{url('login/facebook/')}}'><span class='icon icon-facebook-logo'></span> {{__('Sign in with ',['name' => 'Facebook'])}}</a>
                          	</div>
                        </div>
                        <div class="form-group row">
                            <div class="social-login-button col-md-6 btn">
                              <a class='btn social-login' href='{{url('login/google/')}}'><span class='icon icon-google-logo'></span> {{__('Sign in with ',['name' => 'Google'])}}</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
