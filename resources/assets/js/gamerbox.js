$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });



$('#blog_post_content').on('paste',function() {
  setTimeout(function() {
      //regexp =  /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/;
      urlreg =  /([a-z]+\:\/+)([^\/\s]*)([a-z0-9\-@\^=%&;\/~\+]*)[\?]?([^ \#]*)#?([^ \#]*)/ig;
      str = $("#blog_post_content").val()
      while ((m = urlreg.exec(str)) !== null) {
          if (m.index === urlreg.lastIndex) {
              urlreg.lastIndex++;
          }
          console.log(m[0]);
          var url = m[0];
      }
      if (url !== null)
      {
      var postData = {
        //'url' : $("#blog_post_content").val()
        'url' : url
        };
      $.ajax({
           type: "POST",
           url: "http://develop.gamerbox.com.tw/preview.php",
           data: postData , //assign the var here
           success: function(msg){
             if(msg.status_code !== 'E00000'){
               console.log(msg.status_code);
             }else{
              $('#blog_preview_data').val(JSON.stringify(msg.data));
              $('#blog_post_preview_image').attr('src',msg.data.image) ;
              $('#blog_post_preview_title').text(msg.data.title) ;
              $('#blog_post_preview_desc').text(msg.data.description) ;
              RemoveImagePreview();
              $('#blog_image_input_button').prop('disabled', true);;
              $("#blog_post_preview").show();
              //console.log(msg.data.image);
             }

           }

      });
  }else {
    console.log('not url');
  }
  }, 500);

});


$(document).ready(function() {
    $("#blog_post_preview").hide();
    $("#blog_image_preview").hide();
});

$('#blog_post_preview_close').on('click',function(){
  removeBlogPostPreview();
});

function removeBlogPostPreview(){
  $('#blog_preview_data').val('');
  $('#blog_post_preview_image').attr('src','') ;
  $('#blog_post_preview_title').text('') ;
  $('#blog_post_preview_desc').text('') ;
  $('#blog_image_input_button').prop('disabled', false);
  $("#blog_post_preview").hide();
}

$('#blog_image_input_button').on('click',function(){
   $("#blog_image").click();
});

function PreviewImage(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#post_image_preview').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
            $('#blog_image_preview').show();
        }
    }
$("#blog_image").change(function(){
    PreviewImage(this);
});


$("#post_image_preview_close").on("click",function(){
  RemoveImagePreview();
});

function RemoveImagePreview(){
  $('#post_image_preview').attr('src','');
  $('#blog_image').val('');
  $('#blog_image_preview').hide();
}
/*$("#post_blog").on("change",function(){
  var blog_image = $('#blog_image').val();
  var blog_content = $('#blog_post_content').val();
  var blog_preview_data = $('#blog_preview_data').val();
  if(blog_image !== null || blog_content !== null || blog_preview_data !== null){
    $("#blog_post_submit").removeAttr('disabled');
    console.log('enabled');
  }else{
    $("#blog_post_submit").attr('disabled');
    console.log('disabled');
  }
});*/

$("#blog_post_content").on("input",function(){

  var blog_content = $('#blog_post_content').val();

  if( blog_content !== null && blog_content !== ''){
    $("#blog_post_submit_button").prop('disabled', false);
    console.log('enabled');
  }else{
    $("#blog_post_submit_button").prop('disabled', true);
    console.log('disabled');
  }
});

$("#blog_post_submit_button").on('click',function(){
  var form = $('#post_blog')[0];
  var formdata = new FormData(form);
  $("#blog_post_submit_button").prop("disabled", true);
  $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        url: "blog/submit",
        data: formdata,
        processData: false,  // Important!
        contentType: false,
        cache: false,
        success : function(data){
          console.log(data);
          getLastOneBlog();
          resetBlogForm();
          $("#blog_post_submit_button").prop("disabled", false);
        },
        error : function(e){
          console.log(e);
          $("#blog_post_submit_button").prop("disabled", false);
        }
      });
});

function getLastOneBlog() {
                $.post('/blog/getLastOne', function(data){
                    $('.blog.start.here').after(data.posts);
                });
                /*
                $(".blog_Edit").bind("click",function(){
                  console.log('blog_Edit clicked');
                  console.log($(this).data('id'));
                });

                $(".blog_Delete").bind("click",function(){
                  console.log('blog_Delete clicked');
                  $('#delete_blog_id').val($(this).data('id'));
                  $('#BlogDeleteModal').modal('toggle');
                });*/
}

function resetBlogForm(){
  removeBlogPostPreview();
  RemoveImagePreview();
  $('#post_blog')[0].reset();
  $('#BlogModal').modal('toggle');
  console.log('rest form');
}


$('body').on('click',".blog_Report",function(){
  console.log('blog_Report clicked');
  console.log(this.id);
});

/*$(".blog_Delete").on('click',function(){
  console.log('blog_Delete clicked');
  $('#delete_blog_id').val($(this).data('id'));
  $('#BlogDeleteModal').modal('toggle');
});*/

$('body').on('click',".blog_Delete",function(){
  console.log('blog_Delete clicked');
  $('#delete_blog_id').val($(this).data('id'));
  $('#BlogDeleteModal').modal('toggle');
});



$("body").on('click',".blog_Edit",function(){
  console.log('blog_Edit clicked');
  console.log($(this).data('id'));


});

function DeleteBlog(){
  var form = $('#delete_blog')[0];
  var formdata = new FormData(form);
  //$('#'+$('#delete_blog_id').val()).remove();

  $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        url: "blog/delete",
        data: formdata,
        processData: false,  // Important!
        contentType: false,
        cache: false,
        success : function(data){
          if(data==='success'){
            $('#'+$('#delete_blog_id').val()).remove();
          }else{
            $('#'+$('#delete_blog_id').val()).remove();
          }
          console.log(data);

        },
        error : function(e){
          console.log(e);

        }
      });

}

$('#BlogDeleteConfirm').on('click',function(){
  DeleteBlog();
  $('#BlogDeleteModal').modal('toggle');
});

$(document).ready(function() {

    $(window).scroll(fetchPosts);

    function fetchPosts() {

        var page = $('.endless-pagination').data('next-page');

        if(page !== null) {

            clearTimeout( $.data( this, "scrollCheck" ) );

            $.data( this, "scrollCheck", setTimeout(function() {
                var scroll_position_for_posts_load = $(window).height() + $(window).scrollTop() + 100;

                if(scroll_position_for_posts_load >= $(document).height()) {
                    $.get(page, function(data){
                        $('.posts').append(data.posts);
                        $('.endless-pagination').data('next-page', data.next_page);
                    });
                }
            }, 350))

        }
    }

});






$('body').on('click','.blog_comment_button',function(){
  postData = {"_method" : 'POST','blog_id' : $(this).data('id') ,'_token' : $('input[name="_token"]').attr('value')};
  //console.log(postData);
  $.post('blog/GetCommentList',postData,function(data){
  });

});


$('body').on('click','.blog_YO_button',function(){
  var btn = $(this);
  id = 'span#'+$(this).data('id')+'.YO-count';
  postData = {"_method" : 'POST','blog_id' : $(this).data('id') ,'_token' : $('input[name="_token"]').attr('value')};
  $.post('blog/like',postData,function(data){
    if(data === 'success'){
      btn.children('.icon-YO').removeClass('icon-YO').addClass('icon-unYO');
      btn.removeClass('blog_YO_button').addClass('blog_unYO_button');

      console.log(id);
      count = parseInt($(id).text());
      console.log(count);
      $(id).text(count +1);
    }else if(data === 'exists'){
      btn.children('.icon-YO').removeClass('icon-YO').addClass('icon-unYO');
      btn.removeClass('blog_YO_button').addClass('blog_unYO_button');

    }else{
      console.log(data);
    }
  });

  //$('#'+$(this).data('id')+'.blog').children('YO-count').val(222);

});

$('body').on('click','.blog_unYO_button',function(){
  var btn = $(this);
  id = 'span#'+$(this).data('id')+'.YO-count';
  postData = {"_method" : 'POST','blog_id' : $(this).data('id') ,'_token' : $('input[name="_token"]').attr('value')};
  $.post('blog/unlike',postData,function(data){
    if(data === 'success'){
      btn.children('.icon-unYO').removeClass('icon-unYO').addClass('icon-YO');
      btn.removeClass('blog_unYO_button').addClass('blog_YO_button');
      console.log(id);
      count = parseInt($(id).text());
      console.log(count);
      $(id).text(count -1);
    }else if(data === 'not exists'){
      btn.children('.icon-unYO').removeClass('icon-unYO').addClass('icon-YO');
      btn.removeClass('blog_unYO_button').addClass('blog_YO_button');
    }else{
      console.log(data);
    }
  });

  //$('#'+$(this).data('id')+'.blog').children('YO-count').val(222);

});
