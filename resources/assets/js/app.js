
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('./gamerbox');
window.Vue = require('vue');
window.default_locale = "{{ config('app.lang') }}";
require("anchorme").default; // if installed via NPM
import anchorme from "anchorme";
import _ from 'lodash';
import ReadMore from 'vue-read-more';
import VueTimeago from 'vue-timeago'

Vue.use(ReadMore);
Vue.use(VueTimeago, {
  name: 'timeago', // Component name, `Timeago` by default
  locale: default_locale, // Default locale
  locales: {
    'zh-TW': require('date-fns/locale/zh_TW'),
    'en': require('date-fns/locale/en'),
  }
})
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

//Vue.component('example-component', require('./components/ExampleComponent.vue'));

Vue.component('profile', require('./components/profile.vue'));
Vue.component('profiletabs', require('./components/profiletabs.vue'));

const app = new Vue({
    el: '#app',


});
